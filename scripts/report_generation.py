"""
SigMon Report Generator
=======================

The SigMon Report Generator tool generates reports (HTML) of the input HWC test files (CSV). The tool is intended to
confirm that automated successful tests are correctly analyzed and reported.


Examples
--------

Assuming report_generation.py is in your present working directory and all files and directories referenced exist:

python report_generation.py --help
    prints help to standard output

python report_generation.py my_test_file.csv my_reports
    generates HTML reports of the tests listed in my_test_file.csv and outputs them in the folder named my_reports

python report_generation.py my_file.csv my_other_file.csv my_final_file.csv my_reports
    generates HTML reports of the tests listed in my_file.csv, my_other_file.csv, and my_final_file.csv and outputs
    them in the folder named my_reports
"""

import argparse
import logging
import os
import pathlib
import sys
import tempfile

import pandas as pd
import papermill

from lhcsmpowering import analyses

# Get arguments
parser = argparse.ArgumentParser(
    description="SigMon Report Generator: generates a report for all tests listed in the input files"
)
parser.add_argument("infile", nargs="+", help="input csv file containing tests to be executed")
parser.add_argument("outdir", help="output directory in which reports will be saved")
parser.add_argument(
    "-p",
    "--only-passed",
    help="only generate reports for test cases that are in the input marked as passed",
    action="store_true",
)
parser.add_argument(
    "-f",
    "--only-failed",
    help="only generate reports for test cases that are in the input marked as failed",
    action="store_true",
)
args = parser.parse_args()

if args.only_passed and args.only_failed:
    print("Flags --only-passed and --only-failed are incompatible.")
    exit(1)

# Get in- and output paths
csv_paths = [pathlib.Path(f) for f in args.infile]
out_dir = pathlib.Path(args.outdir)

for path in csv_paths:
    if not (path.is_file() and path.suffix == ".csv"):
        print(f"Error: {path} is not a CSV file path.")
        exit(1)

if not out_dir.exists() or out_dir.is_file():
    print(f"Error: {out_dir} is not a directory.")
    exit(1)

# Load the csv files
expected_columns = ["testName", "systemName", "campaign", "executionStartTime", "executionEndTime"] + (
    ["validatedAnalysisResult"] if args.only_passed or args.only_failed else []
)
tests_to_run = pd.concat([pd.read_csv(path, usecols=expected_columns) for path in csv_paths])
tests_to_run = tests_to_run.drop_duplicates()

# Create temporary working path
tmp_notebook_path = pathlib.Path(tempfile.gettempdir()) / (tempfile.gettempprefix() + "_notebook_srg.ipynb")

tests_with_exceptions = []

logging.basicConfig(level=logging.ERROR)

# Generate the report using papermill
# Iterate over the rows of the dataframe
for _, row in tests_to_run.iterrows():

    if (args.only_passed and row["validatedAnalysisResult"] is False) or (
        args.only_failed and row["validatedAnalysisResult"] is True
    ):
        continue

    # Arrange
    notebook_parameters = {
        "hwc_test": row["testName"],
        "circuit_name": row["systemName"],
        "campaign": row["campaign"],
        "t_start": row["executionStartTime"],
        "t_end": row["executionEndTime"],
        "expert_signature": "UNDEFINED",
        "expert_decision": "UNDEFINED",
        "generate_report": False,
    }

    test_label = row["systemName"] + "_" + row["executionStartTime"].replace(" ", "_").replace(":", "-")

    # Get path of the Jupyter Notebook for the current test parameters
    notebook_path = analyses.get_notebook_path(notebook_parameters["hwc_test"], notebook_parameters["circuit_name"])
    if not notebook_path.is_file():
        print(f"Skipping execution of {test_label}: The notebook at {notebook_path} does not exist.")
        continue

    report_path = out_dir / notebook_path.stem / f"{test_label}.html"

    # Create the report directory if it does not exist
    report_path.parent.mkdir(exist_ok=True)

    try:
        papermill.execute_notebook(notebook_path, tmp_notebook_path, notebook_parameters)
    except Exception as e:
        tests_with_exceptions.append((row, report_path))
        logging.exception(f"An error occurred while executing the notebook: {e}")
    finally:
        os.system(
            f"{sys.executable} -m jupyter nbconvert --to html {tmp_notebook_path.absolute()} "
            f"--output {report_path.absolute()}"
        )
        tmp_notebook_path.unlink()

# Report exceptions that occurred
if len(tests_with_exceptions) == 0:
    print("\nHooray! There were no test executions that raised exceptions.")
else:
    print(f"\nThere were {len(tests_with_exceptions)} test executions that raised exceptions:")
    for row, report_path in tests_with_exceptions:
        print(
            f"  Test: {row['testName']},    Circuit: {row['systemName']},    Campaign: {row['campaign']}\n"
            f"    from {row['executionStartTime']}    to {row['executionEndTime']}\n"
            f"    saved at {report_path}"
        )
