import logging

import pandas as pd
import pytest

from lhcsmpowering.checks import signal_checks


def test_check_signal_sampling_interval_signal_with_correct_interval(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([1, 2, 3], index=[0, 2, 4], name="signal")
    expected_interval = 2

    # act
    result = signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())

    # assert
    assert result is True
    assert caplog.messages == ["Check passed: the signal signal has a sampling interval of 2 ns."]


def test_check_signal_sampling_interval_signal_with_incorrect_interval(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([1, 2, 3], index=[0, 2, 4], name="signal")
    expected_interval = 3

    # act
    result = signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())

    # assert
    assert result is False
    assert caplog.messages == [
        "Check failed: the signal signal has a sampling interval of 2 ns "
        "instead of an expected sampling interval of 3 ns."
    ]


def test_check_signal_sampling_interval_signal_with_varying_intervals(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([1, 2, 3], index=[0, 2, 5], name="signal")
    expected_interval = 2

    # act
    result = signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())

    # assert
    assert result is True
    assert caplog.messages == [
        "The signal signal has varying sampling intervals. The smallest interval will be used.",
        "Check passed: the signal signal has a sampling interval of 2 ns.",
    ]


def test_check_signal_sampling_interval_signal_with_less_than_two_elements(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([1], index=[0], name="signal")
    expected_interval = 2

    # act
    result = signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())

    # assert
    assert result is False
    assert caplog.messages == ["Check failed: the signal signal has less than 2 elements."]


def test_check_signal_sampling_interval_signal_with_empty_series(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series(dtype=float, index=[], name="signal")
    expected_interval = 2

    # act
    result = signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())

    # assert
    assert result is False
    assert caplog.messages == ["Check failed: the signal signal has less than 2 elements."]


def test_check_signal_sampling_interval_signal_with_non_numeric_index(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([1, 2, 3], index=["a", "b", "c"], name="signal")
    expected_interval = 1

    # act
    with pytest.raises(TypeError):
        signal_checks.check_signal_sampling_interval(signal, expected_interval, logging.getLogger())
