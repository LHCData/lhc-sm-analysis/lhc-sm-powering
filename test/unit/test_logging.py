import logging

import pytest
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import analyses
from lhcsmpowering.analyses import commons


def test_logger_get_filtered_logs():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PCC.1",
        "RCBH11.L1B2",
        "Recommissioning 2023/2024",
        "2024-02-15 16:37:14.227000000",
        "2024-02-15 16:53:56.139000000",
    )
    analysis_class = analyses.Pcc1On60AAnalysis("", test_parameters)
    tag = "TAG"

    logger_adapter = commons.start_logging_new_check(analysis_class._logger, tag)

    # act
    logger_adapter.info("My log")

    # assert
    assert analysis_class.get_filtered_logs(tag) == "\tMy log"


def test_logger_adapter_filter_log():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PCC.1",
        "RCBH11.L1B2",
        "Recommissioning 2023/2024",
        "2024-02-15 16:37:14.227000000",
        "2024-02-15 16:53:56.139000000",
    )
    analysis_class = analyses.Pcc1On60AAnalysis("", test_parameters)
    tag = "TAG"
    another_tag = "ANOTHER_TAG"

    # act
    logger_adapter = commons.start_logging_new_check(analysis_class._logger, tag)
    logger_adapter.info("My log")

    # assert
    assert analysis_class.get_filtered_logs(another_tag) == ""


def test_logger_adapter_indentation():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PCC.1",
        "RCBH11.L1B2",
        "Recommissioning 2023/2024",
        "2024-02-15 16:37:14.227000000",
        "2024-02-15 16:53:56.139000000",
    )
    analysis_class = analyses.Pcc1On60AAnalysis("", test_parameters)
    tag = "TAG"

    # act
    logger_adapter = commons.start_logging_new_check(analysis_class._logger, tag, indentation=2)
    logger_adapter.info("My log")

    # assert
    assert analysis_class.get_filtered_logs(tag) == "\t\tMy log"


def test_start_logging_new_check(caplog: pytest.LogCaptureFixture):
    # act
    logger_adapter = commons.start_logging_new_check(logging.getLogger(), "TAG")
    logger_adapter.info("My log")

    # assert
    assert caplog.messages == ["TAG", "\tMy log"]


def test_start_logging_new_check_layered(caplog: pytest.LogCaptureFixture):
    # act
    logger_adapter1 = commons.start_logging_new_check(logging.getLogger(), "TAG")
    logger_adapter2 = commons.start_logging_new_check(logger_adapter1, "DEEPER_TAG")
    logger_adapter2.info("My log")
    logger_adapter1.info("My other log")

    # assert
    assert caplog.messages == ["TAG", "\tDEEPER_TAG", "\t\tMy log", "\tMy other log"]
