from __future__ import annotations

import logging
import math

import pandas as pd
import pytest
from lhcsmapi.Time import Time

from lhcsmpowering.checks import analog_checks


@pytest.fixture(name="dummy_logger_adapter")
def fixture_dummy_logger_adapter():
    return logging.LoggerAdapter(logging.getLogger(), {"tags": "dummy tag"})


TEST_DEVICE_NAME = "dummy device"


def test_check_min_plateau_duration_no_plateau_found(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_minimum_plateau_duration(
        TEST_DEVICE_NAME, pd.Series(name="STATUS.I_MEAS", dtype=float), None, 5, dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: No plateau found for dummy device STATUS.I_MEAS signal."]


def test_check_min_plateau_duration_check_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_minimum_plateau_duration(
        TEST_DEVICE_NAME, pd.Series([3.0], name="STATUS.I_MEAS"), (1, 5), 5, dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The plateau duration for dummy device STATUS.I_MEAS signal is 0.000000004000 s, "
        "which is less than the minimum required duration of 0.000000005000 s."
    ]


def test_check_min_plateau_duration_check_passed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_minimum_plateau_duration(
        TEST_DEVICE_NAME, pd.Series(name="STATUS.I_MEAS", dtype=float), (1, 5), 3, dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The plateau duration for dummy device STATUS.I_MEAS signal is 0.000000004000 s, "
        "which is greater than the minimum required duration of 0.000000003000 s."
    ]


def test_check_plateau_values_absolute_deviation_no_plateau_found(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_plateau_values_absolute_deviation(
        TEST_DEVICE_NAME, pd.Series(name="STATUS.I_MEAS", dtype=float), None, 5.0, 3.0, "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: No plateau found for dummy device STATUS.I_MEAS signal."]


def test_check_plateau_values_absolute_deviation_no_plateau_found_when_skipping_last_point(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_plateau_values_absolute_deviation(
        TEST_DEVICE_NAME,
        pd.Series([1.0, 2.0, 3.0, 4.0, 5.0], name="STATUS.I_MEAS"),
        (0, 0),
        5.0,
        3.0,
        "unit",
        dummy_logger_adapter,
        skip_last_point=True,
    )

    assert res is False
    assert caplog.messages == [
        f"Check failed: No STATUS.I_MEAS data found for device {TEST_DEVICE_NAME} "
        f"between {Time.to_string_short(0)} and {Time.to_string_short(0)}."
    ]


def test_check_plateau_values_absolute_deviation_check_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_plateau_values_absolute_deviation(
        TEST_DEVICE_NAME,
        pd.Series([1.0, 2.0, 3.0, 4.0, 5.0], name="STATUS.I_MEAS"),
        (1, 3),
        3.0,
        0.5,
        "unit",
        dummy_logger_adapter,
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The maximum plateau level deviation for dummy device STATUS.I_MEAS signal is 1.0000 unit, "
        "which exceeds the threshold of 0.5000 unit. Expected plateau level: 3.0000 unit."
    ]


def test_check_plateau_values_absolute_deviation_check_passed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_plateau_values_absolute_deviation(
        TEST_DEVICE_NAME,
        pd.Series([1.0, 2.0, 3.0, 4.0, 5.0], name="STATUS.I_MEAS"),
        (1, 3),
        3.0,
        1.0,
        "unit",
        dummy_logger_adapter,
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The maximum plateau level deviation for dummy device STATUS.I_MEAS signal is 1.000 unit, "
        "which is within the threshold of 1.000 unit. Expected plateau level: 3.000 unit."
    ]


@pytest.mark.parametrize(
    ("time_window", "reference_window", "expected_logs"),
    [
        (None, (1, 5), ["Check failed: Time window for device dummy device is None."]),
        ((None, 3), (1, 5), ["Check failed: The start of the comparison interval for device dummy device is missing."]),
        ((1, None), (1, 5), ["Check failed: The end of the comparison interval for device dummy device is missing."]),
        ((1, 3), None, ["Check failed: Time window for reference device dummy device is None."]),
        (
            (1, 3),
            (None, 5),
            ["Check failed: The start of the comparison interval for reference device dummy device is missing."],
        ),
        (
            (1, 3),
            (1, None),
            ["Check failed: The end of the comparison interval for reference device dummy device is missing."],
        ),
    ],
)
def test_check_absolute_deviation_wrt_reference_no_timestamp(
    time_window,
    reference_window,
    expected_logs,
    caplog: pytest.LogCaptureFixture,
    dummy_logger_adapter: logging.LoggerAdapter,
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series(dtype=float),
        time_window,
        pd.Series(dtype=float),
        reference_window,
        4.0,
        "unit",
        dummy_logger_adapter,
    )

    assert res is False
    assert caplog.messages == expected_logs


def test_check_absolute_deviation_wrt_reference_no_data(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series(dtype=float),
        (1, 3),
        pd.Series(dtype=float),
        (1, 5),
        4.0,
        "unit",
        dummy_logger_adapter,
    )

    assert res is False
    assert caplog.messages == ["Check failed: No data found for device dummy device."]


def test_check_absolute_deviation_wrt_reference_check_passed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series([1.0, 2.0, 4.0, 4.0, 5.0], name="signal"),
        (1, 3),
        pd.Series([1.0, 2.0, 3.0, 4.0, 5.0], name="ref signal"),
        (1, 5),
        1.0,
        "unit",
        dummy_logger_adapter,
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The dummy device signal maximum deviation is 1.000 unit compared to "
        "reference, which is within the threshold of ±1.000 unit. Comparison period: 0.00 s."
    ]


def test_check_absolute_deviation_wrt_reference_check_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series([1.0, 2.0, 4.0, 7.0, 5.0], name="signal"),
        (1, 3),
        pd.Series([1.0, 2.0, 3.0, 4.0, 5.0], name="ref signal"),
        (1, 5),
        1.0,
        "unit",
        dummy_logger_adapter,
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The dummy device signal deviation is 3.000 unit compared to reference, "
        "which exceeds the threshold of ±1.000 unit."
    ]


def test_check_absolute_deviation_wrt_reference_check_unsynced_signals(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series(index=[0, 2, 4, 6, 8], data=[0, 2, 4, 6, 8], name="signal"),
        (0, 8),
        pd.Series(index=[0, 3, 5, 7, 9], data=[0, 3, 5, 7, 9], name="ref signal"),
        (0, 8),
        0.1,
        "unit",
        dummy_logger_adapter,
    )

    assert res is True
    assert caplog.messages == [
        "Interpolating missing values in the signal and reference signal data.",
        "Check passed: The dummy device signal maximum deviation is 0.0000 unit compared to reference, "
        "which is within the threshold of ±0.1000 unit. Comparison period: 0.00 s.",
    ]


def test_check_absolute_deviation_wrt_reference_align_right(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series([0.1, 0.1, 1.0, 1.0], name="signal"),
        (0, 1),
        pd.Series([1.0, 1.0, 0.0, 0.0], name="ref signal"),
        (0, 3),
        0.5,
        "unit",
        dummy_logger_adapter,
        align_right=True,
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The dummy device signal maximum deviation is 0.1000 unit compared to reference, "
        "which is within the threshold of ±0.5000 unit. Comparison period: 0.00 s."
    ]


def test_check_absolute_deviation_wrt_reference_empty_signal_after_window(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series([0.1, 0.1, 1.0, 1.0], name="signal"),
        (10, 11),
        pd.Series([1.0, 1.0, 0.0, 0.0], name="ref signal"),
        (0, 3),
        0.5,
        "unit",
        dummy_logger_adapter,
        align_right=True,
    )

    assert res is False
    assert caplog.messages == ["Check failed: No signal data found for device dummy device for the specified window."]


def test_check_absolute_deviation_wrt_reference_empty_reference_signal_after_window(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):

    res = analog_checks.check_absolute_deviation_wrt_reference(
        TEST_DEVICE_NAME,
        pd.Series([0.1, 0.1, 1.0, 1.0], name="signal"),
        (0, 1),
        pd.Series([1.0, 1.0, 0.0, 0.0], name="ref signal"),
        (10, 15),
        0.5,
        "unit",
        dummy_logger_adapter,
        align_right=True,
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: No reference ref signal data found for device dummy device for the specified window."
    ]


def test_check_signal_is_within_bound_no_data(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series(name="signal", dtype=float), (0, 1), (-math.inf, 5), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: No signal data found for device dummy device."]


def test_check_signal_is_within_bound_none_bound(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, None), (-math.inf, 5), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: No timestamps found for dummy device."]


def test_check_signal_is_within_bound_no_data_after_timestamp(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (10, 14), (-math.inf, 5), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: No signal data found for device dummy device "
        f"between {Time.to_string_short(10)} and {Time.to_string_short(14)}."
    ]


def test_check_signal_is_within_min_bound_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (2, math.inf), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The minimum value of signal is 1.000 unit, "
        "which is below the threshold of 2.000 unit for device dummy device."
    ]


def test_check_signal_is_within_max_bound_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (-math.inf, 2), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The maximum value of signal is 3.000 unit, which exceeds the threshold of "
        "2.000 unit for device dummy device."
    ]


def test_check_signal_is_within_bound_passed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_signal_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (-math.inf, 4), "unit", dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The values of dummy device signal are within the margin of -inf to 4.000 unit."
    ]


def test_check_signal_value_is_within_absolute_margin_normal_case(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series([1.0, 2.0, 3.0], name="ABC")

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, 1, 2.5, 0.5, dummy_logger_adapter, "unit"
    )

    # assert
    assert result is True
    assert caplog.messages == [
        "Check passed: Signal ABC was equal to 2.000 unit 0.0 seconds after reference timestamp, "
        "which is within the target of 2.500 ± 0.500 unit. "
        f"Reference timestamp: {Time.to_string_short(1)} (1)."
    ]


def test_check_signal_value_is_within_absolute_margin_normal_case_with_offset(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series([1.0, 2.0, 3.0], name="ABC")

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, 0, 2.5, 0.5, dummy_logger_adapter, "unit", offset=1
    )

    # assert
    assert result is True
    assert caplog.messages == [
        "Check passed: Signal ABC was equal to 2.000 unit 1e-09 seconds after reference timestamp, "
        "which is within the target of 2.500 ± 0.500 unit. "
        f"Reference timestamp: {Time.to_string_short(0)} (0)."
    ]


def test_check_signal_value_is_within_absolute_margin_timestamp_none(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series([1.0, 2.0, 3.0], name="ABC")

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, None, 2.0, 0.5, dummy_logger_adapter, "unit"
    )

    # assert
    assert result is False
    assert caplog.messages == ["Check failed: Reference timestamp not found for signal ABC."]


def test_check_signal_value_is_within_absolute_margin_out_of_margin(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series([1.0, 2.0, 3.0], name="ABC")

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, 1, 3.0, 0.1, dummy_logger_adapter, "unit"
    )

    # assert
    assert result is False
    assert caplog.messages == [
        "Check failed: Signal ABC was equal to 2.000 unit 0.0 seconds after reference timestamp, "
        "which is outside the target of 3.000 ± 0.100 unit. "
        f"Reference timestamp: {Time.to_string_short(1)} (1)."
    ]


def test_check_signal_value_is_within_absolute_margin_no_data(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series(name="ABC", dtype=float)

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, 1, 2.0, 0.5, dummy_logger_adapter, "unit"
    )

    # assert
    assert result is False
    assert caplog.messages == ["Check failed: No data found for signal ABC."]


def test_check_signal_value_is_within_absolute_margin_no_data_at_timestamp(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # arrange
    signal = pd.Series([0], name="ABC")

    # act
    result = analog_checks.check_signal_value_is_within_absolute_margin(
        signal, 1, 2.0, 0.5, dummy_logger_adapter, "unit"
    )

    # assert
    assert result is False
    assert caplog.messages == [
        "Check failed: Signal ABC not present 0.0 seconds after reference timestamp. "
        f"Reference timestamp: {Time.to_string_short(1)} (1)."
    ]


def test_check_target_current_is_equal_to_reference_successful(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # act
    result = analog_checks.check_target_current_is_equal_to_reference(3.0, 3.0, dummy_logger_adapter)

    # assert
    assert result is True
    assert caplog.messages == [
        "Check passed: Target current is 3.000 A, which is equal to the reference target current of 3.000 A."
    ]


def test_check_target_current_is_equal_to_reference_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    # act
    result = analog_checks.check_target_current_is_equal_to_reference(2.0, 3.0, dummy_logger_adapter)

    # assert
    assert result is False
    assert caplog.messages == [
        "Check failed: Target current is 2.000 A, which is different from the reference target current of 3.000 A."
    ]


@pytest.mark.parametrize(
    ("reference", "measured", "digit_count", "expected"),
    [
        (1.0, [1.0], 2, ("1.0", ["1.0"])),
        (1.0, [0.1], 2, ("1.0", ["0.1"])),
        (100.0, [0.4], 3, ("100", ["0"])),
        (0.02346, [1.0], 3, ("0.0235", ["1.0000"])),
        (0.02346, [0.000188], 3, ("0.0235", ["0.0002"])),
        (1.02345, [0.000188], 3, ("1.02", ["0.00"])),
        (1.02345, [0.0188, 111.123456], 4, ("1.023", ["0.019", "111.123"])),
        (1.02345, [0.0188, 111.123456, 8], 4, ("1.023", ["0.019", "111.123", "8.000"])),
        (-1.02345, [0.0188, -111.123456, 8], 4, ("-1.023", ["0.019", "-111.123", "8.000"])),
        (math.nan, [0.0188, 111.123456, 8], 4, ("nan", ["0.0188", "111.1235", "8.0000"])),
        (math.inf, [0.0188, 111.123456, 8], 4, ("inf", ["0.0188", "111.1235", "8.0000"])),
        (-math.inf, [0.0188, 111.123456, 8], 4, ("-inf", ["0.0188", "111.1235", "8.0000"])),
        (0, [0.0188, 111.123456, 8], 4, ("0.0000", ["0.0188", "111.1235", "8.0000"])),
    ],
)
def test_format_with_significant_digits(
    reference: float, measured: list[float], digit_count: int, expected: tuple[str, list[str]]
):
    assert analog_checks.format_with_significant_digits(reference, measured, digit_count) == expected


@pytest.mark.parametrize(
    ("signal", "bounds", "expected", "logged_messages"),
    [
        # Test case 1: Signal entirely within bounds
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {1: (0, 10), 4: (0, 10)},
            True,
            [
                f"Check passed: Signal signal is within bounds between {Time.to_string_short(1)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
        # Test case 2: Signal out of bounds at some points
        (
            pd.Series([15, 16, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {1: (0, 10), 4: (0, 10)},
            False,
            [
                f"Check failed: Signal signal is out of bounds at 2 time points between {Time.to_string_short(1)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
        # Test case 3: Signal out of bounds throughout
        (
            pd.Series([11, 12, 13, 14], index=[1, 2, 3, 4], name="signal"),
            {1: (5, 10), 4: (5, 10)},
            False,
            [
                f"Check failed: Signal signal is out of bounds at 4 time points between {Time.to_string_short(1)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
        # Test case 4: No signal within the timeframe (empty signal)
        (
            pd.Series(name="signal", dtype=float),
            {1: (0, 10), 4: (0, 10)},
            False,
            ["Check failed: Signal signal is empty."],
        ),
        # Test case 7: start greater than end
        (
            pd.Series(name="signal", dtype=float),
            {4: (0, 10), 2: (0, 10)},
            False,
            ["Check failed: Signal signal is empty."],
        ),
        # Test case 8: multiple points passing case
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {1: (0, 10), 2: (0, 10), 3: (0, 10), 4: (0, 10)},
            True,
            [
                f"Check passed: Signal signal is within bounds between {Time.to_string_short(1)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
        # Test case 9: multiple points failing case
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {4: (0, 5), 1: (0, 10), 2: (0, 10), 3: (0, 10)},
            False,
            [
                f"Check failed: Signal signal is out of bounds at 1 time points between {Time.to_string_short(3)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
        # Test case 10: points not in index
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {4: (0, 5), 11: (0, 10), 12: (0, 10), 3: (0, 10)},
            False,
            [
                f"Check failed: Signal signal is out of bounds at 1 time points between {Time.to_string_short(3)} "
                f"and {Time.to_string_short(4)}.",
                "Check failed: Timestamp interval (4, 11) is invalid.",
                "Check failed: Timestamp interval (11, 12) is invalid.",
            ],
        ),
        # Test case 11: not enough bounds
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {4: (0, 5)},
            False,
            ["Check failed: At least two bounds are required for the check."],
        ),
        # Test case 11: empty dataframe between bounds
        (
            pd.Series([5, 6, 7, 8], index=[1, 4, 5, 6], name="signal"),
            {2: (0, 5), 3: (0, 5)},
            False,
            [
                f"Check failed: No signal signal data between {Time.to_string_short(2)} "
                f"and {Time.to_string_short(3)}."
            ],
        ),
        # Test case 12: infinite bounds
        (
            pd.Series([5, 6, 7, 8], index=[1, 2, 3, 4], name="signal"),
            {-math.inf: (0, 10), math.inf: (0, 10)},
            True,
            [
                f"Check passed: Signal signal is within bounds between {Time.to_string_short(1)} "
                f"and {Time.to_string_short(4)}."
            ],
        ),
    ],
)
def test_check_signal_is_within_bounded_region(
    signal: pd.Series,
    bounds: dict[int, tuple[float, float]],
    expected: bool,
    logged_messages: list[str],
    caplog: pytest.LogCaptureFixture,
    dummy_logger_adapter: logging.LoggerAdapter,
):
    result = analog_checks.check_signal_is_within_bounded_region(
        signal=signal, bounds=bounds, logger=dummy_logger_adapter
    )
    assert result == expected
    assert caplog.messages == logged_messages


def test_check_average_value_is_between_no_data(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series(name="signal", dtype=float), (0, 1), (-5.0, 5.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: No signal data found for device dummy device."]


def test_check_average_value_is_between_no_data_after_timestamp(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (10, 14), (-5.0, 5.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: No signal data found for device dummy device "
        f"between {Time.to_string_short(10)} and {Time.to_string_short(14)}."
    ]


def test_check_average_value_is_between_failed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (-1.0, 1.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: The average value of signal is 2.000 unit, which is outside of the range from "
        "-1.000 to 1.000 unit for device dummy device."
    ]


def test_check_average_value_is_between_nan_and_not_nan(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (math.nan, 4.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: one or more boundaries were NaN. Boundaries were " f"{math.nan} and {4.0}."
    ]


def test_check_average_value_is_between_not_nan_and_nan(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (1.0, math.nan), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: one or more boundaries were NaN. Boundaries were " f"{1.0} and {math.nan}."
    ]


def test_check_average_value_is_between_passed(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (1.0, 4.0), "unit", dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: The average value of signal for dummy device is 2.000 unit, which is within "
        "the allowed range from 1.000 to 4.000 unit."
    ]


def test_check_average_value_is_between_passed_with_swapped_boundaries(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_average_value_is_within_bounds(
        TEST_DEVICE_NAME, pd.Series([1, 2, 3], name="signal"), (0, 2), (4.0, 1.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: lower bound was larger than upper bound. Boundaries were 4.0 and 1.0."]


def test_check_value_is_between_none_value(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, None, "Value", (-5.0, 5.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: Value is invalid (None)."]


def test_check_value_is_between_nan_value(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, math.nan, "Value", (-5.0, 5.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: Value is invalid (nan)."]


def test_check_value_is_between_failed(caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 2.0, "Value", (-1.0, 1.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: Value is 2.000 unit, which is outside of the range from "
        "-1.000 to 1.000 unit for device dummy device."
    ]


def test_check_value_is_between_nan_and_not_nan(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 2.0, "Value", (math.nan, 4.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: one or more boundaries were NaN. Boundaries were " f"{math.nan} and {4.0}."
    ]


def test_check_value_is_between_not_nan_and_nan(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 2.0, "Value", (1.0, math.nan), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == [
        "Check failed: one or more boundaries were NaN. Boundaries were " f"{1.0} and {math.nan}."
    ]


def test_check_value_is_between_minus_inf_and_not_nan(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, -1000000000000, "Value", (-math.inf, 1.0), "unit", dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: Value for dummy device is -1000000000000.000 unit, which is within "
        "the allowed range from -inf to 1.000 unit."
    ]


def test_check_value_is_between_not_nan_and_inf(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 1000000000000, "Value", (1.0, math.inf), "unit", dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: Value for dummy device is 1000000000000.0000 unit, which is within "
        "the allowed range from 1.0000 to inf unit."
    ]


def test_check_value_is_between_passed(caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 2.0, "Value", (1.0, 4.0), "unit", dummy_logger_adapter
    )

    assert res is True
    assert caplog.messages == [
        "Check passed: Value for dummy device is 2.000 unit, which is within "
        "the allowed range from 1.000 to 4.000 unit."
    ]


def test_check_value_is_between_passed_with_swapped_boundaries(
    caplog: pytest.LogCaptureFixture, dummy_logger_adapter: logging.LoggerAdapter
):
    res = analog_checks.check_value_is_within_bounds(
        TEST_DEVICE_NAME, 2.0, "Value", (4.0, 1.0), "unit", dummy_logger_adapter
    )

    assert res is False
    assert caplog.messages == ["Check failed: lower bound was larger than upper bound. Boundaries were 4.0 and 1.0."]
