import re

import pytest
from lhcsmapi.Time import Time

from lhcsmpowering import analyses


def test_is_the_analysis_compatible_with_test_parameters_true_case():
    # arrange
    test_parameters = analyses.TestParameterInput(
        "PIC2 CIRCUIT QUENCH VIA QPS",
        "RQ5.R8",
        "Recommissioning 2022/2023",
        Time.to_unix_timestamp("2023-03-20 16:21:35.946000000+01:00"),
        Time.to_unix_timestamp("2023-03-20 17:10:23.353000000+01:00"),
    )

    # act
    analyses.Pic2CircuitQuenchViaQpsForIPQAnalysis("", test_parameters)


@pytest.mark.parametrize(
    ("test_parameters", "expected_error"),
    [
        (
            analyses.TestParameterInput(  # invalid hwc test
                "INVALID HWC TEST",
                "RQ5.R8",
                "Recommissioning 2022/2023",
                Time.to_unix_timestamp("2023-03-20 16:21:35.946000000+01:00"),
                Time.to_unix_timestamp("2023-03-20 17:10:23.353000000+01:00"),
            ),
            "Unsupported HWC test: INVALID HWC TEST",
        ),
        (
            analyses.TestParameterInput(
                "PIC2 CIRCUIT QUENCH VIA QPS",
                "INVALID CIRCUIT NAME",
                "Recommissioning 2022/2023",
                Time.to_unix_timestamp("2023-03-20 16:21:35.946000000+01:00"),
                Time.to_unix_timestamp("2023-03-20 17:10:23.353000000+01:00"),
            ),
            "Circuit name INVALID CIRCUIT NAME not present in the metadata.",
        ),
        (
            analyses.TestParameterInput(
                "PIC2 CIRCUIT QUENCH VIA QPS",
                "RB.A12",
                "Recommissioning 2022/2023",
                Time.to_unix_timestamp("2023-03-20 16:21:35.946000000+01:00"),
                Time.to_unix_timestamp("2023-03-20 17:10:23.353000000+01:00"),
            ),
            "Unsupported circuit type RB for HWC test PIC2 CIRCUIT QUENCH VIA QPS. "
            "Supported circuit types: 600A, IPD, IPQ",
        ),
        (
            analyses.TestParameterInput(
                "PLI1.c3",
                "RQ5.R8",
                "Recommissioning post LS2",
                Time.to_unix_timestamp("2021-05-07 21:27:55.407000000"),
                Time.to_unix_timestamp("2021-05-07 21:38:20.442000000"),
            ),
            "Circuit RQ5.R8 is not compatible with the analysis Pic2CircuitQuenchViaQpsForIPQAnalysis.",
        ),
    ],
)
def test_is_the_analysis_compatible_with_test_parameters_false_case(
    test_parameters: analyses.TestParameterInput, expected_error: str
):
    with pytest.raises(ValueError, match=re.escape(expected_error)):
        analyses.Pic2CircuitQuenchViaQpsForIPQAnalysis("", test_parameters)
