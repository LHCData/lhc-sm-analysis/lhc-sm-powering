from __future__ import annotations

import math
from collections.abc import Callable

import pandas as pd
import pytest
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering.analyses.pno import pno_a1


@pytest.mark.parametrize(
    ("i_ref", "i_ref_plateau_i_pno", "expected"),
    [
        (pd.Series(dtype=float), (1, 2), (None, None)),
        (pd.Series([1, 0, 0, 0, 3, 5, 5, 5, 2, -1, -1, -1, -1, 1]), (5, 7), ((1, 3), None)),
        (pd.Series([1, 1, 1, 1, 3, 5, 5, 5, 2, 0, 0, 0, 0, 1]), (5, 7), (None, (9, 12))),
        (pd.Series([1, 0, 0, 0, 3, 5, 5, 5, 2, 0, 0, 0, 0, 1]), (5, 7), ((1, 3), (9, 12))),
        (pd.Series([0, 1, 0, 0, 3, 5, 5, 5, 2, 0, 0, 0, 0, 1]), (5, 7), ((2, 3), (9, 12))),
        (pd.Series([0, 1, 0, 0, 3, 5, 5, 5, 2, 0, 0, 1, 0, 1]), (5, 7), ((2, 3), (9, 10))),
    ],
)
def test_calculate_i_min_op_plateaus(
    i_ref: pd.Series,
    i_ref_plateau_i_pno: tuple[int, int],
    expected: tuple[tuple[int, int] | None, tuple[int, int] | None],
):
    i_min_op = 0
    assert pno_a1.calculate_i_min_op_plateaus(i_ref, i_ref_plateau_i_pno, i_min_op) == expected


@pytest.mark.parametrize(
    ("r_meas", "r_lead_pos", "r_lead_neg", "r_tot_measured", "expected"),
    [
        (
            pd.Series(dtype=float, name="R_MEAS"),
            pd.Series(dtype=float, name="R_LEAD_POS"),
            pd.Series(dtype=float, name="R_LEAD_NEG"),
            4,
            pd.Series(dtype=float, name="R_MAG"),
        ),
        (
            pd.Series([10.0, 20, 30], name="R_MEAS"),
            pd.Series([0.0, 0, 0], name="R_LEAD_POS"),
            pd.Series([0.0, 0, 0], name="R_LEAD_NEG"),
            4,
            pd.Series([6.0, 16, 26], name="R_MAG"),
        ),
        (
            pd.Series([10.0, 20, 30], name="R_MEAS"),
            pd.Series([0.0, 0, 0], name="R_LEAD_POS"),
            pd.Series([1.0, 2, 3], name="R_LEAD_NEG"),
            0,
            pd.Series([9.0, 18, 27], name="R_MAG"),
        ),
        (
            pd.Series([10.0, 20, 30], name="R_MEAS"),
            pd.Series([4.0, 3, 2], name="R_LEAD_POS"),
            pd.Series([0.0, 0, 0], name="R_LEAD_NEG"),
            0,
            pd.Series([6.0, 17, 28], name="R_MAG"),
        ),
        (
            pd.Series([10.0, 20, 30], name="R_MEAS"),
            pd.Series([2.0, 2, 3], name="R_LEAD_POS"),
            pd.Series([1.0, 2, 2], name="R_LEAD_NEG"),
            4,
            pd.Series([3.0, 12, 21], name="R_MAG"),
        ),
        (
            pd.Series([10.0, 20, 30], name="R_MEAS"),
            pd.Series([2.0, 2, 3], name="R_LEAD_POS"),
            pd.Series([1.0, 2, 2, 0], name="R_LEAD_NEG"),
            4,
            pd.Series([3.0, 12, 21, math.nan], name="R_MAG"),
        ),
        (
            pd.Series([7.0, 7, 5], name="R_MEAS"),
            pd.Series([2.0, 2, 3], name="R_LEAD_POS"),
            pd.Series([1.0, 2, 2], name="R_LEAD_NEG"),
            4,
            pd.Series([0.0, -1, -4], name="R_MAG"),
        ),
    ],
)
def test_calculate_magnet_resistance(
    r_meas: pd.Series, r_lead_pos: pd.Series, r_lead_neg: pd.Series, r_tot_measured: float, expected: pd.Series
):
    pd.testing.assert_series_equal(
        pno_a1.calculate_magnet_resistance(r_meas, r_lead_pos, r_lead_neg, r_tot_measured), expected
    )


@pytest.mark.parametrize(
    ("v_meas", "u_lead_pos", "u_lead_neg", "i_meas", "r_tot_measured", "didt_pno", "expected"),
    [
        (
            pd.Series(dtype=float),
            pd.Series(dtype=float),
            pd.Series(dtype=float),
            pd.Series(dtype=float),
            4,
            5,
            pd.Series(dtype=float, name="L_MAG"),
        ),
        (pd.Series([31.0]), pd.Series([3.0]), pd.Series([5.0]), pd.Series([3.0]), 4, 5, pd.Series([2.2], name="L_MAG")),
        (
            pd.Series(dtype=float),
            pd.Series([3.0]),
            pd.Series([5.0]),
            pd.Series([3.0]),
            4,
            5,
            pd.Series([math.nan], name="L_MAG"),
        ),
        (
            pd.Series([31.0]),
            pd.Series(dtype=float),
            pd.Series([5.0]),
            pd.Series([3.0]),
            4,
            5,
            pd.Series([math.nan], name="L_MAG"),
        ),
        (
            pd.Series([31.0]),
            pd.Series([3.0]),
            pd.Series(dtype=float),
            pd.Series([3.0]),
            4,
            5,
            pd.Series([math.nan], name="L_MAG"),
        ),
        (
            pd.Series([30.0, 23]),
            pd.Series([3.0, 1]),
            pd.Series([5.0, 3]),
            pd.Series([3.0, 1]),
            4,
            5,
            pd.Series([2.0, 3], name="L_MAG"),
        ),
        (
            pd.Series([31.0]),
            pd.Series([3.0, 3.0]),
            pd.Series([5.0]),
            pd.Series([3.0]),
            4,
            5,
            pd.Series([2.2, math.nan], name="L_MAG"),
        ),
    ],
)
def test_calculate_magnet_inductance(
    v_meas: pd.Series,
    u_lead_pos: pd.Series,
    u_lead_neg: pd.Series,
    i_meas: pd.Series,
    r_tot_measured: float,
    didt_pno: float,
    expected: pd.Series,
):
    pd.testing.assert_series_equal(
        pno_a1.calculate_magnet_inductance(v_meas, u_lead_pos, u_lead_neg, i_meas, r_tot_measured, didt_pno), expected
    )


@pytest.mark.parametrize(
    ("i_meas", "bounds", "i_ref_plateau_i_pno", "i_ref_plateau_i_min_op", "expected"),
    [
        (pd.Series(dtype=float), (5, 10), (4, 6), ((1, 2), (8, 9)), ((None), (None))),
        (pd.Series([0, 0, 2, 4, 7, 9, 9, 9, 6, 4, 1, 0, 0]), (2, 7), None, ((0, 1), (11, 12)), (None, None)),
        (pd.Series([0, 0, 2, 4, 7, 9, 9, 9, 6, 4, 1, 0, 0]), (2, 7), (5, 7), (None, (11, 12)), (None, (8, 9))),
        (pd.Series([0, 0, 2, 4, 7, 9, 9, 9, 6, 4, 1, 0, 0]), (2, 7), (5, 7), ((0, 1), None), ((2, 4), None)),
        (pd.Series([0, 0, 2, 4, 7, 9, 9, 9, 6, 4, 1, 0, 0]), (2, 7), (5, 7), ((0, 1), (11, 12)), ((2, 4), (8, 9))),
    ],
)
def test_calculate_l_mag_check_windows(
    i_meas: pd.Series,
    bounds: tuple[float, float],
    i_ref_plateau_i_pno: tuple[int, int] | None,
    i_ref_plateau_i_min_op: tuple[tuple[int, int] | None, tuple[int, int] | None],
    expected: tuple[tuple[int, int] | None, tuple[int, int] | None],
):
    assert pno_a1.calculate_l_mag_check_windows(i_meas, bounds, i_ref_plateau_i_pno, i_ref_plateau_i_min_op) == expected


@pytest.mark.parametrize(
    ("circuit_name", "expected"),
    [
        ("RCBH11.L2B1", pno_a1.get_parameters_for_60a),
        ("RCBCV5.L8B2", pno_a1.get_parameters_for_80a_rplb_rcb),
        ("RCBCH10.L8B2", pno_a1.get_parameters_for_120a_rplb_rcb),
        ("RCBCH6.R3B2", pno_a1.get_parameters_for_80a_rpmc_rcb),
        ("RCBCH10.R3B2", pno_a1.get_parameters_for_120a_rpmc_rcb),
        ("RCTX3.L8", pno_a1.get_parameters_for_low_current_rplb_triplets),
        ("RCOSX3.R8", pno_a1.get_parameters_for_high_current_rplb_triplets),
        ("RCSX3.L8", pno_a1.get_parameters_for_high_current_rplb_triplets),
    ],
)
def test_parameter_mapping(circuit_name: str, expected: Callable) -> None:
    assert pno_a1.get_pno_a1_parameters_function(circuit_name, 0) == expected


@pytest.mark.parametrize("circuit_name", signal_metadata.get_circuit_names("80-120A", 0))
def test_parameter_mapping_works_for_every_circuit_name(circuit_name: str) -> None:
    pno_a1.get_pno_a1_parameters_function(circuit_name, 0)
