from __future__ import annotations

import pandas as pd
import pytest

import lhcsmpowering.analyses.pno.pno_a3 as pno_a3
from lhcsmpowering.analyses.commons import Cycle, DataSource, Plateau


# Define helper functions outside the test
def create_plateau(edges):
    return Plateau(edges, 0, 0, DataSource.NXCALS)


def create_cycle_data(plateau_edges):
    return pno_a3.PnoA3Analysis.CycleData(
        timestamp_fgc=None,
        i_pno_plateau=create_plateau(plateau_edges),
        i_min_op_plateau=None,
        u_diff_std_dev_at_i_pno=None,
        u_res_std_dev_at_i_pno=None,
        abs_i_dcct_drift_at_i_pno=None,
        u_res_point_before_i_pno_plateau=None,
        u_res_point_after_i_pno_plateau=None,
    )


# Create test data to be used in parameterize
cycles_with_none_plateaus = {Cycle.POSITIVE: create_cycle_data(None), Cycle.NEGATIVE: create_cycle_data(None)}
positive_cycle_only = {Cycle.POSITIVE: create_cycle_data((1, 1))}
both_cycles_simple = {Cycle.POSITIVE: create_cycle_data((1, 1)), Cycle.NEGATIVE: create_cycle_data((3, 3))}
both_cycles_complex = {Cycle.POSITIVE: create_cycle_data((1, 2)), Cycle.NEGATIVE: create_cycle_data((4, 5))}


@pytest.mark.parametrize(
    ("u_res", "i_meas", "cycles", "expected_resistance"),
    [
        # Empty series with both cycles but None plateaus
        (pd.Series(dtype=float), pd.Series(dtype=float), cycles_with_none_plateaus, None),
        # Series with positive cycle only (RU circuit)
        (pd.Series([1.0, 1, 4, -1, 0]), pd.Series([0.0, 4, 0, -4, 0]), positive_cycle_only, 0.25),
        # Series with both cycles
        (pd.Series([1.0, 1, 4, -1, 0]), pd.Series([0.0, 4, 0, -4, 0]), both_cycles_simple, 0.25),
        # More complex case with both cycles
        (pd.Series([1.0, 1, 0, 4, -1, 1, 0]), pd.Series([0.0, 3, 4, 0, -4, -5, 0]), both_cycles_complex, 0.0625),
    ],
)
def test_calculate_resistance(
    u_res: pd.Series,
    i_meas: pd.Series,
    cycles: dict[Cycle, pno_a3.PnoA3Analysis.CycleData],
    expected_resistance: float | None,
):
    result = pno_a3.PnoA3Analysis.calculate_resistance_at_i_pno(u_res, i_meas, cycles)

    # For float comparisons, we need to handle the case where the result is None
    if expected_resistance is None:
        assert result is None
    else:
        assert pytest.approx(result) == expected_resistance
