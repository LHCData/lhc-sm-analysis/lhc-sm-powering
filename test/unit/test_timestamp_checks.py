import logging

import pandas as pd
import pytest
from lhcsmapi.Time import Time

from lhcsmpowering.checks import timestamp_checks


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_passed(caplog: pytest.LogCaptureFixture):
    # arrange
    timestamp_qps_a = 1620227369029000000
    timestamp_qps_b = 1620227369030000000

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger()
    )

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: QPS PM timestamps are 1.0 ms apart."]


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_different_gap(caplog: pytest.LogCaptureFixture):
    # arrange
    timestamp_qps_a = 1620227369029000000
    timestamp_qps_b = 1620227369031000000

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger(), 2_000_000
    )

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: QPS PM timestamps are 2.0 ms apart."]


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_failed_no_qps_timestamp(
    caplog: pytest.LogCaptureFixture,
):
    # arrange
    timestamp_qps_a = None
    timestamp_qps_b = None

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger()
    )

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: QPS PM timestamps are None."]


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_failed_no_qps_a_timestamp(
    caplog: pytest.LogCaptureFixture,
):
    # arrange
    timestamp_qps_a = None
    timestamp_qps_b = 1620227369029000000

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger()
    )

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: QPS PM A timestamp is None."]


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_failed_no_qps_b_timestamp(
    caplog: pytest.LogCaptureFixture,
):
    # arrange
    timestamp_qps_a = 1620227369029000000
    timestamp_qps_b = None

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger()
    )

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: QPS PM B timestamp is None."]


def test_check_qps_pm_data_headers_timestamps_are_one_millisecond_apart_failed_timestamps_not_one_millisecond_apart(
    caplog: pytest.LogCaptureFixture,
):
    # arrange
    timestamp_qps_a = 1620227369029000000
    timestamp_qps_b = 1620227369031000000

    # act
    res = timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
        timestamp_qps_a, timestamp_qps_b, logging.getLogger()
    )

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: QPS PM timestamps are not 1ms apart. "
        f"Timestamps: QPS A: {Time.to_string_short(1620227369029000000)} (1620227369029000000), "
        f"QPS B: {Time.to_string_short(1620227369031000000)} (1620227369031000000)."
    ]


def test_check_timestamps_are_within_margin_passed(caplog: pytest.LogCaptureFixture):
    # arrange
    dict_timestamps = {"timestamp_a": 1620227369029000000, "timestamp_b": 1620227369030000000}
    margin = 1_000_000

    # act
    res = timestamp_checks.check_timestamps_are_within_margin(dict_timestamps, margin, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: Timestamps are within the margin of 1,000,000 ns."]


def test_check_timestamps_are_within_margin_failed_no_timestamp(caplog: pytest.LogCaptureFixture):
    # arrange
    dict_timestamps = {"timestamp_a": 1620227369029000000, "timestamp_b": None, "timestamp_c": None}
    margin = 1_000_000

    # act
    res = timestamp_checks.check_timestamps_are_within_margin(dict_timestamps, margin, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: No timestamps found for timestamp_b, timestamp_c."]


def test_check_timestamps_are_within_margin_failed_timestamps_not_within_margin(caplog: pytest.LogCaptureFixture):
    # arrange
    dict_timestamps = {"timestamp_a": 1620227369029000000, "timestamp_b": 1620227369031000000}
    margin = 1_000_000

    # act
    res = timestamp_checks.check_timestamps_are_within_margin(dict_timestamps, margin, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: Timestamps are not within the margin of 1,000,000 ns. "
        "Timestamps: timestamp_a: 1620227369029000000, timestamp_b: 1620227369031000000."
    ]


def test_check_fgc_pm_data_headers_length_failed(caplog: pytest.LogCaptureFixture):
    # arrange
    fgc_name = "fgc"
    fgc_pm_dumps = pd.DataFrame()

    # act
    res = timestamp_checks.check_fgc_pm_data_headers_length(fgc_name, fgc_pm_dumps, 3, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: Found 0 PM timestamp for FGC fgc, expected 3."]


def test_check_fgc_pm_data_headers_length_passed_with_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    fgc_name = "fgc"
    fgc_pm_dumps = pd.DataFrame({"timestamp": [1620227369029000000, 1620227369030000000, 1620227369031000000]})

    # act
    res = timestamp_checks.check_fgc_pm_data_headers_length(fgc_name, fgc_pm_dumps, 3, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: Found exactly 3 PM timestamps for FGC fgc, as expected."]


def test_check_fgc_pm_data_headers_length_passed_no_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    fgc_name = "fgc"
    fgc_pm_dumps = pd.DataFrame()

    # act
    res = timestamp_checks.check_fgc_pm_data_headers_length(fgc_name, fgc_pm_dumps, 0, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == [
        "Check passed: No FGC PM dumps were found for fgc in the analyzed interval, as expected."
    ]


def test_check_timestamp_precedence_passed_with_timestamp_gap(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence(
        "PIC", 0, "FGC", 20_000_000, logging.getLogger(), max_allowed_timestamp_gap=40_000_000
    )

    # assert
    assert res is True
    assert caplog.messages == [
        "Check passed: PIC timestamp precedes FGC timestamp by 20.0 ms, which is within the allowed gap of 40.0 ms."
    ]


def test_check_timestamp_precedence_passed_without_timestamp_gap(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence("PIC", 0, "FGC", 20_000_000, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: PIC timestamp precedes FGC timestamp by 20.0 ms, as expected."]


def test_check_check_timestamp_precedence_failed_no_pic_timestamp(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence("PIC", None, "FGC", 20_000_000, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: No PIC timestamp found."]


def test_check_timestamp_precedence_failed_no_fgc_timestamp(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence("PIC", 0, "FGC", None, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: No FGC timestamp found."]


def test_check_timestamp_precedence_failed_pic_after_fgc(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence("PIC", 40_000_000, "FGC", 0, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: PIC timestamp was expected to precede FGC timestamp, "
        "but PIC timestamp occurred 40.0 ms after FGC timestamp."
    ]


def test_check_timestamp_precedence_failed_gap_too_large(caplog: pytest.LogCaptureFixture):
    # act
    res = timestamp_checks.check_timestamp_precedence(
        "PIC", 0, "FGC", 60_000_000, logging.getLogger(), max_allowed_timestamp_gap=40_000_000
    )

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: PIC timestamp precedes FGC timestamp by 60.0 ms, "
        "which exceeds the maximum allowed gap of 40.0 ms. "
    ]


def test_check_ee_pm_data_headers_length_passed_with_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    ee_pm_data_headers = pd.DataFrame({"timestamp": [1620227369029000000, 1620227369030000000]})
    ee_name = "ee"

    # act
    res = timestamp_checks.check_ee_pm_data_headers_length(ee_name, ee_pm_data_headers, 2, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: Found exactly 2 PM timestamps for EE ee, as expected."]


def test_check_ee_pm_data_headers_length_passed_without_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    ee_pm_data_headers = pd.DataFrame({"source": [], "timestamp": []})
    ee_name = "ee"

    # act
    res = timestamp_checks.check_ee_pm_data_headers_length(ee_name, ee_pm_data_headers, 0, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: No PM dumps were found for EE ee in the analyzed interval, as expected."]


def test_check_ee_pm_data_headers_length_failed_no_warning(caplog: pytest.LogCaptureFixture):
    # arrange
    ee_pm_data_headers = pd.DataFrame({"source": [], "timestamp": []})
    ee_name = "ee"

    # act
    res = timestamp_checks.check_ee_pm_data_headers_length(ee_name, ee_pm_data_headers, 2, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: Found 0 PM timestamp for EE ee, expected 2."]


def test_check_ee_pm_data_headers_length_failed_with_warning(caplog: pytest.LogCaptureFixture):
    # arrange
    ee_pm_data_headers = pd.DataFrame({"source": ["RCD.A12B1"], "timestamp": [1620227369029000000]})
    ee_name = "ee"

    # act
    res = timestamp_checks.check_ee_pm_data_headers_length(ee_name, ee_pm_data_headers, 0, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: Found 1 PM timestamp for EE ee, expected 0.",
        "The energy extraction system should be checked, as it was fired.",
    ]


def test_check_qps_pm_data_headers_length_passed_with_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    qps_pm_data_headers = pd.DataFrame({"timestamp": [1620227369029000000, 1620227369030000000]})

    # act
    res = timestamp_checks.check_qps_pm_data_headers_length(qps_pm_data_headers, 2, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: Found exactly 2 QPS PM timestamps, as expected."]


def test_check_qps_pm_data_headers_length_passed_without_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    qps_pm_data_headers = pd.DataFrame({"source": [], "timestamp": []})

    # act
    res = timestamp_checks.check_qps_pm_data_headers_length(qps_pm_data_headers, 0, logging.getLogger())

    # assert
    assert res is True
    assert caplog.messages == ["Check passed: No QPS PM dumps were found in the analyzed interval, as expected."]


def test_check_qps_pm_data_headers_length_failed_no_warning(caplog: pytest.LogCaptureFixture):
    # arrange
    qps_pm_data_headers = pd.DataFrame({"source": [], "timestamp": []})

    # act
    res = timestamp_checks.check_qps_pm_data_headers_length(qps_pm_data_headers, 2, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == ["Check failed: Found 0 QPS PM timestamp, expected 2."]


def test_check_qps_pm_data_headers_length_length_failed_with_warning(caplog: pytest.LogCaptureFixture):
    # arrange
    qps_pm_data_headers = pd.DataFrame({"source": ["RD1.L2"], "timestamp": [1620227369029000000]})

    # act
    res = timestamp_checks.check_qps_pm_data_headers_length(qps_pm_data_headers, 0, logging.getLogger())

    # assert
    assert res is False
    assert caplog.messages == [
        "Check failed: Found 1 QPS PM timestamp, expected 0.",
        "The quench heaters should be checked, as they might have been fired.",
    ]
