import pathlib

import pytest
from lhcsmapi import reference
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import GenericCircuitType
from lhcsmapi.Time import Time

from lhcsmpowering import analyses


def test_get_analysis_instance():
    # arrange
    test_parameters = analyses.TestParameterInput(
        "PIC2 CIRCUIT QUENCH VIA QPS",
        "RQ5.R8",
        "Recommissioning 2022/2023",
        Time.to_unix_timestamp("2023-03-20 16:21:35.946000000+01:00"),
        Time.to_unix_timestamp("2023-03-20 17:10:23.353000000+01:00"),
    )

    # act
    result = analyses.get_analysis_instance("identifier", test_parameters)

    # assert
    assert isinstance(result, analyses.Pic2CircuitQuenchViaQpsForIPQAnalysis)
    assert result._identifier == "identifier"


def test_get_supported_hwc_tests():
    assert "PLI1.c3" in analyses.get_supported_hwc_tests()


def test_get_supported_circuit_types():
    # arrange
    hwc_test = "PIC2 CIRCUIT QUENCH VIA QPS"

    # act
    result = analyses.get_supported_circuit_types(hwc_test)

    # assert
    assert result == {GenericCircuitType.A600, GenericCircuitType.IPQ, GenericCircuitType.IPD}


def test_get_supported_circuit_types_invalid_hwc_test():
    # arrange
    hwc_test = "hwc_test"

    # act
    with pytest.raises(ValueError, match=r"Unsupported HWC test: hwc_test\. Supported HWC tests:.*"):
        analyses.get_supported_circuit_types(hwc_test)


def test_get_reference_test_selection_strategy():
    # arrange
    hwc_test = "PLI1.c3"
    circuit_name = "RQ5.R8"

    # act
    result = analyses.get_reference_test_selection_strategy(hwc_test, circuit_name)

    # assert
    assert result == reference.ReferenceTestSelectionStrategy.LAST


def test_get_analysis_test_input_parameters_without_reference():
    # arrange
    hwc_summary = signal_metadata.get_hwc_summary()
    hwc_test = "PIC2 CIRCUIT QUENCH VIA QPS"
    circuit_name = "RQ5.R8"
    campaign = "Recommissioning 2022/2023"
    t_start = "2023-03-20 16:21:35.946000000+01:00"
    t_end = "2023-03-20 17:10:23.353000000+01:00"

    # act
    result = analyses.get_analysis_test_input_parameters(hwc_summary, hwc_test, circuit_name, campaign, t_start, t_end)

    # assert
    assert result == analyses.TestParameterInput(
        hwc_test, circuit_name, campaign, Time.to_unix_timestamp(t_start), Time.to_unix_timestamp(t_end)
    )


def test_get_analysis_test_input_parameters_with_reference():
    # arrange
    hwc_summary = signal_metadata.get_hwc_summary()
    hwc_test = "PNO.d3"
    circuit_name = "RSD1.A23B1"
    campaign = "Recommissioning 2021/2022"
    t_start = "2022-03-16 18:46:05.119000000+01:00"
    t_end = "2022-03-16 19:29:40.449000000+01:00"

    # act
    test_input_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary, hwc_test, circuit_name, campaign, t_start, t_end
    )

    # assert
    assert test_input_parameters == analyses.TestParameterInputWithRef(
        hwc_test,
        circuit_name,
        campaign,
        Time.to_unix_timestamp(t_start),
        Time.to_unix_timestamp(t_end),
        Time.to_unix_timestamp("2021-09-28 18:47:07.991"),
        Time.to_unix_timestamp("2021-09-28 19:33:28.617"),
    )


def test_get_analysis_test_input_parameters_invalid_hwc():
    # arrange
    hwc_summary = signal_metadata.get_hwc_summary()
    hwc_test = "hwc_test"
    circuit_name = "RSD1.A23B1"
    campaign = "Recommissioning 2021/2022"
    t_start = Time.to_unix_timestamp("2022-03-16 18:46:05.119000000+01:00")
    t_end = Time.to_unix_timestamp("2022-03-16 19:29:40.449000000+01:00")

    with pytest.raises(ValueError, match=r"Unsupported HWC test: hwc_test\. Supported HWC tests:.*"):
        analyses.get_analysis_test_input_parameters(hwc_summary, hwc_test, circuit_name, campaign, t_start, t_end)


def test_get_notebook_path():
    # arrange
    hwc_test = "PNO.d3"
    circuit_name = "RSD1.A23B1"

    # act
    result = analyses.get_notebook_path(hwc_test, circuit_name)

    # assert
    assert isinstance(result, pathlib.Path)
    assert str(result).endswith("lhcsmpowering/analyses/pno/pno_d3/AN_600A_PNO.d3.ipynb")
