from __future__ import annotations

import logging
import math
from collections.abc import Iterable

import pandas as pd
import pytest

from lhcsmpowering import processing


@pytest.mark.parametrize(
    ("plateaus", "timestamp", "expected"),
    [
        ([(0, 10), (20, 30)], None, None),  # timestamp is None
        ([(0, 10), (20, 30)], -1, None),
        ([(0, 10), (20, 30)], 0, None),
        ([(0, 10), (20, 30)], 1, (0, 10)),
        ([(0, 10), (20, 30)], 10, (0, 10)),
        ([(0, 10), (20, 30)], 11, (0, 10)),
        ([(0, 10), (20, 30)], 19, (0, 10)),
        ([(0, 10), (20, 30)], 20, (0, 10)),
        ([(0, 10), (20, 30)], 21, (20, 30)),
        ([(0, 10), (20, 30)], 30, (20, 30)),
        ([(0, 10), (20, 30)], 31, (20, 30)),
        ([], 31, None),
        ([(20, 30), (0, 10)], 31, (20, 30)),
        ([(20, 30), (0, 10)], 25, (20, 30)),
        ([(20, 30), (0, 10)], 14, (0, 10)),
        ([(20, 30), (0, 10)], 5, (0, 10)),
        ([(20, 30), (0, 10)], -1, None),
    ],
)
def test_find_last_plateau_that_starts_before(
    plateaus: list[tuple[int, int]], timestamp: int | None, expected: tuple[int, int] | None
):
    assert processing.find_last_plateau_that_starts_before(timestamp, plateaus) == expected


@pytest.mark.parametrize(
    ("plateaus", "timestamp", "expected"),
    [
        ([(20, 30), (0, 10)], None, None),  # timestamp is None
        ([(20, 30), (0, 10)], -1, (0, 10)),
        ([(20, 30), (0, 10)], 0, (20, 30)),
        ([(20, 30), (0, 10)], 1, (20, 30)),
        ([(20, 30), (0, 10)], 19, (20, 30)),
        ([(20, 30), (0, 10)], 20, None),
        ([(20, 30), (0, 10)], 21, None),
        ([(20, 30), (0, 10)], 30, None),
        ([(20, 30), (0, 10)], 31, None),
        ([], 15, None),
        ([(0, 10), (20, 30)], -1, (0, 10)),
        ([(0, 10), (20, 30)], 0, (20, 30)),
        ([(0, 10), (20, 30)], 1, (20, 30)),
        ([(0, 10), (20, 30)], 19, (20, 30)),
    ],
)
def test_find_first_plateau_that_starts_after(
    plateaus: list[tuple[int, int]], timestamp: int | None, expected: tuple[int, int] | None
):
    assert processing.find_first_plateau_that_starts_after(timestamp, plateaus) == expected


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([6, 6, 6, 5, 5]), 3),
        (pd.Series([6, 6, 5, 5, 1]), 2),
        (pd.Series([6, 5, 5, 1, 1]), 1),
        (pd.Series([5, 5, 1, 1, 1]), 1),
        (pd.Series([5, 1, 1, 1, 1]), None),
        (pd.Series([1, 1, 1, 1, 1]), None),
        (pd.Series([6, 5, 1, 5, 1]), 1),
        (pd.Series([5, 5, 5, 5, 5]), 1),
    ],
)
def test_detect_first_point_satisfying_predicate(signal: pd.Series, expected: int | None):
    assert (
        processing.detect_first_point_satisfying_predicate(signal, t_start=1, t_end=3, pred=lambda x: x == 5)
        == expected
    )


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([9, 9, 9, 9, 9]), None),
        (pd.Series([9, 9, 9, 9, 1]), None),
        (pd.Series([9, 9, 9, 1, 1]), 3),
        (pd.Series([9, 9, 1, 1, 1]), 2),
        (pd.Series([9, 1, 1, 1, 1]), 1),
        (pd.Series([1, 1, 1, 1, 1]), 1),
        (pd.Series([9, 9, 1, 9, 1]), 2),
        (pd.Series([9, 9, 5, 9, 9]), None),
    ],
)
def test_detect_first_point_below_threshold(signal: pd.Series, expected: int | None):
    assert processing.detect_first_point_below_threshold(signal, t_start=1, t_end=3, threshold=5) == expected


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([9, 9, 9, 9, 9]), 1),
        (pd.Series([1, 9, 9, 9, 9]), 1),
        (pd.Series([1, 1, 9, 9, 9]), 2),
        (pd.Series([1, 1, 1, 9, 9]), 3),
        (pd.Series([1, 1, 1, 1, 9]), None),
        (pd.Series([1, 1, 1, 1, 1]), None),
        (pd.Series([1, 1, 9, 1, 9]), 2),
        (pd.Series([1, 1, 5, 1, 1]), None),
    ],
)
def test_detect_first_point_above_threshold(signal: pd.Series, expected: int | None):
    assert processing.detect_first_point_above_threshold(signal, t_start=1, t_end=3, threshold=5) == expected


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([6, 6, 6, 5, 5]), 3),
        (pd.Series([6, 6, 5, 5, 1]), 3),
        (pd.Series([6, 5, 5, 1, 1]), 2),
        (pd.Series([5, 5, 1, 1, 1]), 1),
        (pd.Series([5, 1, 1, 1, 1]), None),
        (pd.Series([1, 1, 1, 1, 1]), None),
        (pd.Series([6, 5, 1, 5, 1]), 3),
        (pd.Series([5, 5, 5, 5, 5]), 3),
    ],
)
def test_detect_last_point_satisfying_predicate(signal: pd.Series, expected: int | None):
    assert (
        processing.detect_last_point_satisfying_predicate(signal, t_start=1, t_end=3, pred=lambda x: x == 5) == expected
    )


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([9, 9, 9, 9, 9]), None),
        (pd.Series([1, 9, 9, 9, 9]), None),
        (pd.Series([1, 1, 9, 9, 9]), 1),
        (pd.Series([1, 1, 1, 9, 9]), 2),
        (pd.Series([1, 1, 1, 1, 9]), 3),
        (pd.Series([1, 1, 1, 1, 1]), 3),
        (pd.Series([1, 9, 1, 9, 9]), 2),
        (pd.Series([9, 9, 5, 9, 9]), None),
    ],
)
def test_detect_last_point_below_threshold(signal: pd.Series, expected: int | None):
    assert processing.detect_last_point_below_threshold(signal, t_start=1, t_end=3, threshold=5) == expected


@pytest.mark.parametrize(
    ("signal", "expected"),
    [
        (pd.Series(dtype=float), None),
        (pd.Series([1]), None),
        (pd.Series([9, 9, 9, 9, 9]), 3),
        (pd.Series([9, 9, 9, 9, 1]), 3),
        (pd.Series([9, 9, 9, 1, 1]), 2),
        (pd.Series([9, 9, 1, 1, 1]), 1),
        (pd.Series([9, 1, 1, 1, 1]), None),
        (pd.Series([1, 1, 1, 1, 1]), None),
        (pd.Series([9, 1, 9, 1, 1]), 2),
        (pd.Series([1, 1, 5, 1, 1]), None),
    ],
)
def test_detect_last_point_above_threshold(signal: pd.Series, expected: int | None):
    assert processing.detect_last_point_above_threshold(signal, t_start=1, t_end=3, threshold=5) == expected


def test_decode_state():
    # arrange
    signal = pd.Series(
        {
            0: "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
            1: "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
            2: "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
            3: "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|PWR_FAILURE|VS_POWER_ON (11177)",
            4: "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
            5: "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
            6: "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
            7: "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
            8: "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (10633)",
            9: "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (10633)",
        },
        name="RPHGA.UJ67.RQ10.R6B1:STATE",
    )
    # act
    result = processing.decode_state(signal, "LOG_PLEASE")

    # assert
    assert result.equals(
        pd.Series(
            {0: False, 1: False, 2: False, 3: True, 4: True, 5: True, 6: True, 7: True, 8: False, 9: False},
            name="RPHGA.UJ67.RQ10.R6B1:STATE LOG_PLEASE",
        )
    )


def test_detect_when_signal_changes_normal_case(caplog: pytest.LogCaptureFixture):
    """
    Test that the EDSL can detect a successful WHERE clause
    """
    signal = pd.Series({-10_000_000: True, -3_000_000: False}, name="RQ5.L8:CMD_PWR_PERM_B1_PIC")

    # act
    result = processing.detect_when_signal_first_changes_to(
        signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger()
    )

    # assert
    assert result == -3_000_000, caplog.messages
    assert caplog.messages == []


def test_detect_when_signal_changes_no_change(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series({-10_000_000: True, -3_000_000: False}, name="RQ5.L8:CMD_PWR_PERM_B1_PIC")

    # act
    result = processing.detect_when_signal_first_changes_to(
        signal, True, 0, (-5_000_000, 5_000_000), logging.getLogger()
    )

    # assert
    assert result is None, caplog.messages
    assert caplog.messages == [
        "Change to True not detected in signal RQ5.L8:CMD_PWR_PERM_B1_PIC "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0."
    ]


def test_detect_when_signal_changes_to_signal_is_empty(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series(name="RQ5.L8:CMD_PWR_PERM_B1_PIC", dtype=bool)

    # act
    result = processing.detect_when_signal_first_changes_to(
        signal, True, 0, (-5_000_000, 5_000_000), logging.getLogger()
    )

    # assert
    assert result is None, caplog.messages
    assert caplog.messages == ["Change to True not detected in signal RQ5.L8:CMD_PWR_PERM_B1_PIC: signal empty."]


def test_detect_when_signal_changes_multiple_changes(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series(
        {-10_000_000: True, -3_000_000: False, -2_000_000: True, -1_000_000: False}, name="RQ5.L8:CMD_PWR_PERM_B1_PIC"
    )

    # act
    result = processing.detect_when_signal_first_changes_to(
        signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger()
    )

    # assert
    assert result == -3_000_000, caplog.messages
    assert caplog.messages == [
        "Multiple changes to False detected in signal RQ5.L8:CMD_PWR_PERM_B1_PIC "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0."
    ]


def test_detect_last_point_before_signal_changes_to(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([True, True, False, False], name="signal")

    # act
    result = processing.detect_last_point_before_signal_changes_to(signal, False, 0, (0, 3), logging.getLogger())

    # assert
    assert result == 1, caplog.messages
    assert caplog.messages == []


def test_detect_last_point_before_signal_changes_to_no_change(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([True, True, True, True], name="signal")

    # act
    result = processing.detect_last_point_before_signal_changes_to(signal, False, 0, (0, 3), logging.getLogger())

    # assert
    assert result is None, caplog.messages
    assert caplog.messages == [
        "Change to False not detected in signal signal within the 0 to 3 nanoseconds time window "
        "relative to the reference timestamp 0."
    ]


@pytest.fixture
def sample_signal():
    return pd.Series([True, True, False, True, False, False, True, True, True])


def test_count_changes(sample_signal):
    time_window_start = 1
    time_window_end = 7
    change_to = True

    result = processing.count_changes(sample_signal, time_window_start, time_window_end, change_to)

    assert result == 2


def test_count_points_with_value_empty_signal():
    # arrange
    signal = pd.Series([], name="signal", dtype=float)
    value = True
    time_window_start = 0
    time_window_end = 100

    # act
    result = processing.count_points_with_value(signal, value, time_window_start, time_window_end)

    # assert
    assert result == 0


def test_count_points_with_value_no_matching_points():
    # arrange
    signal = pd.Series([False, False, False, False], name="signal")
    value = True
    time_window_start = 0
    time_window_end = 100

    # act
    result = processing.count_points_with_value(signal, value, time_window_start, time_window_end)

    # assert
    assert result == 0


def test_count_points_with_value_single_matching_point():
    # arrange
    signal = pd.Series([False, False, True, False], name="signal")
    value = True
    time_window_start = 0
    time_window_end = 100

    # act
    result = processing.count_points_with_value(signal, value, time_window_start, time_window_end)

    # assert
    assert result == 1


def test_count_points_with_value_multiple_matching_points():
    # arrange
    signal = pd.Series([True, False, True, True, False, True], name="signal")
    value = True
    time_window_start = 0
    time_window_end = 100

    # act
    result = processing.count_points_with_value(signal, value, time_window_start, time_window_end)

    # assert
    assert result == 4


@pytest.mark.parametrize(
    ("signal", "plateau_level", "expected"),
    [
        (pd.Series(dtype=float), 1, []),
        (pd.Series([0, 1, 1, 0, 1, 1, 1]), 1, [(1, 2), (4, 6)]),
        (pd.Series([0]), 1, []),
        (pd.Series([0, 1, 1, 1, 0]), 2, []),
        (pd.Series([0, 1, 1, 1, 0]), 0, [(0, 0), (4, 4)]),
        (pd.Series([0, 1, 1, 1, 0]), 1, [(1, 3)]),
        (pd.Series([0, 1, 1, 2, 1, 1, 0]), 1, [(1, 2), (4, 5)]),
        (pd.Series([0, 1, 1, 0, -1, -1, 0]), -1, [(4, 5)]),
    ],
)
def test_get_regions_equal_to(signal: pd.Series, plateau_level: float, expected: tuple[int, int] | None):
    assert processing.get_regions_equal_to(signal, plateau_level) == expected


@pytest.mark.parametrize(("index", "expected"), [(0, 10), (1, 20), (2, None), (-1, 20)])
def test_read_timestamp_from_pm_headers_at_index(index: int, expected: int | None):
    # arrange
    pm_headers = pd.DataFrame({"source": {0: "B20L5", 1: "C20L5"}, "timestamp": {0: 10, 1: 20}})

    # act
    timestamp = processing.read_timestamp_from_pm_headers_at_index(pm_headers, index)

    # assert
    assert timestamp == expected


@pytest.mark.parametrize(
    ("signal", "time_frames", "expected"),
    [
        # Empty signal
        (pd.Series(dtype=float), [(0, 1)], 0.0),
        # Window is None
        (pd.Series([1]), [None], 0.0),
        # Empty signal with time frame
        (pd.Series([1]), [(1, 1)], 0.0),
        # Single point window
        (pd.Series([1]), [(0, 0)], 1.0),
        # One window
        (pd.Series([1, 0, 1, 2]), [(0, 3)], 1.0),
        # Multiple windows
        (pd.Series([-2, -1, 1, 2, 4, 3]), [(0, 1), (4, 5)], 1),
        # Multiple windows with different sizes
        (pd.Series([1, 1, 2, 2, 2]), [(0, 1), (2, 4)], 1.6),
        # Data contains NaN
        (pd.Series([9, math.nan, 1, 2]), [(0, 3)], 4.0),
    ],
)
def test_calculate_signal_average(signal: pd.Series, time_frames: Iterable[tuple[int, int] | None], expected: float):
    assert processing.calculate_signal_average(signal, time_frames) == expected


@pytest.mark.parametrize(
    ("signal", "time_frame", "value_bound", "expected"),
    [
        (pd.Series(dtype=float), (0, 1), (0, 1), None),
        (pd.Series([0, 0.5, 1, 1], index=[5, 6, 7, 8]), (0, 1), (0, 1), None),
        (pd.Series([0, 0.5, 1, 1]), (0, 3), (5, 6), None),
        (pd.Series([0, 0.5, 1, 1]), (5, 8), (0, 1), None),
        (pd.Series([0, 0.5, 1, 1]), (0, 3), (0, 1), (0, 3)),
        (pd.Series([0, 0.5, 1, 1]), (0, 1), (0, 1), (0, 1)),
        (pd.Series([-1, 0.5, 2, 3, 1, 0.5]), (0, 5), (0, 1), (1, 5)),
        (pd.Series([-1, 0.5, 2, 3, 1, 0.5]), (0, 5), (1, 0), (1, 5)),
        (pd.Series([-1, 0.5, 2, 3, 1, 0.5]), (0, 5), (2, 3), (2, 3)),
        (pd.Series([-1, 0.5, 2, 3, 1, 0.5, 0]), (0, 5), (0.5, 0.5), (1, 5)),
        (pd.Series([0, 0, 1, 2, 3, 4, 5, 5]), (1, 6), (1, 3), (2, 4)),
        (pd.Series([5, 5, 4, 3, 2, 1, 0, 0]), (1, 6), (1, 3), (3, 5)),
    ],
)
def test_get_time_window_where_signal_is_between(
    signal: pd.Series, time_frame: tuple[int, int], value_bound: tuple[float, float], expected: float
):
    assert processing.get_time_window_where_signal_is_between(signal, time_frame, value_bound) == expected


@pytest.mark.parametrize(
    ("time_frame", "max_time", "expected"),
    [
        (None, 5, None),
        ((5, 12), 5, (7, 12)),
        ((-30, -5), 9, (-14, -5)),
        ((-30, -5), 52, (-30, -5)),
        ((7, 10), 3, (7, 10)),
    ],
)
def test_shorten_time_window_aligned_right(
    time_frame: tuple[int, int] | None, max_time: int, expected: tuple[int, int]
):
    assert processing.shorten_time_window_aligned_right(time_frame, max_time) == expected


@pytest.mark.parametrize(
    ("signal", "plateau_level", "expected"),
    [
        (pd.Series(dtype=float), 1, None),
        (pd.Series([0, 1, 1, 1, 0]), 2, None),
        (pd.Series([0, 1, 1, 1, 0]), 0, (0, 0)),
        (pd.Series([0, 1, 1, 1, 0]), 1, (1, 3)),
        (pd.Series([0, 1, 1, 2, 1, 1, 0]), 1, (1, 2)),
        (pd.Series([0, 1, 1, 0, -1, -1, 0]), -1, (4, 5)),
    ],
)
def test_get_first_plateau(signal: pd.Series, plateau_level: float, expected: tuple[int, int] | None):
    assert processing.get_first_plateau(signal, plateau_level) == expected


def test_get_first_plateau_max_allowed_deviation_from_plateau_level():
    assert processing.get_first_plateau(pd.Series([0, 0.5, 1, 1.5, 2]), 1, 0.5) == (1, 3)


@pytest.mark.parametrize(
    ("signal", "window_duration", "polyorder", "deriv", "dt", "expected"),
    [
        (pd.Series(dtype=float, name="S"), 3, 2, 1, None, pd.Series(dtype=float, name="dS")),
        (
            pd.Series([1.0, 1, 1, 1, 1, 1, 1], name="S"),
            3,
            2,
            1,
            None,
            pd.Series([0.0, 0, 0], index=[2, 3, 4], name="dS"),
        ),
        (
            pd.Series([1.0, 2, 3, 4, 5, 6, 7], name="S"),
            3e9,
            2,
            1,
            1,
            pd.Series([1.0, 1, 1], index=[2, 3, 4], name="dS"),
        ),
    ],
)
def test_derivative_sav_gol(
    signal: pd.Series, window_duration: int, polyorder: int, deriv: int, dt: float | None, expected: pd.Series
):
    res = processing.derivative_sav_gol(signal, window_duration, polyorder, deriv, dt)
    if not res.empty:
        res = res.iloc[2:5]  # only use the more stable middle part
    pd.testing.assert_series_equal(res, expected, atol=1e-6)


@pytest.mark.parametrize(
    ("signal", "timestamps", "expected"),
    [
        # Empty cases
        (pd.Series(dtype=float), [], pd.Series([], dtype=float)),
        (pd.Series([1.0], [1]), [], pd.Series([], dtype=float)),
        # Basic interpolation cases
        (pd.Series([0.0, 1.0], [0, 1]), [0, 0.5, 1], pd.Series([0.0, 0.5, 1.0], [0, 0.5, 1])),
        # Exact matches
        (pd.Series([1.0, 2.0, 3.0], [100, 200, 300]), [100, 200, 300], pd.Series([1.0, 2.0, 3.0], [100, 200, 300])),
        # Out of bounds cases
        (pd.Series([1.0, 2.0], [100, 200]), [50, 150, 250], pd.Series([1.5], [150])),
        # Edge cases with uneven spacing
        (pd.Series([0.0, 1.0, 4.0], [0, 100, 400]), [50, 250], pd.Series([0.5, 2.5], [50, 250])),
        # Real-world like timestamps
        (
            pd.Series([1.0, 2.0], [1708010475000000000, 1708010476000000000]),
            [1708010475500000000],
            pd.Series([1.5], [1708010475500000000]),
        ),
        # Multiple interpolation points
        (
            pd.Series([0.0, 10.0], [0, 1000]),
            [0, 250, 500, 750, 1000],
            pd.Series([0.0, 2.5, 5.0, 7.5, 10.0], [0, 250, 500, 750, 1000]),
        ),
        # Unsorted data
        (pd.Series([1.0, 0.0, 2.0], [100, 0, 200]), [50, 150], pd.Series([0.5, 1.5], [50, 150])),
        # No interpolated timestamp between start and end of signal
        (pd.Series([1, 2, 3], index=[5, 10, 15]), [-100, 0, 100, 200], pd.Series(dtype=float)),
    ],
)
def test_interpolate_signal(signal: pd.Series, timestamps: list[int], expected: pd.Series):
    result = processing.interpolate_signal(signal, timestamps)
    pd.testing.assert_series_equal(
        result,
        expected,
        check_names=False,  # Names might include _INTERPOLATED suffix
        check_dtype=False,  # Allow for float32/float64 differences
    )


@pytest.mark.parametrize(
    ("u_lead", "i_meas", "expected"),
    [
        (pd.Series(dtype=float), pd.Series(dtype=float), pd.Series(dtype=float)),
        (pd.Series([10.0, 20, 10]), pd.Series([5.0, 5, 2]), pd.Series([2.0, 4, 5])),
        (pd.Series([10.0, 20, 10]), pd.Series([5.0, 5, 2, 2]), pd.Series([2.0, 4, 5, math.nan])),
        (pd.Series([10.0, 20, 10, 20]), pd.Series([5.0, 5, 2]), pd.Series([2.0, 4, 5, math.nan])),
        (pd.Series([10.0, 10, math.inf]), pd.Series([5.0, 5, 2]), pd.Series([2.0, 2, math.inf])),
    ],
)
def test_calculate_lead_resistance(u_lead: pd.Series, i_meas: pd.Series, expected: pd.Series) -> None:
    pd.testing.assert_series_equal(processing.calculate_resistance(u_lead, i_meas, "R"), expected.rename("R"))


@pytest.mark.parametrize(
    ("data", "threshold", "expected_output"),
    [
        (
            pd.Series([1.0, 2.5, 10.0, 3.0, 0.5, 15.0, 2.0]),
            5.0,
            pd.Series([1.0, 2.5, 3.0, 0.5, 2.0], index=[0, 1, 3, 4, 6]),
        ),
        (pd.Series([1.0, 2.5, 3.0, 0.5, 2.0]), 5.0, pd.Series([1.0, 2.5, 3.0, 0.5, 2.0])),
        (pd.Series([10.0, 15.0, 20.0]), 5.0, pd.Series(dtype=float)),
        (pd.Series(dtype=float), 5.0, pd.Series(dtype=float)),
    ],
)
def test_remove_spikes(data: pd.Series, threshold: float, expected_output: pd.Series):
    pd.testing.assert_series_equal(processing.remove_spikes(data, threshold), expected_output)
