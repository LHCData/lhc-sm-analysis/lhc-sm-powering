import pathlib
from datetime import datetime
from unittest.mock import patch

import numpy as np
import pandas as pd
import pytest
from lhcsmapi.Time import Time

from lhcsmpowering import widgets


def test_create_timestamp_table():
    # arrange
    timestamp_dct = {
        "FGC_B1 (lhc_self)": 1633365305340000000,
        "FGC_B2 (lhc_self)": 1633365305340000000,
        "FGC_B1 (lhc_ext)": None,
        "FGC_B2 (lhc_ext)": None,
        "STARTING_B1 (1)": 1633365247220000000,
        "STARTING_B2 (1)": 1633365247220000000,
        "STARTING_B1 (2)": None,
        "STARTING_B2 (2)": None,
    }
    timestamp_df = widgets.create_timestamp_table(timestamp_dct)

    # assert
    expected_df = pd.DataFrame(
        {
            "Source": {
                0: "STARTING_B1 (1)",
                1: "STARTING_B2 (1)",
                2: "FGC_B1 (lhc_self)",
                3: "FGC_B2 (lhc_self)",
                4: "FGC_B1 (lhc_ext)",
                5: "FGC_B2 (lhc_ext)",
                6: "STARTING_B1 (2)",
                7: "STARTING_B2 (2)",
            },
            "Timestamp": {
                0: "1633365247220000000",
                1: "1633365247220000000",
                2: "1633365305340000000",
                3: "1633365305340000000",
                4: np.nan,
                5: np.nan,
                6: np.nan,
                7: np.nan,
            },
            "Date and time": {
                0: Time.to_string_short(1633365247220000000),
                1: Time.to_string_short(1633365247220000000),
                2: Time.to_string_short(1633365305340000000),
                3: Time.to_string_short(1633365305340000000),
                4: np.nan,
                5: np.nan,
                6: np.nan,
                7: np.nan,
            },
            "dt w.r.t. origin [ms]": {
                0: 0.0,
                1: 0.0,
                2: 58120.0,
                3: 58120.0,
                4: np.nan,
                5: np.nan,
                6: np.nan,
                7: np.nan,
            },
            "dt w.r.t. FGC [ms]": {
                0: -58120.0,
                1: -58120.0,
                2: 0.0,
                3: 0.0,
                4: np.nan,
                5: np.nan,
                6: np.nan,
                7: np.nan,
            },
        }
    )
    pd.testing.assert_frame_equal(timestamp_df, expected_df)


def test_create_timestamp_table_none_case():
    # arrange
    timestamp_dct = {"FGC": None, "PIC": 1617341936594000000}
    timestamp_df = widgets.create_timestamp_table(timestamp_dct)

    # assert
    expected_df = pd.DataFrame(
        {
            "Source": {0: "PIC", 1: "FGC"},
            "Timestamp": {0: "1617341936594000000", 1: np.nan},
            "Date and time": {0: Time.to_string_short(1617341936594000000), 1: np.nan},
            "dt w.r.t. origin [ms]": {0: 0.0, 1: np.nan},
            "dt w.r.t. FGC [ms]": {0: np.nan, 1: np.nan},
            "dt w.r.t. PIC [ms]": {0: 0.0, 1: np.nan},
        }
    )
    pd.testing.assert_frame_equal(timestamp_df, expected_df)


@pytest.mark.parametrize(
    ("circuit_name", "hwc_test", "expected"),
    [
        (  # 60A
            "RCBH11.L1B2",
            "PCC.1",
            pathlib.Path(
                "/my/path/60A/RCBH11/RCBH11.L1B2/PCC.1/RCBH11.L1B2_PCC.1-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # 80-120A
            "RCBCH10.L1B1",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/80-120A/RCBCH10/RCBCH10.L1B1/PIC2_POWERING_FAILURE/RCBCH10.L1B1_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # 600A
            "RCBXH1.R2",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/600A/RCBX/RCBXH1.R2/PIC2_POWERING_FAILURE/RCBXH1.R2_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # IPD
            "RD1.L2",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/IPD/RD1.L2/PIC2_POWERING_FAILURE/RD1.L2_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # IPQ
            "RQ5.L1",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/IPQ/RQ5/RQ5.L1/PIC2_POWERING_FAILURE/RQ5.L1_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # IT
            "RQX.L1",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/IT/RQX.L1/PIC2_POWERING_FAILURE/RQX.L1_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # RQ
            "RQD.A12",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/RQ/RQ.A12/PIC2_POWERING_FAILURE/RQD.A12_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
        (  # RB
            "RB.A12",
            "PIC2 POWERING FAILURE",
            pathlib.Path(
                "/my/path/RB/RB.A12/PIC2_POWERING_FAILURE/RB.A12_PIC2_POWERING_FAILURE-2023-03-18-09h56-2025-02-28-16h27_PASSED.html"
            ),
        ),
    ],
)
def test_get_report_destination_path(circuit_name: str, hwc_test: str, expected: pathlib.PosixPath):
    # arrange
    base_path = pathlib.Path("/my/path")
    t_start = Time.to_unix_timestamp("2023-03-18 09:56:41.276000000")
    expert_decision = "PASSED"

    # act
    with patch("lhcsmpowering.widgets.datetime") as mock_datetime:
        mock_datetime.now.return_value = datetime(2025, 2, 28, 16, 27, 41, 276000)

        report_destination = widgets.get_report_destination(base_path, circuit_name, hwc_test, t_start, expert_decision)

    # assert
    assert report_destination == expected
