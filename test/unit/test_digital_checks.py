import logging

import pandas as pd
import pytest

from lhcsmpowering.checks import digital_checks


@pytest.mark.parametrize(
    "signal",
    [
        pd.Series({-10_000_000: True, -3_000_000: False}),
        pd.Series({-10_000_000: True, -3_000_000: False, -2_000_000: True}),
        pd.Series({-10_000_000: True, -3_000_000: False, -2_000_000: True, -1_000_000: False}),
    ],
)
def test_check_signal_changes_to_success_case(signal: pd.Series, caplog: pytest.LogCaptureFixture):
    # arrange
    signal.name = "RQ5.L8:CMD_PWR_PERM_B1_PIC"

    # act
    result = digital_checks.check_signal_changes_to(signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger())

    # assert
    assert result is True, caplog.messages
    assert caplog.messages == [
        "Check passed: RQ5.L8:CMD_PWR_PERM_B1_PIC changed to False "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0."
    ]


@pytest.mark.parametrize(
    "signal",
    [
        pd.Series(dtype=bool),
        pd.Series({-3_000_000: False}),
        pd.Series({-10_000_000: True}),
        pd.Series({-10_000_000: True, 13_000_000: False}),
    ],
)
def test_check_signal_changes_to_failed_case(signal: pd.Series, caplog: pytest.LogCaptureFixture):
    # arrange
    signal.name = "RQ5.L8:CMD_PWR_PERM_B1_PIC"

    # act
    result = digital_checks.check_signal_changes_to(signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger())

    # assert
    assert result is False, caplog.messages
    assert caplog.messages == [
        "Check failed: RQ5.L8:CMD_PWR_PERM_B1_PIC has not changed to False "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0."
    ]


@pytest.mark.parametrize(
    "signal",
    [
        pd.Series({-10_000_000: True, -3_000_000: False}),
        pd.Series({-10_000_000: True, -3_000_000: False, -2_000_000: True}),
        pd.Series({-10_000_000: True, -3_000_000: False, -2_000_000: True, 6_000_000: False}),
    ],
)
def test_check_signal_changes_once_to_success_case(signal: pd.Series, caplog: pytest.LogCaptureFixture):
    # arrange
    signal.name = "RQ5.L8:CMD_PWR_PERM_B1_PIC"

    # act
    result = digital_checks.check_signal_changes_once_to(signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger())

    # assert
    assert result is True, caplog.messages
    assert caplog.messages == [
        "Check passed: RQ5.L8:CMD_PWR_PERM_B1_PIC changed once to False "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0."
    ]


@pytest.mark.parametrize(
    ("signal", "changes"),
    [
        (pd.Series(dtype=bool), 0),
        (pd.Series({-3_000_000: False}), 0),
        (pd.Series({-10_000_000: True}), 0),
        (pd.Series({-10_000_000: True, 10_000_000: False}), 0),
        (pd.Series({-10_000_000: True, -3_000_000: False, -2_000_000: True, -1_000_000: False}), 2),
    ],
)
def test_check_signal_changes_once_to_failed_case(signal: pd.Series, changes: int, caplog: pytest.LogCaptureFixture):
    # arrange
    signal.name = "RQ5.L8:CMD_PWR_PERM_B1_PIC"

    # act
    result = digital_checks.check_signal_changes_once_to(signal, False, 0, (-5_000_000, 5_000_000), logging.getLogger())

    # assert
    assert result is False, caplog.messages
    assert caplog.messages == [
        "Check failed: RQ5.L8:CMD_PWR_PERM_B1_PIC was expected to change once to False "
        "within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 0, "
        f"but it was found {changes} times."
    ]


SIGNAL_REMAINS_TO_COMMONLY_PASSED_CASES = [
    pd.Series({0: True, 4_000_000: True}),
    pd.Series({0: True, 4_000_000: False}),
    pd.Series({0: True, 2_000_000: True, 4_000_000: False}),
    pd.Series({0: True, 1_000_000: True, 2_000_000: True, 3_000_000: True, 4_000_000: False}),
    pd.Series({1_000_000: True, 2_000_000: True, 3_000_000: True, 4_000_000: False}),
]


@pytest.mark.parametrize("signal", SIGNAL_REMAINS_TO_COMMONLY_PASSED_CASES)
def test_check_signal_remains_equal_to_normal_case(signal: pd.Series, caplog: pytest.LogCaptureFixture):
    assert (
        digital_checks.check_signal_remains_equal_to(signal, True, 0, (1_000_000, 3_000_000), logging.getLogger())
        is True
    ), caplog.messages


@pytest.mark.parametrize("signal", SIGNAL_REMAINS_TO_COMMONLY_PASSED_CASES)
def test_check_signal_between_two_timestamps_remains_equal_to_normal_case(
    signal: pd.Series, caplog: pytest.LogCaptureFixture
):
    # act
    result = digital_checks.check_signal_between_two_timestamps_remains_equal_to(
        signal, True, 1_000_000, 3_000_000, logging.getLogger()
    )
    assert result is True, caplog.messages
    assert caplog.messages == ["Check passed: None remained equal to True between 1000000 and 3000000, as expected."]


@pytest.mark.parametrize("signal", SIGNAL_REMAINS_TO_COMMONLY_PASSED_CASES)
def test_check_signal_between_two_timestamps_remains_equal_to_normal_case_with_offset(
    signal: pd.Series, caplog: pytest.LogCaptureFixture
):
    # act
    result = digital_checks.check_signal_between_two_timestamps_remains_equal_to(
        signal, True, 1_000_000, 3_000_000, logging.getLogger(), start_offset=1_000, end_offset=-1_000
    )
    assert result is True, caplog.messages
    assert caplog.messages == [
        "Check passed: None remained equal to True "
        "between 1000000 offsetted by 1,000 ns and 3000000 offsetted by -1,000 ns, as expected."
    ]


@pytest.mark.parametrize(
    ("signal", "logs"),
    [
        (
            pd.Series(dtype=bool),
            ["Check failed: MY_SIGNAL was expected to remains equals to True but the signal is empty."],
        ),
        (
            pd.Series({2_000_000: True, 4_000_000: True}),
            [
                "Check failed: The last recorded value of MY_SIGNAL before the start of the time window "
                "was expected to be True, but no data was found before 1000000."
            ],
        ),
        (
            pd.Series({0: False, 2_000_000: True, 4_000_000: True}),
            [
                "Check failed: the last recorded value of MY_SIGNAL before the start of the time window "
                "was expected to be True, but it was recorded as False before 1000000."
            ],
        ),
        (
            pd.Series({0: False, 1_000_000: True, 2_000_000: False, 3_000_000: True, 4_000_000: False}),
            [
                "Check failed: MY_SIGNAL was expected to only have True values between 1000000 and 3000000, "
                "but False values were found 1 times."
            ],
        ),
    ],
)
def test_check_signal_between_two_timestamps_remains_equal_to_empty_signal(
    signal: pd.Series, logs: list[str], caplog: pytest.LogCaptureFixture
):
    # arrange
    signal.name = "MY_SIGNAL"

    # act
    result = digital_checks.check_signal_between_two_timestamps_remains_equal_to(
        signal, True, 1_000_000, 3_000_000, logging.getLogger()
    )
    assert result is False, caplog.messages
    assert caplog.messages == logs


def test_check_signal_between_two_timestamps_remains_equal_to_none_timestamp(caplog: pytest.LogCaptureFixture):
    # arrange
    signal = pd.Series([True])

    # act
    result = digital_checks.check_signal_between_two_timestamps_remains_equal_to(
        signal, True, None, None, logging.getLogger()
    )

    # assert
    assert result is False, caplog.messages
    assert caplog.messages == ["Check failed: Time window start is None.", "Check failed: Time window end is None."]
