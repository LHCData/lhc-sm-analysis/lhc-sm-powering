from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import analyses

from . import common


def test_pcc_1_on_80_120a():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PCC.1",
        "RCBYVS5.L2B2",
        "Recommissioning post LS2",
        "2021-03-23 20:25:40.075000000",
        "2021-03-23 20:38:04.455000000",
    )

    # act
    analysis_class = analyses.Pcc1On80to120AAnalysis("", test_parameters)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 3 PM timestamps for FGC RPLB.UA23.RCBYVS5.L2B2, as expected.",
        "PC current profile consistency check",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 12.30 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_MEAS signal is 0.00050 A, which is within the threshold of 0.01440 A. Expected plateau level: -10.00000 A.",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 12.30 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_MEAS signal is 0.00053 A, which is within the threshold of 0.01440 A. Expected plateau level: 10.00000 A.",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 11.82 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_MEAS signal is 0.00065 A, which is within the threshold of 0.01440 A. Expected plateau level: -10.00000 A.",
        "PC voltage profile consistency check",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 12.30 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLB.UA23.RCBYVS5.L2B2 STATUS.V_MEAS maximum deviation is 0.01842 V compared to reference, which is within the threshold of ±0.05000 V. Comparison period: 12.05 s.",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 12.30 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLB.UA23.RCBYVS5.L2B2 STATUS.V_MEAS maximum deviation is 0.02149 V compared to reference, which is within the threshold of ±0.05000 V. Comparison period: 11.57 s.",
        "\tCheck passed: The plateau duration for RPLB.UA23.RCBYVS5.L2B2 STATUS.I_REF signal is 11.82 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLB.UA23.RCBYVS5.L2B2 STATUS.V_MEAS maximum deviation is 0.01689 V compared to reference, which is within the threshold of ±0.05000 V. Comparison period: 11.57 s.",
        "PC voltage decay (positive cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLB.UA23.RCBYVS5.L2B2 STATUS.V_MEAS maximum deviation is 0.0102 V compared to reference, which is within the threshold of ±0.5000 V. Comparison period: 17.22 s.",
        "PC voltage decay (negative cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLB.UA23.RCBYVS5.L2B2 STATUS.V_MEAS maximum deviation is 0.0102 V compared to reference, which is within the threshold of ±0.5000 V. Comparison period: 18.32 s.",
        "Earth current check",
        "\tCheck passed: The values of RPLB.UA23.RCBYVS5.L2B2 I_EARTH_PCNT are within the margin of 0.0000 to 15.00 %.",
        "\tCheck passed: The values of RPLB.UA23.RCBYVS5.L2B2 I_EARTH_PCNT are within the margin of 0.0000 to 15.00 %.",
        "\tCheck passed: The values of RPLB.UA23.RCBYVS5.L2B2 I_EARTH_PCNT are within the margin of 0.0000 to 15.00 %.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
