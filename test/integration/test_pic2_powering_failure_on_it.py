from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_powering_failure_it():
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PIC2 POWERING FAILURE",
        "RQX.L1",
        "Recommissioning 2022/2023",
        "2023-03-17 13:07:21.901000000",
        "2023-03-17 13:22:55.701000000",
    )

    # act
    analysis_class = analyses.Pic2PoweringFailureForITAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNC&source=RQX.L1&fromTimestampInNanos=1679054841901000000&durationInNanos=933800000000",
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 3 PM timestamps for FGC RPHFC.UL14.RQX.L1, as expected.",
        "\tCheck passed: Found exactly 3 PM timestamps for FGC RPMBC.UL14.RTQX1.L1, as expected.",
        "\tCheck passed: Found exactly 3 PM timestamps for FGC RPHGC.UL14.RTQX2.L1, as expected.",
        "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
        "FGC MAIN status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "FGC TRIM1 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "FGC TRIM2 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "PIC status check",
        "\tCheck passed: RQX.L1:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: RQX.L1:ST_FAILURE_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1679054953702000000.",
        "\tCheck passed: RQX.L1:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: RQX.L1:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: RQX.L1:ST_DISCHARGE_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
        "\tCheck passed: RQX.L1:CMD_DISCHARGE_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679054953720000000.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
