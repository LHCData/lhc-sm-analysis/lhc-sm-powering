import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "captured_output"),
    [
        (
            "PNO.d3",
            "RQT12.L6B1",
            "Recommissioning 2023/2024",
            "2024-02-13 19:11:23.407000",
            "2024-02-13 19:50:00.866000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 2 PM timestamps for FGC RPMBA.UA63.RQT12.L6B1, as expected.",
                "PC current profile consistency check (positive cycle)",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 STATUS.I_REF signal is 2.620 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 STATUS.I_MEAS signal is 0.0018 A, which is within the threshold of 0.1100 A. Expected plateau level: 560.0000 A.",
                "PC voltage profile consistency check (positive cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 STATUS.I_REF (ref) signal is 2.680 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.V_MEAS maximum deviation is 0.0154 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 2.62 s.",
                "PC current decay check (positive cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.I_MEAS maximum deviation is 0.68 A compared to reference, which is within the threshold of ±55.00 A. Comparison period: 0.28 s.",
                "PC voltage decay check (positive cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.V_MEAS maximum deviation is 0.002 V compared to reference, which is within the threshold of ±1.000 V. Comparison period: 59.94 s.",
                "Earth current check (positive cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 STATUS.I_EARTH_PCNT are within the margin of -10.00 to 10.00 %.",
                "PC current profile consistency check (negative cycle)",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 STATUS.I_REF signal is 2.600 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 STATUS.I_MEAS signal is 0.0018 A, which is within the threshold of 0.1100 A. Expected plateau level: -560.0000 A.",
                "PC voltage profile consistency check (negative cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 STATUS.I_REF (ref) signal is 2.680 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.V_MEAS maximum deviation is 0.0154 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 2.60 s.",
                "PC current decay check (negative cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.I_MEAS maximum deviation is 0.39 A compared to reference, which is within the threshold of ±55.00 A. Comparison period: 0.28 s.",
                "PC voltage decay check (negative cycle)",
                "\tCheck passed: Target current is 560.0 A, which is equal to the reference target current of 560.0 A.",
                "\tCheck passed: The RPMBA.UA63.RQT12.L6B1 STATUS.V_MEAS maximum deviation is 0.002 V compared to reference, which is within the threshold of ±1.000 V. Comparison period: 59.94 s.",
                "Earth current check (negative cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 STATUS.I_EARTH_PCNT are within the margin of -10.00 to 10.00 %.",
            ],
        ),
        (
            "PNO.d3",
            "RU.R4",
            "Recommissioning 2023/2024",
            "2024-02-23 20:11:22.497",
            "2024-02-23 20:44:33.002",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA47.RU.R4, as expected.",
                "PC current profile consistency check (positive cycle)",
                "\tCheck passed: The plateau duration for RPMBB.UA47.RU.R4 STATUS.I_REF signal is 2.660 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBB.UA47.RU.R4 STATUS.I_MEAS signal is 0.00146 A, which is within the threshold of 0.08000 A. Expected plateau level: 410.00000 A.",
                "PC voltage profile consistency check (positive cycle)",
                "\tCheck passed: Target current is 410.0 A, which is equal to the reference target current of 410.0 A.",
                "\tCheck passed: The plateau duration for RPMBB.UA47.RU.R4 STATUS.I_REF (ref) signal is 2.680 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The RPMBB.UA47.RU.R4 STATUS.V_MEAS maximum deviation is 0.0154 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 2.66 s.",
                "PC current decay check (positive cycle)",
                "\tCheck passed: Target current is 410.0 A, which is equal to the reference target current of 410.0 A.",
                "\tCheck passed: The RPMBB.UA47.RU.R4 STATUS.I_MEAS maximum deviation is 26.28 A compared to reference, which is within the threshold of ±40.00 A. Comparison period: 1.62 s.",
                "PC voltage decay check (positive cycle)",
                "\tCheck passed: Target current is 410.0 A, which is equal to the reference target current of 410.0 A.",
                "\tCheck passed: The RPMBB.UA47.RU.R4 STATUS.V_MEAS maximum deviation is 0.031 V compared to reference, which is within the threshold of ±1.000 V. Comparison period: 0.98 s.",
                "Earth current check (positive cycle)",
                "\tCheck passed: The values of RPMBB.UA47.RU.R4 STATUS.I_EARTH_PCNT are within the margin of -10.00 to 10.00 %.",
            ],
        ),
    ],
)
def test_pno_d3(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, captured_output: list[str]):
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.PnoD3For600AAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == captured_output
