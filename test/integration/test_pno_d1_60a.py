from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import analyses

from . import common


def test_pno_d1_on_60a():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PNO.d1",
        "RCBH11.L1B2",
        "Recommissioning 2022/2023",
        "2023-03-08 10:57:38.151000000",
        "2023-03-08 11:14:21.103000000",
    )

    # act
    analysis_class = analyses.PnoD1On60AAnalysis("", test_parameters)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 2 PM timestamps for FGC RPLA.12L1.RCBH11.L1B2, as expected.",
        "PC current profile consistency check (positive cycle)",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 2.040 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS signal is 0.00139 A, which is within the threshold of 0.01100 A. Expected plateau level: 60.00000 A.",
        "PC voltage profile consistency check (positive cycle)",
        "\tCheck passed: Target current is 60.00 A, which is equal to the reference target current of 60.00 A.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 2.540 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.02041 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 1.54 s.",
        "PC current decay check (positive cycle)",
        "\tCheck passed: Target current is 60.00 A, which is equal to the reference target current of 60.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS maximum deviation is 0.0447 A compared to reference, which is within the threshold of ±0.5500 A. Comparison period: 285.46 s.",
        "PC voltage decay check (positive cycle)",
        "\tCheck passed: Target current is 60.00 A, which is equal to the reference target current of 60.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.0044 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 285.42 s.",
        "Earth current check (positive cycle)",
        "\tCheck passed: The values of RPLA.12L1.RCBH11.L1B2 STATUS.I_EARTH_PCNT are within the margin of 0.0000 to 10.00 %.",
        "PC current profile consistency check (negative cycle)",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 2.040 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS signal is 0.00140 A, which is within the threshold of 0.01100 A. Expected plateau level: -60.00000 A.",
        "PC voltage profile consistency check (negative cycle)",
        "\tCheck passed: Target current is -60.00 A, which is equal to the reference target current of -60.00 A.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 2.040 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.01315 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 1.54 s.",
        "PC current decay check (negative cycle)",
        "\tCheck passed: Target current is -60.00 A, which is equal to the reference target current of -60.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS maximum deviation is 0.0485 A compared to reference, which is within the threshold of ±0.5500 A. Comparison period: 286.96 s.",
        "PC voltage decay check (negative cycle)",
        "\tCheck passed: Target current is -60.00 A, which is equal to the reference target current of -60.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.0024 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 286.92 s.",
        "Earth current check (negative cycle)",
        "\tCheck passed: The values of RPLA.12L1.RCBH11.L1B2 STATUS.I_EARTH_PCNT are within the margin of 0.0000 to 10.00 %.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
