import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (
            "PLI2.e3",
            "RQ10.R6",
            "Recommissioning 2023/2024",
            "2024-02-28 09:47:33.125000000",
            "2024-02-28 10:12:50.512000000",
            [
                "Post Mortem returned no FGC data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=FGC&className=lhc_self_pmd&source=RPHGA.UJ67.RQ10.R6B1&fromTimestampInNanos=1709110053125000000&durationInNanos=1494755000000",
                "Post Mortem returned no FGC data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=FGC&className=lhc_self_pmd&source=RPHGA.UJ67.RQ10.R6B2&fromTimestampInNanos=1709110053125000000&durationInNanos=1498755000000",
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQA&source=RQ10.R6&fromTimestampInNanos=1709110053125000000&durationInNanos=1517387000000",
                "PM buffers absence and timestamps check",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B1:STATE SLOW_ABORT changed once to True within the 1,709,110,053,125,000,000 to 1,709,111,570,512,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B2:STATE SLOW_ABORT changed once to True within the 1,709,110,053,125,000,000 to 1,709,111,570,512,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B1:STATE FLT_OFF changed once to True within the 1,709,110,053,125,000,000 to 1,709,111,570,512,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B2:STATE FLT_OFF changed once to True within the 1,709,110,053,125,000,000 to 1,709,111,570,512,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "\tCheck passed: No FGC PM dumps were found for RPHGA.UJ67.RQ10.R6B1 in the analyzed interval, as expected.",
                "\tCheck passed: No FGC PM dumps were found for RPHGA.UJ67.RQ10.R6B2 in the analyzed interval, as expected.",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B1 SLOW ABORT timestamp precedes RPHGA.UJ67.RQ10.R6B1 FLT OFF timestamp by 218000.0 ms, as expected.",
                "\tCheck passed: RPHGA.UJ67.RQ10.R6B2 SLOW ABORT timestamp precedes RPHGA.UJ67.RQ10.R6B2 FLT OFF timestamp by 222000.0 ms, as expected.",
                "Current profile consistency check",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_REF signal is 62.00 s, which is greater than the minimum required duration of 58.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_MEAS signal is 0.005 A, which is within the threshold of 1.078 A. Expected plateau level: 100.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_REF signal is 302.0 s, which is greater than the minimum required duration of 298.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_MEAS signal is 0.007 A, which is within the threshold of 1.078 A. Expected plateau level: 249.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_REF signal is 684.0 s, which is greater than the minimum required duration of 598.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B1 RPHGA.UJ67.RQ10.R6B1:I_MEAS signal is 0.008 A, which is within the threshold of 1.078 A. Expected plateau level: 2900.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_REF signal is 66.00 s, which is greater than the minimum required duration of 58.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_MEAS signal is 0.005 A, which is within the threshold of 1.078 A. Expected plateau level: 100.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_REF signal is 302.0 s, which is greater than the minimum required duration of 298.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_MEAS signal is 0.009 A, which is within the threshold of 1.078 A. Expected plateau level: 249.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_REF signal is 302.0 s, which is greater than the minimum required duration of 298.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_MEAS signal is 0.007 A, which is within the threshold of 1.078 A. Expected plateau level: 2900.000 A.",
                "\tCheck passed: The plateau duration for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_REF signal is 298.0 s, which is greater than the minimum required duration of 298.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHGA.UJ67.RQ10.R6B2 RPHGA.UJ67.RQ10.R6B2:I_MEAS signal is 0.006 A, which is within the threshold of 1.078 A. Expected plateau level: 1600.000 A.",
                "DFB regulation check",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC01_05R6_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC02_05R6_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC03_05R6_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC01_05R6_TT893.TEMPERATURECALC are within the margin of 280.0 to 320.0 K.",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC02_05R6_TT893.TEMPERATURECALC are within the margin of 280.0 to 320.0 K.",
                "\tCheck passed: The values of DFLCS.5R6.RQ10.R6 DALC03_05R6_TT893.TEMPERATURECALC are within the margin of 280.0 to 320.0 K.",
            ],
        )
    ],
)
def test_pli2_e3(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]):
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )
    spark = get_or_create()

    # act
    analysis_class = analyses.Pli2E3Analysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
