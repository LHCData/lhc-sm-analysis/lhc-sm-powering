import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (  # 600A without ee
            "RCO.A81B2",
            "Recommissioning 2023/2024",
            "2024-02-19 19:43:11.621000000",
            "2024-02-19 19:46:44.304000000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RCO.A81B2&fromTimestampInNanos=1708368191621000000&durationInNanos=212683000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPLB.UA87.RCO.A81B2, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1708368269220000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1708368269220000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE remained equal to False within the -20,000,000 to 4,000,000 nanoseconds time window relative to the reference timestamp 1708368269220000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1708368269220000000.",
                "PIC status check",
                "\tCheck passed: RCO.A81B2:ST_FAILURE_PIC remained equal to True within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1708368269212000000.",
                "\tCheck passed: RCO.A81B2:ST_ABORT_PIC changed to False within the 0 to 2,000,000 nanoseconds time window relative to the reference timestamp 1708368269212000000.",
                "\tCheck passed: RCO.A81B2:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1708368269212000000.",
            ],
        ),
        (  # 600A with ee
            "RU.L4",
            "Recommissioning 2022/2023",
            "2023-03-10 14:18:40.591000000",
            "2023-03-10 14:27:55.704000000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RU.L4&fromTimestampInNanos=1678454320591000000&durationInNanos=555113000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA43.RU.L4, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE remained equal to False between 1678454395150000000 offsetted by -20,000,000 ns and 1678454395160000000 offsetted by 4,000,000 ns, as expected.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "PIC status check",
                "\tCheck passed: RU.L4:ST_FAILURE_PIC remained equal to True within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1678454395151000000.",
                "\tCheck passed: RU.L4:ST_ABORT_PIC changed to False within the 0 to 2,000,000 nanoseconds time window relative to the reference timestamp 1678454395151000000.",
                "\tCheck passed: RU.L4:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1678454395151000000.",
                "EE status check",
                "\tCheck passed: ST_FPA_REC_0 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: ST_FPA_REC_1 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: ST_SW_A_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: ST_SW_B_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "\tCheck passed: ST_SW_Z_OPEN remained equal to False within the -500,000,000 to 2,000,000,000 nanoseconds time window relative to the reference timestamp 1678454395150000000.",
                "EE voltage check",
                "\tCheck passed: Signal U_CAP_A was equal to 60.28 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2023-03-10 14:19:55.150 (1678454395150000000).",
                "\tCheck passed: Signal U_CAP_B was equal to 60.04 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2023-03-10 14:19:55.150 (1678454395150000000).",
                "\tCheck passed: Signal U_CAP_Z was equal to 166.8 V 0.03 seconds after reference timestamp, which is within the target of 165.0 ± 35.0 V. Reference timestamp: 2023-03-10 14:19:55.150 (1678454395150000000).",
            ],
        ),
    ],
)
def test_pic2_fast_abort_req_via_pic_600a(
    circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]
):
    # arrange
    spark = get_or_create()
    hwc_summary = signal_metadata.get_hwc_summary()
    test_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary, "PIC2 FAST ABORT REQ VIA PIC", circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pic2FastAbortReqViaPicFor600AAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
