from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_circuit_quench_via_qps_ipq():
    # arrange
    spark = get_or_create()
    hwc_summary = signal_metadata.get_hwc_summary()
    test_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary,
        "PIC2 CIRCUIT QUENCH VIA QPS",
        "RQ4.L1",
        "Recommissioning 2022/2023",
        "2023-03-18 08:41:55.259000000",
        "2023-03-18 09:15:12.238000000",
    )

    # act
    analysis_class = analyses.Pic2CircuitQuenchViaQpsForIPQAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHRA.RR13.RQ4.L1B1, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHRA.RR13.RQ4.L1B2, as expected.",
        "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
        "\tCheck passed: RQ4.L1:ST_ABORT_PIC changed to False within the 1,679,125,315,259,000,000 to 1,679,127,312,238,000,000 nanoseconds time window relative to the reference timestamp 0.",
        "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
        "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
        "FGC B1 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -30,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "FGC B2 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -30,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "PIC status check",
        "\tCheck passed: RQ4.L1:ST_ABORT_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: RQ4.L1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: RQ4.L1:ST_ABORT_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: RQ4.L1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "QPS status check",
        "\tCheck passed: stat.RQ4.L1:ST_CIRCUIT_OK_QPS changed to False within the -1,000,000 to 600,000,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: stat.RQ4.L1:ST_CIRCUIT_OK_QPS changed to False within the -1,000,000 to 600,000,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: stat.RQ4.L1:ST_CIRCUIT_OK_QPS changed to False within the -1,000,000 to 600,000,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "\tCheck passed: stat.RQ4.L1:ST_CIRCUIT_OK_QPS changed to False within the -1,000,000 to 600,000,000 nanoseconds time window relative to the reference timestamp 1679125485824000000.",
        "QDS linear ramp on LD1 check",
        "\tCheck passed: Signal stat.DFLCS.RR13.RQ4.L1.LD1:U_HTS was equal to 0.003038 V 0.0 seconds after reference timestamp, which is within the target of 0.003100 ± 0.000150 V. Reference timestamp: 2023-03-18 08:44:45.818 (1679125485818000000).",
        "\tCheck passed: stat.DFLCS.RR13.RQ4.L1.LD1:ST_LEAD_OK changed once to False within the -500,000,000 to 500,000,000 nanoseconds time window relative to the reference timestamp 1679125485818000000.",
        "QH discharge check",
        "\tCheck passed: QH discharge parameters are within expected ranges.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
