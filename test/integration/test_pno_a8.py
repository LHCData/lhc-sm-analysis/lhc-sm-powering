import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (
            "PNO.a8",
            "RD1.R2",
            "Recommissioning 2022/2023",
            "2023-03-21 13:17:09.869000000",
            "2023-03-21 13:35:03.698000000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQA&source=RD1.R2&fromTimestampInNanos=1679401029869000000&durationInNanos=1073829000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHF.UA27.RD1.R2, as expected.",
                "\tCheck passed: RD1.R2:CMD_PWR_PERM_PIC changed once to False within the 1,679,401,063,580,000,000 to 1,679,402,103,698,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "PC current profile consistency check",
                "\tCheck passed: The plateau duration for RPHF.UA27.RD1.R2 RPHF.UA27.RD1.R2:I_REF signal is 62.00 s, which is greater than the minimum required duration of 58.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA27.RD1.R2 RPHF.UA27.RD1.R2:I_MEAS signal is 0.0075 A, which is within the threshold of 0.5500 A. Expected plateau level: 200.0000 A.",
                "\tCheck passed: The plateau duration for RPHF.UA27.RD1.R2 RPHF.UA27.RD1.R2:I_REF signal is 62.00 s, which is greater than the minimum required duration of 58.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA27.RD1.R2 RPHF.UA27.RD1.R2:I_MEAS signal is 0.0107 A, which is within the threshold of 0.5500 A. Expected plateau level: 5550.0000 A.",
                "\tCheck passed: The plateau duration for RPHF.UA27.RD1.R2 STATUS.I_REF signal is 60.90 s, which is greater than the minimum required duration of 60.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA27.RD1.R2 STATUS.I_MEAS signal is 0.0095 A, which is within the threshold of 0.5500 A. Expected plateau level: 200.0000 A.",
            ],
        )
    ],
)
def test_pno_a8_analysis(
    hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]
) -> None:
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )
    spark = get_or_create()

    # act
    analysis_class = analyses.PnoA8Analysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
