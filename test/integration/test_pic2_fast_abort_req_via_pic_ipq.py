from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_fast_abort_req_via_pic_ipq():
    # arrange
    spark = get_or_create()
    hwc_summary = signal_metadata.get_hwc_summary()
    test_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary,
        "PIC2 FAST ABORT REQ VIA PIC",
        "RQ4.R1",
        "Recommissioning 2022/2023",
        "2023-03-18 10:41:54.722000000",
        "2023-03-18 10:49:04.437000000",
    )

    # act
    analysis_class = analyses.Pic2FastAbortReqViaPicForIPQAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQB&source=RQ4.R1&fromTimestampInNanos=1679132514722000000&durationInNanos=429715000000",
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHRA.RR17.RQ4.R1B1, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHRA.RR17.RQ4.R1B2, as expected.",
        "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
        "FGC B1 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -30,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "FGC B2 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -30,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679132549600000000.",
        "PIC status check",
        "\tCheck passed: RQ4.R1:ST_ABORT_PIC changed once to False within the 0 to 2,000,000 nanoseconds time window relative to the reference timestamp 1679132549585000000.",
        "\tCheck passed: RQ4.R1:ST_FAILURE_PIC remained equal to True between 1679132549585000000 offsetted by -20,000,000 ns and 1679132549587000000 offsetted by 4,000,000 ns, as expected.",
        "\tCheck passed: RQ4.R1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1679132549585000000.",
        "\tCheck passed: RQ4.R1:ST_ABORT_PIC changed once to False within the 0 to 2,000,000 nanoseconds time window relative to the reference timestamp 1679132549585000000.",
        "\tCheck passed: RQ4.R1:ST_FAILURE_PIC remained equal to True between 1679132549585000000 offsetted by -20,000,000 ns and 1679132549587000000 offsetted by 4,000,000 ns, as expected.",
        "\tCheck passed: RQ4.R1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1679132549585000000.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
