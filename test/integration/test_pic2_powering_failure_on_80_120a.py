import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        # C
        (
            "PIC2 POWERING FAILURE",
            "RCBCH10.L4B2",
            "Recommissioning 2022/2023",
            "2023-03-06 17:07:33.141000000",
            "2023-03-06 17:09:25.721000000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPLB.UJ43.RCBCH10.L4B2, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678118875200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678118875200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -25,000,000 to 25,000,000 nanoseconds time window relative to the reference timestamp 1678118875200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -25,000,000 to 45,000,000 nanoseconds time window relative to the reference timestamp 1678118875200000000.",
                "PIC status check",
                "\tCheck passed: RCBCH10.L4B2:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1678118875200000000.",
                "\tCheck passed: RCBCH10.L4B2:ST_FAILURE_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1678118875181000000.",
            ],
        ),
        # B1
        (
            "PIC2 POWERING FAILURE",
            "RCBCH10.R3B2",
            "Recommissioning 2023/2024",
            "2023-03-06 17:11:20.366000000",
            "2023-03-06 17:13:15.757000000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMC.UJ33.RCBCH10.R3B2, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -25,000,000 to 25,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -25,000,000 to 45,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "PIC status check",
                "\tCheck passed: RCBCH10.R3B2:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "\tCheck passed: RCBCH10.R3B2:ST_FAILURE_PIC changed once to False within the -2,000,000 to -1,000,000 nanoseconds time window relative to the reference timestamp 1678119105181000000.",
                "\tCheck passed: RCBCH10.R3B2:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
                "\tCheck passed: RCBCH10.R3B2:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678119105200000000.",
            ],
        ),
    ],
)
def test_pic2_powering_failure_on_80_120a(
    hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]
):
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pic2PoweringFailureFor80To120AAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
