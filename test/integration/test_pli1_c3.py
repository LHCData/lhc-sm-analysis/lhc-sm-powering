from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pli1_c3():
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PLI1.c3",
        "RQ6.R1",
        "Recommissioning post LS2",
        "2021-03-30 19:48:38.602000000",
        "2021-03-30 20:05:55.027000000",
    )

    # act
    analysis_class = analyses.Pli1C3Analysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQA&source=RQ6.R1&fromTimestampInNanos=1617126518602000000&durationInNanos=1036425000000",
        "PM buffers presence and timestamps check",
        "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHSB.RR17.RQ6.R1B1, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHSB.RR17.RQ6.R1B2, as expected.",
        "\tCheck passed: RQ6.R1:ST_ABORT_PIC changed once to False within the 1,617,126,518,602,000,000 to 1,617,127,555,027,000,000 nanoseconds time window relative to the reference timestamp 0.",
        "\tCheck passed: PIC timestamp precedes RPHSB.RR17.RQ6.R1B1 FGC timestamp by 14.0 ms, which is within the allowed gap of 40.0 ms.",
        "\tCheck passed: PIC timestamp precedes RPHSB.RR17.RQ6.R1B2 FGC timestamp by 14.0 ms, which is within the allowed gap of 40.0 ms.",
        "PC current profile consistency check",
        "\tCheck passed: The plateau duration for RPHSB.RR17.RQ6.R1B1 STATUS.I_REF signal is 300.7 s, which is greater than the minimum required duration of 300.0 s.",
        "\tCheck passed: The maximum plateau level deviation for RPHSB.RR17.RQ6.R1B1 STATUS.I_MEAS signal is 0.0209 A, which is within the threshold of 0.8620 A. Expected plateau level: 206.0000 A.",
        "\tCheck passed: The plateau duration for RPHSB.RR17.RQ6.R1B2 STATUS.I_REF signal is 300.7 s, which is greater than the minimum required duration of 300.0 s.",
        "\tCheck passed: The maximum plateau level deviation for RPHSB.RR17.RQ6.R1B2 STATUS.I_MEAS signal is 0.0117 A, which is within the threshold of 0.8620 A. Expected plateau level: 206.0000 A.",
        "PC voltage profile consistency check",
        "\tCheck passed: The plateau duration for RPHSB.RR17.RQ6.R1B1 STATUS.I_REF signal is 300.8 s, which is greater than the minimum required duration of 300.0 s.",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B1 STATUS.V_MEAS maximum deviation is 0.04510 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 300.63 s.",
        "\tCheck passed: The plateau duration for RPHSB.RR17.RQ6.R1B2 STATUS.I_REF signal is 300.8 s, which is greater than the minimum required duration of 300.0 s.",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B2 STATUS.V_MEAS maximum deviation is 0.04258 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 300.63 s.",
        "PC current decay check",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B1 STATUS.I_MEAS maximum deviation is 0.680 A compared to reference, which is within the threshold of ±4.310 A. Comparison period: 21.10 s.",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B2 STATUS.I_MEAS maximum deviation is 0.927 A compared to reference, which is within the threshold of ±4.310 A. Comparison period: 21.70 s.",
        "PC voltage decay check",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B1 STATUS.V_MEAS maximum deviation is 0.02788 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 21.00 s.",
        "\tCheck passed: The RPHSB.RR17.RQ6.R1B2 STATUS.V_MEAS maximum deviation is 0.03041 V compared to reference, which is within the threshold of ±0.06000 V. Comparison period: 21.60 s.",
        "Earth current check",
        "\tCheck passed: The values of RPHSB.RR17.RQ6.R1B2 I_EARTH_PCNT are within the margin of -15.00 to 15.00 %.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
