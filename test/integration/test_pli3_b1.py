import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (
            "PLI3.b1",
            "RQTD.A12B2",
            "Recommissioning 2022/2023",
            "2023-03-21 06:58:49.227000000",
            "2023-03-21 07:25:18.304000000",
            [
                "Multiple changes to False detected in signal RQTD.A12B2:ST_ABORT_PIC within the 1,679,378,329,227,000,000 to 1,679,379,918,304,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA23.RQTD.A12B2, as expected.",
                "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
                "\tCheck passed: RQTD.A12B2:ST_ABORT_PIC changed to False within the 1,679,378,329,227,000,000 to 1,679,379,918,304,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
                "\tCheck passed: Found exactly 1 PM timestamp for EE UA23.RQTD.A12B2, as expected.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "PC current profile consistency check",
                "\tCheck passed: The plateau duration for RPMBB.UA23.RQTD.A12B2 STATUS.I_REF signal is 28.300 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBB.UA23.RQTD.A12B2 STATUS.I_MEAS signal is 0.0008 A, which is within the threshold of 0.1100 A. Expected plateau level: 200.0000 A.",
                "PC current decay check",
                "\tCheck passed: Target current is 200.0 A, which is equal to the reference target current of 200.0 A.",
                "\tCheck passed: The plateau duration for RPMBB.UA23.RQTD.A12B2 STATUS.I_REF signal is 28.360 s, which is greater than the minimum required duration of 2.000 s.",
                "\tCheck passed: The RPMBB.UA23.RQTD.A12B2 STATUS.I_MEAS maximum deviation is 6.53 A compared to reference, which is within the threshold of ±55.00 A. Comparison period: 2.40 s.",
                "Energy extraction discharge check",
                "\tEE discharge checks passed. Please run the 600AEE_PM_Analysis.ipynb notebook for more information.",
            ],
        )
    ],
)
def test_pli3_b1(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]):
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pli3B1Analysis("", test_parameters)
    analysis_class.set_spark(get_or_create())
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert set(expected_logs).issubset(common.get_log_strings(analysis_class))
