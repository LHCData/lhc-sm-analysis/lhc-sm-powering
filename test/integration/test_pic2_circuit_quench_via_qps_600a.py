import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (  # 600A without ee
            "RCBXV1.R5",
            "Recommissioning 2021/2022",
            "2022-02-18 19:55:06.659000000",
            "2022-02-18 20:08:05.189000000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UL557.RCBXV1.R5, as expected.",
                "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
                "\tCheck passed: RCBXV1.R5:ST_ABORT_PIC changed to False within the 1,645,210,506,659,000,000 to 1,645,211,285,189,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE remained equal to False within the -20,000,000 to 4,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000.",
                "PIC status check",
                "\tCheck passed: RCBXV1.R5:ST_FAILURE_PIC remained equal to True within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1645210627419000000.",
                "\tCheck passed: RCBXV1.R5:ST_ABORT_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1645210627419000000.",
                "\tCheck passed: RCBXV1.R5:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1645210627419000000.",
                "QPS status check",
                "\tCheck passed: stat.RCBXV1.R5:ST_CIRCUIT_OK_QPS changed to False within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1645210627419000000.",
                "\tCheck passed: the signal stat.RCBXV1.R5:ST_CIRCUIT_OK_QPS has a sampling interval of 200,000,000 ns.",
                "\tCheck passed: stat.RCBXV1.R5:ST_CIRCUIT_OK_QPS changed to False within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1645210627419000000.",
                "\tCheck passed: the signal stat.RCBXV1.R5:ST_CIRCUIT_OK_QPS has a sampling interval of 200,000,000 ns.",
                "QDS linear ramp on LD1 check",
                "\tCheck passed: Signal stat.DFLBS.3R5.RCBXV1.R5.LD1:U_HTS was equal to 0.003094 V 0.0 seconds after reference timestamp, which is within the target of 0.003000 ± 0.000200 V. Reference timestamp: 2022-02-18 19:57:28.420 (1645210648420000000).",
                "\tCheck failed: stat.DFLBS.3R5.RCBXV1.R5.LD1:ST_LEAD_OK was expected to change once to False within the -500,000,000 to 500,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000, but it was found 0 times.",
                "\tTrying to check with a different time slot to avoid firmware bug.",
                "\tCheck passed: stat.DFLBS.3R5.RCBXV1.R5.LD1:ST_LEAD_OK changed once to False within the 18,000,000,000 to 22,000,000,000 nanoseconds time window relative to the reference timestamp 1645210627420000000.",
                "EE status check",
                "\tNo EE for circuit RCBXV1.R5, skipping EE checks",
                "EE voltage check",
                "\tNo EE for circuit RCBXV1.R5, skipping EE checks",
            ],
        ),
        (  # 600A with ee RR
            "ROD.A45B1",
            "Recommissioning 2021/2022",
            "2022-02-01 18:17:08.299000000",
            "2022-02-01 18:32:52.182000000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBD.RR53.ROD.A45B1, as expected.",
                "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
                "\tCheck passed: Found exactly 1 PM timestamp for EE RR53.ROD.A45B1, as expected.",
                "\tCheck passed: ROD.A45B1:ST_ABORT_PIC changed to False within the 1,643,735,828,299,000,000 to 1,643,736,772,182,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE remained equal to False between 1643735928703000000 offsetted by -20,000,000 ns and 1643735928720000000 offsetted by 4,000,000 ns, as expected.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "PIC status check",
                "\tCheck passed: ROD.A45B1:ST_FAILURE_PIC remained equal to True within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "\tCheck passed: ROD.A45B1:ST_ABORT_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "\tCheck passed: ROD.A45B1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "QPS status check",
                "\tCheck passed: stat.ROD.A45B1:ST_CIRCUIT_OK_QPS remained equal to True within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "\tCheck passed: the signal stat.ROD.A45B1:ST_CIRCUIT_OK_QPS has a sampling interval of 41,000,000 ns.",
                "\tCheck passed: stat.ROD.A45B1:ST_CIRCUIT_OK_QPS remained equal to True within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "\tCheck passed: the signal stat.ROD.A45B1:ST_CIRCUIT_OK_QPS has a sampling interval of 41,000,000 ns.",
                "QDS linear ramp on LD1 check",
                "\tCheck passed: Signal stat.DFLBS.7L5.ROD.A45B1.LD1:U_HTS was equal to 0.003081 V 0.0 seconds after reference timestamp, which is within the target of 0.003000 ± 0.000200 V. Reference timestamp: 2022-02-01 18:18:48.706 (1643735928706000000).",
                "\tCheck passed: stat.DFLBS.7L5.ROD.A45B1.LD1:ST_LEAD_OK changed once to False within the -500,000,000 to 500,000,000 nanoseconds time window relative to the reference timestamp 1643735928706000000.",
                "EE status check",
                "\tCheck passed: ST_FPA_REC_0 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: ST_FPA_REC_1 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: ST_SW_A_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: ST_SW_B_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "\tCheck passed: ST_SW_Z_OPEN remained equal to False within the -500,000,000 to 2,000,000,000 nanoseconds time window relative to the reference timestamp 1643735928703000000.",
                "EE voltage check",
                "\tCheck passed: Signal U_CAP_A was equal to 62.14 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2022-02-01 18:18:48.703 (1643735928703000000).",
                "\tCheck passed: Signal U_CAP_B was equal to 65.18 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2022-02-01 18:18:48.703 (1643735928703000000).",
                "\tCheck passed: Signal U_CAP_Z was equal to 162.6 V 0.03 seconds after reference timestamp, which is within the target of 165.0 ± 35.0 V. Reference timestamp: 2022-02-01 18:18:48.703 (1643735928703000000).",
            ],
        ),
        (  # 600A with ee
            "RSF1.A56B1",
            "Recommissioning 2021/2022",
            "2022-02-04 19:31:00.096000000",
            "2022-02-04 19:47:21.747000000",
            [
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA63.RSF1.A56B1, as expected.",
                "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
                "\tCheck passed: Found exactly 1 PM timestamp for EE UA63.RSF1.A56B1, as expected.",
                "\tCheck passed: RSF1.A56B1:ST_ABORT_PIC changed to False within the 1,643,999,460,096,000,000 to 1,644,000,441,747,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
                "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT changed once to True within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE remained equal to False between 1643999593172000000 offsetted by -20,000,000 ns and 1643999593180000000 offsetted by 4,000,000 ns, as expected.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -20,000,000 to 30,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "PIC status check",
                "\tCheck passed: RSF1.A56B1:ST_FAILURE_PIC remained equal to True within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1643999593175000000.",
                "\tCheck passed: RSF1.A56B1:ST_ABORT_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1643999593175000000.",
                "\tCheck passed: RSF1.A56B1:CMD_ABORT_PIC changed to False within the -1,000 to 1,000 nanoseconds time window relative to the reference timestamp 1643999593175000000.",
                "QPS status check",
                "\tCheck passed: stat.RSF1.A56B1:ST_CIRCUIT_OK_QPS changed to False within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1643999593175000000.",
                "\tCheck passed: the signal stat.RSF1.A56B1:ST_CIRCUIT_OK_QPS has a sampling interval of 200,000,000 ns.",
                "\tCheck passed: stat.RSF1.A56B1:ST_CIRCUIT_OK_QPS changed to False within the -2,000,000 to 400,000,000 nanoseconds time window relative to the reference timestamp 1643999593175000000.",
                "\tCheck passed: the signal stat.RSF1.A56B1:ST_CIRCUIT_OK_QPS has a sampling interval of 200,000,000 ns.",
                "QDS linear ramp on LD1 check",
                "\tCheck passed: Signal stat.DFLBS.5L6.RSF1.A56B1.LD1:U_HTS was equal to 0.003019 V 0.0 seconds after reference timestamp, which is within the target of 0.003000 ± 0.000200 V. Reference timestamp: 2022-02-04 19:33:31.977 (1643999611977000000).",
                "\tCheck failed: stat.DFLBS.5L6.RSF1.A56B1.LD1:ST_LEAD_OK was expected to change once to False within the -500,000,000 to 500,000,000 nanoseconds time window relative to the reference timestamp 1643999593177000000, but it was found 0 times.",
                "\tTrying to check with a different time slot to avoid firmware bug.",
                "\tCheck passed: stat.DFLBS.5L6.RSF1.A56B1.LD1:ST_LEAD_OK changed once to False within the 18,000,000,000 to 22,000,000,000 nanoseconds time window relative to the reference timestamp 1643999593177000000.",
                "EE status check",
                "\tCheck passed: ST_FPA_REC_0 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: ST_FPA_REC_1 changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: ST_SW_A_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: ST_SW_B_OPEN changed to True within the -200,000,000 to 200,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "\tCheck passed: ST_SW_Z_OPEN remained equal to False within the -500,000,000 to 2,000,000,000 nanoseconds time window relative to the reference timestamp 1643999593172000000.",
                "EE voltage check",
                "\tCheck passed: Signal U_CAP_A was equal to 51.40 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2022-02-04 19:33:13.172 (1643999593172000000).",
                "\tCheck passed: Signal U_CAP_B was equal to 49.76 V 0.03 seconds after reference timestamp, which is within the target of 51.00 ± 35.00 V. Reference timestamp: 2022-02-04 19:33:13.172 (1643999593172000000).",
                "\tCheck passed: Signal U_CAP_Z was equal to 168.4 V 0.03 seconds after reference timestamp, which is within the target of 165.0 ± 35.0 V. Reference timestamp: 2022-02-04 19:33:13.172 (1643999593172000000).",
            ],
        ),
    ],
)
def test_pic2_circuit_quench_via_qps(
    circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]
):
    # arrange
    hwc_test = "PIC2 CIRCUIT QUENCH VIA QPS"
    spark = get_or_create()
    hwc_summary = signal_metadata.get_hwc_summary()
    test_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary, hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pic2CircuitQuenchViaQpsFor600AAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
