from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import analyses

from . import common


def test_pno_d1_on_80_120a():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PNO.d1",
        "RCSX3.R2",
        "Recommissioning 2022/2023",
        "2023-03-20 20:52:08.681000000",
        "2023-03-20 21:05:50.612000000",
    )

    # act
    analysis_class = analyses.PnoD1On80to120AAnalysis("", test_parameters)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 2 PM timestamps for FGC RPLB.UA27.RCSX3.R2, as expected.",
        "PC current profile consistency check (positive cycle)",
        "\tCheck passed: The plateau duration for RPLB.UA27.RCSX3.R2 STATUS.I_REF signal is 2.580 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLB.UA27.RCSX3.R2 STATUS.I_MEAS signal is 0.00278 A, which is within the threshold of 0.02000 A. Expected plateau level: 105.00000 A.",
        "PC voltage profile consistency check (positive cycle)",
        "\tCheck passed: Target current is 105.0 A, which is equal to the reference target current of 105.0 A.",
        "\tCheck passed: The plateau duration for RPLB.UA27.RCSX3.R2 STATUS.I_REF signal is 3.000 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The RPLB.UA27.RCSX3.R2 STATUS.V_MEAS maximum deviation is 0.0154 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 2.08 s.",
        "PC voltage decay check (positive cycle)",
        "\tCheck passed: Target current is 105.0 A, which is equal to the reference target current of 105.0 A.",
        "\tCheck passed: The RPLB.UA27.RCSX3.R2 STATUS.V_MEAS maximum deviation is 0.0000 V compared to reference, which is within the threshold of ±0.5000 V. Comparison period: 0.04 s.",
        "Earth current check (positive cycle)",
        "\tCheck passed: The values of RPLB.UA27.RCSX3.R2 STATUS.I_EARTH_PCNT are within the margin of 0.0000 to 15.00 %.",
        "PC current profile consistency check (negative cycle)",
        "\tCheck passed: The plateau duration for RPLB.UA27.RCSX3.R2 STATUS.I_REF signal is 2.560 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLB.UA27.RCSX3.R2 STATUS.I_MEAS signal is 0.00200 A, which is within the threshold of 0.02000 A. Expected plateau level: -105.00000 A.",
        "PC voltage profile consistency check (negative cycle)",
        "\tCheck passed: Target current is -105.0 A, which is equal to the reference target current of -105.0 A.",
        "\tCheck passed: The plateau duration for RPLB.UA27.RCSX3.R2 STATUS.I_REF signal is 3.000 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The RPLB.UA27.RCSX3.R2 STATUS.V_MEAS maximum deviation is 0.0102 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 2.06 s.",
        "PC voltage decay check (negative cycle)",
        "\tCheck passed: Target current is -105.0 A, which is equal to the reference target current of -105.0 A.",
        "\tCheck passed: The RPLB.UA27.RCSX3.R2 STATUS.V_MEAS maximum deviation is 0.0051 V compared to reference, which is within the threshold of ±0.5000 V. Comparison period: 0.04 s.",
        "Earth current check (negative cycle)",
        "\tCheck passed: The values of RPLB.UA27.RCSX3.R2 STATUS.I_EARTH_PCNT are within the margin of 0.0000 to 15.00 %.",
    ]

    assert common.get_log_strings(analysis_class) == expected_logs
