from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_powering_failure_rq():
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PIC2 POWERING FAILURE",
        "RQD.A12",
        "Recommissioning 2022/2023",
        "2023-03-16 19:31:50.706000000",
        "2023-03-16 19:35:41.762000000",
    )

    # act
    analysis_class = analyses.Pic2PoweringFailureForRQAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMCNMQ_PMSTD&source=RQD.A12&fromTimestampInNanos=1678991510706000000&durationInNanos=231056000000",
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMSNRQ&source=UA23.RQD.A12&fromTimestampInNanos=1678991510706000000&durationInNanos=231056000000",
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHE.UA23.RQD.A12, as expected.",
        "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
        "\tCheck passed: No PM dumps were found for EE UA23.RQD.A12 in the analyzed interval, as expected.",
        "FGC status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "PIC status check",
        "\tCheck passed: RQD.A12:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: RQD.A12:ST_FAILURE_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1678991611181000000.",
        "\tCheck passed: RQD.A12:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: RQD.A12:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: RQD.A12:ST_DISCHARGE_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
        "\tCheck passed: RQD.A12:CMD_DISCHARGE_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678991611200000000.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
