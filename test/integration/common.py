from lhcsmapi.api.analysis import Analysis


def get_log_strings(analysis_class: Analysis) -> list[str]:
    return [record.getMessage() for record in analysis_class.get_logs()]
