from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pno_b1():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PNO.b1",
        "RCD.A67B1",
        "Recommissioning post LS2",
        "2021-06-25 20:52:01.936000",
        "2021-06-25 21:14:51.765000",
    )

    # act
    analysis_class = analyses.PnoB1Analysis("", test_parameters)
    analysis_class.set_spark(get_or_create())
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Multiple changes to False detected in signal RCD.A67B1:ST_ABORT_PIC within the 1,624,647,121,936,000,000 to 1,624,648,491,765,000,000 nanoseconds time window relative to the reference timestamp 0.",
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA67.RCD.A67B1, as expected.",
        "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for EE UA67.RCD.A67B1, as expected.",
        "\tCheck passed: RCD.A67B1:ST_ABORT_PIC changed to False within the 1,624,647,121,936,000,000 to 1,624,648,491,765,000,000 nanoseconds time window relative to the reference timestamp 0.",
        "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
        "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
        "PC current profile consistency check",
        "\tCheck passed: The plateau duration for RPMBB.UA67.RCD.A67B1 STATUS.I_REF signal is 23.560 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The maximum plateau level deviation for RPMBB.UA67.RCD.A67B1 STATUS.I_MEAS signal is 0.0033 A, which is within the threshold of 0.1100 A. Expected plateau level: 550.0000 A.",
        "PC current decay check",
        "\tCheck passed: Target current is 550.0 A, which is equal to the reference target current of 550.0 A.",
        "\tCheck passed: The plateau duration for RPMBB.UA67.RCD.A67B1 STATUS.I_REF signal is 109.960 s, which is greater than the minimum required duration of 2.000 s.",
        "\tCheck passed: The RPMBB.UA67.RCD.A67B1 STATUS.I_MEAS maximum deviation is 26.15 A compared to reference, which is within the threshold of ±82.50 A. Comparison period: 0.04 s.",
        "Energy extraction discharge check",
        "\tEE discharge checks passed. Please run the 600AEE_PM_Analysis.ipynb notebook for more information.",
    ]
    assert set(expected_logs).issubset(set(common.get_log_strings(analysis_class)))
