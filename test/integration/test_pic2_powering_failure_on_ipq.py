from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_powering_failure_on_ipq():
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PIC2 POWERING FAILURE",
        "RQ4.L6",
        "Recommissioning 2022/2023",
        "2023-03-17 12:28:11.350000000",
        "2023-03-17 12:32:20.175000000",
    )

    # act
    analysis_class = analyses.Pic2PoweringFailureForIPQAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQB&source=RQ4.L6&fromTimestampInNanos=1679052491350000000&durationInNanos=248825000000",
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHH.UA63.RQ4.L6B1, as expected.",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHH.UA63.RQ4.L6B2, as expected.",
        "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
        "FGC B1 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "FGC B2 status check",
        "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679052645220000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -20,000,000 to 20,000,000 nanoseconds time window relative to the reference timestamp 1679052645220000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679052645220000000.",
        "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -5,000,000 to 5,000,000 nanoseconds time window relative to the reference timestamp 1679052645220000000.",
        "PIC status check",
        "\tCheck passed: RQ4.L6:CMD_PWR_PERM_B1_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: RQ4.L6:CMD_PWR_PERM_B2_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: RQ4.L6:ST_FAILURE_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1679052524701000000.",
        "\tCheck passed: RQ4.L6:ST_FAILURE_PIC changed once to False within the -2,000,000 to 0 nanoseconds time window relative to the reference timestamp 1679052524701000000.",
        "\tCheck passed: RQ4.L6:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 130,500,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
        "\tCheck passed: RQ4.L6:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 130,500,000,000 nanoseconds time window relative to the reference timestamp 1679052524720000000.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
