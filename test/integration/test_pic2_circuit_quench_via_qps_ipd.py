from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pic2_circuit_quench_via_qps_ipd():
    # arrange
    spark = get_or_create()
    hwc_summary = signal_metadata.get_hwc_summary()
    test_parameters = analyses.get_analysis_test_input_parameters(
        hwc_summary,
        "PIC2 CIRCUIT QUENCH VIA QPS",
        "RD1.L2",
        "Recommissioning 2022/2023",
        "2023-03-20 15:46:45.815000000",
        "2023-03-20 16:23:13.673000000",
    )

    # act
    analysis_class = analyses.Pic2CircuitQuenchViaQpsForIPDAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers presence and timestamps check",
        "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHF.UA23.RD1.L2, as expected.",
        "\tCheck passed: Found exactly 2 QPS PM timestamps, as expected.",
        "\tCheck passed: RD1.L2:ST_ABORT_PIC changed to False within the 1,679,323,605,815,000,000 to 1,679,325,793,673,000,000 nanoseconds time window relative to the reference timestamp 0.",
        "\tCheck passed: QPS PM timestamps are 1.0 ms apart.",
        "\tCheck passed: Timestamps are within the margin of 40,000,000 ns.",
        "QDS linear ramp on LD1 check",
        "\tCheck passed: Signal stat.DFLCS.3L2.RD1.L2.LD1:U_HTS was equal to 0.003231 V 0.0 seconds after reference timestamp, which is within the target of 0.003100 ± 0.000150 V. Reference timestamp: 2023-03-20 15:53:37.245 (1679324017245000000).",
        "QH discharge check",
        "\tCheck passed: QH discharge parameters are within expected ranges.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
