import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (
            "PNO.a3",
            "RQT12.L6B1",
            "Recommissioning 2023/2024",
            "2024-02-15 16:21:14.764",
            "2024-02-15 17:36:23.610",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RQT12.L6B1&fromTimestampInNanos=1708010474764000000&durationInNanos=4508846000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 2 PM timestamps for FGC RPMBA.UA63.RQT12.L6B1, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "PC current profile consistency check (positive cycle)",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_REF signal is 30.00 s, which is greater than the minimum required duration of 25.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_MEAS signal is 0.0196 A, which is within the threshold of 0.1100 A. Expected plateau level: 0.0000 A.",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_REF signal is 1502 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_MEAS signal is 0.0025 A, which is within the threshold of 0.1100 A. Expected plateau level: 550.0000 A.",
                "Current signal accuracy check (positive cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 RQT12.L6B1:I_DCCT - RPMBA.UA63.RQT12.L6B1:I_MEAS_INTERPOLATED are within the margin of -16.00 to 16.00 A.",
                "\tCheck passed: I_DCCT drift at I_PNO (POSITIVE) for RPMBA.UA63.RQT12.L6B1 is 2.932 A, which is within the allowed range from 0.000 to 6.000 A.",
                "Differential voltage check (positive cycle)",
                "\tCheck passed: The average value of RQT12.L6B1:U_DIFF for RPMBA.UA63.RQT12.L6B1 is 0.00301 V, which is within the allowed range from 0.00000 to 0.01000 V.",
                "\tCheck passed: U_DIFF standard deviation at I_PNO (POSITIVE) for RPMBA.UA63.RQT12.L6B1 is 0.000000 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Resistive voltage check (positive cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 RQT12.L6B1:U_RES are within the margin of 0.0000 to 0.07000 V.",
                "\tCheck passed: U_RES standard deviation at I_PNO (POSITIVE) for RPMBA.UA63.RQT12.L6B1 is 0.000242 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Circuit resistance check (positive cycle)",
                "\tCheck passed: R_CIRCUIT for RPMBA.UA63.RQT12.L6B1 is 0.000000720 Ohm, which is within the allowed range from 0.000000000 to 0.000005000 Ohm.",
                "PC current profile consistency check (negative cycle)",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_REF signal is 30.00 s, which is greater than the minimum required duration of 25.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_MEAS signal is 0.0170 A, which is within the threshold of 0.1100 A. Expected plateau level: -0.0000 A.",
                "\tCheck passed: The plateau duration for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_REF signal is 1502 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBA.UA63.RQT12.L6B1 RPMBA.UA63.RQT12.L6B1:I_MEAS signal is 0.0025 A, which is within the threshold of 0.1100 A. Expected plateau level: -550.0000 A.",
                "Current signal accuracy check (negative cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 RQT12.L6B1:I_DCCT - RPMBA.UA63.RQT12.L6B1:I_MEAS_INTERPOLATED are within the margin of -16.00 to 16.00 A.",
                "\tCheck passed: I_DCCT drift at I_PNO (NEGATIVE) for RPMBA.UA63.RQT12.L6B1 is 1.466 A, which is within the allowed range from 0.000 to 6.000 A.",
                "Differential voltage check (negative cycle)",
                "\tCheck passed: The average value of RQT12.L6B1:U_DIFF for RPMBA.UA63.RQT12.L6B1 is 0.00317 V, which is within the allowed range from 0.00000 to 0.01000 V.",
                "\tCheck passed: U_DIFF standard deviation at I_PNO (NEGATIVE) for RPMBA.UA63.RQT12.L6B1 is 0.001434 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Resistive voltage check (negative cycle)",
                "\tCheck passed: The values of RPMBA.UA63.RQT12.L6B1 RQT12.L6B1:U_RES are within the margin of 0.0000 to 0.07000 V.",
                "\tCheck passed: U_RES standard deviation at I_PNO (NEGATIVE) for RPMBA.UA63.RQT12.L6B1 is 0.000267 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Circuit resistance check (negative cycle)",
                "\tCheck passed: R_CIRCUIT for RPMBA.UA63.RQT12.L6B1 is 0.000000720 Ohm, which is within the allowed range from 0.000000000 to 0.000005000 Ohm.",
                "Current lead temperature check",
                "\tCheck passed: The values of DFLBS.5L6.RQT12.L6B1 DAKB17_05L6_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLBS.5L6.RQT12.L6B1 DAKB18_05L6_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLBS.5L6.RQT12.L6B1 DAKB17_05L6_TT893.TEMPERATURECALC are within the margin of 280.0 to 323.0 K.",
                "\tCheck passed: The values of DFLBS.5L6.RQT12.L6B1 DAKB18_05L6_TT893.TEMPERATURECALC are within the margin of 280.0 to 323.0 K.",
            ],
        ),
        (
            "PNO.a3",
            "RCO.A56B2",
            "Recommissioning 2023/2024",
            "2024-02-24 08:51:38.895",
            "2024-02-24 09:57:41.887",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RCO.A56B2&fromTimestampInNanos=1708761098895000000&durationInNanos=3962992000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 2 PM timestamps for FGC RPLB.UA63.RCO.A56B2, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "PC current profile consistency check (positive cycle)",
                "\tCheck passed: The plateau duration for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_REF signal is 30.00 s, which is greater than the minimum required duration of 25.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_MEAS signal is 0.00271 A, which is within the threshold of 0.02000 A. Expected plateau level: 0.00000 A.",
                "\tCheck passed: The plateau duration for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_REF signal is 1500 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: The maximum plateau level deviation for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_MEAS signal is 0.00343 A, which is within the threshold of 0.02000 A. Expected plateau level: 100.00000 A.",
                "Current signal accuracy check (positive cycle)",
                "\tCheck passed: The values of RPLB.UA63.RCO.A56B2 RCO.A56B2:I_DCCT - RPLB.UA63.RCO.A56B2:I_MEAS_INTERPOLATED are within the margin of -16.00 to 16.00 A.",
                "\tCheck passed: I_DCCT drift at I_PNO (POSITIVE) for RPLB.UA63.RCO.A56B2 is 0.293 A, which is within the allowed range from 0.000 to 6.000 A.",
                "Differential voltage check (positive cycle)",
                "\tCheck passed: The average value of RCO.A56B2:U_DIFF for RPLB.UA63.RCO.A56B2 is 0.00295 V, which is within the allowed range from 0.00000 to 0.01000 V.",
                "\tCheck passed: U_DIFF standard deviation at I_PNO (POSITIVE) for RPLB.UA63.RCO.A56B2 is 0.000689 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Resistive voltage check (positive cycle)",
                "\tCheck passed: The values of RPLB.UA63.RCO.A56B2 RCO.A56B2:U_RES are within the margin of 0.0000 to 0.07000 V.",
                "\tCheck passed: U_RES standard deviation at I_PNO (POSITIVE) for RPLB.UA63.RCO.A56B2 is 0.000935 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Circuit resistance check (positive cycle)",
                "\tCheck passed: R_CIRCUIT for RPLB.UA63.RCO.A56B2 is 0.00001503 Ohm, which is within the allowed range from 0.00000000 to 0.00002000 Ohm.",
                "PC current profile consistency check (negative cycle)",
                "\tCheck passed: The plateau duration for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_REF signal is 30.00 s, which is greater than the minimum required duration of 25.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_MEAS signal is 0.00340 A, which is within the threshold of 0.02000 A. Expected plateau level: -0.00000 A.",
                "\tCheck passed: The plateau duration for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_REF signal is 1500 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: The maximum plateau level deviation for RPLB.UA63.RCO.A56B2 RPLB.UA63.RCO.A56B2:I_MEAS signal is 0.00317 A, which is within the threshold of 0.02000 A. Expected plateau level: -100.00000 A.",
                "Current signal accuracy check (negative cycle)",
                "\tCheck passed: The values of RPLB.UA63.RCO.A56B2 RCO.A56B2:I_DCCT - RPLB.UA63.RCO.A56B2:I_MEAS_INTERPOLATED are within the margin of -16.00 to 16.00 A.",
                "\tCheck passed: I_DCCT drift at I_PNO (NEGATIVE) for RPLB.UA63.RCO.A56B2 is 0.293 A, which is within the allowed range from 0.000 to 6.000 A.",
                "Differential voltage check (negative cycle)",
                "\tCheck passed: The average value of RCO.A56B2:U_DIFF for RPLB.UA63.RCO.A56B2 is 0.00281 V, which is within the allowed range from 0.00000 to 0.01000 V.",
                "\tCheck passed: U_DIFF standard deviation at I_PNO (NEGATIVE) for RPLB.UA63.RCO.A56B2 is 0.001565 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Resistive voltage check (negative cycle)",
                "\tCheck passed: The values of RPLB.UA63.RCO.A56B2 RCO.A56B2:U_RES are within the margin of 0.0000 to 0.07000 V.",
                "\tCheck passed: U_RES standard deviation at I_PNO (NEGATIVE) for RPLB.UA63.RCO.A56B2 is 0.000852 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Circuit resistance check (negative cycle)",
                "\tCheck passed: R_CIRCUIT for RPLB.UA63.RCO.A56B2 is 0.00001503 Ohm, which is within the allowed range from 0.00000000 to 0.00002000 Ohm.",
                "Current lead temperature check",
                "\tCheck passed: The values of DFLBS.5L6.RCO.A56B2 DAKF01_05L6_TT891A.TEMPERATURECALC are within the margin of 35.00 to 44.00 K.",
                "\tCheck passed: The values of DFLBS.5L6.RCO.A56B2 DAKF01_05L6_TT893.TEMPERATURECALC are within the margin of 280.0 to 323.0 K.",
            ],
        ),
        (
            "PNO.a3",
            "RU.R4",
            "Recommissioning 2023/2024",
            "2024-02-24 17:03:16.327000",
            "2024-02-24 18:18:22.500000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RU.R4&fromTimestampInNanos=1708790596327000000&durationInNanos=4506173000000",
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMSN600&source=UA47.RU.R4&fromTimestampInNanos=1708790596327000000&durationInNanos=4506173000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UA47.RU.R4, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "\tCheck passed: No PM dumps were found for EE UA47.RU.R4 in the analyzed interval, as expected.",
                "PC current profile consistency check (positive cycle)",
                "\tCheck passed: The plateau duration for RPMBB.UA47.RU.R4 RPMBB.UA47.RU.R4:I_REF signal is 30.00 s, which is greater than the minimum required duration of 25.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBB.UA47.RU.R4 RPMBB.UA47.RU.R4:I_MEAS signal is 0.01000 A, which is within the threshold of 0.08000 A. Expected plateau level: 0.00000 A.",
                "\tCheck passed: The plateau duration for RPMBB.UA47.RU.R4 RPMBB.UA47.RU.R4:I_REF signal is 1500 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: The maximum plateau level deviation for RPMBB.UA47.RU.R4 RPMBB.UA47.RU.R4:I_MEAS signal is 0.00104 A, which is within the threshold of 0.08000 A. Expected plateau level: 400.00000 A.",
                "Current signal accuracy check (positive cycle)",
                "\tCheck passed: The values of RPMBB.UA47.RU.R4 RU.R4:I_DCCT - RPMBB.UA47.RU.R4:I_MEAS_INTERPOLATED are within the margin of -16.00 to 16.00 A.",
                "\tCheck passed: I_DCCT drift at I_PNO (POSITIVE) for RPMBB.UA47.RU.R4 is 0.293 A, which is within the allowed range from 0.000 to 6.000 A.",
                "Differential voltage check (positive cycle)",
                "\tCheck passed: The average value of RU.R4:U_DIFF for RPMBB.UA47.RU.R4 is 0.00513 V, which is within the allowed range from 0.00000 to 0.01000 V.",
                "\tCheck passed: U_DIFF standard deviation at I_PNO (POSITIVE) for RPMBB.UA47.RU.R4 is 0.002572 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Resistive voltage check (positive cycle)",
                "\tCheck passed: The values of RPMBB.UA47.RU.R4 RU.R4:U_RES are within the margin of 0.0000 to 0.07000 V.",
                "\tCheck passed: U_RES standard deviation at I_PNO (POSITIVE) for RPMBB.UA47.RU.R4 is 0.001629 V, which is within the allowed range from 0.000000 to 0.005000 V.",
                "Circuit resistance check (positive cycle)",
                "\tCheck passed: Target current is 400.0 A, which is equal to the reference target current of 400.0 A.",
                "\tCheck passed: The plateau duration for RPMBB.UA47.RU.R4 RPMBB.UA47.RU.R4:I_REF (ref test) signal is 1500 s, which is greater than the minimum required duration of 1500 s.",
                "\tCheck passed: R_CIRCUIT for RPMBB.UA47.RU.R4 is 0.00001619 Ohm, which is within the allowed range from 0.00001451 to 0.00001773 Ohm.",
                "Current lead temperature check",
                "\tCheck passed: The values of DFLBS.5R4.RU.R4 DMKB01_05R4_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLBS.5R4.RU.R4 DMKB04_05R4_TT891A.TEMPERATURECALC are within the margin of 46.00 to 54.00 K.",
                "\tCheck passed: The values of DFLBS.5R4.RU.R4 DMKB01_05R4_TT893.TEMPERATURECALC are within the margin of 280.0 to 323.0 K.",
                "\tCheck passed: The values of DFLBS.5R4.RU.R4 DMKB04_05R4_TT893.TEMPERATURECALC are within the margin of 280.0 to 323.0 K.",
            ],
        ),
    ],
)
def test_pno_a3(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]):
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.PnoA3Analysis("", test_parameters)
    analysis_class.set_spark(get_or_create())
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert common.get_log_strings(analysis_class) == expected_logs
