import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (
            "PLI1.c2",
            "RD1.L2",
            "Recommissioning post LS2",
            "2021-04-02 07:32:56.911",
            "2021-04-02 07:40:51.689",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRD&source=RD1.L2&fromTimestampInNanos=1617341576911000000&durationInNanos=474778000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHF.UA23.RD1.L2, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "\tCheck passed: RD1.L2:ST_ABORT_PIC changed once to False within the 1,617,341,576,911,000,000 to 1,617,342,051,689,000,000 nanoseconds time window relative to the reference timestamp 0.",
                "\tCheck passed: PIC timestamp precedes RPHF.UA23.RD1.L2 FGC timestamp by 6.0 ms, which is within the allowed gap of 40.0 ms.",
                "PC current profile consistency check",
                "\tCheck passed: The plateau duration for RPHF.UA23.RD1.L2 STATUS.I_REF signal is 62.08 s, which is greater than the minimum required duration of 60.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA23.RD1.L2 STATUS.I_MEAS signal is 0.016 A, which is within the threshold of 1.160 A. Expected plateau level: 200.000 A.",
                "\tCheck passed: The plateau duration for RPHF.UA23.RD1.L2 STATUS.I_REF signal is 240.5 s, which is greater than the minimum required duration of 240.0 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA23.RD1.L2 STATUS.I_MEAS signal is 0.013 A, which is within the threshold of 1.160 A. Expected plateau level: 350.000 A.",
                "PC voltage profile consistency check",
                "\tCheck passed: The plateau duration for RPHF.UA23.RD1.L2 STATUS.I_REF signal is 62.28 s, which is greater than the minimum required duration of 60.00 s.",
                "\tCheck passed: The maximum plateau level deviation for RPHF.UA23.RD1.L2 STATUS.I_MEAS signal is 0.011 A, which is within the threshold of 1.160 A. Expected plateau level: 200.000 A.",
                "\tCheck passed: The plateau duration for RPHF.UA23.RD1.L2 STATUS.I_REF signal is 240.5 s, which is greater than the minimum required duration of 240.0 s.",
                "\tCheck passed: The RPHF.UA23.RD1.L2 STATUS.V_MEAS maximum deviation is 0.03070 V compared to reference, which is within the threshold of ±0.08000 V. Comparison period: 240.48 s.",
                "PC current decay check",
                "\tCheck passed: The RPHF.UA23.RD1.L2 STATUS.I_MEAS maximum deviation is 1.736 A compared to reference, which is within the threshold of ±5.800 A. Comparison period: 40.22 s.",
                "PC voltage decay check",
                "\tCheck passed: The RPHF.UA23.RD1.L2 STATUS.V_MEAS maximum deviation is 0.01310 V compared to reference, which is within the threshold of ±0.08000 V. Comparison period: 40.18 s.",
                "Earth current check",
                "\tCheck passed: The values of RPHF.UA23.RD1.L2 I_EARTH_PCNT are within the margin of -10.00 to 10.00 %.",
            ],
        )
    ],
)
def test_pli1_c2(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]):
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pli1C2Analysis("", test_parameters)
    analysis_class.set_spark(get_or_create())
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
