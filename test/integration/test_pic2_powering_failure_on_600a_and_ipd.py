import pytest
from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_logs"),
    [
        (  # 600a
            "PIC2 POWERING FAILURE",
            "RCBXH1.L1",
            "Recommissioning 2022/2023",
            "2023-03-14 16:24:55.732000000",
            "2023-03-14 16:27:50.238000000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNA&source=RCBXH1.L1&fromTimestampInNanos=1678807495732000000&durationInNanos=174506000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPMBB.UL14.RCBXH1.L1, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -25,000,000 to 25,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -25,000,000 to 45,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "PIC status check",
                "\tCheck passed: RCBXH1.L1:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "\tCheck passed: RCBXH1.L1:ST_FAILURE_PIC changed once to False within the -2,000,000 to -1,000,000 nanoseconds time window relative to the reference timestamp 1678807575681000000.",
                "\tCheck passed: RCBXH1.L1:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
                "\tCheck passed: RCBXH1.L1:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1678807575700000000.",
            ],
        ),
        (  # ipd
            "PIC2 POWERING FAILURE",
            "RD1.L2",
            "Recommissioning 2022/2023",
            "2023-03-20 15:44:24.071000000",
            "2023-03-20 15:46:39.253000000",
            [
                "Post Mortem returned no QPS data for the following query: http://pm-rest.cern.ch/v2//pmdata/header/within/duration?system=QPS&className=DQAMGNRQA&source=RD1.L2&fromTimestampInNanos=1679323464071000000&durationInNanos=135182000000",
                "PM buffers presence and timestamps check",
                "\tCheck passed: Found exactly 1 PM timestamp for FGC RPHF.UA23.RD1.L2, as expected.",
                "\tCheck passed: No QPS PM dumps were found in the analyzed interval, as expected.",
                "FGC status check",
                "\tCheck passed: STATUS.ST_FAULTS FAST_ABORT remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_DISCH_RQ remained equal to False within the -5,000,000,000 to 5,000,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PWR_FAILURE changed once to True within the -25,000,000 to 25,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "\tCheck passed: STATUS.ST_UNLATCHED PC_PERMIT changed once to False within the -25,000,000 to 45,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "PIC status check",
                "\tCheck passed: RD1.L2:CMD_PWR_PERM_PIC changed once to False within the -40,000,000 to 40,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "\tCheck passed: RD1.L2:ST_FAILURE_PIC changed once to False within the -2,000,000 to -1,000,000 nanoseconds time window relative to the reference timestamp 1679323504261000000.",
                "\tCheck passed: RD1.L2:CMD_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
                "\tCheck passed: RD1.L2:ST_ABORT_PIC remained equal to True within the -10,000,000,000 to 10,000,000,000 nanoseconds time window relative to the reference timestamp 1679323504280000000.",
            ],
        ),
    ],
)
def test_pic2_powering_failure_on_600a_and_ipd(
    hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str, expected_logs: list[str]
):
    # arrange
    spark = get_or_create()
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )

    # act
    analysis_class = analyses.Pic2PoweringFailureFor600AAndIPDAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    assert common.get_log_strings(analysis_class) == expected_logs
