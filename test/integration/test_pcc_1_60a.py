from lhcsmapi.metadata import signal_metadata
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

from . import common


def test_pcc_1_on_60a():
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(),
        "PCC.1",
        "RCBH11.L1B2",
        "Recommissioning 2023/2024",
        "2024-02-15 16:37:14.227000000",
        "2024-02-15 16:53:56.139000000",
    )
    spark = get_or_create()

    # act
    analysis_class = analyses.Pcc1On60AAnalysis("", test_parameters)
    analysis_class.set_spark(spark)
    analysis_class.query()
    analysis_class.analyze()

    # assert
    assert analysis_class.get_analysis_output() is True
    expected_logs = [
        "PM buffers and timestamps check",
        "\tCheck passed: Found exactly 3 PM timestamps for FGC RPLA.12L1.RCBH11.L1B2, as expected.",
        "PC current profile consistency check",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS signal is 0.00102 A, which is within the threshold of 0.01100 A. Expected plateau level: -10.00000 A.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS signal is 0.00102 A, which is within the threshold of 0.01100 A. Expected plateau level: 10.00000 A.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The maximum plateau level deviation for RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS signal is 0.00100 A, which is within the threshold of 0.01100 A. Expected plateau level: -10.00000 A.",
        "PC voltage profile consistency check",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.00650 V compared to reference, which is within the threshold of ±0.04000 V. Comparison period: 11.23 s.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.00795 V compared to reference, which is within the threshold of ±0.04000 V. Comparison period: 11.23 s.",
        "\tCheck passed: The plateau duration for RPLA.12L1.RCBH11.L1B2 STATUS.I_REF signal is 11.48 s, which is greater than the minimum required duration of 10.00 s.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.00798 V compared to reference, which is within the threshold of ±0.04000 V. Comparison period: 11.23 s.",
        "PC current decay check (positive cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS maximum deviation is 0.0143 A compared to reference, which is within the threshold of ±0.5500 A. Comparison period: 168.22 s.",
        "PC voltage decay check (positive cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.0023 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 168.22 s.",
        "PC current decay check (negative cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.I_MEAS maximum deviation is 0.0147 A compared to reference, which is within the threshold of ±0.5500 A. Comparison period: 168.22 s.",
        "PC voltage decay check (negative cycle)",
        "\tCheck passed: Target current is 10.00 A, which is equal to the reference target current of 10.00 A.",
        "\tCheck passed: The RPLA.12L1.RCBH11.L1B2 STATUS.V_MEAS maximum deviation is 0.0024 V compared to reference, which is within the threshold of ±0.1000 V. Comparison period: 168.22 s.",
        "Earth current check",
        "\tCheck passed: The values of RPLA.12L1.RCBH11.L1B2 I_EARTH_PCNT are within the margin of 0.0000 to 10.00 %.",
        "\tCheck passed: The values of RPLA.12L1.RCBH11.L1B2 I_EARTH_PCNT are within the margin of 0.0000 to 10.00 %.",
        "\tCheck passed: The values of RPLA.12L1.RCBH11.L1B2 I_EARTH_PCNT are within the margin of 0.0000 to 10.00 %.",
    ]
    assert common.get_log_strings(analysis_class) == expected_logs
