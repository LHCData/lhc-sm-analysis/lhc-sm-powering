import logging
import os
import pathlib

import pandas as pd
import pytest
from lhcsmapi.api.analysis import AnalysisError
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time
from nxcals.spark_session_builder import get_or_create

from lhcsmpowering import analyses

resources_dir = pathlib.Path(__file__).parent / "resources"

csv_file_pattern = os.getenv("CSV_FILE_PATTERN", "*.csv")

if len(list(resources_dir.glob(csv_file_pattern))) == 0:
    raise FileNotFoundError(f"CSV file with given pattern {csv_file_pattern} not found in {resources_dir}")

test_df = pd.concat([pd.read_csv(csv_file) for csv_file in resources_dir.glob(csv_file_pattern)], ignore_index=True)


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end", "expected_result", "captured_output"),
    [
        (
            row["testName"],
            row["systemName"],
            row["campaign"],
            row["executionStartTime"],
            row["executionEndTime"],
            row["validatedAnalysisResult"],
            row["capturedOutput"],
        )
        for _, row in test_df.iterrows()
    ],
)
def test_validation(
    hwc_test: str,
    circuit_name: str,
    campaign: str,
    t_start: str,
    t_end: str,
    expected_result: bool,
    captured_output: str,
):
    """
    Test the analysis of the powering tests

    Note: Exceptions are not caught because we assume that there should not be any (besides AnalysisError)
    If there are, the test should fail.
    """
    # arrange
    test_parameters = analyses.get_analysis_test_input_parameters(
        signal_metadata.get_hwc_summary(), hwc_test, circuit_name, campaign, t_start, t_end
    )
    spark = get_or_create()

    # act
    analysis = analyses.get_analysis_instance("", test_parameters)
    analysis.set_spark(spark)
    try:
        analysis.query()
        analysis.analyze()
        res = analysis.get_analysis_output()
    except AnalysisError:
        res = False

    # assert
    assert res == expected_result, (
        f"Test failed for class {type(analysis).__name__} with {test_parameters!r}, "
        f"the expected result was {expected_result} but the actual result was {res}. "
        "\nNotebook parameters: hwc_test, circuit_name, campaign, t_start, t_end = "
        f"{(hwc_test, circuit_name, campaign, Time.to_string_short(t_start), Time.to_string_short(t_end))}"
        f"\nOld captured output: {captured_output}."
        f"\nNew captured output: {''.join([logging.Formatter().format(log) for log in analysis.get_logs()])}"
    )
