import pathlib

import papermill
import pytest

from lhcsmpowering import analyses


@pytest.mark.parametrize("notebook_path", analyses.get_notebook_paths())
def test_uncommented_input_parameters(notebook_path: pathlib.Path):
    assert papermill.inspect_notebook(notebook_path) == {}


@pytest.mark.parametrize(
    ("hwc_test", "circuit_name", "campaign", "t_start", "t_end"),
    [
        # 60A
        (
            "PCC.1",
            "RCBH11.L1B2",
            "Recommissioning 2023/2024",
            "2024-02-15 16:37:14.227000000",
            "2024-02-15 16:53:56.139000000",
        ),
        (
            "PNO.d1",
            "RCBH11.L1B2",
            "Recommissioning 2022/2023",
            "2023-03-08 10:57:38.151000000",
            "2023-03-08 11:14:21.103000000",
        ),
        (
            "PNO.a1",
            "RCBH11.L1B2",
            "Recommissioning 2023/2024",
            "2024-02-15 20:56:47.691000000",
            "2024-02-15 21:18:35.643000000",
        ),
        # 80-120A
        (
            "PCC.1",
            "RCBYVS5.L2B2",
            "Recommissioning post LS2",
            "2021-03-23 20:25:40.075000000",
            "2021-03-23 20:38:04.455000000",
        ),
        (
            "PIC2 POWERING FAILURE",
            "RCBCH10.L4B2",
            "Recommissioning 2022/2023",
            "2023-03-06 17:07:33.141000000",
            "2023-03-06 17:09:25.721000000",
        ),
        (
            "PNO.d1",
            "RCSX3.R2",
            "Recommissioning 2022/2023",
            "2023-03-20 20:52:08.681000000",
            "2023-03-20 21:05:50.612000000",
        ),
        (
            "PNO.a1",
            "RCBCH8.L8B2",
            "Recommissioning post LS2",
            "2021-02-02 07:41:42.461000",
            "2021-02-02 08:04:08.114000",
        ),
        # 600A
        (
            "PCC.5",
            "RCBXH1.R2",
            "Recommissioning 2022/2023",
            "2023-03-20 16:33:57.179000000",
            "2023-03-20 17:07:37.417000000",
        ),
        ("PCC.5", "RU.L4", "Recommissioning 2023/2024", "2024-02-15 13:50:08.462", "2024-02-15 14:52:43.740"),
        (
            "PIC2 CIRCUIT QUENCH VIA QPS",
            "RSF1.A56B1",
            "Recommissioning 2021/2022",
            "2022-02-04 19:31:00.096000000",
            "2022-02-04 19:47:21.747000000",
        ),
        (
            "PIC2 POWERING FAILURE",
            "RCBXH1.L1",
            "Recommissioning 2022/2023",
            "2023-03-14 16:24:55.732000000",
            "2023-03-14 16:27:50.238000000",
        ),
        (
            "PNO.d3",
            "RQT12.L6B1",
            "Recommissioning 2023/2024",
            "2024-02-13 19:11:23.407000",
            "2024-02-13 19:50:00.866000",
        ),
        (
            "PIC2 FAST ABORT REQ VIA PIC",
            "RU.L4",
            "Recommissioning 2022/2023",
            "2023-03-10 14:18:40.591000000",
            "2023-03-10 14:27:55.704000000",
        ),
        (
            "PNO.a3",
            "RQT12.L6B1",
            "Recommissioning 2023/2024",
            "2024-02-15 16:21:14.764000",
            "2024-02-15 17:36:23.610000",
        ),
        (
            "PLI3.b1",
            "RQTD.A12B2",
            "Recommissioning 2022/2023",
            "2023-03-21 06:58:49.227000000",
            "2023-03-21 07:25:18.304000000",
        ),
        ("PNO.b1", "RCD.A67B1", "Recommissioning post LS2", "2021-06-25 20:52:01.936000", "2021-06-25 21:14:51.765000"),
        # IPD
        (
            "PIC2 CIRCUIT QUENCH VIA QPS",
            "RD2.L1",
            "Recommissioning 2022/2023",
            "2023-03-17 20:38:55.008000000",
            "2023-03-17 21:13:35.949000000",
        ),
        (
            "PIC2 POWERING FAILURE",
            "RD1.L2",
            "Recommissioning 2022/2023",
            "2023-03-20 15:44:24.071000000",
            "2023-03-20 15:46:39.253000000",
        ),
        ("PLI1.c2", "RD1.L2", "Recommissioning post LS2", "2021-04-02 07:32:56.911", "2021-04-02 07:40:51.689"),
        (
            "PNO.a8",
            "RD1.R2",
            "Recommissioning 2022/2023",
            "2023-03-21 13:17:09.869000000",
            "2023-03-21 13:35:03.698000000",
        ),
        # IPQ
        (
            "PIC2 CIRCUIT QUENCH VIA QPS",
            "RQ5.L8",
            "Recommissioning 2022/2023",
            "2023-08-25 07:31:10.073000000",
            "2023-08-25 08:03:33.250000000",
        ),
        (
            "PIC2 POWERING FAILURE",
            "RQ4.L6",
            "Recommissioning 2022/2023",
            "2023-03-17 12:28:11.350000000",
            "2023-03-17 12:32:20.175000000",
        ),
        (
            "PLI1.c3",
            "RQ6.R1",
            "Recommissioning post LS2",
            "2021-03-30 19:48:38.602000000",
            "2021-03-30 20:05:55.027000000",
        ),
        (
            "PLI2.e3",
            "RQ10.R6",
            "Recommissioning 2023/2024",
            "2024-02-28 09:47:33.125000000",
            "2024-02-28 10:12:50.512000000",
        ),
        (
            "PIC2 FAST ABORT REQ VIA PIC",
            "RQ4.R1",
            "Recommissioning 2022/2023",
            "2023-03-18 10:41:54.722000000",
            "2023-03-18 10:49:04.437000000",
        ),
        # IT
        (
            "PIC2 POWERING FAILURE",
            "RQX.L1",
            "Recommissioning 2022/2023",
            "2023-03-17 13:07:21.901000000",
            "2023-03-17 13:22:55.701000000",
        ),
        # RQ
        (
            "PIC2 POWERING FAILURE",
            "RQD.A12",
            "Recommissioning 2022/2023",
            "2023-03-16 19:31:50.706000000",
            "2023-03-16 19:35:41.762000000",
        ),
    ],
)
def test_notebook(hwc_test: str, circuit_name: str, campaign: str, t_start: str, t_end: str):
    # arrange
    notebook_path = analyses.get_notebook_path(hwc_test, circuit_name)
    report_path = pathlib.Path(__file__).parent.parent.parent / "reports" / f"report_{notebook_path.name}"
    notebook_parameters = {
        "hwc_test": hwc_test,
        "circuit_name": circuit_name,
        "campaign": campaign,
        "t_start": t_start,
        "t_end": t_end,
        "expert_decision": "UNDEFINED",
        "generate_report": False,
    }

    # act
    papermill.execute_notebook(notebook_path, report_path, notebook_parameters)
