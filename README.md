# LHC Hardware Commissioning Powering Test Analysis

This repository contains automated analyses of the powering tests conducted on the Large Hadron Collider's (LHC) superconducting circuits. These tests are crucial for ensuring the reliable operation of the LHC's magnet and interlock systems.

This library provides both analysis classes, able to provide an automated test result with meaningful logs for each check performed, as well as notebooks providing a more visual analysis.

## Getting Started

Please proceed to follow the [lhc-sm-api Getting Started procedure](https://sigmon.docs.cern.ch/getting-started/). Then, install the package using the following command:

```console
pip install lhcsmpowering
```

## Supported Analyses

The analyses below are sorted by execution order.

### For 60A circuits

| Test name                                              | Responsibility | Signatures | Automated |
|--------------------------------------------------------|----------------|------------|-----------|
| [PCC.1](src/lhcsmpowering/analyses/pcc/pcc_1/on_60a)   | PO             | PO         | ✅         |
| [PNO.d1](src/lhcsmpowering/analyses/pno/pno_d1/on_60a) | MP3, PO        | MP3        | ✅         |
| [PNO.a1](src/lhcsmpowering/analyses/pno/pno_a1)        | MP3            | MP3        | ✅         |

### For 80-120A circuits

| Test name                                                                                 | Responsibility | Signatures | Automated |
|-------------------------------------------------------------------------------------------|----------------|------------|-----------|
| [PCC.1](src/lhcsmpowering/analyses/pcc/pcc_1/on_80_120a)                                  | PO             | PO         | ✅         |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_80_120a) | MI             | MI         | ✅         |
| [PNO.d1](src/lhcsmpowering/analyses/pno/pno_d1/on_80_120a)                                | MP3, PO        | MP3        | ✅         |
| [PNO.a1](src/lhcsmpowering/analyses/pno/pno_a1)                                           | MP3            | MP3        | ✅         |

### For 600A circuits

| Test name                                                                                          | Responsibility | Signatures | Automated |
|----------------------------------------------------------------------------------------------------|----------------|------------|-----------|
| [PCC.5](src/lhcsmpowering/analyses/pcc/pcc_5)                                                      | PO             | PO         | ✅         |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_600a_and_ipd)     | MI             | MI         | ✅         |
| [PIC2 CIRCUIT QUENCH VIA QPS](src/lhcsmpowering/analyses/pic2/pic2_circuit_quench_via_qps/on_600a) | MI, MP3        | MI, MP3    | ✅         |
| [PIC2 FAST ABORT REQ VIA PIC](src/lhcsmpowering/analyses/pic2/pic2_fast_abort_req_via_pic/on_600a) | MI             | MI         | ✅         |
| PCS                                                                                                | MP3            | MP3        |           |
| [PLI3.b1 (EE circuits only)](src/lhcsmpowering/analyses/pli3/pli3_b1)                              | MP3            | MP3        | ✅         |
| [PNO.d3](src/lhcsmpowering/analyses/pno/pno_d3)                                                    | MP3, PO        | MP3        | ✅         |
| [PNO.b1 (EE circuits only)](src/lhcsmpowering/analyses/pno/pno_b1)                                 | MP3            | MP3        | ✅         |
| PNO.a3                                                                                             | MP3            | MP3        |           |
| PNO.x1                                                                                             | MP3            | MP3        |           |
| PNO.x2                                                                                             | MP3            | MP3        |           |
| PNO.x3                                                                                             | MP3            | MP3        |           |

### For IPD circuits

| Test name                                                                                         | Responsibility | Signatures | Automated    |
|---------------------------------------------------------------------------------------------------|----------------|------------|--------------|
| PCC.3                                                                                             | PO             | PO         |              |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_600a_and_ipd)    | MI             | MI         | ✅            |
| [PIC2 CIRCUIT QUENCH VIA QPS](src/lhcsmpowering/analyses/pic2/pic2_circuit_quench_via_qps/on_ipd) | MI, MP3        | MI, MP3    | ✅ (only MP3) |
| PIC2 FAST ABORT REQ VIA PIC                                                                       | MI             | MI         |              |
| [PLI1.c2](src/lhcsmpowering/analyses/pli1/pli1_c2)                                                | MP3, PO        | MP3        | ✅            |
| PLI2.f2                                                                                           | MP3            | MP3        |              |
| PLI3.c5                                                                                           | MP3, PO        | MP3, PO    |              |
| [PNO.a8](src/lhcsmpowering/analyses/pno/pno_a8)                                                   | MP3            | MP3        | ✅            |
| PNO.c6                                                                                            | MP3, PO        | MP3, PO    |              |

### For IPQ circuits

| Test name                                                                                         | Responsibility | Signatures | Automated        |
|---------------------------------------------------------------------------------------------------|----------------|------------|------------------|
| PCC.4                                                                                             | PO             | PO         |                  |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_ipq)             | MI             | MI         | ✅                |
| [PIC2 CIRCUIT QUENCH VIA QPS](src/lhcsmpowering/analyses/pic2/pic2_circuit_quench_via_qps/on_ipq) | MI, MP3        | MI, MP3    | ✅                |
| [PIC2 FAST ABORT REQ VIA PIC](src/lhcsmpowering/analyses/pic2/pic2_fast_abort_req_via_pic/on_ipq) | MI             | MI         | ✅                |
| [PLI1.c3](src/lhcsmpowering/analyses/pli1/pli1_c3)                                                | MP3, PO        | MP3, PO    | Approval pending |
| PLI2.f3                                                                                           | MP3            | MP3        |                  |
| [PLI2.e3](src/lhcsmpowering/analyses/pli2/pli2_e3)                                                | MP3            | MP3        | ✅                |
| PNO.a7                                                                                            | MP3            | MP3        |                  |
| PNO.c4                                                                                            | MP3, PO        | MP3, PO    |                  |

### For IT circuits

| Test name                                                                            | Responsibility | Signatures | Automated |
|--------------------------------------------------------------------------------------|----------------|------------|-----------|
| PCC.T4                                                                               | PO             | PO         |           |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_it) | MI             | MI         | ✅         |
| PIC2 CIRCUIT QUENCH VIA QPS                                                          | MI, MP3        | MI, MP3    |           |
| PIC2 FAST ABORT REQ VIA PIC                                                          | MI             | MI         |           |
| PNO.d12                                                                              | PO             | PO         |           |
| PNO.d13                                                                              | PO             | PO         |           |
| PLI3.f6                                                                              | MP3, PO        | MP3, PO    |           |
| PNO.d14                                                                              | MP3, PO        | MP3, PO    |           |
| PNO.d15                                                                              | PO             | PO         |           |
| PNO.a9                                                                               | MP3, PO        | MP3, PO    |           |
| PNO.d16                                                                              | MP3, PO        | MP3, PO    |           |
| PNO.d17                                                                              | MP3, PO        | MP3, PO    |           |

### For RQ circuits

| Test name                                                                            | Responsibility | Signatures | Automated |
|--------------------------------------------------------------------------------------|----------------|------------|-----------|
| PCC.3                                                                                | PO             | PO         |           |
| [PIC2 POWERING FAILURE](src/lhcsmpowering/analyses/pic2/pic2_powering_failure/on_rq) | MI             | MI         | ✅         |
| PIC2 CIRCUIT QUENCH VIA QPS                                                          | MI, MP3        | MI, MP3    |           |
| PIC2 FAST ABORT REQ VIA PIC                                                          | MI             | MI         |           |
| PLI1.b3                                                                              | MP3, PO        | MP3, PO    |           |
| PLI1.d2                                                                              | MP3, PO        | MP3, PO    |           |
| PLI2.s1                                                                              | MP3            | MP3        |           |
| PLI2.b3                                                                              | MP3, PO        | MP3, PO    |           |
| PLI2.e2                                                                              | PO             | PO         |           |
| PLI2.f1                                                                              | MP3, PO        | MP3, PO    |           |
| PLIM.b3                                                                              | MP3, PO        | MP3, PO    |           |
| PLIS.s2                                                                              | MP3            | MP3        |           |
| PLI3.a5                                                                              | MP3            | MP3        |           |
| PLI3.b3                                                                              | MP3, PO        | MP3, PO    |           |
| PNO.b3                                                                               | MP3, PO        | MP3, PO    |           |
| PNO.a6                                                                               | MP3            | MP3        |           |

### For RB circuits

| Test name                   | Responsibility | Signatures | Automated |
|-----------------------------|----------------|------------|-----------|
| PCC.2                       | PO             | PO         |           |
| PIC2 POWERING FAILURE       | MI             | MI         |           |
| PIC2 CIRCUIT QUENCH VIA QPS | MI, MP3        | MI, MP3    |           |
| PIC2 FAST ABORT REQ VIA PIC | MI             | MI         |           |
| PLI1.a2                     | MP3            | MP3        |           |
| PLI1.b2                     | MP3, PO        | MP3, PO    |           |
| PLI1.d2                     | MP3, PO        | MP3, PO    |           |
| PLI2.s1                     | MP3            | MP3        |           |
| PLI2.b2                     | MP3, PO        | MP3, PO    |           |
| PLI2.e2                     | PO             | PO         |           |
| PLI2.f1                     | MP3, PO        | MP3, PO    |           |
| PLIM.b2                     | MP3, PO        | MP3, PO    |           |
| PLIS.s2                     | MP3            | MP3        |           |
| PLI3.a5                     | MP3            | MP3        |           |
| PLI3.d2                     | MP3, PO        | MP3, PO    |           |
| PNO.b2                      | MP3, PO        | MP3, PO    |           |
| PNO.a6                      | MP3            | MP3        |           |
