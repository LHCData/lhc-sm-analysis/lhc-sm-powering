from __future__ import annotations

import logging

import pandas as pd

from lhcsmpowering import processing

# Examples of usages are provided in the unit tests, which can be found in the test/unit folder,
# in the test_digital_checks.py file.


def check_signal_changes_to(
    signal: pd.Series,
    change_to: bool,
    timestamp: int | None,
    time_window: processing.TimeWindow,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """
    Check if the given signal changes to the given value within a timeslot around the reference_timestamp.
    Is equal to the following eDSL assertion:
    assertThat(<signal>).changesTo(<change_to>)
                        .startingAt(<relative_timestamp + time_window[0]>)
                        .endingAt(<relative_timestamp + time_window[1]>)

    Args:
        signal: The signal seriesto check.
        change_to: The value after change.
        timestamp: The reference timestamp for the analysis.
        time_window: (time_window_start, time_window_end) in nanoseconds
        log: The logger object for logging messages.

    Returns:
        True if the check passes (the signal changes to the desired value in the designated time window),
        False otherwise
    """
    if timestamp is None:
        log.error("Check failed: Reference timestamp is None.")
        return False

    time_window_start, time_window_end = (time_window[0] + timestamp, time_window[1] + timestamp)
    count = processing.count_changes(signal, time_window_start, time_window_end, change_to)

    if count == 0:
        log.error(
            f"Check failed: {signal.name} has not changed to {change_to} "
            f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
            f"relative to the reference timestamp {timestamp}."
        )
        return False

    log.info(
        f"Check passed: {signal.name} changed to {change_to} "
        f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
        f"relative to the reference timestamp {timestamp}."
    )
    return True


def check_signal_changes_once_to(
    signal: pd.Series,
    change_to: bool,
    timestamp: int | None,
    time_window: processing.TimeWindow,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """
    Check if the given signal changes once to the given value within a timeslot around the reference_timestamp.
    Is equal to the following eDSL assertion:
    assertThat(<signal>).changesOnceTo(<change_to>)
    .starting(<time_slot[0]>).before(<relative_timestamp>).endingAt(<relative_timestamp + time_slot[1]>)

    Args:
        signal: The signal series to analyze.
        change_to: The value after change.
        timestamp: The reference timestamp for this analysis.
        time_window: (time_window_start, time_window_end) in nanoseconds.
        log: The logger object for logging messages.

    Returns:
        True if the check passes (the signal changes just once to the desired value inside the designated time window),
        False otherwise
    """
    if timestamp is None:
        log.error("Reference timestamp is None.")
        return False

    time_window_start, time_window_end = (time_window[0] + timestamp, time_window[1] + timestamp)
    count = processing.count_changes(signal, time_window_start, time_window_end, change_to)

    if count != 1:
        log.error(
            f"Check failed: {signal.name} was expected to change once to {change_to} "
            f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
            f"relative to the reference timestamp {timestamp}, "
            f"but it was found {count} times."
        )
        return False

    log.info(
        f"Check passed: {signal.name} changed once to {change_to} "
        f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
        f"relative to the reference timestamp {timestamp}."
    )
    return True


def check_signal_between_two_timestamps_remains_equal_to(
    signal: pd.Series,
    value: bool,
    time_window_start: int | None,
    time_window_end: int | None,
    log: logging.Logger | logging.LoggerAdapter,
    start_offset: int = 0,
    end_offset: int = 0,
) -> bool:
    """
    Check if the given signal is strictly equal to the given value within a timeslot around the timestamp,
    i.e. the signal last value before the start of the time window is equal to the given value,
    and there are no changes to the opposite value in the time window.

    Args:
        signal: The signal series for this analysis.
        value: The expected value of the state for the signal.
        time_window_start: The start of the time window in nanoseconds.
        time_window_end: The end of the time window in nanoseconds.
        log: The logger object for logging messages.
        start_offset: The offset in nanoseconds to apply to the start of the time window (can be negative).
        end_offset: The offset in nanoseconds to apply to the end of the time window (can be negative).

    Returns:
        True if the check passes, False otherwise
    """
    if signal.empty:
        log.error(f"Check failed: {signal.name} was expected to remains equals to {value} but the signal is empty.")
        return False

    if time_window_start is None or time_window_end is None:
        if time_window_start is None:
            log.error("Check failed: Time window start is None.")
        if time_window_end is None:
            log.error("Check failed: Time window end is None.")
        return False

    offset_start_string = "" if start_offset == 0 else f" offsetted by {start_offset:,} ns"
    offset_end_string = "" if end_offset == 0 else f" offsetted by {end_offset:,} ns"

    values_before_time_window = signal.loc[: time_window_start + start_offset]
    if values_before_time_window.empty:
        log.error(
            f"Check failed: The last recorded value of {signal.name} before the start of the time window "
            f"was expected to be {value}, but no data was found before {time_window_start}{offset_start_string}."
        )
        return False

    last_value_before_time_window = values_before_time_window.iloc[-1]
    if last_value_before_time_window != value:
        log.error(
            f"Check failed: the last recorded value of {signal.name} before the start of the time window "
            f"was expected to be {value}, "
            f"but it was recorded as {not value} before {time_window_start}{offset_start_string}."
        )
        return False

    points_with_opposite_value = processing.count_points_with_value(
        signal, not value, time_window_start + start_offset, time_window_end + end_offset
    )

    if points_with_opposite_value != 0:
        if time_window_start == time_window_end:
            log.error(
                f"Check failed: {signal.name} was expected to only have {value} values "
                f"within the {start_offset:,} to {end_offset:,} nanoseconds time window "
                f"relative to the reference timestamp {time_window_start}, "
                f"but {not value} values were found {points_with_opposite_value} times."
            )
        else:
            log.error(
                f"Check failed: {signal.name} was expected to only have {value} values "
                f"between {time_window_start}{offset_start_string} and {time_window_end}{offset_end_string}, "
                f"but {not value} values were found {points_with_opposite_value} times."
            )
        return False

    if time_window_start == time_window_end:
        log.info(
            f"Check passed: {signal.name} remained equal to {value} "
            f"within the {start_offset:,} to {end_offset:,} nanoseconds time window "
            f"relative to the reference timestamp {time_window_start}."
        )
    else:
        log.info(
            f"Check passed: {signal.name} remained equal to {value} "
            f"between {time_window_start}{offset_start_string} and {time_window_end}{offset_end_string}, as expected."
        )
    return True


def check_signal_remains_equal_to(
    signal: pd.Series,
    value: bool,
    timestamp: int | None,
    time_window: processing.TimeWindow,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """
    Check if the given signal is strictly equal to the given value within a timeslot around the timestamp,
    i.e. the signal is equal to the given value from the start and to the end of the time window.
    Is equal to the following eDSL assertion with 2 additional checks:
    assertThat(<signal>).isEqualTo(<value>)
    .starting(<time_window[0]>).before(<timestamp>).ending(<time_window[1]>).after(<timestamp>)

    Args:
        signal: The signal series for this analysis.
        value: The expected value of the state for the signal.
        timestamp: The reference timestamp for this analysis.
        time_window: (time_window_start, time_window_end) in nanoseconds.
        log: The logger object for logging messages.

    Returns:
        True if the check passes (the signal is equal to the desired value in the interval around
        the timestamp), False otherwise
    """

    return check_signal_between_two_timestamps_remains_equal_to(
        signal, value, timestamp, timestamp, log, time_window[0], time_window[1]
    )
