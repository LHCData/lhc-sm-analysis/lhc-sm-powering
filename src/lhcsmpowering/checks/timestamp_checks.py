from __future__ import annotations

import logging
from collections.abc import Mapping

import pandas as pd
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time

# Examples of usages are provided in the unit tests, which can be found in the test/unit folder,
# in the test_timestamp_checks.py file.


def check_qps_pm_data_headers_timestamps_gap(
    timestamp_qps_a: int | None,
    timestamp_qps_b: int | None,
    log: logging.Logger | logging.LoggerAdapter,
    expected_gap: int = 1_000_000,
) -> bool:
    if timestamp_qps_a is None and timestamp_qps_b is None:
        log.error("Check failed: QPS PM timestamps are None.")
        return False

    if timestamp_qps_a is None:
        log.error("Check failed: QPS PM A timestamp is None.")
        return False

    if timestamp_qps_b is None:
        log.error("Check failed: QPS PM B timestamp is None.")
        return False

    if abs(timestamp_qps_a - timestamp_qps_b) != expected_gap:
        log.error(
            f"Check failed: QPS PM timestamps are not 1ms apart. "
            f"Timestamps: QPS A: {Time.to_string_short(timestamp_qps_a)} ({timestamp_qps_a}), "
            f"QPS B: {Time.to_string_short(timestamp_qps_b)} ({timestamp_qps_b})."
        )
        return False

    log.info(f"Check passed: QPS PM timestamps are {expected_gap / 1e6} ms apart.")
    return True


def check_timestamps_are_within_margin(
    dict_timestamps: Mapping[str, int | None], margin: int, log: logging.Logger | logging.LoggerAdapter
) -> bool:
    """Check that timestamps are within a margin

    Args:
        dict_timestamps: Dictionary containing as keys an id of the timestamp, and as a value the actual timestamps.
        margin: The margin in nanoseconds.
        log: Logger object for logging messages.

    Returns:
        bool: True if all the timestamps are within the margin, False otherwise.
    """
    # check if any of the timestamps are None
    if None in dict_timestamps.values():
        none_timestamps = [
            timestamp_name for timestamp_name, timestamp_value in dict_timestamps.items() if timestamp_value is None
        ]
        log.error(f"Check failed: No timestamps found for {', '.join(none_timestamps)}.")
        return False

    filtered_dict_timestamps = {  # needed for mypy
        timestamp_name: timestamp_value
        for timestamp_name, timestamp_value in dict_timestamps.items()
        if timestamp_value is not None
    }
    if max(filtered_dict_timestamps.values()) - min(filtered_dict_timestamps.values()) > margin:
        timestamp_string = ", ".join(
            [
                f"{timestamp_name}: {timestamp_value}"
                for timestamp_name, timestamp_value in sorted(filtered_dict_timestamps.items(), key=lambda x: x[1])
            ]
        )

        log.error(
            f"Check failed: Timestamps are not within the margin of {margin:,} ns. Timestamps: {timestamp_string}."
        )
        return False

    log.info(f"Check passed: Timestamps are within the margin of {margin:,} ns.")
    return True


def check_timestamp_precedence(
    first_label: str,
    first_timestamp: int | None,
    second_label: str,
    second_timestamp: int | None,
    log: logging.Logger | logging.LoggerAdapter,
    max_allowed_timestamp_gap: int | None = None,
) -> bool:
    """Check if the first timestamp precedes the second timestamp by at most max_allowed_timestamp_gap.

    Args:
        first_label: The label for the first timestamp (e.g., "PIC").
        second_label: The label for the second timestamp (e.g., "FGC").
        first_timestamp: The first timestamp.
        second_timestamp: The second timestamp.
        max_allowed_timestamp_gap: The maximum allowed gap in nanoseconds.
        log: The logger object for logging messages.

    Returns:
        bool: True if 0 <= (second_timestamp - first_timestamp) <= max_allowed_timestamp_gap, False otherwise.
    """
    if first_timestamp is None:
        log.error(f"Check failed: No {first_label} timestamp found.")
        return False

    if second_timestamp is None:
        log.error(f"Check failed: No {second_label} timestamp found.")
        return False

    timestamp_gap = second_timestamp - first_timestamp

    if timestamp_gap < 0:
        log.error(
            f"Check failed: {first_label} timestamp was expected to precede {second_label} timestamp, "
            f"but {first_label} timestamp occurred {abs(timestamp_gap) / 1e6:.1f} ms after {second_label} timestamp."
        )
        return False

    if max_allowed_timestamp_gap is not None and timestamp_gap > max_allowed_timestamp_gap:
        log.error(
            f"Check failed: {first_label} timestamp precedes {second_label} timestamp by {timestamp_gap / 1e6:.1f} ms, "
            f"which exceeds the maximum allowed gap of {max_allowed_timestamp_gap / 1e6:.1f} ms. "
        )
        return False

    if max_allowed_timestamp_gap is None:
        log.info(
            f"Check passed: {first_label} timestamp precedes {second_label} timestamp by {timestamp_gap / 1e6:.1f} ms, "
            "as expected."
        )
    else:
        log.info(
            f"Check passed: {first_label} timestamp precedes {second_label} timestamp by {timestamp_gap / 1e6:.1f} ms, "
            f"which is within the allowed gap of {max_allowed_timestamp_gap / 1e6:.1f} ms."
        )
    return True


def check_ee_pm_data_headers_length(
    ee: str, ee_pm_data_headers: pd.DataFrame, expected_length: int, log: logging.Logger | logging.LoggerAdapter
) -> bool:
    """
    Check if the EE PM data headers length is equal to the expected length.
    """
    if len(ee_pm_data_headers) != expected_length:
        log.error(
            f"Check failed: Found {len(ee_pm_data_headers)} PM timestamp{'s' if len(ee_pm_data_headers) > 1 else ''} "
            f"for EE {ee}, expected {expected_length}."
        )
        if expected_length == 0:
            log.warning("The energy extraction system should be checked, as it was fired.")
        return False

    if expected_length == 0:
        log.info(f"Check passed: No PM dumps were found for EE {ee} in the analyzed interval, as expected.")
    else:
        log.info(
            f"Check passed: Found exactly {len(ee_pm_data_headers)} "
            f"PM timestamp{'s' if len(ee_pm_data_headers) > 1 else ''} for EE {ee}, as expected."
        )
    return True


def check_fgc_pm_data_headers_length(
    fgc: str, fgc_pm_data_headers: pd.DataFrame, expected_length: int, log: logging.Logger | logging.LoggerAdapter
) -> bool:
    """
    Check if the FGC PM data headers length is equal to the expected length.
    """
    if len(fgc_pm_data_headers) != expected_length:
        log.error(
            f"Check failed: Found {len(fgc_pm_data_headers)} PM timestamp{'s' if len(fgc_pm_data_headers) > 1 else ''} "
            f"for FGC {fgc}, expected {expected_length}."
        )
        return False

    if expected_length == 0:
        log.info(f"Check passed: No FGC PM dumps were found for {fgc} in the analyzed interval, as expected.")
    else:
        log.info(
            f"Check passed: Found exactly {len(fgc_pm_data_headers)} "
            f"PM timestamp{'s' if len(fgc_pm_data_headers) > 1 else ''} for FGC {fgc}, as expected."
        )
    return True


def check_qps_pm_data_headers_length(
    qps_pm_data_headers: pd.DataFrame, expected_length: int, log: logging.Logger | logging.LoggerAdapter
) -> bool:
    """
    Check if the QPS PM pm data headers length is equal to the expected length.
    """
    if len(qps_pm_data_headers) != expected_length:
        log.error(
            f"Check failed: Found {len(qps_pm_data_headers)} QPS "
            f"PM timestamp{'s' if len(qps_pm_data_headers) > 1 else ''}, expected {expected_length}."
        )
        if expected_length == 0:
            circuit_name = qps_pm_data_headers["source"].iloc[0]
            if signal_metadata.get_generic_circuit_type_for_circuit_name(circuit_name) in (
                signal_metadata.GenericCircuitType.IPD,
                signal_metadata.GenericCircuitType.IPQ,
                signal_metadata.GenericCircuitType.IT,
            ):
                log.warning("The quench heaters should be checked, as they might have been fired.")
        return False
    if expected_length == 0:
        log.info("Check passed: No QPS PM dumps were found in the analyzed interval, as expected.")
    else:
        log.info(
            f"Check passed: Found exactly {len(qps_pm_data_headers)} QPS "
            f"PM timestamp{'s' if len(qps_pm_data_headers) > 1 else ''}, as expected."
        )
    return True
