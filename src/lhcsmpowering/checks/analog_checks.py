from __future__ import annotations

import logging
import math
from itertools import pairwise

import numpy as np
import pandas as pd
from lhcsmapi.Time import Time

# Examples of usages are provided in the unit tests, which can be found in the test/unit folder,
# in the test_analog_checks.py file.


def check_signal_value_is_within_absolute_margin(
    signal: pd.Series,
    timestamp: int | None,
    target_value: float,
    margin: float,
    log: logging.Logger | logging.LoggerAdapter,
    unit: str,
    offset: int = 0,
) -> bool:
    if signal.empty:
        log.error(f"Check failed: No data found for signal {signal.name}.")
        return False

    if timestamp is None:
        log.error(f"Check failed: Reference timestamp not found for signal {signal.name}.")
        return False

    signal = signal.loc[timestamp + offset :]
    if signal.empty:
        log.error(
            f"Check failed: Signal {signal.name} not present {(offset*1e-9):.4} seconds after reference timestamp. "
            f"Reference timestamp: {Time.to_string_short(timestamp)} ({timestamp})."
        )
        return False

    value = signal.iloc[0]
    target_str, [meas_str, margin_str] = format_with_significant_digits(target_value, [value, margin])
    if not (target_value - margin <= value <= target_value + margin):
        log.error(
            f"Check failed: Signal {signal.name} was equal to {meas_str} {unit} "
            f"{(offset*1e-9):.4} seconds after reference timestamp"
            f", which is outside the target of {target_str} ± {margin_str} {unit}. "
            f"Reference timestamp: {Time.to_string_short(timestamp)} ({timestamp})."
        )
        return False

    log.info(
        f"Check passed: Signal {signal.name} was equal to {meas_str} {unit} "
        f"{(offset*1e-9):.4} seconds after reference timestamp"
        f", which is within the target of {target_str} ± {margin_str} {unit}. "
        f"Reference timestamp: {Time.to_string_short(timestamp)} ({timestamp})."
    )
    return True


def check_minimum_plateau_duration(
    device: str,
    signal: pd.Series,
    plateau: tuple[int, int] | tuple[None, None] | None,
    min_required_plateau_duration: int,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """
    Check if the duration of the plateau for a given device is greater than the minimum required duration.

    Args:
        device: The name of the device being checked.
        plateau: The start and end timestamps of the plateau, or None if no plateau is found.
        min_required_plateau_duration: The minimum required duration for the plateau in nanoseconds.
        log: The logger object used for logging messages.

    Returns:
        True if the plateau duration is greater than the minimum required duration, False otherwise.
    """
    if plateau is None or plateau[0] is None or plateau[1] is None:
        log.error(f"Check failed: No plateau found for {device} {signal.name} signal.")
        return False

    plateau_start, plateau_end = plateau
    plateau_duration = plateau_end - plateau_start

    target_str, [meas_str] = format_with_significant_digits(
        min_required_plateau_duration * 1e-9, [plateau_duration * 1e-9]
    )

    if plateau_duration < min_required_plateau_duration:
        log.error(
            f"Check failed: The plateau duration for {device} {signal.name} signal is {meas_str} s, "
            f"which is less than the minimum required duration of {target_str} s."
        )
        return False

    log.info(
        f"Check passed: The plateau duration for {device} {signal.name} signal is {meas_str} s, "
        f"which is greater than the minimum required duration of {target_str} s."
    )
    return True


def check_plateau_values_absolute_deviation(
    device: str,
    signal: pd.Series,
    plateau: tuple[int, int] | tuple[None, None] | None,
    expected_plateau_level: float,
    max_allowed_absolute_deviation: float,
    unit: str,
    log: logging.Logger | logging.LoggerAdapter,
    skip_last_point: bool = False,
) -> bool:
    if plateau is None or plateau[0] is None or plateau[1] is None:
        log.error(f"Check failed: No plateau found for {device} {signal.name} signal.")
        return False

    plateau_start, plateau_end = plateau
    plateau_signal = _get_valid_signal_slice(signal, plateau_start, plateau_end, device, log)
    if plateau_signal is None:
        return False
    if skip_last_point:
        if len(plateau_signal) > 1:
            plateau_signal = plateau_signal[:-1]
        else:
            log.error(
                f"Check failed: No {signal.name} data found for device {device} "
                f"between {Time.to_string_short(plateau_start)} and {Time.to_string_short(plateau_end)}."
            )
            return False

    max_absolute_deviation = (plateau_signal - expected_plateau_level).abs().max()

    target_dev_str, [target_plat_str, meas_dev_str] = format_with_significant_digits(
        max_allowed_absolute_deviation, [expected_plateau_level, max_absolute_deviation]
    )

    if max_absolute_deviation > max_allowed_absolute_deviation:
        log.error(
            f"Check failed: The maximum plateau level deviation for {device} {signal.name} "
            f"signal is {meas_dev_str} {unit}, "
            f"which exceeds the threshold of {target_dev_str} {unit}. "
            f"Expected plateau level: {target_plat_str} {unit}."
        )
        return False

    log.info(
        f"Check passed: The maximum plateau level deviation for {device} {signal.name} "
        f"signal is {meas_dev_str} {unit}, "
        f"which is within the threshold of {target_dev_str} {unit}. "
        f"Expected plateau level: {target_plat_str} {unit}."
    )
    return True


def check_absolute_deviation_wrt_reference(
    device: str,
    signal: pd.Series,
    time_window: tuple[int | None, int | None] | None,
    reference_signal: pd.Series,
    reference_time_window: tuple[int | None, int | None] | None,
    max_allowed_absolute_difference: float,
    unit: str,
    log: logging.Logger | logging.LoggerAdapter,
    skipped_nanoseconds_from_the_start: int = 0,
    skipped_nanoseconds_at_the_end: int = 0,
    align_right: bool = False,
) -> bool:
    """
    Check the absolute deviation of a signal with respect (compared) to a reference signal.

    Args:
        device: The name of the device.
        signal: The signal to be checked.
        time_window: The start and end timestamp of the decay.
        reference_signal: The reference signal.
        reference_time_window: The start and end timestamp of the reference decay.
        max_allowed_absolute_difference: The maximum allowed absolute difference between the signal
                                         start and end compared to the reference signal's start and end.
        unit: The unit of measurement for the signal.
        log: The logger object for logging error and info messages.
        skipped_nanoseconds_from_the_start The number of nanoseconds to skip at the beginning.
        skipped_nanoseconds_at_the_end The number of nanoseconds to skip at the end.
        align_right: If True, the signals are synchronized to the right. Defaults to False.

    Returns:
        True if the absolute deviation is within the allowed threshold, False otherwise.
    """

    if time_window is None:
        log.error(f"Check failed: Time window for device {device} is None.")
        return False

    start, end = time_window
    if start is None:
        log.error(f"Check failed: The start of the comparison interval for device {device} is missing.")
        return False

    if end is None:
        log.error(f"Check failed: The end of the comparison interval for device {device} is missing.")
        return False

    if reference_time_window is None:
        log.error(f"Check failed: Time window for reference device {device} is None.")
        return False

    reference_start, reference_end = reference_time_window
    if reference_start is None:
        log.error(f"Check failed: The start of the comparison interval for reference device {device} is missing.")
        return False

    if reference_end is None:
        log.error(f"Check failed: The end of the comparison interval for reference device {device} is missing.")
        return False

    if signal.empty:
        log.error(f"Check failed: No data found for device {device}.")
        return False

    if reference_signal.empty:
        log.error(f"Check failed: No reference data found for device {device}.")
        return False

    if skipped_nanoseconds_from_the_start > 0:
        start += skipped_nanoseconds_from_the_start
        reference_start += skipped_nanoseconds_from_the_start

    if skipped_nanoseconds_at_the_end > 0:
        end -= skipped_nanoseconds_at_the_end
        reference_end -= skipped_nanoseconds_at_the_end
    min_duration = min(end - start, reference_end - reference_start)

    if align_right:
        start = end - min_duration
        reference_start = reference_end - min_duration

    if signal.empty:
        log.error(f"Check failed: No {signal.name} data found for device {device} for the specified window.")
        return False

    signal = signal.set_axis(signal.index - start)
    reference_signal = reference_signal.set_axis(reference_signal.index - reference_start)

    interpolation = False
    merged_df = pd.concat([signal, reference_signal], axis=1, keys=["signal", "reference_signal"]).sort_index()
    if merged_df.loc[0:min_duration].isnull().values.any():
        interpolation = True
        merged_df = merged_df.interpolate(method="index", limit_area="inside")

    merged_df = merged_df.loc[0:min_duration]
    if merged_df["signal"].isnull().all():
        log.error(f"Check failed: No {signal.name} data found for device {device} for the specified window.")
        return False

    if merged_df["reference_signal"].isnull().all():
        log.error(
            f"Check failed: No reference {reference_signal.name} data found for device {device} "
            "for the specified window."
        )
        return False

    if interpolation:
        log.info("Interpolating missing values in the signal and reference signal data.")

    max_absolute_deviation = float((merged_df["signal"] - merged_df["reference_signal"]).abs().max())
    target_str, [meas_str] = format_with_significant_digits(max_allowed_absolute_difference, [max_absolute_deviation])
    if max_absolute_deviation > max_allowed_absolute_difference:
        log.error(
            f"Check failed: The {device} {signal.name} deviation is {meas_str} {unit} "
            f"compared to reference, which exceeds the threshold of ±{target_str} {unit}."
        )
        return False

    log.info(
        f"Check passed: The {device} {signal.name} maximum deviation is {meas_str} {unit} "
        f"compared to reference, which is within the threshold of ±{target_str} {unit}. "
        f"Comparison period: {min_duration * 1e-09:.2f} s."
    )
    return True


def check_signal_is_within_bounds(
    device: str,
    signal: pd.Series,
    time_window: tuple[int | None, int | None] | None,
    bounds: tuple[float, float],
    unit: str,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """
    Check if a signal is within certain bounds.

    Args:
        device: The name of the device being checked.
        signal: The series containing the signal to be checked.
        time_window: The start and end timestamp to be checked.
        unit: The unit of the data.
        log: The logger object for logging messages.
        bounds: Lower and upper boundary of the allowed value for the signal.

    Returns:
        True if the signal is within the allowed bounds, False otherwise.
    """

    if time_window is None or time_window[0] is None or time_window[1] is None:
        log.error(f"Check failed: No timestamps found for {device}.")
        return False

    if _check_bounds(bounds, log) is False:
        return False

    signal_data = _get_valid_signal_slice(signal, time_window[0], time_window[1], device, log)
    if signal_data is None:
        return False

    upper_bound_str, [measured_max_str] = format_with_significant_digits(bounds[1], [signal_data.max()])
    if signal_data.max() > bounds[1]:
        log.error(
            f"Check failed: The maximum value of {signal_data.name} is {measured_max_str} {unit}, "
            f"which exceeds the threshold of {upper_bound_str} {unit} for device {device}."
        )
        return False

    lower_bound_str, [measured_min_str] = format_with_significant_digits(bounds[0], [signal_data.min()])
    if signal_data.min() < bounds[0]:
        log.error(
            f"Check failed: The minimum value of {signal_data.name} is {measured_min_str} {unit}, "
            f"which is below the threshold of {lower_bound_str} {unit} for device {device}."
        )
        return False

    log.info(
        f"Check passed: The values of {device} {signal_data.name} "
        f"are within the margin of {lower_bound_str} to {upper_bound_str} {unit}."
    )
    return True


def check_target_current_is_equal_to_reference(
    target_current: float, reference_target_current: float, logger: logging.LoggerAdapter
) -> bool:
    ref_target_str, [target_str] = format_with_significant_digits(reference_target_current, [target_current])
    if target_current != reference_target_current:
        logger.error(
            f"Check failed: Target current is {target_str} A, "
            f"which is different from the reference target current of {ref_target_str} A."
        )
        return False

    logger.info(
        f"Check passed: Target current is {target_str} A, "
        f"which is equal to the reference target current of {ref_target_str} A."
    )
    return True


def check_signal_is_within_bounded_region(
    signal: pd.Series, bounds: dict[int | float, tuple[float, float]], logger: logging.LoggerAdapter
) -> bool:
    """
    Checks if the signal is within the parallelogram formed by the upper and lower bounds between
    consecutive timestamps.

    Args:
        signal: The signal values indexed by time (timestamps).
        bounds: The upper and lower bounds indexed by time (timestamps).
        logger: Logger for reporting.
        unit: The unit of the signal for logging purposes.
    Returns:
        bool: True if all signal values are within bounds, False otherwise.
    """
    if signal.empty:
        logger.error(f"Check failed: Signal {signal.name} is empty.")
        return False

    if len(bounds) < 2:
        logger.error("Check failed: At least two bounds are required for the check.")
        return False

    timestamps_and_bounds = sorted(bounds.items())
    result = True

    for (timestamp_start, (start_lower_bound, start_upper_bound)), (
        timestamp_end,
        (end_lower_bound, end_upper_bound),
    ) in pairwise(timestamps_and_bounds):
        if timestamp_start == -math.inf:
            timestamp_start = signal.index[0]
        if timestamp_end == math.inf:
            timestamp_end = signal.index[-1]

        timestamp_start = int(timestamp_start)
        timestamp_end = int(timestamp_end)

        if not (signal.index[0] <= timestamp_start < timestamp_end <= signal.index[-1]):
            logger.error(f"Check failed: Timestamp interval ({timestamp_start}, {timestamp_end}) is invalid.")
            result = False
            continue

        signal_within_timeframe = signal.loc[timestamp_start:timestamp_end]
        if signal_within_timeframe.empty:
            logger.error(
                f"Check failed: No signal {signal.name} data between "
                f"{Time.to_string_short(timestamp_start)} and {Time.to_string_short(timestamp_end)}."
            )
            result = False
            continue

        times = signal_within_timeframe.index

        # Linearly interpolate the bounds at each time point within the timeframe
        upper_bounds = np.linspace(start_upper_bound, end_upper_bound, len(signal_within_timeframe))
        lower_bounds = np.linspace(start_lower_bound, end_lower_bound, len(signal_within_timeframe))

        within_bounds = (signal_within_timeframe >= lower_bounds) & (signal_within_timeframe <= upper_bounds)

        if not within_bounds.all():
            out_of_bounds_times = times[~within_bounds]
            logger.error(
                f"Check failed: Signal {signal.name} is out of bounds at "
                f"{len(out_of_bounds_times)} time points between {Time.to_string_short(timestamp_start)}"
                f" and {Time.to_string_short(timestamp_end)}."
            )
            result = False
            continue

    if result:
        start_time = int(timestamps_and_bounds[0][0] if timestamps_and_bounds[0][0] != -math.inf else signal.index[0])
        end_time = int(timestamps_and_bounds[-1][0] if timestamps_and_bounds[-1][0] != math.inf else signal.index[-1])
        logger.info(
            f"Check passed: Signal {signal.name} is within bounds between "
            f"{Time.to_string_short(start_time)} and {Time.to_string_short(end_time)}."
        )
    return result


def check_average_value_is_within_bounds(
    device: str,
    signal: pd.Series,
    time_window: tuple[int, int] | None,
    bounds: tuple[float, float],
    unit: str,
    log: logging.Logger | logging.LoggerAdapter,
    skipped_nanoseconds_from_the_start: int = 0,
    skipped_nanoseconds_at_the_end: int = 0,
) -> bool:
    """
    Check if the average value of the signal data is between the allowed minimum and maximum margin.

    Args:
        device: The name of the device being checked.
        signal: The pandas Series containing the signal data to be checked.
        time_window: A tuple specifying the start and end timestamps (in nanoseconds) for slicing the signal,
            or None if no valid time window is available.
        bounds: The lower and upper allowed limits for the average signal value.
        unit: The unit of measurement for the signal values.
        log: The logger object for logging messages.
        skipped_nanoseconds_from_the_start: Number of nanoseconds to exclude from the start of the time window.
        skipped_nanoseconds_at_the_end: Number of nanoseconds to exclude from the end of the time window.

    Returns:
        True if the average value of the signal within the specified time window is within bounds, False otherwise.
    """
    if time_window is None:
        log.error(f"Check failed: No timestamps found for {device}.")
        return False

    valid_signal_slice = _get_valid_signal_slice(
        signal,
        time_window[0] + skipped_nanoseconds_from_the_start,
        time_window[1] - skipped_nanoseconds_at_the_end,
        device,
        log,
    )

    if valid_signal_slice is None:
        return False

    return check_value_is_within_bounds(
        device, valid_signal_slice.mean(), f"The average value of {valid_signal_slice.name}", bounds, unit, log
    )


def check_value_is_within_bounds(
    device: str,
    value: float | None,
    name: str,
    bounds: tuple[float, float],
    unit: str,
    log: logging.Logger | logging.LoggerAdapter,
) -> bool:
    """Check if a value is between the lower and upper bound.

    Args:
        device: The name of the device being checked.
        value: The value to be checked.
        name: The name of the value.
        bounds: The lower and upper boundary of the allowed value.
        unit: The unit of the value.
        log: The logger object for logging messages.

    Returns:
        True if lower bound <= value <= upper bound, otherwise false.
    """
    if value is None or math.isnan(value):
        log.error(f"Check failed: {name} is invalid ({value}).")
        return False

    if _check_bounds(bounds, log) is False:
        return False

    upper_bound_str, [value_str, lower_bound_str] = format_with_significant_digits(bounds[1], [value, bounds[0]])

    if value > bounds[1] or value < bounds[0]:
        log.error(
            f"Check failed: {name} is {value_str} {unit}, "
            f"which is outside of the range from {lower_bound_str} to "
            f"{upper_bound_str} {unit} for device {device}."
        )
        return False

    log.info(
        f"Check passed: {name} for {device} is {value_str} {unit}, "
        f"which is within the allowed range from {lower_bound_str} to {upper_bound_str} {unit}."
    )
    return True


def _check_bounds(bounds: tuple[float, float], log: logging.Logger | logging.LoggerAdapter) -> bool:
    lower_bound, upper_bound = bounds

    if math.isnan(lower_bound) or math.isnan(upper_bound):
        log.error(
            f"Check failed: one or more boundaries were NaN. Boundaries were " f"{lower_bound} and {upper_bound}."
        )
        return False

    if lower_bound > upper_bound:
        log.error(
            f"Check failed: lower bound was larger than upper bound. Boundaries were "
            f"{lower_bound} and {upper_bound}."
        )
        return False

    return True


def _get_valid_signal_slice(
    signal: pd.Series,
    start: int | None,
    end: int | None,
    device: str | None,
    log: logging.Logger | logging.LoggerAdapter,
    skipped_nanoseconds: int = 0,
) -> pd.Series | None:
    if start is None or end is None:
        log.error("Check failed: No timestamps found" + ("" if device is None else f" for {device}") + ".")
        return None
    if signal.empty:
        log.error(
            f"Check failed: No {signal.name} data found" + ("" if device is None else f" for device {device}") + "."
        )
        return None

    signal = signal.loc[start + skipped_nanoseconds : end]

    if signal.empty:
        log.error(
            f"Check failed: No {signal.name} data found for device {device} "
            f"between {Time.to_string_short(start)} and {Time.to_string_short(end)}."
        )
        return None

    if signal.isna().any():
        log.error(
            f"Check failed: Signal {signal.name}"
            + ("" if device is None else f" for device {device}")
            + " has NaN values."
        )
        return None

    return signal


def format_with_significant_digits(target: float, measured: list[float], digit_count: int = 4) -> tuple[str, list[str]]:
    """Returns the given numbers as strings (base 10) such that target has digit_count>0 significant
    digits and the numbers in measured have the same number of digits after the decimal place as the
    converted target. For the special case of target being 0, NaN or +/-infinity, digit_count
    digits will be appended after the decimal point.
    """
    if digit_count < 1:
        raise ValueError("Digit count was less than 1.")
    if target == 0.0 or math.isnan(target) or math.isinf(target):
        decimals = digit_count
    else:
        leading_digit_pos = math.floor(math.log10(abs(target)))
        decimals = max(-leading_digit_pos - 1 + digit_count, 0)
    return (f"{target:.{decimals}f}", [f"{m:.{decimals}f}" for m in measured])
