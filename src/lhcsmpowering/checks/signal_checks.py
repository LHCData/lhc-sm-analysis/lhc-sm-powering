from __future__ import annotations

import logging

import pandas as pd

# Examples of usages are provided in the unit tests, which can be found in the test/unit folder,
# in the test_signal_checks.py file.


def check_signal_sampling_interval(
    signal: pd.Series, expected_interval: int, log: logging.Logger | logging.LoggerAdapter
) -> bool:
    """
    Check if the signal has the expected sampling interval.
    The sampling interval is calculated as the smallest difference between two consecutive indices of the signal.
    A warning is issued to the logger if the signal has varying sampling intervals.

    Args:
        signal: The signal to check.
        expected_interval: The expected sampling interval in nanoseconds.
        log: The logger to use for logging.

    Returns:
        True if the signal has the expected sampling interval, False otherwise.
    """
    if len(signal) < 2:
        log.error(f"Check failed: the signal {signal.name} has less than 2 elements.")
        return False

    # Calculate the differences between consecutive indices
    intervals = signal.index.to_series().diff().dropna().astype(int)

    # Check if all intervals are equal
    if not (intervals == intervals.iloc[0]).all():
        log.warning(f"The signal {signal.name} has varying sampling intervals. The smallest interval will be used.")

    interval = intervals.min()
    if interval != expected_interval:
        log.error(
            f"Check failed: the signal {signal.name} has a sampling interval of {interval:,} ns "
            f"instead of an expected sampling interval of {expected_interval:,} ns."
        )
        return False

    log.info(f"Check passed: the signal {signal.name} has a sampling interval of {interval:,} ns.")
    return True
