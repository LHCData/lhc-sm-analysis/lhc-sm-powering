# PCC.1 - POWER CONVERTER CONFIGURATION 4Q

Circuits: 60A

## Introduction

The aim of this test is validate the configuration and the performance of the power converter. It also checks the crowbar activation at low current.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **3 x FGC PM** (class `lhc_self`).

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top level (-`I_PCC`, `I_PCC`, -`I_PCC`) and maintains it for at least `TIME_PCC`.
    - The measured current (`I_MEAS`) during all `TIME_PCC` flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

3. **PC voltage profile consistency check**
    - `I_REF` of a reference\* curve reaches the required flat top level (-`I_PCC`, `I_PCC`, -`I_PCC`) and maintains it for at last `TIME_PCC`.
    - The measured voltage (`V_MEAS`) during all `TIME_PCC` flat tops must be within **±40 mV** of the reference\* curve. Note: A comparison is performed **250 ms** after the beginning of the flat top and lasts until the end of the shorter signal.

4. **PC current decay check (positive cycle)**
    - The measured current (`I_MEAS`) decay **40 ms after the second PM timestamp** and until the moment the current reaches **500 mA** must be within **±1%** of the reference\* decay curve (percent of the nominal current `I_PNO`). Note: If the current doesn’t fully decay down to 500 mA in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay **40 ms after the second PM timestamp** and until the moment the current reaches **500 mA** must be within **±100 mV** of the reference\* decay curve. Note: If the current doesn’t fully decay down to 500 mA in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

6. **PC current decay check (negative cycle)**
    - The measured current (`I_MEAS`) decay **40 ms after the third PM timestamp** and until the moment the current reaches **-500 mA** must be within **±1%** of the reference\* decay curve (percent of the nominal current `I_PNO`). Note: If the current doesn’t fully decay down to -500 mA in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

7. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay **40 ms after the third PM timestamp** and until the moment the current reaches **-500 mA** must be within **±100 mV** of the reference\* decay curve. Note: If the current doesn’t fully decay down to -500 mA in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

8. **Earth current check**
    - The measured percentage earth current (`I_EARTH_PCNT`) during `TIME_PCC` flat tops must be from **0%** to **10%**.

## Source

1. _Test Procedure and Acceptance Criteria for the 60 A Circuits_, EDMS 874724 v.4.5, <https://edms.cern.ch/document/874724/4.5>.
