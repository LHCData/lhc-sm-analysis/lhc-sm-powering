from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

import lhcsmpowering.processing as processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, timestamp_checks


class Pcc1On60AAnalysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):
    NANOSECONDS_AFTER_PM_DECAY_START = 40_000_000  # 40 ms

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers and timestamps check"

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0002  # 0.02% (percent of I_PNO)

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = "PC voltage profile consistency check"
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU = 0.04  # 40 mV (of the reference curve)
    SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_PLATEAU = 250_000_000  # 250 ms

    PC_CURRENT_DECAY_CHECK_POSITIVE_CYCLE_TAG = "PC current decay check (positive cycle)"
    FACTOR_OF_THE_NOMINAL_CURRENT_AT_DECAY = 0.01  # 1% (percent of I_PNO)
    I_MEAS_DECAY_END_POSITIVE_CYCLE = 0.5  # 500mA

    PC_VOLTAGE_DECAY_CHECK_POSITIVE_CYCLE_TAG = "PC voltage decay check (positive cycle)"
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY = 0.1  # 100mV (of the reference curve)

    PC_CURRENT_DECAY_CHECK_NEGATIVE_CYCLE_TAG = "PC current decay check (negative cycle)"
    I_MEAS_DECAY_END_NEGATIVE_CYCLE = -0.5  # -500mA

    PC_VOLTAGE_DECAY_CHECK_NEGATIVE_CYCLE_TAG = "PC voltage decay check (negative cycle)"

    EARTH_CURRENT_CHECK_TAG = "Earth current check"
    I_EARTH_PCNT_ABSOLUTE_BOUNDS = (0, 10)  # % (absolute value)

    def query(self) -> None:
        super().query()

        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        [self.fgc_ref] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.hwc_powering_test_parameters_ref = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )

        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.i_pcc = float(self.hwc_powering_test_parameters[self.fgc]["I_PCC"]["value"])
        self.time_pcc = int(self.hwc_powering_test_parameters[self.fgc]["TIME_PCC"]["value"] * 1e9)

        self.ref_i_pcc = float(self.hwc_powering_test_parameters_ref[self.fgc_ref]["I_PCC"]["value"])

        self.fgc_pm_self = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.fgc_pm_self_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_self, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.fgc_pm_self_ref = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )
        self.fgc_pm_self_data_headers_ref = query.query_pm_data_headers(
            "FGC",
            self.fgc_pm_self_ref,
            self.fgc_ref,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )

        self.timestamp_fgc_pm1 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers, 0)
        self.timestamp_fgc_pm2 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers, 1)
        self.timestamp_fgc_pm3 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers, 2)

        self.timestamp_fgc_pm2_ref = processing.read_timestamp_from_pm_headers_at_index(
            self.fgc_pm_self_data_headers_ref, 1
        )
        self.timestamp_fgc_pm3_ref = processing.read_timestamp_from_pm_headers_at_index(
            self.fgc_pm_self_data_headers_ref, 2
        )

        # FGC PM2
        (self.i_meas_pm2, self.i_ref_pm2, self.v_meas_pm2, self.i_earth_pm2_pcnt) = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_self,
            self.fgc,
            ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS", "I_EARTH_PCNT"],
            self.timestamp_fgc_pm2,
        )
        # FGC PM2 REF
        self.i_meas_pm2_ref, self.i_ref_pm2_ref, self.v_meas_pm2_ref = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_self_ref,
            self.fgc_ref,
            ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS"],
            self.timestamp_fgc_pm2_ref,
        )

        # FGC PM3
        self.i_meas_pm3, self.v_meas_pm3 = query.query_pm_data_signals(
            "FGC", self.fgc_pm_self, self.fgc, ["STATUS.I_MEAS", "STATUS.V_MEAS"], self.timestamp_fgc_pm3
        )
        # FGC PM3 REF
        self.i_meas_pm3_ref, self.v_meas_pm3_ref = query.query_pm_data_signals(
            "FGC", self.fgc_pm_self_ref, self.fgc_ref, ["STATUS.I_MEAS", "STATUS.V_MEAS"], self.timestamp_fgc_pm3_ref
        )

        self.i_ref_pm2_plateaus = processing.get_regions_equal_to(
            self.i_ref_pm2.loc[: self.timestamp_fgc_pm2].abs(), self.i_pcc
        )
        self.i_ref_pm2_plateaus_ref = processing.get_regions_equal_to(
            self.i_ref_pm2_ref.loc[: self.timestamp_fgc_pm2_ref].abs(), self.ref_i_pcc
        )

        self.point_of_required_current_pm2 = (
            processing.detect_first_point_below_threshold(
                self.i_meas_pm2,
                self.timestamp_fgc_pm2 + self.NANOSECONDS_AFTER_PM_DECAY_START,
                self.i_meas_pm2.index[-1],
                self.I_MEAS_DECAY_END_POSITIVE_CYCLE,
            )
            if self.timestamp_fgc_pm2 is not None
            else None
        )
        if self.point_of_required_current_pm2 is None and not self.i_meas_pm2.empty:
            self.point_of_required_current_pm2 = self.i_meas_pm2.index[-1]

        self.point_of_required_current_pm3 = (
            processing.detect_first_point_above_threshold(
                self.i_meas_pm3,
                self.timestamp_fgc_pm3 + self.NANOSECONDS_AFTER_PM_DECAY_START,
                self.i_meas_pm3.index[-1],
                self.I_MEAS_DECAY_END_NEGATIVE_CYCLE,
            )
            if self.timestamp_fgc_pm3 is not None
            else None
        )

        if self.point_of_required_current_pm3 is None and not self.i_meas_pm3.empty:
            self.point_of_required_current_pm3 = self.i_meas_pm3.index[-1]

    def analyze(self) -> None:
        super().analyze()

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_self_data_headers, 3, logger_adapter
                )
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)
        self.pc_current_profile_consistency_check = False
        if len(self.i_ref_pm2_plateaus) < 3:
            logger_adapter.critical("Check failed: Prerequisite not met: less than 3  TIME_PCC plateaus found.")
        else:
            self.pc_current_profile_consistency_check = all(
                [
                    (
                        analog_checks.check_minimum_plateau_duration(
                            self.fgc, self.i_ref_pm2, plateau, self.time_pcc, logger_adapter
                        )
                        and analog_checks.check_plateau_values_absolute_deviation(
                            self.fgc,
                            self.i_meas_pm2,
                            plateau,
                            expected_plateau_level,
                            self.FACTOR_OF_THE_NOMINAL_CURRENT * self.i_pno,
                            "A",
                            logger_adapter,
                        )
                    )
                    for plateau, expected_plateau_level in zip(
                        self.i_ref_pm2_plateaus, (-self.i_pcc, self.i_pcc, -self.i_pcc), strict=False
                    )
                ]
            )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG)
        self.pc_voltage_profile_consistency_check = False
        if self.pc_current_profile_consistency_check is False:
            logger_adapter.critical(
                f"Check failed: Prerequisite not met: {self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG} failed."
            )
        elif len(self.i_ref_pm2_plateaus_ref) < 3:
            logger_adapter.critical(
                "Check failed: Prerequisite not met: less than 3 TIME_PCC plateaus found for I_REF reference."
            )
        else:
            self.pc_voltage_profile_consistency_check = analog_checks.check_target_current_is_equal_to_reference(
                self.i_pcc, self.ref_i_pcc, logger_adapter
            ) and all(
                [
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.i_ref_pm2_ref, plateau, self.time_pcc, logger_adapter
                    )
                    and analog_checks.check_absolute_deviation_wrt_reference(
                        self.fgc,
                        self.v_meas_pm2,
                        plateau,
                        self.v_meas_pm2_ref,
                        reference_plateau,
                        self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU,
                        "V",
                        logger_adapter,
                        skipped_nanoseconds_from_the_start=self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_PLATEAU,
                    )
                    for plateau, reference_plateau in zip(
                        self.i_ref_pm2_plateaus[:3], self.i_ref_pm2_plateaus_ref[:3], strict=False
                    )
                ]
            )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_POSITIVE_CYCLE_TAG)
        self.pc_current_decay_positive_cycle_check = analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.ref_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.i_meas_pm2,
            (self.timestamp_fgc_pm2, self.point_of_required_current_pm2),
            self.i_meas_pm2_ref,
            (self.timestamp_fgc_pm2_ref, self.i_meas_pm2_ref.index[-1]),
            self.FACTOR_OF_THE_NOMINAL_CURRENT_AT_DECAY * self.i_pno,
            "A",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_POSITIVE_CYCLE_TAG)
        self.pc_voltage_decay_positive_cycle_check = analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.ref_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.v_meas_pm2,
            (self.timestamp_fgc_pm2, self.point_of_required_current_pm2),
            self.v_meas_pm2_ref,
            (self.timestamp_fgc_pm2_ref, self.v_meas_pm2_ref.index[-1]),
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY,
            "V",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_NEGATIVE_CYCLE_TAG)
        self.pc_current_decay_negative_cycle_check = analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.ref_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.i_meas_pm3,
            (self.timestamp_fgc_pm3, self.point_of_required_current_pm3),
            self.i_meas_pm3_ref,
            (self.timestamp_fgc_pm3_ref, self.i_meas_pm3_ref.index[-1]),
            self.FACTOR_OF_THE_NOMINAL_CURRENT_AT_DECAY * self.i_pno,
            "A",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_NEGATIVE_CYCLE_TAG)
        self.pc_voltage_decay_negative_cycle_check = analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.ref_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.v_meas_pm3,
            (self.timestamp_fgc_pm3, self.point_of_required_current_pm3),
            self.v_meas_pm3_ref,
            (self.timestamp_fgc_pm3_ref, self.v_meas_pm3_ref.index[-1]),
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY,
            "V",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG)
        self.earth_current_check = all(
            [
                analog_checks.check_signal_is_within_bounds(
                    self.fgc, self.i_earth_pm2_pcnt, plateau, self.I_EARTH_PCNT_ABSOLUTE_BOUNDS, "%", logger_adapter
                )
                for plateau in self.i_ref_pm2_plateaus[:3]
            ]
        )

        self.analysis_output = all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.pc_current_profile_consistency_check,
                self.pc_voltage_profile_consistency_check,
                self.pc_current_decay_positive_cycle_check,
                self.pc_voltage_decay_positive_cycle_check,
                self.pc_current_decay_negative_cycle_check,
                self.pc_voltage_decay_negative_cycle_check,
                self.earth_current_check,
            ]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
