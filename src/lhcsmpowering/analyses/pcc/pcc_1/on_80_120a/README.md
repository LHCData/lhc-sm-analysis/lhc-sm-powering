# PCC.1 - POWER CONVERTER CONFIGURATION 4Q

Circuits: 80-120A

## Introduction

The aim of this test is to validate the configuration and the performance of the power converter. It also checks the crowbar activation at low current.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **3 x FGC PM** (class `lhc_self`).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level (`-I_PCC`, `I_PCC`, `-I_PCC`) and maintains it for at least `TIME_PCC`.
    - The measured current (`I_MEAS`) during all `TIME_PCC` flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

3. **PC voltage profile consistency check (positive cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level (`-I_PCC`, `I_PCC`, `-I_PCC`) and maintains it for at least `TIME_PCC`.
    - The measured voltage (`V_MEAS`) during all `TIME_PCC` flat tops must be within **±50 mV** of the reference\* curve. A comparison is performed **250 ms** after the beginning of the flat top and lasts until the end of the shorter signal.

4. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay **40 ms** (or **250 ms** for an RPMC fgc) after the second PM timestamp until the moment the voltage reaches **-1.2 V** must be within **±500 mV** of the reference\* decay curve. Note: If the voltage does not fully decay down to -1.2 V in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay **40 ms** (or **250 ms** for an RPMC fgc) after the third PM timestamp until the moment the voltage reaches **1.2 V** must be within **±500 mV** of the reference\* decay curve. Note: If the voltage does not fully decay down to 1.2 V in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

6. **Earth current check**
    - The measured percentage earth current (`I_EARTH_PCNT`) during `TIME_PCC` flat tops must be from **0%** to **15%**.

(\*) The reference signal is the last successful test.

## Source

1. _Test Procedure and Acceptance Criteria for the 80 A and 120 A Dipole Corrector Circuits_, EDMS 874722 v.4.4, <https://edms.cern.ch/document/874722/4.4>.
