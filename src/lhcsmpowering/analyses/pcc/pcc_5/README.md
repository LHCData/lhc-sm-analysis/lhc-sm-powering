# PCC.5 – POWER CONVERTER CONFIGURATION 4Q

Circuits: 600A

## Introduction

The aim of this test is to validate the configuration and the performance of the power converter.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **3 x FGC PM** (class `lhc_self`).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level (`-I_PCC`, `I_PCC`, `-I_PCC`, `I_PCC`) and maintains it for at least `TIME_PCC` for the first 3 ones, and for at least `TIME_CROWBAR` for the last one. **Note:**  If the required number of plateaus is not found in the PM data due to limited buffer length, the NXCALS data is queried. In this case, the required plateau duration is reduced by **2 s** to account for the lower signal resolution.
    - The measured current (`I_MEAS`) during all flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter. **Note:** the last point of the last plateau needs to be skipped, as that value might drop before `I_REF`.

3. **PC voltage profile consistency check (positive cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level (`-I_PCC`, `I_PCC`, `-I_PCC`, `I_PCC`) and maintains it for at for at least `TIME_PCC` the first 3 ones, and for at least `TIME_CROWBAR` for the last one.
    **Note:**  If the required number of plateaus is not found in the PM data due to limited buffer length, the NXCALS data is queried. In this case, the required plateau duration is reduced by **2 s** to account for the lower signal resolution.
    - The measured voltage (`V_MEAS`) during all flat tops must be within **±50 mV** (**±200 mV** for RPMC) of the reference\* curve.

4. **PC current decay check (positive cycle)**
    - The measured current (`I_MEAS`) decay from **40 ms after the second FGC PM timestamp** and the moment the current reaches **5 A** must be within **±1%** of the reference\* decay curve (percent of the nominal current `I_PNO`). **Note:** If the current doesn’t fully decay down in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available. Note: This check is not performed for RPLB FGCs, as current decay data is not recorded.

5. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms after the second FGC PM timestamp** and the end of the voltage decay must be within **±500 mV** (**±2 V** for RPMC) of the reference\* decay curve. The end of the voltage decay is defined as the last point before the voltage difference between two consecutive points goes below **-0.1 V**. The start of the detection period is set **60 ms after the second FGC PM timestamp**. If no such point is detected, the last point of the signal will be used.

6. **Earth current check (positive cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during all `TIME_PCC` flat tops and the first `TIME_CROWBAR` flat top must be from **-10%** to **10%** (**0** to **15%** for RPLB).

7. **PC current profile consistency check (negative cycle)**
    - `I_REF` reaches the required flat top level (`-I_PCC`) and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) during `TIME_CROWBAR` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter. Note: the last point needs to be skipped, as that value might drop before `I_REF`.

8. **PC voltage profile consistency check (negative cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level (`-I_PCC`) and maintains it for at last `TIME_CROWBAR`.
    - The measured voltage (`V_MEAS`) during the `TIME_CROWBAR` flat top must be within **±50 mV** (**±200 mV** for RPMC) of the reference\* curve.

9. **PC current decay check (negative cycle)**
    - The measured current (`I_MEAS`) decay from **40 ms after the third FGC PM timestamp** and the moment the current reaches **-5 A** must be within **±1%** of the reference\* decay curve (percent of the nominal current `I_PNO`).
      **Note:** If the current doesn’t fully decay down in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.  
      **Note:** This check is not performed for RPLB FGCs, as current decay data is not recorded.

10. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms after the third FGC PM timestamp** and the end of the voltage decay must be within **±500 mV** (**±2 V** for RPMC) of the reference\* decay curve. The end of the voltage decay is defined as the last point before the voltage difference between two consecutive points goes above **0.1 V**. The start of the detection period is set **60 ms after the third FGC PM timestamp**. If no such point is detected, the last point of the signal will be used.

11. **Earth current check (negative cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `TIME_CROWBAR` flat top must be from **-10%** to **10%** (**0** to **15%** for RPLB).

(\*) The reference signal is the last successful test.
