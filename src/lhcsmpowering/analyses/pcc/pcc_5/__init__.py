from __future__ import annotations

from collections import OrderedDict
from collections.abc import Sequence
from dataclasses import dataclass, fields
from types import MappingProxyType

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

import lhcsmpowering.processing as processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.analyses.commons import DataSource
from lhcsmpowering.checks import analog_checks, timestamp_checks


class Pcc5Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):

    @dataclass(frozen=True)
    class Signals:
        i_meas: pd.Series
        i_ref: pd.Series
        v_meas: pd.Series
        i_earth_pcnt: pd.Series

    @dataclass(frozen=True)
    class FgcData:
        timestamp_fgc: int | None
        signals: dict[DataSource, Pcc5Analysis.Signals]
        plateaus: Sequence[tuple[tuple[int, int], DataSource]]
        end_current_decay: int | None
        end_voltage_decay: int | None

    @dataclass(frozen=True)
    class ReferenceSignals:
        i_meas: pd.Series
        i_ref: pd.Series
        v_meas: pd.Series

    @dataclass(frozen=True)
    class ReferenceFgcData:
        timestamp_fgc: int | None
        signals: dict[DataSource, Pcc5Analysis.ReferenceSignals]
        plateaus: Sequence[tuple[tuple[int, int], DataSource]]

    @dataclass(frozen=True)
    class CycleCheck:
        pc_current_profile_consistency_check: bool
        pc_voltage_profile_consistency_check: bool
        pc_current_decay_check: bool
        pc_voltage_decay_check: bool
        earth_current_check: bool

        def __bool__(self) -> bool:
            return all(getattr(self, field.name) for field in fields(self))

    @dataclass(frozen=True)
    class PlateauCharacteristics:
        level: float
        duration: int

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers and timestamps check"

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC current profile consistency check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC current profile consistency check (negative cycle)",
        }
    )
    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0002  # 0.02 % (percent of I_PNO)
    NXCALS_PLATEAU_DURATION_OFFSET = 2_000_000_000  # 2 s

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC voltage profile consistency check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC voltage profile consistency check (negative cycle)",
        }
    )
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMBA_RPMBB_RPMBD_RPLB = 0.05  # 50 mV
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMC = 0.2  # 200 mV

    PC_CURRENT_DECAY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC current decay check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC current decay check (negative cycle)",
        }
    )
    NANOSECONDS_AFTER_PM_DECAY_START = 40_000_000  # 40 ms
    FACTOR_OF_THE_NOMINAL_CURRENT_AT_DECAY = 0.01  # 1 % (percent of I_PNO)
    I_MEAS_CURRENT_DECAY_END = 5  # 5 A

    PC_VOLTAGE_DECAY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC voltage decay check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC voltage decay check (negative cycle)",
        }
    )
    SKIPPED_FIRST_NANOSECONDS_FOR_SPIKE_DETECTION = 60_000_000  # 60 ms
    SPIKE_DETECTION_THRESHOLD = -0.1  # -0.1 V difference
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB = 0.5  # 500 mV
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMC = 2.0  # 2 V

    EARTH_CURRENT_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "Earth current check (positive cycle)",
            commons.Cycle.NEGATIVE: "Earth current check (negative cycle)",
        }
    )
    I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPLB = (0, 15)  # % (absolute value)
    I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPMC_RPMBA_RPMBB_RPMBD = (-10, 10)  # % (absolute value)

    def query(self) -> None:
        super().query()

        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        [self.fgc_ref] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)

        self.maximum_allowed_absolute_voltage_deviation_at_plateau = (
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMC
            if self.fgc_ref.startswith("RPMC")
            else self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMBA_RPMBB_RPMBD_RPLB
        )
        self.maximum_allowed_absolute_voltage_deviation_at_decay = (
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMC
            if self.fgc_ref.startswith("RPMC")
            else self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB
        )
        self.i_earth_pcnt_absolute_bounds = (
            self.I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPLB
            if self.fgc.startswith("RPLB")
            else self.I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPMC_RPMBA_RPMBB_RPMBD
        )

        self.fgc_pm_self = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        reference_fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.i_pcc = float(self.hwc_powering_test_parameters[self.fgc]["I_PCC"]["value"])
        self.time_crowbar = int(self.hwc_powering_test_parameters[self.fgc]["TIME_CROWBAR"]["value"] * 1e9)
        self.time_pcc = int(self.hwc_powering_test_parameters[self.fgc]["TIME_PCC"]["value"] * 1e9)

        self.reference_hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )
        self.reference_i_pcc = float(self.reference_hwc_powering_test_parameters[self.fgc_ref]["I_PCC"]["value"])

        self.expected_plateau_characteristics_mapping = OrderedDict(
            (
                (
                    commons.Cycle.POSITIVE,
                    [
                        self.PlateauCharacteristics(-self.i_pcc, self.time_pcc),
                        self.PlateauCharacteristics(self.i_pcc, self.time_pcc),
                        self.PlateauCharacteristics(-self.i_pcc, self.time_pcc),
                        self.PlateauCharacteristics(self.i_pcc, self.time_crowbar),
                    ],
                ),
                (commons.Cycle.NEGATIVE, [self.PlateauCharacteristics(-self.i_pcc, self.time_crowbar)]),
            )
        )

        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_self, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.fgc_data = {
            cycle: self._query_fgc_data(
                self.fgc,
                cycle,
                processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, index),
                self.expected_plateau_characteristics_mapping[cycle],
                is_ref=False,
            )
            for cycle, index in ((commons.Cycle.POSITIVE, 1), (commons.Cycle.NEGATIVE, 2))
        }

        reference_fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC",
            reference_fgc_pm_class_name,
            self.fgc_ref,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )
        self.reference_fgc_data = {
            cycle: self._query_fgc_data(
                self.fgc_ref,
                cycle,
                processing.read_timestamp_from_pm_headers_at_index(reference_fgc_pm_data_headers, index),
                self.expected_plateau_characteristics_mapping[cycle],
                is_ref=True,
            )
            for cycle, index in ((commons.Cycle.POSITIVE, 1), (commons.Cycle.NEGATIVE, 2))
        }

    def _query_fgc_data(
        self,
        fgc: str,
        cycle: commons.Cycle,
        timestamp_fgc: int | None,
        expected_plateau_characteristics: Sequence[PlateauCharacteristics],
        is_ref: bool,
    ) -> Pcc5Analysis.FgcData:
        pm_signals = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_self,
            fgc,
            ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS", "STATUS.I_EARTH_PCNT"],
            timestamp_fgc,
        )
        if is_ref is True:
            pm_signals = [signal.rename(f"{signal.name} (ref test)") for signal in pm_signals]

        signals = {DataSource.PM: self.Signals(*pm_signals)}

        pm_i_pcc_plateaus = (
            (
                processing.get_regions_equal_to(signals[DataSource.PM].i_ref.loc[:timestamp_fgc].abs(), self.i_pcc)
                if cycle == commons.Cycle.POSITIVE
                else (
                    # Cutting data 2 * self.time_crowbar before the fgc timestamp to avoid detecting plateaus
                    # from the positive cycle. Given that the negative cycle has only one plateau of interest,
                    # we can safely cut the data without losing any relevant information.
                    processing.get_regions_equal_to(
                        signals[DataSource.PM].i_ref.loc[timestamp_fgc - 2 * self.time_crowbar : timestamp_fgc],
                        -self.i_pcc,
                    )
                )
            )
            if timestamp_fgc is not None
            else []
        )

        # if the start of the first plateau corresponds to the start of the PM data,
        # we proceed to remove it as it is incomplete, and we would rather rely on NXCALS
        if len(pm_i_pcc_plateaus) > 0 and pm_i_pcc_plateaus[0][0] == signals[DataSource.PM].i_ref.index[0]:
            pm_i_pcc_plateaus = pm_i_pcc_plateaus[1:]

        pm_plateaus = [(plateau, DataSource.PM) for plateau in pm_i_pcc_plateaus]
        nxcals_plateaus = []
        if timestamp_fgc is not None and len(pm_i_pcc_plateaus) < len(expected_plateau_characteristics):
            nxcals_signals = query.query_cmw_by_variables(
                self._spark,
                self.input.t_start_ref if is_ref else self.input.t_start,
                self.input.t_end_ref - self.input.t_start_ref if is_ref else self.input.t_end - self.input.t_start,
                [f"{fgc}:I_MEAS", f"{fgc}:I_REF", f"{fgc}:V_MEAS", f"{fgc}:I_EARTH_PCNT"],
            )
            nxcals_signals[3] /= 100  # Convert I_EARTH_PCNT to a percentage
            if is_ref is True:
                nxcals_signals = [signal.rename(f"{signal.name} (ref test)") for signal in nxcals_signals]
            signals[DataSource.NXCALS] = self.Signals(*nxcals_signals)

            nxcals_i_pcc_plateaus = processing.get_regions_equal_to(signals[DataSource.NXCALS].i_ref.abs(), self.i_pcc)[
                : len(expected_plateau_characteristics) - len(pm_i_pcc_plateaus)
            ]
            nxcals_plateaus = [(plateau, DataSource.NXCALS) for plateau in nxcals_i_pcc_plateaus]

        plateaus = nxcals_plateaus + pm_plateaus

        detect_end_current_decay = (
            processing.detect_first_point_below_threshold
            if cycle == commons.Cycle.POSITIVE
            else processing.detect_first_point_above_threshold
        )
        end_current_decay = (
            detect_end_current_decay(
                signals[DataSource.PM].i_meas,
                timestamp_fgc + self.NANOSECONDS_AFTER_PM_DECAY_START,
                signals[DataSource.PM].i_meas.index[-1],
                self.I_MEAS_CURRENT_DECAY_END * cycle.multiplier(),
            )
            if timestamp_fgc is not None
            else None
        )
        if end_current_decay is None and not signals[DataSource.PM].i_meas.empty:
            end_current_decay = signals[DataSource.PM].i_meas.index[-1]

        end_voltage_decay = None
        if timestamp_fgc is not None and not signals[DataSource.PM].v_meas.empty:
            detect_spike = (
                processing.detect_first_point_below_threshold
                if cycle == commons.Cycle.POSITIVE
                else processing.detect_first_point_above_threshold
            )
            spike = detect_spike(
                signals[DataSource.PM].v_meas.diff().dropna(),
                timestamp_fgc + self.SKIPPED_FIRST_NANOSECONDS_FOR_SPIKE_DETECTION,
                signals[DataSource.PM].v_meas.index[-1],
                self.SPIKE_DETECTION_THRESHOLD * cycle.multiplier(),
            )
            if spike is not None:
                point_before_spike = (
                    signals[DataSource.PM].v_meas.index[signals[DataSource.PM].v_meas.index < spike].max()
                    if spike is not None
                    else None
                )
                end_voltage_decay = point_before_spike
            else:
                end_voltage_decay = signals[DataSource.PM].v_meas.index[-1]

        return self.FgcData(
            timestamp_fgc=timestamp_fgc,
            signals=signals,
            plateaus=plateaus,
            end_current_decay=end_current_decay,
            end_voltage_decay=end_voltage_decay,
        )

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.cycle_checks = {
            cycle: self._check_cycle(cycle, expected_plateau_characteristics)
            for cycle, expected_plateau_characteristics in self.expected_plateau_characteristics_mapping.items()
        }

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        return timestamp_checks.check_fgc_pm_data_headers_length(self.fgc, self.fgc_pm_data_headers, 3, logger_adapter)

    def _check_cycle(
        self, cycle: commons.Cycle, expected_plateau_characteristics: list[PlateauCharacteristics]
    ) -> Pcc5Analysis.CycleCheck:
        return self.CycleCheck(
            pc_current_profile_consistency_check=self._check_pc_current_profile_consistency(
                cycle, expected_plateau_characteristics
            ),
            pc_voltage_profile_consistency_check=self._check_pc_voltage_profile_consistency(
                cycle, expected_plateau_characteristics
            ),
            pc_current_decay_check=self._check_pc_current_decay(cycle),
            pc_voltage_decay_check=self._check_pc_voltage_decay(cycle),
            earth_current_check=self._check_earth_current(cycle),
        )

    def _check_pc_current_profile_consistency(
        self, cycle: commons.Cycle, expected_plateau_characteristics: list[PlateauCharacteristics]
    ) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )

        if len(self.fgc_data[cycle].plateaus) < len(expected_plateau_characteristics):
            logger_adapter.critical(
                f"Check failed: Prerequisite not met: less than {len(expected_plateau_characteristics)} "
                "I_PCC plateaus found for I_REF."
            )
            return False

        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc,
                        self.fgc_data[cycle].signals[source].i_ref,
                        plateau,
                        (
                            expected_plateau.duration
                            if source == DataSource.PM
                            else expected_plateau.duration - self.NXCALS_PLATEAU_DURATION_OFFSET
                        ),
                        logger_adapter,
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        self.fgc,
                        self.fgc_data[cycle].signals[source].i_meas,
                        plateau,
                        expected_plateau.level,
                        self.FACTOR_OF_THE_NOMINAL_CURRENT * self.i_pno,
                        "A",
                        logger_adapter,
                        skip_last_point=(plateau == self.fgc_data[cycle].plateaus[-1][0]),
                    )
                )
                for (plateau, source), expected_plateau in zip(
                    self.fgc_data[cycle].plateaus, expected_plateau_characteristics, strict=True
                )
            ]
        )

    def _check_pc_voltage_profile_consistency(
        self, cycle: commons.Cycle, expected_plateau_characteristics: list[PlateauCharacteristics]
    ) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )

        if (
            analog_checks.check_target_current_is_equal_to_reference(self.i_pcc, self.reference_i_pcc, logger_adapter)
            is False
        ):
            return False

        if len(self.fgc_data[cycle].plateaus) < len(expected_plateau_characteristics):
            logger_adapter.critical(
                f"Check failed: Prerequisite not met: less than {len(expected_plateau_characteristics)} "
                "I_PCC plateaus found for I_REF."
            )
            return False

        if len(self.reference_fgc_data[cycle].plateaus) < len(expected_plateau_characteristics):
            logger_adapter.critical(
                f"Check failed: Prerequisite not met: less than {len(expected_plateau_characteristics)} "
                "I_PCC plateaus found for reference I_REF."
            )
            return False

        return all(
            [
                analog_checks.check_minimum_plateau_duration(
                    self.fgc,
                    self.reference_fgc_data[cycle].signals[reference_source].i_ref,
                    reference_plateau,
                    (
                        expected_plateau.duration
                        if reference_source == DataSource.PM
                        else expected_plateau.duration - self.NXCALS_PLATEAU_DURATION_OFFSET
                    ),
                    logger_adapter,
                )
                and analog_checks.check_absolute_deviation_wrt_reference(
                    self.fgc,
                    self.fgc_data[cycle].signals[source].v_meas,
                    plateau,
                    self.reference_fgc_data[cycle].signals[reference_source].v_meas,
                    reference_plateau,
                    self.maximum_allowed_absolute_voltage_deviation_at_plateau,
                    "V",
                    logger_adapter,
                )
                for (plateau, source), (reference_plateau, reference_source), expected_plateau in zip(
                    self.fgc_data[cycle].plateaus,
                    self.reference_fgc_data[cycle].plateaus,
                    expected_plateau_characteristics,
                    strict=True,
                )
            ]
        )

    def _check_pc_current_decay(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG[cycle])
        if self.fgc.startswith("RPLB"):
            logger_adapter.info("Check skipped: RPLB FGCs do not have a current decay data.")
            return True

        return analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.reference_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.fgc_data[cycle].signals[DataSource.PM].i_meas,
            (self.fgc_data[cycle].timestamp_fgc, self.fgc_data[cycle].end_current_decay),
            self.reference_fgc_data[cycle].signals[DataSource.PM].i_meas,
            (self.reference_fgc_data[cycle].timestamp_fgc, self.reference_fgc_data[cycle].end_current_decay),
            self.FACTOR_OF_THE_NOMINAL_CURRENT_AT_DECAY * self.i_pno,
            "A",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

    def _check_pc_voltage_decay(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_TAG[cycle])
        return analog_checks.check_target_current_is_equal_to_reference(
            self.i_pcc, self.reference_i_pcc, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.fgc_data[cycle].signals[DataSource.PM].v_meas,
            (self.fgc_data[cycle].timestamp_fgc, self.fgc_data[cycle].end_voltage_decay),
            self.reference_fgc_data[cycle].signals[DataSource.PM].v_meas,
            (self.reference_fgc_data[cycle].timestamp_fgc, self.reference_fgc_data[cycle].end_voltage_decay),
            self.maximum_allowed_absolute_voltage_deviation_at_decay,
            "V",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.NANOSECONDS_AFTER_PM_DECAY_START,
        )

    def _check_earth_current(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG[cycle])
        return all(
            [
                analog_checks.check_signal_is_within_bounds(
                    self.fgc,
                    self.fgc_data[cycle].signals[source].i_earth_pcnt,
                    plateau,
                    self.i_earth_pcnt_absolute_bounds,
                    "%",
                    logger_adapter,
                )
                for (plateau, source) in self.fgc_data[cycle].plateaus
            ]
        )

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                all(bool(cycle_check) for cycle_check in self.cycle_checks.values()),
            ]
        )
