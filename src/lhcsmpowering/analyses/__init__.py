"""Contains the analysis classes for the Powering Tests."""

from __future__ import annotations

import pathlib
from dataclasses import dataclass
from datetime import datetime

import lhcsmapi.reference as reference
import pandas as pd
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import GenericCircuitType
from lhcsmapi.Time import Time

from .commons import PoweringAnalysis, TestParameterInput, TestParameterInputWithRef
from .pcc.pcc_1.on_60a import Pcc1On60AAnalysis
from .pcc.pcc_1.on_80_120a import Pcc1On80to120AAnalysis
from .pcc.pcc_5 import Pcc5Analysis
from .pic2.pic2_circuit_quench_via_qps.on_600a import Pic2CircuitQuenchViaQpsFor600AAnalysis
from .pic2.pic2_circuit_quench_via_qps.on_ipd import Pic2CircuitQuenchViaQpsForIPDAnalysis
from .pic2.pic2_circuit_quench_via_qps.on_ipq import Pic2CircuitQuenchViaQpsForIPQAnalysis
from .pic2.pic2_fast_abort_req_via_pic.on_600a import Pic2FastAbortReqViaPicFor600AAnalysis
from .pic2.pic2_fast_abort_req_via_pic.on_ipq import Pic2FastAbortReqViaPicForIPQAnalysis
from .pic2.pic2_powering_failure.on_80_120a import Pic2PoweringFailureFor80To120AAnalysis
from .pic2.pic2_powering_failure.on_600a_and_ipd import Pic2PoweringFailureFor600AAndIPDAnalysis
from .pic2.pic2_powering_failure.on_ipq import Pic2PoweringFailureForIPQAnalysis
from .pic2.pic2_powering_failure.on_it import Pic2PoweringFailureForITAnalysis
from .pic2.pic2_powering_failure.on_rq import Pic2PoweringFailureForRQAnalysis
from .pli1.pli1_c2 import Pli1C2Analysis
from .pli1.pli1_c3 import Pli1C3Analysis
from .pli2.pli2_e3 import Pli2E3Analysis
from .pli3.pli3_b1 import Pli3B1Analysis
from .pno.pno_a1 import PnoA1Analysis
from .pno.pno_a3 import PnoA3Analysis
from .pno.pno_a8 import PnoA8Analysis
from .pno.pno_b1 import PnoB1Analysis
from .pno.pno_d1.on_60a import PnoD1On60AAnalysis
from .pno.pno_d1.on_80_120a import PnoD1On80to120AAnalysis
from .pno.pno_d3 import PnoD3For600AAnalysis


@dataclass
class AnalysisDetails:
    analysis_class: type[PoweringAnalysis]
    notebook_path: pathlib.Path
    reference_test_selection_strategy: reference.ReferenceTestSelectionStrategy | None = None


base_dir = pathlib.Path(__file__).parent

POWERING_ANALYSES_MAPPING: dict[str, dict[GenericCircuitType, AnalysisDetails]] = {
    "PCC.1": {
        GenericCircuitType.A60: AnalysisDetails(
            Pcc1On60AAnalysis,
            base_dir / "pcc" / "pcc_1" / "on_60a" / "AN_60A_PCC.1_PC.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        ),
        GenericCircuitType.A80_120: AnalysisDetails(
            Pcc1On80to120AAnalysis,
            base_dir / "pcc" / "pcc_1" / "on_80_120a" / "AN_80-120A_PCC.1_PC.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        ),
    },
    "PCC.5": {
        GenericCircuitType.A600: AnalysisDetails(
            Pcc5Analysis,
            base_dir / "pcc" / "pcc_5" / "AN_600A_PCC.5.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PLI1.c2": {
        GenericCircuitType.IPD: AnalysisDetails(
            Pli1C2Analysis,
            base_dir / "pli1" / "pli1_c2" / "AN_IPD_PLI1.c2.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PLI1.c3": {
        GenericCircuitType.IPQ: AnalysisDetails(
            Pli1C3Analysis,
            base_dir / "pli1" / "pli1_c3" / "AN_IPQ_PLI1.c3.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PLI2.e3": {
        GenericCircuitType.IPQ: AnalysisDetails(Pli2E3Analysis, base_dir / "pli2" / "pli2_e3" / "AN_IPQ_PLI2.e3.ipynb")
    },
    "PLI3.b1": {
        GenericCircuitType.A600: AnalysisDetails(
            Pli3B1Analysis,
            base_dir / "pli3" / "pli3_b1" / "AN_600A_PLI3.b1.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PIC2 CIRCUIT QUENCH VIA QPS": {
        GenericCircuitType.A600: AnalysisDetails(
            Pic2CircuitQuenchViaQpsFor600AAnalysis,
            base_dir / "pic2" / "pic2_circuit_quench_via_qps" / "on_600a" / "AN_600A_PIC2_CIRCUIT_QUENCH_VIA_QPS.ipynb",
        ),
        GenericCircuitType.IPD: AnalysisDetails(
            Pic2CircuitQuenchViaQpsForIPDAnalysis,
            base_dir / "pic2" / "pic2_circuit_quench_via_qps" / "on_ipd" / "AN_IPD_PIC2_CIRCUIT_QUENCH_VIA_QPS.ipynb",
        ),
        GenericCircuitType.IPQ: AnalysisDetails(
            Pic2CircuitQuenchViaQpsForIPQAnalysis,
            base_dir / "pic2" / "pic2_circuit_quench_via_qps" / "on_ipq" / "AN_IPQ_PIC2_CIRCUIT_QUENCH_VIA_QPS.ipynb",
        ),
    },
    "PIC2 FAST ABORT REQ VIA PIC": {
        GenericCircuitType.A600: AnalysisDetails(
            Pic2FastAbortReqViaPicFor600AAnalysis,
            base_dir / "pic2" / "pic2_fast_abort_req_via_pic" / "on_600a" / "AN_600A_PIC2_FAST_ABORT_REQ_VIA_PIC.ipynb",
        ),
        GenericCircuitType.IPQ: AnalysisDetails(
            Pic2FastAbortReqViaPicForIPQAnalysis,
            base_dir / "pic2" / "pic2_fast_abort_req_via_pic" / "on_ipq" / "AN_IPQ_PIC2_FAST_ABORT_REQ_VIA_PIC.ipynb",
        ),
    },
    "PIC2 POWERING FAILURE": {
        GenericCircuitType.A80_120: AnalysisDetails(
            Pic2PoweringFailureFor80To120AAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_80_120a" / "AN_80-120A_PIC2_POWERING_FAILURE.ipynb",
        ),
        GenericCircuitType.A600: AnalysisDetails(
            Pic2PoweringFailureFor600AAndIPDAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_600a_and_ipd" / "AN_IPD_600A_PIC2_POWERING_FAILURE.ipynb",
        ),
        GenericCircuitType.IPD: AnalysisDetails(
            Pic2PoweringFailureFor600AAndIPDAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_600a_and_ipd" / "AN_IPD_600A_PIC2_POWERING_FAILURE.ipynb",
        ),
        GenericCircuitType.IPQ: AnalysisDetails(
            Pic2PoweringFailureForIPQAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_ipq" / "AN_IPQ_PIC2_POWERING_FAILURE.ipynb",
        ),
        GenericCircuitType.IT: AnalysisDetails(
            Pic2PoweringFailureForITAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_it" / "AN_IT_PIC2_POWERING_FAILURE.ipynb",
        ),
        GenericCircuitType.RQ: AnalysisDetails(
            Pic2PoweringFailureForRQAnalysis,
            base_dir / "pic2" / "pic2_powering_failure" / "on_rq" / "AN_RQ_PIC2_POWERING_FAILURE.ipynb",
        ),
    },
    "PNO.a1": {
        GenericCircuitType.A60: AnalysisDetails(PnoA1Analysis, base_dir / "pno" / "pno_a1" / "AN_PNO.a1.ipynb"),
        GenericCircuitType.A80_120: AnalysisDetails(PnoA1Analysis, base_dir / "pno" / "pno_a1" / "AN_PNO.a1.ipynb"),
    },
    "PNO.a3": {
        GenericCircuitType.A600: AnalysisDetails(
            PnoA3Analysis,
            base_dir / "pno" / "pno_a3" / "AN_600A_PNO.a3.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PNO.a8": {
        GenericCircuitType.IPD: AnalysisDetails(PnoA8Analysis, base_dir / "pno" / "pno_a8" / "AN_IPD_PNO.a8.ipynb")
    },
    "PNO.d1": {
        GenericCircuitType.A60: AnalysisDetails(
            PnoD1On60AAnalysis,
            base_dir / "pno" / "pno_d1" / "on_60a" / "AN_60A_PNO.d1.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        ),
        GenericCircuitType.A80_120: AnalysisDetails(
            PnoD1On80to120AAnalysis,
            base_dir / "pno" / "pno_d1" / "on_80_120a" / "AN_80-120A_PNO.d1.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        ),
    },
    "PNO.d3": {
        GenericCircuitType.A600: AnalysisDetails(
            PnoD3For600AAnalysis,
            base_dir / "pno" / "pno_d3" / "AN_600A_PNO.d3.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
    "PNO.b1": {
        GenericCircuitType.A600: AnalysisDetails(
            PnoB1Analysis,
            base_dir / "pno" / "pno_b1" / "AN_600A_PNO.b1.ipynb",
            reference.ReferenceTestSelectionStrategy.LAST,
        )
    },
}


def __get_hwc_test_circuit_mapping(hwc_test: str) -> dict[GenericCircuitType, AnalysisDetails]:
    if hwc_test not in POWERING_ANALYSES_MAPPING:
        raise ValueError(
            f"Unsupported HWC test: {hwc_test}. Supported HWC tests: {list(POWERING_ANALYSES_MAPPING.keys())}."
        )

    return POWERING_ANALYSES_MAPPING[hwc_test]


def __get_analysis_details(hwc_test: str, circuit_name: str) -> AnalysisDetails:
    circuit_mapping = __get_hwc_test_circuit_mapping(hwc_test)

    circuit_type = signal_metadata.get_generic_circuit_type_for_circuit_name(circuit_name)
    if circuit_type not in circuit_mapping:
        raise ValueError(
            f"Unsupported circuit type {circuit_type} for HWC test {hwc_test}. "
            f"Supported circuit types: {', '.join(list(circuit_type for circuit_type in circuit_mapping))}"
        )

    return circuit_mapping[circuit_type]


def get_analysis_class(hwc_test: str, circuit_name: str) -> type[PoweringAnalysis]:
    """Returns the analysis class for a given HWC test and circuit name."""
    return __get_analysis_details(hwc_test, circuit_name).analysis_class


def get_notebook_path(hwc_test: str, circuit_name: str) -> pathlib.Path:
    """Returns the notebook path for a given HWC test and circuit name."""
    return __get_analysis_details(hwc_test, circuit_name).notebook_path


def get_reference_test_selection_strategy(
    hwc_test: str, circuit_name: str
) -> reference.ReferenceTestSelectionStrategy | None:
    """Returns the reference test selection strategy for a given HWC test and circuit name."""
    return __get_analysis_details(hwc_test, circuit_name).reference_test_selection_strategy


def get_notebook_paths() -> list[pathlib.Path]:
    return [
        analysis_details.notebook_path
        for hwc_details in POWERING_ANALYSES_MAPPING.values()
        for analysis_details in hwc_details.values()
    ]


def get_analysis_instance(
    identifier: str, test_input_parameters: TestParameterInput | TestParameterInputWithRef
) -> PoweringAnalysis:
    return get_analysis_class(test_input_parameters.hwc_test, test_input_parameters.circuit_name)(
        identifier, test_input_parameters
    )


def get_supported_hwc_tests() -> list[str]:
    return sorted(POWERING_ANALYSES_MAPPING.keys())


def get_supported_circuit_types(hwc_test: str) -> set[GenericCircuitType]:
    return set(__get_hwc_test_circuit_mapping(hwc_test).keys())


def get_analysis_test_input_parameters(
    hwc_summary: pd.DataFrame,
    hwc_test: str,
    circuit_name: str,
    campaign: str,
    t_start: int | str | datetime | pd.Timestamp,
    t_end: int | str | datetime | pd.Timestamp,
) -> TestParameterInput | TestParameterInputWithRef:
    """
    Returns the input parameters for a specific analysis.

    Args:
        hwc_summary: HWC summary dataframe
        hwc_test: HWC test name
        circuit_name: circuit name
        campaign: campaign name
        t_start: start time of the analysis
        t_end: end time of the analysis
        path_summary_file: path to the summary file

    Returns:
        Input parameters for the analysis

    Raises:
        ValueError: if the selected HWC test is not supported by the analysis module
    """
    selection_strategy = get_reference_test_selection_strategy(hwc_test, circuit_name)
    if selection_strategy is None:
        return TestParameterInput(
            hwc_test, circuit_name, campaign, Time.to_unix_timestamp(t_start), Time.to_unix_timestamp(t_end)
        )

    t_start_ref, t_end_ref = reference.get_reference_test(
        hwc_summary, circuit_name, hwc_test, Time.to_string_short(t_start), selection_strategy
    )
    if t_start_ref is None or t_end_ref is None:
        raise ValueError(
            f"Reference test not found for circuit {circuit_name} and HWC test {hwc_test} "
            f"with the selection strategy {selection_strategy}."
        )

    return TestParameterInputWithRef(
        hwc_test,
        circuit_name,
        campaign,
        Time.to_unix_timestamp(t_start),
        Time.to_unix_timestamp(t_end),
        Time.to_unix_timestamp(t_start_ref),
        Time.to_unix_timestamp(t_end_ref),
    )
