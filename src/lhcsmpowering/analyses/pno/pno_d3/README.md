# 600A: PNO.d3 – BIPOLAR POWERING FAILURE

## Introduction

The aim of this test is to verify the correct functionality of the PC when a powering failure is generated. QDS will trip due to the fast change in the `I_DCCT` signal and cause a fast power abort. The test is performed at `I_PNO` + `I_DELTA` level to ensure that the magnets are trained above the nominal level. The test consists of a positive and a negative cycle, except for RU circuits where only the positive cycle is performed.

**Remark:** If a quench occurs during ramp-up or at maximum current, the MP3 expert must run the FPA Notebook on SWAN and store the results in the Quench Database. In case of a QDS trip or quench, then the MP3 should run the 600A Energy Extraction discharge analysis on SWAN.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM** (class `lhc_self`) for `RU` circuits, otherwise **2 x FGC PM** (class `lhc_self`).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) from the start of the flat top to the minimum between the end of the flat top and the first FGC PM timestamp must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter. Note: The last point is skipped in the comparison.

3. **PC voltage profile consistency check (positive cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured voltage (`V_MEAS`) from the start of the flat top to the minimum between the end of the flat top and the first FGC PM timestamp must be within **±100 mV** (**±200 mV** for RPMC) of the reference\* curve.

4. **PC current decay check (positive cycle)**
    - The measured current (`I_MEAS`) decay from the first FGC PM timestamp until the current reaches **5 A**  must be within **±10%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current doesn’t fully decay down to **5 A** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available. Note: This check is not performed for RPLB FGCs, as current decay data is not recorded.

5. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay from **60 ms** (**100 ms** for RPMC) after the first FGC PM timestamp and the end of the voltage decay must be within **±1 V** (**±2 V** for RPMC) of the reference\* decay curve. The end of the voltage decay is defined as the last point before the voltage difference between two consecutive points goes below **-0.1 V**. The start of the detection period is set **60 ms** (**100 ms** for RPMC) after the first FGC PM timestamp . If no such point is detected, the last point of the signal will be used.

6. **Earth current check (positive cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `I_PNO+I_DELTA` flat top must be from **-10%** to **10%** (**0** to **15%** for RPLB).

7. **PC current profile consistency check (negative cycle)**
    - `I_REF` reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) from the start of the flat top to the minimum between the end of the flat top and the second FGC PM timestamp must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.  Note: The last point is skipped in the comparison.

8. **PC voltage profile consistency check (negative cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`
    - The measured voltage (V_MEAS) from the start of the flat top to the minimum between the end of the flat top and the second FGC PM timestamp must be within **±100 mV** (**±200 mV** for RPMC) of the reference\* curve.

9. **PC current decay check (negative cycle)**
    - The measured current (`I_MEAS`) decay from the second FGC PM timestamp until the current reaches **-5 A**  must be within **±10%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current doesn’t fully decay down to **-5 A** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.
    **Note:** This check is not performed for RPLB FGCs, as current decay data is not recorded.

10. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay from **60 ms** (**100 ms** for RPMC) after the second FGC PM timestamp and the end of the voltage decay must be within **±1 V** (**±2 V** for RPMC) of the reference\* decay curve. The end of the voltage decay is defined as the last point before the voltage difference between two consecutive points goes above **0.1 V**. The start of the detection period is set **60 ms** (**100 ms** for RPMC) after the second FGC PM timestamp. If no such point is detected, the last point of the signal will be used.

11. **Earth current check (negative cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `-I_PNO-I_DELTA` flat top must be from **-10%** to **10%** (**0** to **15%** for RPLB).

(\*) The reference signal is the last successful test.
