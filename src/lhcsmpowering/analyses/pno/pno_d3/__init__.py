from __future__ import annotations

from dataclasses import dataclass, fields
from types import MappingProxyType

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, timestamp_checks


class PnoD3For600AAnalysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_RU_CIRCUITS = 1
    EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_OTHERS = 2

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC current profile consistency check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC current profile consistency check (negative cycle)",
        }
    )
    CURRENT_PROFILE_NOMINAL_CURRENT_FACTOR = 0.0002  # 0.02%

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC voltage profile consistency check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC voltage profile consistency check (negative cycle)",
        }
    )
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMBA_RPMBB_RPMBD_RPLB = 0.1  # 100 mV
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMC = 0.2  # 200 mV

    PC_CURRENT_DECAY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC current decay check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC current decay check (negative cycle)",
        }
    )
    CURRENT_DECAY_NOMINAL_CURRENT_FACTOR = 0.10  # 10%
    END_CURRENT_DECAY_THRESHOLD = 5  # 5 A

    PC_VOLTAGE_DECAY_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "PC voltage decay check (positive cycle)",
            commons.Cycle.NEGATIVE: "PC voltage decay check (negative cycle)",
        }
    )
    SPIKE_DETECTION_THRESHOLD = -0.1  # -0.1 V difference
    SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB = 60_000_000  # 50 ms
    SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY_RPMC = 100_000_000  # 100 ms
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB = 1  # 1 V
    MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMC = 2  # 2 V

    EARTH_CURRENT_CHECK_TAG = MappingProxyType(
        {
            commons.Cycle.POSITIVE: "Earth current check (positive cycle)",
            commons.Cycle.NEGATIVE: "Earth current check (negative cycle)",
        }
    )
    I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPLB = (0, 15)  # % (absolute value)
    I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPMBA_RPMBB_RPMBD_RPMC = (-10, 10)  # % (absolute value)

    @dataclass(frozen=True)
    class FgcData:
        timestamp_fgc: int | None
        i_meas: pd.Series
        i_ref: pd.Series
        v_meas: pd.Series
        i_earth_pcnt: pd.Series
        i_ref_plateau: tuple[int, int] | None
        end_current_decay: int | None
        end_voltage_decay: int | None

    @dataclass(frozen=True)
    class CycleCheck:
        pc_current_profile_consistency_check: bool
        pc_voltage_profile_consistency_check: bool
        pc_current_decay_check: bool
        pc_voltage_decay_check: bool
        pc_earth_current_check: bool

        def __bool__(self) -> bool:
            return all(getattr(self, field.name) for field in fields(self))

    def query(self) -> None:
        super().query()
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        [self.reference_fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)

        self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY = (
            self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY_RPMC
            if self.fgc.startswith("RPMC")
            else self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB
        )

        self.I_EARTH_PCNT_ABSOLUTE_BOUNDS = (
            self.I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPLB
            if self.fgc.startswith("RPLB")
            else self.I_EARTH_PCNT_ABSOLUTE_BOUNDS_RPMBA_RPMBB_RPMBD_RPMC
        )

        self.fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start, "self"
        )
        self.reference_fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.time_crowbar = int(self.hwc_powering_test_parameters[self.fgc]["TIME_CROWBAR"]["value"] * 1e9)
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        i_delta = float(self.hwc_powering_test_parameters[self.fgc]["I_DELTA"]["value"])
        self.plateau_level = self.i_pno + i_delta

        reference_hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )
        reference_i_pno = float(reference_hwc_powering_test_parameters[self.reference_fgc]["I_PNO"]["value"])
        reference_i_delta = float(reference_hwc_powering_test_parameters[self.reference_fgc]["I_DELTA"]["value"])
        self.reference_plateau_level = reference_i_pno + reference_i_delta

        self.maximum_allowed_absolute_voltage_deviation_at_plateau = (
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMC
            if self.fgc.startswith("RPMC")
            else self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_PLATEAU_RPMBA_RPMBB_RPMBD_RPLB
        )

        self.maximum_allowed_absolute_voltage_deviation_at_decay = (
            self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMC
            if self.fgc.startswith("RPMC")
            else self.MAXIMUM_ALLOWED_ABSOLUTE_VOLTAGE_DEVIATION_AT_DECAY_RPMBA_RPMBB_RPMBD_RPLB
        )

        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        reference_fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC",
            self.reference_fgc_pm_class_name,
            self.reference_fgc,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )

        self.cycles = (
            [commons.Cycle.POSITIVE]
            if self.input.circuit_name.startswith("RU")
            else [commons.Cycle.POSITIVE, commons.Cycle.NEGATIVE]
        )
        self.data = {
            cycle: self._query_fgc_data(
                processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, index), cycle, False
            )
            for index, cycle in enumerate(self.cycles)
        }
        self.reference_data = {
            cycle: self._query_fgc_data(
                processing.read_timestamp_from_pm_headers_at_index(reference_fgc_pm_data_headers, index), cycle, True
            )
            for index, cycle in enumerate(self.cycles)
        }

    def _query_fgc_data(self, timestamp_fgc: int | None, cycle: commons.Cycle, is_ref: bool) -> FgcData:
        i_meas, i_ref, v_meas, i_earth_pcnt = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_class_name,
            self.fgc,
            ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS", "STATUS.I_EARTH_PCNT"],
            timestamp_fgc,
        )

        if is_ref:
            for signal in [i_meas, i_ref, v_meas, i_earth_pcnt]:
                signal.name = f"{signal.name} (ref)"

        i_ref_plateau_level = self.reference_plateau_level if is_ref else self.plateau_level
        i_ref_plateau = processing.get_first_plateau(
            i_ref.loc[:timestamp_fgc], i_ref_plateau_level * cycle.multiplier()
        )

        detect_first_point_past_threshold = (
            processing.detect_first_point_below_threshold
            if cycle == commons.Cycle.POSITIVE
            else processing.detect_first_point_above_threshold
        )
        end_current_decay = (
            detect_first_point_past_threshold(
                i_meas,
                timestamp_fgc,
                i_meas.index[-1] if not i_meas.empty else None,
                self.END_CURRENT_DECAY_THRESHOLD * cycle.multiplier(),
            )
            if timestamp_fgc is not None
            else None
        )
        if end_current_decay is None and not i_meas.empty:
            end_current_decay = i_meas.index[-1]

        detect_first_point_past_threshold = (
            processing.detect_first_point_above_threshold
            if cycle == commons.Cycle.POSITIVE
            else processing.detect_first_point_below_threshold
        )

        end_voltage_decay = None
        if timestamp_fgc is not None and not v_meas.empty:
            detect_spike = (
                processing.detect_first_point_below_threshold
                if cycle == commons.Cycle.POSITIVE
                else processing.detect_first_point_above_threshold
            )
            spike = detect_spike(
                v_meas.diff().dropna(),
                timestamp_fgc + self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY,
                v_meas.index[-1],
                self.SPIKE_DETECTION_THRESHOLD * cycle.multiplier(),
            )
            if spike is not None:
                point_before_spike = v_meas.index[v_meas.index < spike].max() if spike is not None else None
                end_voltage_decay = point_before_spike
            else:
                end_voltage_decay = v_meas.index[-1]

        return PnoD3For600AAnalysis.FgcData(
            timestamp_fgc=timestamp_fgc,
            i_meas=i_meas,
            i_ref=i_ref,
            v_meas=v_meas,
            i_earth_pcnt=i_earth_pcnt,
            i_ref_plateau=i_ref_plateau,
            end_current_decay=end_current_decay,
            end_voltage_decay=end_voltage_decay,
        )

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamp()
        self.cycle_checks = {cycle: self._check_cycle(cycle) for cycle in self.cycles}

    def _check_pm_buffers_presence_and_timestamp(self) -> bool:
        check_logger = commons.start_logging_new_check(self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG)
        expected_fgc_pm_data_headers_length = (
            self.EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_RU_CIRCUITS
            if self.input.circuit_name.startswith("RU")
            else self.EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_OTHERS
        )
        return timestamp_checks.check_fgc_pm_data_headers_length(
            self.fgc, self.fgc_pm_data_headers, expected_fgc_pm_data_headers_length, check_logger
        )

    def _check_cycle(self, cycle: commons.Cycle) -> PnoD3For600AAnalysis.CycleCheck:
        return PnoD3For600AAnalysis.CycleCheck(
            self._check_pc_current_profile_consistency(cycle),
            self._check_pc_voltage_profile_consistency(cycle),
            self._check_pc_current_decay(cycle),
            self._check_pc_voltage_decay(cycle),
            self._check_pc_earth_current(cycle),
        )

    def _check_pc_current_profile_consistency(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )
        return all(
            [
                analog_checks.check_minimum_plateau_duration(
                    self.fgc, self.data[cycle].i_ref, self.data[cycle].i_ref_plateau, self.time_crowbar, logger_adapter
                ),
                analog_checks.check_plateau_values_absolute_deviation(
                    self.fgc,
                    self.data[cycle].i_meas,
                    self.data[cycle].i_ref_plateau,
                    self.plateau_level * cycle.multiplier(),
                    self.CURRENT_PROFILE_NOMINAL_CURRENT_FACTOR * self.i_pno,
                    "A",
                    logger_adapter,
                    skip_last_point=True,
                ),
            ]
        )

    def _check_pc_voltage_profile_consistency(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )
        return analog_checks.check_target_current_is_equal_to_reference(
            self.plateau_level, self.reference_plateau_level, logger_adapter
        ) and all(
            [
                analog_checks.check_minimum_plateau_duration(
                    self.fgc,
                    self.reference_data[cycle].i_ref,
                    self.reference_data[cycle].i_ref_plateau,
                    self.time_crowbar,
                    logger_adapter,
                )
                and analog_checks.check_absolute_deviation_wrt_reference(
                    self.fgc,
                    self.data[cycle].v_meas,
                    self.data[cycle].i_ref_plateau,
                    self.reference_data[cycle].v_meas,
                    self.reference_data[cycle].i_ref_plateau,
                    self.maximum_allowed_absolute_voltage_deviation_at_plateau,
                    "V",
                    logger_adapter,
                )
            ]
        )

    def _check_pc_current_decay(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG[cycle])
        if self.fgc.startswith("RPLB"):
            logger_adapter.info("Check skipped: RPLB FGCs do not have a current decay.")
            return True

        return analog_checks.check_target_current_is_equal_to_reference(
            self.plateau_level, self.reference_plateau_level, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.data[cycle].i_meas,
            (self.data[cycle].timestamp_fgc, self.data[cycle].end_current_decay),
            self.reference_data[cycle].i_meas,
            (self.reference_data[cycle].timestamp_fgc, self.reference_data[cycle].end_current_decay),
            self.CURRENT_DECAY_NOMINAL_CURRENT_FACTOR * self.i_pno,
            "A",
            logger_adapter,
        )

    def _check_pc_voltage_decay(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_TAG[cycle])
        return analog_checks.check_target_current_is_equal_to_reference(
            self.plateau_level, self.reference_plateau_level, logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            self.data[cycle].v_meas,
            (self.data[cycle].timestamp_fgc, self.data[cycle].end_voltage_decay),
            self.reference_data[cycle].v_meas,
            (self.reference_data[cycle].timestamp_fgc, self.reference_data[cycle].end_voltage_decay),
            self.maximum_allowed_absolute_voltage_deviation_at_decay,
            "V",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.SKIPPED_FIRST_NANOSECONDS_VOLTAGE_DEVIATION_AT_DECAY,
        )

    def _check_pc_earth_current(self, cycle: commons.Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG[cycle])
        return analog_checks.check_signal_is_within_bounds(
            self.fgc,
            self.data[cycle].i_earth_pcnt,
            self.data[cycle].i_ref_plateau,
            self.I_EARTH_PCNT_ABSOLUTE_BOUNDS,
            "%",
            logger_adapter,
        )

    def get_analysis_output(self) -> bool:
        return self.pm_buffers_presence_and_timestamps_check and all(
            bool(cycle_check) for cycle_check in self.cycle_checks.values()
        )
