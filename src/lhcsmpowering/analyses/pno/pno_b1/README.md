# PNO.B1 - ENERGY EXTRACTION FROM QDS (+I\_PNO)

Circuits: 600A (with EE)

## Introduction

The aim of this test is to check, at nominal current level, the performance of the QPS and EE systems.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM** (class `lhc_self`).
    - **2 x QPS PM**.
    - **1 x EE PM**.
    - `ST_ABORT_PIC` changes to False during the duration of the entire test.
    - QPS PM Board A and B are **1 ms** apart.
    - FGC PM, QPS PMs, EE PM and `ST_ABORT_PIC` from NXCALS are within **±40 ms**.

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top level (`I_PNO`) and maintains it for at least (`TIME_CROWBAR`).
    - The measured current (`I_MEAS`) during (`I_PNO`) flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter. The last point of the plateau is excluded from this check.

3. **PC current decay check**
    - `I_REF` of a reference\* curve reaches the required flat top level (`I_PNO`) and maintains it for at least (`TIME_CROWBAR`).
    - The measured current (`I_MEAS`) decay from the FGC PM timestamp until the current reaches **5 A**  must be within **±15%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current doesn’t fully decay down to **5 A** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available. **40 ms** from the decay start are skipped.

4. **Energy Extraction discharge check**
    - Analysis has been migrated from eDSL and contains equivalent assertions.

(\*) The reference is the last successful test.

## Source

1. _Test Procedure and Acceptance Criteria for the 600 A Circuits_, EDMS 874716 v.5.4, <https://edms.cern.ch/document/874716/5.4>.
