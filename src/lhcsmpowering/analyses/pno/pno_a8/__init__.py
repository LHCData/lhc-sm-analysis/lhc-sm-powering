from __future__ import annotations

from dataclasses import dataclass
from datetime import datetime

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class PnoA8Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):
    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    TIMESTAMP_ALLOWED_WINDOW = 40_000_000  # 40 ms

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0001  # 0.01%
    NXCALS_PLATEAU_DURATION_OFFSET = 2_000_000_000  # 2 s

    @dataclass(frozen=True)
    class FgcSignals:
        i_meas: pd.Series
        i_ref: pd.Series

    @dataclass(frozen=True)
    class FgcData:
        # FGC
        fgc: str
        fgc_pm_class_name: str
        fgc_pm_data_headers: pd.DataFrame
        timestamp_fgc: int | None
        signals: dict[commons.DataSource, PnoA8Analysis.FgcSignals]

    @dataclass(frozen=True)
    class QpsData:
        qps_pm_data_headers: pd.DataFrame

    @dataclass(frozen=True)
    class PicData:
        cmd_pwr_perm_pic: pd.Series
        timestamp_pic: int | None

    def query(self) -> None:
        super().query()

        def query_fgc_data(fgc: str) -> PnoA8Analysis.FgcData:
            fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
                self.input.circuit_name, self.input.t_start, "self"
            )
            fgc_pm_data_headers = query.query_pm_data_headers(
                "FGC", fgc_pm_class_name, fgc, self.input.t_start, self.input.t_end - self.input.t_start
            )
            timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(fgc_pm_data_headers, 0)

            i_meas, i_ref = query.query_pm_data_signals(
                "FGC", fgc_pm_class_name, fgc, ["STATUS.I_MEAS", "STATUS.I_REF"], timestamp_fgc
            )
            pm_signals = self.FgcSignals(i_meas, i_ref)
            fgc_nxcals = signal_metadata.get_fgc_names(
                self.input.circuit_name, int(datetime.now().timestamp()) * int(1e9)
            )[0]
            i_ref = query.query_cmw_by_variables(
                self._spark, self.input.t_start, self.input.t_end - self.input.t_start, f"{fgc_nxcals}:I_REF"
            )
            i_meas = query.query_cmw_by_variables(
                self._spark, self.input.t_start, self.input.t_end - self.input.t_start, f"{fgc_nxcals}:I_MEAS"
            )
            nxcals_signals = self.FgcSignals(i_meas, i_ref)

            return self.FgcData(
                fgc,
                fgc_pm_class_name,
                fgc_pm_data_headers,
                timestamp_fgc,
                {commons.DataSource.PM: pm_signals, commons.DataSource.NXCALS: nxcals_signals},
            )

        def query_qps() -> PnoA8Analysis.QpsData:
            qps_pm_data_class = signal_metadata.get_qps_pm_class_name_for_ipd(
                self.input.circuit_name, self.input.t_start
            )
            qps_pm_data_headers = query.query_pm_data_headers(
                "QPS",
                qps_pm_data_class,
                self.input.circuit_name,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
            )
            return self.QpsData(qps_pm_data_headers)

        def query_pic() -> PnoA8Analysis.PicData:
            cmd_pwr_perm_pic = query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                f"{self.input.circuit_name}:CMD_PWR_PERM_PIC",
                include_latest_data_point_prior_to_start=True,
            )
            timestamp_pic = processing.detect_when_signal_first_changes_to(
                cmd_pwr_perm_pic,
                False,
                0,
                (self.plateaus[0].edges[0] if self.plateaus[0].edges else self.input.t_start, self.input.t_end),
                self._logger,
            )
            return self.PicData(cmd_pwr_perm_pic, timestamp_pic)

        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        self.hwc_params = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        (self.i_min_op, self.i_pno, self.i_delta, self.time_zero) = tuple(
            self.hwc_params[self.fgc][param]["value"] for param in ["I_MIN_OP", "I_PNO", "I_DELTA", "TIME_ZERO"]
        )
        self.time_zero = self.time_zero * 1e9

        self.fgc_data = query_fgc_data(self.fgc)
        self.qps_data = query_qps()
        self.plateaus = self.get_plateau_edges()
        self.pic_data = query_pic()

    def get_plateau_edges(self) -> list[commons.Plateau]:
        plateaus = []
        pm_plateaus_i_min_op = processing.get_regions_equal_to(
            self.fgc_data.signals[commons.DataSource.PM].i_ref, self.i_min_op
        )
        nxcals_plateaus_i_min_op = processing.get_first_plateau(
            self.fgc_data.signals[commons.DataSource.NXCALS].i_ref, self.i_min_op
        )

        # get first I_MIN_OP plateau from NXCALS
        plateaus.append(
            commons.Plateau(
                nxcals_plateaus_i_min_op,
                self.i_min_op,
                self.time_zero - self.NXCALS_PLATEAU_DURATION_OFFSET,
                commons.DataSource.NXCALS,
            )
        )

        # I_PNO + I_DELTA plateau
        plateau = processing.get_first_plateau(
            self.fgc_data.signals[commons.DataSource.PM].i_ref, self.i_pno + self.i_delta
        )
        if plateau is None:
            plateau = processing.get_first_plateau(
                self.fgc_data.signals[commons.DataSource.NXCALS].i_ref, self.i_pno + self.i_delta
            )
            plateaus.append(
                commons.Plateau(
                    plateau,
                    self.i_pno + self.i_delta,
                    self.time_zero - self.NXCALS_PLATEAU_DURATION_OFFSET,
                    commons.DataSource.NXCALS,
                )
            )
        else:
            plateaus.append(commons.Plateau(plateau, self.i_pno + self.i_delta, self.time_zero, commons.DataSource.PM))

        # second I_MIN_OP plateau from PM
        if (
            nxcals_plateaus_i_min_op is not None
            and len(pm_plateaus_i_min_op) > 0
            and pm_plateaus_i_min_op[-1][0] > nxcals_plateaus_i_min_op[1]
        ):  # check if NXCALS and PM plateaus coincide
            plateaus.append(
                commons.Plateau(pm_plateaus_i_min_op[-1], self.i_min_op, self.time_zero, commons.DataSource.PM)
            )
        elif nxcals_plateaus_i_min_op is None and len(pm_plateaus_i_min_op) > 0:
            plateaus.append(
                commons.Plateau(pm_plateaus_i_min_op[-1], self.i_min_op, self.time_zero, commons.DataSource.PM)
            )
        else:
            plateaus.append(commons.Plateau(None, self.i_min_op, self.time_zero, commons.DataSource.PM))

        return plateaus

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        dict_timestamps = {"FGC": self.fgc_data.timestamp_fgc, "PIC": self.pic_data.timestamp_pic}
        return all(
            [
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_data.qps_pm_data_headers, 0, logger_adapter),
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_data.fgc_pm_data_headers, 1, logger_adapter
                ),
                digital_checks.check_signal_changes_once_to(
                    self.pic_data.cmd_pwr_perm_pic,
                    False,
                    0,
                    (self.plateaus[0].edges[0] if self.plateaus[0].edges else self.input.t_start, self.input.t_end),
                    logger_adapter,
                ),
                timestamp_checks.check_timestamps_are_within_margin(
                    dict_timestamps, self.TIMESTAMP_ALLOWED_WINDOW, logger_adapter
                ),
            ]
        )

    def _check_pc_current_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)
        return all(
            [
                all(
                    [
                        analog_checks.check_minimum_plateau_duration(
                            self.fgc,
                            self.fgc_data.signals[plateau.source].i_ref,
                            plateau.edges,
                            plateau.expected_duration,
                            logger_adapter,
                        ),
                        analog_checks.check_plateau_values_absolute_deviation(
                            self.fgc,
                            self.fgc_data.signals[plateau.source].i_meas,
                            plateau.edges,
                            plateau.level,
                            self.FACTOR_OF_THE_NOMINAL_CURRENT * self.i_pno,
                            "A",
                            logger_adapter,
                        ),
                    ]
                )
                for plateau in self.plateaus
            ]
        )

    def analyze(self) -> None:
        super().analyze()
        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.pc_current_profile_check = self._check_pc_current_profile_consistency()

    def get_analysis_output(self) -> bool:
        return all([self.pm_buffers_presence_and_timestamps_check, self.pc_current_profile_check])
