# PNO.A8 – POWERING TO I\_PNO + I\_DELTA

Circuits: IPD

## Introduction

The aim of this test is to ramp up the current in the circuit to `I_PNO` + `I_DELTA`. During this test, training quenches of the magnets in the circuit are expected.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **0 x QPS PM**.
    - **1 x FGC PM**.
    - PIC from NXCALS (`CMD_PWR_PERM_PIC`) is set once to False between the start of the first `I_MIN_OP` plateau and the end of the test.
    - PIC from NXCALS (`CMD_PWR_PERM_PIC`) and FGC PM are within **±40 ms**.

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top levels (`I_MIN_OP`, `I_PNO` + `I_DELTA`, `I_MIN_OP`) and maintains it for at least (`TIME_ZERO`, `TIME_ZERO`, `TIME_ZERO`). **Note:**  If the plateau at the maximum current is not found in the PM data due to limited buffer length, the NXCALS data is queried. In this case, the required plateau duration is reduced by **2 s** to account for the lower signal resolution. The first plateau is always fetched from NXCALS, and the last one from PM.
    - The measured current (`I_MEAS`) during all flat tops must be within **±0.01%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`).

**Remark**: All events with heater-firing at I>400 A must be analyzed using the FAST POWER ABORT (FPA) notebook on SWAN and the results must be stored in the Quench Database.
