# PNO.a7 - POWERING TO I\_PNO + I\_DELTA

Circuit: IPQ

## Introduction

The purpose of this test is to perform the training up to `I_PNO + I_DELTA`. The cycle consists of a simultaneous ramp up and a short stay (`TIME_TOP`) at the current level of `I_PNO + I_DELTA`, followed by a Slow Power Abort (SPA).

The first check includes verifying that the SPA took place and that there was no trip of the Quench Protection System (QPS). The latter condition is ensured when (i) no Post Mortem (PM) data has been created by the QPS during the entire test and (ii) no PM data has been created by Function Generator Controllers (FGC) from the start of the test until the current decay up to `I_MIN_OP` = 100 A. The plateau consistency check involves assessments that duration of the plateau at the highest current level for B1 and B2 is more than 300 s and the flattop level is 1% w.r.t. reference value (\*), both for B1 and B2.

## MP3 Analysis

The MP3 Analysis of PNO.a7 test consists of 2 assessments:

1. **PM buffers absence and timestamps check**
    - **2 x SPA** (`STATE` is `SLOW_ABORT` for B1 and B2).
    - SPA for B1 and B2 are within **±40ms**.
    - 0 x QPS PM (during the entire test) and 0 x FGC PM (from test start until the current decay to `I_MIN_OP` = 100 A for B1).

2. **Current profile consistency check**
    - Duration of the plateau at the highest current level for B1 and B2 is more than **300 s**.
    - Flattop level is within **1%** w.r.t. reference (\*) value for B1 and B2.

   Input parameters in `calculate_plateau_duration_and_level()`: `plateau_end_ts` = `timestamp`{`b1`,`b1_ref`,`b2`,`b2_ref`}`end_relative`, `plateau_min_duration_sec` and `delta`. In particular, the values of `plateau_min_duration_sec` and `delta` are found experimentally. For references: `plateau_min_duration_sec` = 70, `delta` = 0.5. For analyzed signals: `plateau_min_duration_sec` = 1, `delta`  = 0.5 .

(\*) The reference signal is the last successful PNO.a7 test or STEAM simulation.

**Remark**: All events with heater-firing at I>400 A will later be analysed using the FAST POWER ABORT (FPA) notebook and the results will be stored in the quench database.

**Note**: In future, it is envisaged that current level at plateau, plateau duration and minimum operating current will be compared with corresponding parameter values stored in the Accelerator Test Tracking (`I_PNO + I_DELTA`, `TIME_TOP`, `I_MIN_OP`).

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v. 4.0, <https://edms.cern.ch/ui/#!master/navigator/document?D:100720389:100720389:subDocs>
2. _Analysis IPQ_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/Analysis_IPQ>.
3. _Quench Database_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/QuenchDatabase>
