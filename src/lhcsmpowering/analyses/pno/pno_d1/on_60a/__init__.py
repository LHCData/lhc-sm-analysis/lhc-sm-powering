from __future__ import annotations

from lhcsmpowering.analyses import commons
from lhcsmpowering.analyses.pno.pno_d1 import PnoD1Analysis


class PnoD1On60AAnalysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):

    def __init__(self, identifier: str, test_parameters: commons.TestParameterInputWithRef):
        super().__init__(identifier, test_parameters)
        self.analysis = PnoD1Analysis(
            identifier,
            test_parameters,
            PnoD1Analysis.Parameters(
                nominal_factor_for_current_profile=0.0002,  # 0.02%
                #
                maximum_allowed_absolute_voltage_deviation_at_plateau=0.06,  # 60 mV
                voltage_deviation_skipped_nanoseconds_at_plateau_start=500_000_000,  # 500 ms
                #
                include_pc_current_decay_check=True,
                nominal_current_factor_for_current_decay=0.01,  # 1%
                #
                nanoseconds_after_pm_decay_start_non_rpmc_fgc=40_000_000,  # 40 ms
                nanoseconds_after_pm_decay_start_rpmc_pgc=0,  # Unnecessary, 0 ms
                #
                use_current_as_decay_threshold=True,
                decay_threshold=0.5,  # 500 mA
                #
                maximum_allowed_absolute_voltage_deviation_at_decay=0.100,  # 500mV
                #
                i_earth_pcnt_absolute_bounds=(0, 10),  # % (absolute value)
            ),
            self._logger,
        )

    def query(self) -> None:
        super().query()
        self.analysis.query()

    def analyze(self) -> None:
        super().analyze()
        self.analysis.analyze()

    def get_analysis_output(self) -> bool:
        return self.analysis.get_analysis_output()
