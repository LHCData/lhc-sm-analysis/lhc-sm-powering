# PNO.D1 – BIPOLAR POWERING FAILURE

This test verifies the correct functionality of the crowbar. In order to start this test, PCC and PIC2 tests must have been performed and validated.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (class `lhc_self`) (3 is also acceptable).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) during `I_PNO+I_DELTA` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

3. **PC voltage profile consistency check (positive cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured voltage (`V_MEAS`) during `TIME_CROWBAR` must be within **±60 mV** of the reference\* curve. The comparison is performed **500 ms** after the beginning of the flat-top, within the time interval of the shorter signal, and aligned at their endpoints.

4. **PC current decay check (positive cycle)**
    - The measured current (`I_MEAS`) decay from the first FGC PM timestamp until the current reaches **500 mA**  must be within **±1%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current does not fully decay down to **500 mA** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms** after the first FGC PM timestamp until the current reaches **500 mA** must be within **100 mV** of the reference\* decay curve. If the current doesn’t fully decay down to **500 mA** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

6. **Earth current check (positive cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `I_PNO+I_DELTA` flat top must be from **0%** to **10%**.

7. **PC current profile consistency check (negative cycle)**
    - `I_REF` reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) during `-I_PNO-I_DELTA` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

8. **PC voltage profile consistency check (negative cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured voltage (V_MEAS) during `TIME_CROWBAR` must be within **±60 mV** of the reference\* curve. The comparison is performed **500 ms** after the start of the flat-top, within the time interval of the shorter signal, and aligned at their endpoints.

9. **PC current decay check (negative cycle)**
    - The measured current (`I_MEAS`) decay from the second FGC PM timestamp until the current reaches **-500 mA**  must be within **±1%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current does not fully decay down to **-500 mA** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

10. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms** after the second FGC PM timestamp until the current reaches **-500 mA** must be within **±100 mV** of the reference\* decay curve. If the current doesn’t fully decay down to **-500 mA** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

11. **Earth current check (negative cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `-I_PNO-I_DELTA` flat top must be from **0%** to **10%**.

## Source

1. _Test Procedure and Acceptance Criteria for the 60 A Circuits_, EDMS 874724 v.4.5, <https://edms.cern.ch/document/874724/4.5>.
