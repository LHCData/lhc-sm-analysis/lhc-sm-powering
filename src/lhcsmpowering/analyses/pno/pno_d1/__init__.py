from __future__ import annotations

import logging
from dataclasses import dataclass, fields
from types import MappingProxyType

import pandas as pd
from lhcsmapi.api import analysis, query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.analyses.commons import Cycle
from lhcsmpowering.checks import analog_checks, timestamp_checks


class PnoD1Analysis(analysis.Analysis):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "PC current profile consistency check (positive cycle)",
            Cycle.NEGATIVE: "PC current profile consistency check (negative cycle)",
        }
    )

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "PC voltage profile consistency check (positive cycle)",
            Cycle.NEGATIVE: "PC voltage profile consistency check (negative cycle)",
        }
    )

    PC_CURRENT_DECAY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "PC current decay check (positive cycle)",
            Cycle.NEGATIVE: "PC current decay check (negative cycle)",
        }
    )

    PC_VOLTAGE_DECAY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "PC voltage decay check (positive cycle)",
            Cycle.NEGATIVE: "PC voltage decay check (negative cycle)",
        }
    )

    EARTH_CURRENT_CHECK_TAG = MappingProxyType(
        {Cycle.POSITIVE: "Earth current check (positive cycle)", Cycle.NEGATIVE: "Earth current check (negative cycle)"}
    )

    @dataclass(frozen=True)
    class Parameters:
        nominal_factor_for_current_profile: float

        maximum_allowed_absolute_voltage_deviation_at_plateau: float
        voltage_deviation_skipped_nanoseconds_at_plateau_start: int

        include_pc_current_decay_check: bool
        nominal_current_factor_for_current_decay: float

        nanoseconds_after_pm_decay_start_non_rpmc_fgc: int
        nanoseconds_after_pm_decay_start_rpmc_pgc: int

        use_current_as_decay_threshold: bool
        decay_threshold: float

        maximum_allowed_absolute_voltage_deviation_at_decay: float

        i_earth_pcnt_absolute_bounds: tuple[float, float]

    @dataclass(frozen=True)
    class PoweringData:
        timestamp_fgc: int | None
        i_meas: pd.Series
        v_meas: pd.Series
        i_ref: pd.Series
        i_ref_plateau: tuple[int, int] | None
        i_earth_pcnt: pd.Series
        decay_check_end: int | None

    @dataclass(frozen=True)
    class PoweringReferenceData:
        timestamp_fgc: int | None
        i_meas: pd.Series
        i_ref: pd.Series
        i_ref_plateau: tuple[int, int] | None
        v_meas: pd.Series

    @dataclass(frozen=True)
    class CycleChecks:
        pc_current_profile_consistency: bool
        pc_voltage_profile_consistency: bool
        pc_current_decay: bool
        pc_voltage_decay: bool
        earth_current: bool

        def __bool__(self) -> bool:
            return all(getattr(self, field.name) for field in fields(self))

    def __init__(
        self,
        identifier: str,
        input_test_params: commons.TestParameterInputWithRef,
        parameters: PnoD1Analysis.Parameters,
        logger: logging.Logger,
    ):
        super().__init__(identifier)
        self.input = input_test_params
        self.parameters = parameters
        self._logger = logger

    def query(self) -> None:
        super().query()

        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        [self.fgc_ref] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)

        self.nanoseconds_after_pm_decay_start = (
            self.parameters.nanoseconds_after_pm_decay_start_rpmc_pgc
            if self.fgc.startswith("RPMC")
            else self.parameters.nanoseconds_after_pm_decay_start_non_rpmc_fgc
        )

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.hwc_powering_test_parameters_ref = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )

        self.time_crowbar = int(self.hwc_powering_test_parameters[self.fgc]["TIME_CROWBAR"]["value"] * 1e9)
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.i_delta = float(self.hwc_powering_test_parameters[self.fgc]["I_DELTA"]["value"])

        self.i_pno_ref = float(self.hwc_powering_test_parameters_ref[self.fgc]["I_PNO"]["value"])
        self.i_delta_ref = float(self.hwc_powering_test_parameters_ref[self.fgc]["I_DELTA"]["value"])

        self.fgc_pm_self = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.fgc_pm_self_ref = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )

        self.fgc_pm_self_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_self, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.fgc_pm_self_data_headers_ref = query.query_pm_data_headers(
            "FGC",
            self.fgc_pm_self_ref,
            self.fgc_ref,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )

        cycles = ((Cycle.POSITIVE, 0), (Cycle.NEGATIVE, 1))
        self.pm = {sign: self._query_pm(sign, index) for sign, index in cycles}
        self.ref_pm = {sign: self._query_reference_pm(sign, index) for sign, index in cycles}

    def _query_pm(self, cycle: Cycle, pm_index: int) -> PoweringData:

        timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers, pm_index)

        signals = ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS", "STATUS.I_EARTH_PCNT"]

        i_meas, i_ref, v_meas, i_earth_pcnt = query.query_pm_data_signals(
            "FGC", self.fgc_pm_self, self.fgc, signals, timestamp_fgc
        )

        all_i_ref_plateaus = processing.get_regions_equal_to(
            i_ref.loc[:timestamp_fgc], self.target_plateau_current(cycle)
        )
        i_ref_plateau = all_i_ref_plateaus[0] if all_i_ref_plateaus else None

        decay_check_end = self._calculate_decay_check_end(
            cycle, timestamp_fgc, i_meas if self.parameters.use_current_as_decay_threshold else v_meas
        )

        return PnoD1Analysis.PoweringData(
            timestamp_fgc, i_meas, v_meas, i_ref, i_ref_plateau, i_earth_pcnt, decay_check_end
        )

    def _calculate_decay_check_end(self, cycle: Cycle, timestamp_fgc: int | None, signal: pd.Series) -> int | None:
        if self.parameters.use_current_as_decay_threshold:
            detect_first_point = (
                processing.detect_first_point_below_threshold
                if cycle == Cycle.POSITIVE
                else processing.detect_first_point_above_threshold
            )
        else:
            detect_first_point = (
                processing.detect_first_point_above_threshold
                if cycle == Cycle.POSITIVE
                else processing.detect_first_point_below_threshold
            )
        decay_check_end = (
            detect_first_point(
                signal,
                timestamp_fgc + self.nanoseconds_after_pm_decay_start,
                signal.index[-1],
                cycle.multiplier() * self.parameters.decay_threshold,
            )
            if timestamp_fgc is not None
            else None
        )
        if decay_check_end is None and not signal.empty:
            decay_check_end = signal.index[-1]
        return decay_check_end

    def _query_reference_pm(self, cycle: Cycle, pm_index: int) -> PoweringReferenceData:

        timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers_ref, pm_index)

        reference_signals = ["STATUS.I_MEAS", "STATUS.I_REF", "STATUS.V_MEAS"]
        i_meas, i_ref, v_meas = query.query_pm_data_signals(
            "FGC", self.fgc_pm_self_ref, self.fgc_ref, reference_signals, timestamp_fgc
        )

        all_i_ref_plateaus = processing.get_regions_equal_to(
            i_ref.loc[:timestamp_fgc], self.target_plateau_current_ref(cycle)
        )
        i_ref_plateau = all_i_ref_plateaus[0] if all_i_ref_plateaus else None

        return PnoD1Analysis.PoweringReferenceData(timestamp_fgc, i_meas, i_ref, i_ref_plateau, v_meas)

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()

        self.checks = {cycle: self._analyze_cycle(cycle) for cycle in (Cycle.POSITIVE, Cycle.NEGATIVE)}

        self.analysis_output = (
            self.pm_buffers_presence_and_timestamps_check
            and bool(self.checks[Cycle.POSITIVE])
            and bool(self.checks[Cycle.NEGATIVE])
        )

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        pm_buffers_presence_and_timestamps = timestamp_checks.check_fgc_pm_data_headers_length(
            self.fgc, self.fgc_pm_self_data_headers, 2, logger_adapter
        )
        if pm_buffers_presence_and_timestamps is False:
            logger_adapter.info("Trying with 3 PM data headers instead.")
            pm_buffers_presence_and_timestamps = timestamp_checks.check_fgc_pm_data_headers_length(
                self.fgc, self.fgc_pm_self_data_headers, 3, logger_adapter
            )
        return pm_buffers_presence_and_timestamps

    def _analyze_cycle(self, cycle: Cycle) -> CycleChecks:

        return PnoD1Analysis.CycleChecks(
            pc_current_profile_consistency=self._check_pc_current_profile_consistency(cycle),
            pc_voltage_profile_consistency=self._check_pc_voltage_profile_consistency(cycle),
            pc_current_decay=(
                self._check_pc_current_decay_check(cycle) if self.parameters.include_pc_current_decay_check else True
            ),
            pc_voltage_decay=self._check_pc_voltage_decay(cycle),
            earth_current=self._check_earth_current(cycle),
        )

    def _check_pc_current_profile_consistency(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )
        return all(
            [
                analog_checks.check_minimum_plateau_duration(
                    self.fgc, pm.i_ref, pm.i_ref_plateau, self.time_crowbar, logger_adapter
                ),
                analog_checks.check_plateau_values_absolute_deviation(
                    self.fgc,
                    pm.i_meas,
                    pm.i_ref_plateau,
                    self.target_plateau_current(cycle),
                    self.parameters.nominal_factor_for_current_profile * self.i_pno,
                    "A",
                    logger_adapter,
                    skip_last_point=True,
                ),
            ]
        )

    def _check_pc_voltage_profile_consistency(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]
        ref_pm = self.ref_pm[cycle]

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )
        return (
            analog_checks.check_target_current_is_equal_to_reference(
                self.target_plateau_current(cycle), self.target_plateau_current_ref(cycle), logger_adapter
            )
            and analog_checks.check_minimum_plateau_duration(
                self.fgc, ref_pm.i_ref, ref_pm.i_ref_plateau, self.time_crowbar, logger_adapter
            )
            and analog_checks.check_absolute_deviation_wrt_reference(
                self.fgc,
                pm.v_meas,
                pm.i_ref_plateau,
                ref_pm.v_meas,
                ref_pm.i_ref_plateau,
                self.parameters.maximum_allowed_absolute_voltage_deviation_at_plateau,
                "V",
                logger_adapter,
                skipped_nanoseconds_from_the_start=self.parameters.voltage_deviation_skipped_nanoseconds_at_plateau_start,
                align_right=True,
            )
        )

    def _check_pc_current_decay_check(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]
        ref_pm = self.ref_pm[cycle]
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG[cycle])
        return analog_checks.check_target_current_is_equal_to_reference(
            self.target_plateau_current(cycle), self.target_plateau_current_ref(cycle), logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            pm.i_meas,
            (pm.timestamp_fgc, pm.decay_check_end),
            ref_pm.i_meas,
            (ref_pm.timestamp_fgc, int(ref_pm.i_meas.index[-1])),
            self.parameters.nominal_current_factor_for_current_decay * self.i_pno,
            "A",
            logger_adapter,
        )

    def _check_pc_voltage_decay(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]
        ref_pm = self.ref_pm[cycle]

        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_TAG[cycle])
        return analog_checks.check_target_current_is_equal_to_reference(
            self.target_plateau_current(cycle), self.target_plateau_current_ref(cycle), logger_adapter
        ) and analog_checks.check_absolute_deviation_wrt_reference(
            self.fgc,
            pm.v_meas,
            (pm.timestamp_fgc, pm.decay_check_end),
            ref_pm.v_meas,
            (ref_pm.timestamp_fgc, ref_pm.v_meas.index[-1]),
            self.parameters.maximum_allowed_absolute_voltage_deviation_at_decay,
            "V",
            logger_adapter,
            skipped_nanoseconds_from_the_start=self.nanoseconds_after_pm_decay_start,
        )

    def _check_earth_current(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG[cycle])
        return analog_checks.check_signal_is_within_bounds(
            self.fgc,
            pm.i_earth_pcnt,
            pm.i_ref_plateau,
            self.parameters.i_earth_pcnt_absolute_bounds,
            "%",
            logger_adapter,
        )

    def target_plateau_current(self, cycle: Cycle) -> float:
        return cycle.multiplier() * (self.i_pno + self.i_delta)

    def target_plateau_current_ref(self, cycle: Cycle) -> float:
        return cycle.multiplier() * (self.i_pno_ref + self.i_delta_ref)

    def get_analysis_output(self) -> bool:
        return self.analysis_output
