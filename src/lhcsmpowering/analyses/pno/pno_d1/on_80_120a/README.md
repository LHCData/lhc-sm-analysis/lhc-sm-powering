# PNO.D1 – BIPOLAR POWERING FAILURE

This test verifies the correct functionality of the crowbar. In order to start this test, PCC and PIC2 tests must have been performed and validated.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (class `lhc_self`) (3 is also acceptable).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) during `I_PNO+I_DELTA` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

3. **PC voltage profile consistency check (positive cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `I_PNO+I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured voltage (`V_MEAS`) during `TIME_CROWBAR` must be within **±100 mV** of the reference\* curve. The comparison is performed **500 ms** after the start of the flat-top, within the time interval of the shorter signal, and aligned at their endpoints.

4. **PC voltage decay check (positive cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms** (or **250 ms** for an RPMC fgc) after the first PM timestamp until the moment the voltage reaches **-1.2 V** must be within **±500 mV** of the reference\* decay curve.
    **Note:** If the voltage does not fully decay down to **-1.2 V** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **Earth current check (positive cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `I_PNO+I_DELTA` flat top must be from **0%** to **12%**.

6. **PC current profile consistency check (negative cycle)**
    - `I_REF` reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured current (`I_MEAS`) during `-I_PNO-I_DELTA` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

7. **PC voltage profile consistency check (negative cycle)**
    - `I_REF` of a reference\* curve reaches the required flat top level `-I_PNO-I_DELTA` and maintains it for at least `TIME_CROWBAR`.
    - The measured voltage (`V_MEAS`) during `TIME_CROWBAR` must be within **±100 mV** of the reference\* curve. The comparison is performed **500 ms** after the start of the flat-top, within the time interval of the shorter signal, and aligned at their endpoints.

8. **PC voltage decay check (negative cycle)**
    - The measured voltage (`V_MEAS`) decay from **40 ms** (or **250 ms** for an RPMC fgc) after the first PM timestamp until the moment the voltage reaches **1.2 V** must be within **±500 mV** of the reference\* decay curve.
    **Note:** If the voltage does not fully decay down to **1.2 V** in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

9. **Earth current check (negative cycle)**
    - The measured percentage earth current (`I_EARTH_PCNT`) during the `-I_PNO-I_DELTA` flat top must be from **0%** to **15%**.

(\*) The reference signal is the last successful test.

## Source

1. _Test Procedure and Acceptance Criteria for the 80 A and 120 A Dipole Corrector Circuits_, EDMS 874722 v.4.4, <https://edms.cern.ch/document/874722/4.4>.
