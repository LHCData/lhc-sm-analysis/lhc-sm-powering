from __future__ import annotations

from dataclasses import dataclass, fields
from types import MappingProxyType

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.analyses.commons import Cycle, PoweringAnalysis, TestParameterInputWithRef
from lhcsmpowering.checks import analog_checks, timestamp_checks


class PnoA3Analysis(PoweringAnalysis[TestParameterInputWithRef]):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_RU_CIRCUITS = 1
    EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_OTHERS = 2
    I_PNO_PLATEAU_SHORTENING = 500_000_000  # 0.5 s
    I_MIN_OP_PLATEAU_SHORTENING = 5_000_000_000  # 5 s

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "PC current profile consistency check (positive cycle)",
            Cycle.NEGATIVE: "PC current profile consistency check (negative cycle)",
        }
    )

    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0002  # 0.02 %

    CURRENT_SIGNAL_ACCURACY_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "Current signal accuracy check (positive cycle)",
            Cycle.NEGATIVE: "Current signal accuracy check (negative cycle)",
        }
    )
    I_DCCT_I_MEAS_DIFFERENCE_BOUNDS = (-16, 16)  # -16 to 16 A
    I_DCCT_DRIFT_BOUND = 6  # A
    I_DCCT_SPIKE_THRESHOLD = 600.5  # A

    DIFFERENTIAL_VOLTAGE_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "Differential voltage check (positive cycle)",
            Cycle.NEGATIVE: "Differential voltage check (negative cycle)",
        }
    )
    I_MEAS_THRESHOLD_FOR_U_RES_CHECK = 50  # A
    U_DIFF_AT_I_MIN_OP_BOUND = 0.010  # 10 mV
    U_DIFF_STD_DEV_AT_I_PNO_BOUND = 0.005  # 5 mV
    U_DIFF_SPIKE_THRESHOLD = 0.24  # 240 mV

    RESISTIVE_VOLTAGE_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "Resistive voltage check (positive cycle)",
            Cycle.NEGATIVE: "Resistive voltage check (negative cycle)",
        }
    )
    U_RES_BOUND = 0.070  # 70 mV
    U_RES_STD_DEV_AT_I_PNO_BOUND = 0.005  # 5 mV
    U_RES_SPIKE_THRESHOLD = 0.24  # 240 mV

    CIRCUIT_RESISTANCE_CHECK_TAG = MappingProxyType(
        {
            Cycle.POSITIVE: "Circuit resistance check (positive cycle)",
            Cycle.NEGATIVE: "Circuit resistance check (negative cycle)",
        }
    )

    CURRENT_LEAD_TEMPERATURE_CHECK_TAG = "Current lead temperature check"

    MEDIAN_FILTER_WINDOW_SIZE = (2, 2)
    WINDOW_SIZE = sum(MEDIAN_FILTER_WINDOW_SIZE) + 1  # = 5, since MEDIAN_FILTER_WINDOW_SIZE is (2, 2)
    WINDOW_MIN_PERIODS = 1  # used in the .rolling method of the process_signals

    SIGNAL_THRESHOLDS = MappingProxyType(
        {"I_DCCT": I_DCCT_SPIKE_THRESHOLD, "U_DIFF": U_DIFF_SPIKE_THRESHOLD, "U_RES": U_RES_SPIKE_THRESHOLD}
    )

    @dataclass(frozen=True)
    class DfbData:
        dfb: str
        tt891a_signals: list[pd.Series]
        tt893_signals: list[pd.Series]
        criteria: PnoA3Analysis.TemperatureCriteria

    @dataclass(frozen=True)
    class QpsData:
        pm_data_headers: pd.DataFrame
        i_dcct: pd.Series
        i_dcct_raw: pd.Series
        u_diff: pd.Series
        u_diff_raw: pd.Series  # for the notebook
        u_res: pd.Series
        u_res_raw: pd.Series  # for the notebook

    @dataclass(frozen=True)
    class ReferenceQpsData:
        u_res: pd.Series

    @dataclass(frozen=True)
    class EEData:
        ee: str
        pm_data_headers: pd.DataFrame

    @dataclass(frozen=True)
    class TemperatureCriteria:
        tt891a_bounds: tuple[float, float] | None
        tt893_bounds: tuple[float, float]

    @dataclass(frozen=True)
    class ThresholdCriteria:
        threshold: float  # in unit

    @dataclass(frozen=True)
    class ToleranceCriteria:
        tolerance: float  # in %

    @dataclass(frozen=True)
    class CycleData:
        timestamp_fgc: int | None
        i_pno_plateau: commons.Plateau
        i_min_op_plateau: commons.Plateau
        u_diff_std_dev_at_i_pno: float | None
        u_res_std_dev_at_i_pno: float | None
        abs_i_dcct_drift_at_i_pno: float | None
        u_res_point_before_i_pno_plateau: int | None
        u_res_point_after_i_pno_plateau: int | None

    @dataclass(frozen=True)
    class FgcData:
        fgc: str
        pm_data_class: str
        pm_data_headers: pd.DataFrame
        i_pno: float
        i_min_op: float
        i_ref: pd.Series
        i_meas: pd.Series
        v_meas: pd.Series
        r_circuit: float | None
        cycles: dict[Cycle, PnoA3Analysis.CycleData]

    @dataclass(frozen=True)
    class ReferenceFgcData:
        fgc: str
        i_pno: float
        i_ref: pd.Series
        i_meas: pd.Series
        r_circuit: float | None
        cycles: dict[Cycle, PnoA3Analysis.CycleData]

    @dataclass(frozen=True)
    class CycleCheck:
        pc_current_profile_consistency_check: bool
        current_signal_accuracy_check: bool
        differential_voltage_check: bool
        resistive_voltage_check: bool
        circuit_resistance_check: bool

        def __bool__(self) -> bool:
            return all(getattr(self, field.name) for field in fields(self))

    def query(self) -> None:
        super().query()

        self.voltage_taps_reversed = False

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.ref_hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )

        self.resistance_criteria = self._get_pno_a3_circuit_resistance_criteria(self.input.circuit_name)

        self.qps_data = self._query_qps_data()
        self.ref_qps_data = self._query_ref_qps_data()
        self.dfb_data = self._query_dfb_data()
        self.ee_data = self._query_ee()
        self.fgc_data = self._query_fgc_data()
        self.ref_fgc_data = self._query_ref_fgc_data()
        self.i_dcct_i_meas_difference = self.calculate_i_dcct_i_meas_difference()

    def calculate_i_dcct_i_meas_difference(self) -> pd.Series:
        i_meas_interpolated = processing.interpolate_signal(self.fgc_data.i_meas, self.qps_data.i_dcct.index)

        i_dcct_i_meas_difference = self.qps_data.i_dcct.abs() - i_meas_interpolated.abs()
        i_dcct_i_meas_difference.name = self.qps_data.i_dcct.name + " - " + i_meas_interpolated.name

        return i_dcct_i_meas_difference

    def _query_ee(self) -> PnoA3Analysis.EEData | None:
        if not signal_metadata.has_ee(self.input.circuit_name):
            return None
        [ee] = signal_metadata.get_ee_names(self.input.circuit_name, self.input.t_start)
        ee_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMSN600", ee, self.input.t_start, self.input.t_end - self.input.t_start
        )
        return self.EEData(ee, ee_pm_data_headers)

    def _query_qps_data(self) -> PnoA3Analysis.QpsData:

        qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMGNA", self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        i_dcct_raw, u_diff_raw, u_res_raw = query.query_cmw_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            [
                f"{self.input.circuit_name}:I_DCCT",
                f"{self.input.circuit_name}:U_DIFF",
                f"{self.input.circuit_name}:U_RES",
            ],
        )

        i_dcct = self._process_signal(i_dcct_raw, "I_DCCT")
        u_diff = self._process_signal(u_diff_raw, "U_DIFF")
        u_res = self._process_signal(u_res_raw, "U_RES")

        return self.QpsData(
            pm_data_headers=qps_pm_data_headers,
            i_dcct=i_dcct,
            i_dcct_raw=i_dcct_raw,
            u_diff=u_diff,
            u_diff_raw=u_diff_raw,
            u_res=u_res,
            u_res_raw=u_res_raw,
        )

    def _query_ref_qps_data(self) -> PnoA3Analysis.ReferenceQpsData:

        u_res_raw = query.query_cmw_by_variables(
            self._spark,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
            f"{self.input.circuit_name}:U_RES",
        )

        u_res = self._process_signal(u_res_raw, "U_RES")
        u_res.name += " (ref test)"

        return self.ReferenceQpsData(u_res=u_res)

    def _query_fgc_data(self) -> PnoA3Analysis.FgcData:
        [fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)

        time_zero = int(self.hwc_powering_test_parameters[fgc]["TIME_ZERO"]["value"] * 1e9)
        time_pno = int(self.hwc_powering_test_parameters[fgc]["TIME_PNO"]["value"] * 1e9)
        i_pno = float(self.hwc_powering_test_parameters[fgc]["I_PNO"]["value"])
        i_min_op = float(self.hwc_powering_test_parameters[fgc]["I_MIN_OP"]["value"])

        fgc_pm_data_class = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")

        fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_data_class, fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        i_ref, i_meas, v_meas = query.query_cmw_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            [f"{fgc}:I_REF", f"{fgc}:I_MEAS", f"{fgc}:V_MEAS"],
        )

        cycle_pos = self._query_cycle(
            pm_index=0,
            i_ref=i_ref,
            i_meas=i_meas,
            i_pno=i_pno,
            time_pno=time_pno,
            i_min_op=i_min_op,
            time_zero=time_zero,
            fgc_pm_data_headers=fgc_pm_data_headers,
            u_diff=self.qps_data.u_diff,
            u_res=self.qps_data.u_res,
            i_dcct=self.qps_data.i_dcct,
        )
        cycles = {Cycle.POSITIVE: cycle_pos}

        if not self.input.circuit_name.startswith("RU"):
            cycle_neg = self._query_cycle(
                pm_index=1,
                i_ref=i_ref,
                i_meas=i_meas,
                i_pno=i_pno,
                time_pno=time_pno,
                i_min_op=i_min_op,
                time_zero=time_zero,
                fgc_pm_data_headers=fgc_pm_data_headers,
                u_diff=self.qps_data.u_diff,
                u_res=self.qps_data.u_res,
                i_dcct=self.qps_data.i_dcct,
            )
            cycles[Cycle.NEGATIVE] = cycle_neg

        r_circuit = self.calculate_resistance_at_i_pno(self.qps_data.u_res, i_meas, cycles)

        return self.FgcData(
            fgc=fgc,
            pm_data_class=fgc_pm_data_class,
            pm_data_headers=fgc_pm_data_headers,
            i_pno=i_pno,
            i_min_op=i_min_op,
            i_ref=i_ref,
            i_meas=i_meas,
            v_meas=v_meas,
            r_circuit=r_circuit,
            cycles=cycles,
        )

    def _query_cycle(
        self,
        pm_index: int,
        i_ref: pd.Series,
        i_meas: pd.Series,
        i_pno: float,
        time_pno: int,
        i_min_op: float,
        time_zero: int,
        fgc_pm_data_headers: pd.DataFrame,
        u_diff: pd.Series | None = None,
        u_res: pd.Series | None = None,
        i_dcct: pd.Series | None = None,
    ) -> CycleData:

        timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(fgc_pm_data_headers, pm_index)

        def _get_i_pno_plateau() -> commons.Plateau:
            target_i_pno = i_pno if pm_index == 0 else -i_pno
            i_pno_plateau = commons.Plateau(
                processing.get_first_plateau(i_ref, target_i_pno),
                i_pno,  # Store absolute value for consistency
                time_pno - self.I_PNO_PLATEAU_SHORTENING,
                commons.DataSource.NXCALS,
            )
            return i_pno_plateau

        def _get_i_min_op_plateau(i_pno_plateau: commons.Plateau) -> commons.Plateau:
            target_i_min_op = i_min_op if pm_index == 0 else -i_min_op  # in case the reference i_min_op =/= 0?
            i_min_op_plateau = commons.Plateau(
                (
                    processing.find_last_plateau_that_starts_before(
                        i_pno_plateau.edges[0] if i_pno_plateau.edges else None,
                        processing.get_regions_equal_to(i_ref.abs(), target_i_min_op),
                    )
                    if i_pno_plateau.edges is not None
                    else None
                ),
                i_min_op,
                time_zero - self.I_MIN_OP_PLATEAU_SHORTENING,
                commons.DataSource.NXCALS,
            )
            return i_min_op_plateau

        i_pno_plateau = _get_i_pno_plateau()
        i_min_op_plateau = _get_i_min_op_plateau(i_pno_plateau)

        u_diff_std_dev_at_i_pno = None
        u_res_std_dev_at_i_pno = None
        abs_i_dcct_drift_at_i_pno = None

        if i_pno_plateau.edges:
            if u_diff is not None:
                u_diff_data = u_diff.loc[i_pno_plateau.edges[0] : i_pno_plateau.edges[1]]
                if len(u_diff_data) > 1:
                    u_diff_std_dev_at_i_pno = u_diff_data.std(ddof=0)

            if u_res is not None:
                u_res_data = u_res.loc[i_pno_plateau.edges[0] : i_pno_plateau.edges[1]]
                if len(u_res_data) > 1:
                    u_res_std_dev_at_i_pno = u_res_data.std(ddof=0)

            if i_dcct is not None:
                abs_i_dcct_at_i_pno = i_dcct.abs().loc[i_pno_plateau.edges[0] : i_pno_plateau.edges[1]]
                if not abs_i_dcct_at_i_pno.empty:
                    abs_i_dcct_drift_at_i_pno = abs_i_dcct_at_i_pno.max() - abs_i_dcct_at_i_pno.min()

        def get_u_res_point_before_i_pno_plateau() -> int | None:
            if i_pno_plateau.edges is None or i_meas.empty:
                return None
            return processing.detect_last_point_below_threshold(  # i_meas.abs() covers both cycles
                i_meas.abs(), self.input.t_start, i_pno_plateau.edges[0], self.I_MEAS_THRESHOLD_FOR_U_RES_CHECK
            )

        def get_u_res_point_after_i_pno_plateau() -> int | None:
            if i_pno_plateau.edges is None or i_meas.empty:
                return None
            return processing.detect_first_point_below_threshold(  # i_meas.abs() covers both cycles
                i_meas.abs(), i_pno_plateau.edges[1], i_meas.index[-1], self.I_MEAS_THRESHOLD_FOR_U_RES_CHECK
            )

        u_res_point_before_i_pno_plateau = get_u_res_point_before_i_pno_plateau()
        u_res_point_after_i_pno_plateau = get_u_res_point_after_i_pno_plateau()

        return self.CycleData(
            timestamp_fgc=timestamp_fgc,
            i_pno_plateau=i_pno_plateau,
            i_min_op_plateau=i_min_op_plateau,
            u_diff_std_dev_at_i_pno=u_diff_std_dev_at_i_pno,
            u_res_std_dev_at_i_pno=u_res_std_dev_at_i_pno,
            abs_i_dcct_drift_at_i_pno=abs_i_dcct_drift_at_i_pno,
            u_res_point_before_i_pno_plateau=u_res_point_before_i_pno_plateau,
            u_res_point_after_i_pno_plateau=u_res_point_after_i_pno_plateau,
        )

    def _query_ref_fgc_data(self) -> PnoA3Analysis.ReferenceFgcData:
        [fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)

        time_zero = int(self.ref_hwc_powering_test_parameters[fgc]["TIME_ZERO"]["value"] * 1e9)
        time_pno = int(self.ref_hwc_powering_test_parameters[fgc]["TIME_PNO"]["value"] * 1e9)
        i_pno = float(self.ref_hwc_powering_test_parameters[fgc]["I_PNO"]["value"])
        i_min_op = float(self.ref_hwc_powering_test_parameters[fgc]["I_MIN_OP"]["value"])

        fgc_pm_data_class = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )

        fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_data_class, fgc, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )

        i_ref, i_meas = query.query_cmw_by_variables(
            self._spark,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
            [f"{fgc}:I_REF", f"{fgc}:I_MEAS"],
        )

        i_ref.name += " (ref test)"
        i_meas.name += " (ref test)"

        cycle_pos = self._query_cycle(
            pm_index=0,
            i_ref=i_ref,
            i_meas=i_meas,
            i_pno=i_pno,
            time_pno=time_pno,
            i_min_op=i_min_op,
            time_zero=time_zero,
            fgc_pm_data_headers=fgc_pm_data_headers,
        )
        cycles = {Cycle.POSITIVE: cycle_pos}

        if not self.input.circuit_name.startswith("RU"):
            cycle_neg = self._query_cycle(
                pm_index=1,
                i_ref=i_ref,
                i_meas=i_meas,
                i_pno=i_pno,
                time_pno=time_pno,
                i_min_op=i_min_op,
                time_zero=time_zero,
                fgc_pm_data_headers=fgc_pm_data_headers,
            )
            cycles[Cycle.NEGATIVE] = cycle_neg

        r_circuit = self.calculate_resistance_at_i_pno(self.ref_qps_data.u_res, i_meas, cycles)

        return self.ReferenceFgcData(
            fgc=fgc, i_pno=i_pno, i_ref=i_ref, i_meas=i_meas, r_circuit=r_circuit, cycles=cycles
        )

    def _query_dfb_data(self) -> PnoA3Analysis.DfbData:
        [dfb] = signal_metadata.get_dfb_names(self.input.circuit_name, self.input.t_start)
        dfb_prefixes = signal_metadata.get_dfb_prefixes(self.input.circuit_name, dfb, self.input.t_start)
        criteria = self._get_pno_a3_temperature_criteria(self.input.circuit_name)

        if criteria.tt891a_bounds is not None:
            tt891a_signals = query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [f"{dfb_prefix}TT891A.TEMPERATURECALC" for dfb_prefix in dfb_prefixes],
            )
        else:
            tt891a_signals = []

        tt893_signals = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            [f"{dfb_prefix}TT893.TEMPERATURECALC" for dfb_prefix in dfb_prefixes],
        )

        return self.DfbData(dfb=dfb, tt891a_signals=tt891a_signals, tt893_signals=tt893_signals, criteria=criteria)

    def _get_pno_a3_temperature_criteria(self, circuit_name: str) -> PnoA3Analysis.TemperatureCriteria:
        if circuit_name.startswith("RCO"):
            return PnoA3Analysis.TemperatureCriteria(tt891a_bounds=(35, 44), tt893_bounds=(280, 323))
        elif circuit_name.startswith(("RCBX", "RQSX")):
            return PnoA3Analysis.TemperatureCriteria(
                tt891a_bounds=None, tt893_bounds=(280, 331)  # No TT891A readings for RCBX or RQSX
            )
        else:
            # "All other circuits" + "RCS" + "RCU"
            return PnoA3Analysis.TemperatureCriteria(tt891a_bounds=(46, 54), tt893_bounds=(280, 323))

    def _get_pno_a3_circuit_resistance_criteria(
        self, circuit_name: str
    ) -> PnoA3Analysis.ThresholdCriteria | PnoA3Analysis.ToleranceCriteria:
        if circuit_name.startswith("RU"):
            return self.ToleranceCriteria(0.1)
        elif circuit_name.startswith("RCO"):
            return self.ThresholdCriteria(20e-6)
        elif circuit_name.startswith(("RCD", "RCS")):
            return self.ThresholdCriteria(10e-6)
        else:
            return self.ThresholdCriteria(5e-6)

    def analyze(self) -> None:
        super().analyze()
        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.cycle_checks = {cycle: self._check_cycle(cycle) for cycle in self.fgc_data.cycles}
        self.current_lead_temperature_check = self._check_current_lead_temperature()

    def _check_cycle(self, cycle: commons.Cycle) -> PnoA3Analysis.CycleCheck:
        return self.CycleCheck(
            pc_current_profile_consistency_check=self._check_pc_current_profile_consistency(cycle),
            current_signal_accuracy_check=self._check_current_signal_accuracy(cycle),
            differential_voltage_check=self._check_differential_voltage(cycle),
            resistive_voltage_check=self._check_resistive_voltage(cycle),
            circuit_resistance_check=self._check_circuit_resistance(cycle),
        )

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        expected_fgc_pm_data_headers_length = (
            self.EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_RU_CIRCUITS
            if self.input.circuit_name.startswith("RU")
            else self.EXPECTED_FGC_PM_DATA_HEADERS_LENGTH_OTHERS
        )
        return all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc_data.fgc,
                    self.fgc_data.pm_data_headers,
                    expected_fgc_pm_data_headers_length,
                    logger_adapter,
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_data.pm_data_headers, 0, logger_adapter),
                (
                    timestamp_checks.check_ee_pm_data_headers_length(
                        self.ee_data.ee, self.ee_data.pm_data_headers, 0, logger_adapter
                    )
                    if self.ee_data is not None
                    else True
                ),
            ]
        )

    def _check_pc_current_profile_consistency(self, cycle: Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG[cycle]
        )
        cycle_data = self.fgc_data.cycles[cycle]

        return all(
            [
                all(
                    [
                        analog_checks.check_minimum_plateau_duration(
                            self.fgc_data.fgc,
                            self.fgc_data.i_ref,
                            plateau.edges,
                            plateau.expected_duration,
                            logger_adapter,
                        ),
                        analog_checks.check_plateau_values_absolute_deviation(
                            self.fgc_data.fgc,
                            self.fgc_data.i_meas,
                            plateau.edges,
                            plateau.level * cycle.multiplier(),
                            self.FACTOR_OF_THE_NOMINAL_CURRENT * self.fgc_data.i_pno,
                            "A",
                            logger_adapter,
                        ),
                    ]
                )
                for plateau in [cycle_data.i_min_op_plateau, cycle_data.i_pno_plateau]
            ]
        )

    def _check_current_signal_accuracy(self, cycle: Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.CURRENT_SIGNAL_ACCURACY_CHECK_TAG[cycle])
        cycle_data = self.fgc_data.cycles[cycle]

        return all(
            [
                analog_checks.check_signal_is_within_bounds(
                    self.fgc_data.fgc,
                    self.i_dcct_i_meas_difference,
                    cycle_data.i_pno_plateau.edges,
                    self.I_DCCT_I_MEAS_DIFFERENCE_BOUNDS,
                    "A",
                    logger_adapter,
                ),
                analog_checks.check_value_is_within_bounds(
                    self.fgc_data.fgc,
                    cycle_data.abs_i_dcct_drift_at_i_pno,
                    f"I_DCCT drift at I_PNO ({cycle.name})",
                    (0, self.I_DCCT_DRIFT_BOUND),
                    "A",
                    logger_adapter,
                ),
            ]
        )

    def _check_differential_voltage(self, cycle: Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.DIFFERENTIAL_VOLTAGE_CHECK_TAG[cycle])
        cycle_data = self.fgc_data.cycles[cycle]

        return all(
            [
                analog_checks.check_average_value_is_within_bounds(
                    self.fgc_data.fgc,
                    self.qps_data.u_diff.abs(),
                    cycle_data.i_min_op_plateau.edges,
                    (0, self.U_DIFF_AT_I_MIN_OP_BOUND),
                    "V",
                    logger_adapter,
                ),
                analog_checks.check_value_is_within_bounds(
                    self.fgc_data.fgc,
                    cycle_data.u_diff_std_dev_at_i_pno,
                    f"U_DIFF standard deviation at I_PNO ({cycle.name})",
                    (0, self.U_DIFF_STD_DEV_AT_I_PNO_BOUND),
                    "V",
                    logger_adapter,
                ),
            ]
        )

    def _check_resistive_voltage(self, cycle: Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.RESISTIVE_VOLTAGE_CHECK_TAG[cycle])
        cycle_data = self.fgc_data.cycles[cycle]

        return all(
            [
                analog_checks.check_signal_is_within_bounds(
                    self.fgc_data.fgc,
                    self.qps_data.u_res.abs(),
                    (cycle_data.u_res_point_before_i_pno_plateau, cycle_data.u_res_point_after_i_pno_plateau),
                    (0, self.U_RES_BOUND),
                    "V",
                    logger_adapter,
                ),
                analog_checks.check_value_is_within_bounds(
                    self.fgc_data.fgc,
                    cycle_data.u_res_std_dev_at_i_pno,
                    f"U_RES standard deviation at I_PNO ({cycle.name})",
                    (0, self.U_RES_STD_DEV_AT_I_PNO_BOUND),
                    "V",
                    logger_adapter,
                ),
            ]
        )

    def _check_circuit_resistance(self, cycle: Cycle) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.CIRCUIT_RESISTANCE_CHECK_TAG[cycle])
        ref_cycle_data = self.ref_fgc_data.cycles[cycle]

        if self.fgc_data.r_circuit is None:
            logger_adapter.critical(
                "Check failed: Prerequisite not met: R_CIRCUIT could not be calculated "
                "for either current test or reference test."
            )
            return False

        if isinstance(self.resistance_criteria, PnoA3Analysis.ToleranceCriteria):
            if self.ref_fgc_data.r_circuit is None:
                logger_adapter.critical(
                    "Check failed: Prerequisite not met: R_CIRCUIT could not be calculated "
                    "for either current test or reference test."
                )
                return False

            return analog_checks.check_target_current_is_equal_to_reference(
                self.fgc_data.i_pno, self.ref_fgc_data.i_pno, logger_adapter
            ) and all(
                [
                    analog_checks.check_minimum_plateau_duration(
                        self.ref_fgc_data.fgc,
                        self.ref_fgc_data.i_ref,
                        ref_cycle_data.i_pno_plateau.edges,
                        ref_cycle_data.i_pno_plateau.expected_duration,
                        logger_adapter,
                    ),
                    analog_checks.check_value_is_within_bounds(
                        self.fgc_data.fgc,
                        self.fgc_data.r_circuit,
                        "R_CIRCUIT",
                        (
                            (1 - self.resistance_criteria.tolerance) * self.ref_fgc_data.r_circuit,
                            (1 + self.resistance_criteria.tolerance) * self.ref_fgc_data.r_circuit,
                        ),
                        "Ohm",
                        logger_adapter,
                    ),
                ]
            )

        else:
            return analog_checks.check_value_is_within_bounds(
                self.fgc_data.fgc,
                self.fgc_data.r_circuit,
                "R_CIRCUIT",
                (0, self.resistance_criteria.threshold),
                "Ohm",
                logger_adapter,
            )

    def _check_current_lead_temperature(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.CURRENT_LEAD_TEMPERATURE_CHECK_TAG)
        criteria = self.dfb_data.criteria
        checks = []

        for signal in self.dfb_data.tt891a_signals:
            if criteria.tt891a_bounds is not None:
                checks.append(
                    analog_checks.check_signal_is_within_bounds(
                        self.dfb_data.dfb,
                        signal,
                        (self.input.t_start, self.input.t_end),
                        criteria.tt891a_bounds,
                        "K",
                        logger_adapter,
                    )
                )

        for signal in self.dfb_data.tt893_signals:
            checks.append(
                analog_checks.check_signal_is_within_bounds(
                    self.dfb_data.dfb,
                    signal,
                    (self.input.t_start, self.input.t_end),
                    criteria.tt893_bounds,
                    "K",
                    logger_adapter,
                )
            )

        return all(checks)

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                all(bool(cycle_check) for cycle_check in self.cycle_checks.values()),
                self.current_lead_temperature_check,
            ]
        )

    def _process_signal(self, signal: pd.Series, signal_type: str) -> pd.Series:
        processed = processing.remove_spikes(signal.copy(), self.SIGNAL_THRESHOLDS[signal_type])
        processed = processed.rolling(
            window=self.WINDOW_SIZE, center=True, min_periods=self.WINDOW_MIN_PERIODS
        ).median()
        return processed

    @staticmethod
    def calculate_resistance_at_i_pno(
        u_res: pd.Series, i_meas: pd.Series, cycles: dict[Cycle, PnoA3Analysis.CycleData]
    ) -> float | None:
        """
        Calculate the circuit resistance according to the PNO.a3 test requirements.

        For non-RU circuits, the formula is:
            R_circuit = U_PNO / I
            where:
                U_PNO = (U+ - U-)/2
                U+ = Avg(U_RES) at +I_PNO
                U- = Avg(U_RES) at -I_PNO
                I = (I+ - I-)/2
                I+ = Avg(I_MEAS) at +I_PNO
                I- = Avg(I_MEAS) at -I_PNO

        For RU circuits which only have a positive cycle, the formula simplifies to:
            R_circuit = U+ / I+

        If U- is higher than U+, the polarity of the voltage taps is likely reversed
        and the function returns a negative R_circuit with a warning flag.

        Args:
            u_res: The resistive voltage series
            i_meas: The measured current series
            pos_i_pno_plateau: The positive I_PNO plateau time window (start, end)
            neg_i_pno_plateau: The negative I_PNO plateau time window (start, end)
            is_ru: Boolean indicating if the circuit is an RU type circuit

        Returns:
            A tuple containing:
            - The calculated circuit resistance in Ohms, or None if calculation failed
            - A boolean flag indicating if voltage taps might be reversed
        """

        def _calculate_resistance(u: float, i: float) -> float | None:
            return abs(u / i) if i != 0 else None

        pos_i_pno_plateau = cycles[Cycle.POSITIVE].i_pno_plateau.edges

        if Cycle.NEGATIVE not in cycles:
            if pos_i_pno_plateau is None:
                return None

            u_plus = processing.calculate_signal_average(u_res, [pos_i_pno_plateau])
            i_plus = processing.calculate_signal_average(i_meas, [pos_i_pno_plateau])

            return _calculate_resistance(u_plus, i_plus)

        neg_i_pno_plateau = cycles[Cycle.NEGATIVE].i_pno_plateau.edges

        if pos_i_pno_plateau is None or neg_i_pno_plateau is None:
            return None

        u_plus = processing.calculate_signal_average(u_res, [pos_i_pno_plateau])
        u_minus = processing.calculate_signal_average(u_res, [neg_i_pno_plateau])
        i_plus = processing.calculate_signal_average(i_meas, [pos_i_pno_plateau])
        i_minus = processing.calculate_signal_average(i_meas, [neg_i_pno_plateau])

        u_pno = (u_plus - u_minus) / 2
        i = (i_plus - i_minus) / 2

        return _calculate_resistance(u_pno, i)
