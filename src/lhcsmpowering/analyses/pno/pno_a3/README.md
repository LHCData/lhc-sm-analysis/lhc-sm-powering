# PNO.a3 - BIPOLAR CYCLE (±I_PNO)

Circuits: 600A

## Introduction

The aim of the test is to verify the correct functionality of the magnets and current leads at nominal current. Moreover, the offsets and noise levels in QDS signals are tested, as well as the accuracy of the `I_DCCT` signal.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM** (class `lhc_self`) for `RU` circuits, otherwise **2 x FGC PM** (class `lhc_self`).
    - **0 x QPS PM**.
    - **0 x EE PM** (if applicable).

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top level (`I_MIN_OP`, `I_PNO`, `-I_MIN_OP`, `-I_PNO`) and maintains it for at least (`TIME_ZERO`, `TIME_PNO`, `TIME_ZERO`, `TIME_PNO`)\*.
    - The measured current (`I_MEAS`) during (`I_MIN_OP`, `I_PNO`, `-I_MIN_OP`, `-I_PNO`) flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`).

3. **Current signal accuracy check**
    - **-16 A** < (|`I_DCCT`| - |`I_MEAS`|) < **16 A** at `I_PNO` and `-I_PNO`. Note that due to differing signal resolutions, `I_MEAS` values must be interpolated to match `I_DCCT` timestamps.
    - |`I_DCCT`| drift < **6 A**  at `I_PNO` and `-I_PNO`.

4. **Differential voltage check**
    - Avg(|`U_DIFF`|) < **10 mV**  at `I_MIN_OP` and `-I_MIN_OP` .
    - StdDev(`U_DIFF`) < **5 mV** at `I_PNO` and `-I_PNO`.

5. **Resistive voltage check**
    - |`U_RES`| < **70 mV** from when the current first reaches **50 A** before the `I_PNO` plateau to when it reaches **50 A** after (**-50 A** for the negative cycle).
    - StdDev(`U_RES`) < **5 mV** at `I_PNO` and `-I_PNO`.

6. **Circuit resistance check**
    - I_REF of a reference\*\* curve reaches the required nominal flat top level (`I_PNO`, `-I_PNO`) and maintains it for at least `TIME_PNO`.
    - The maximum allowed resistance varies by circuit type:
        - RCO circuits: R_circuit < **20 μΩ**
        - RCD, RCS circuits: R_circuit < **10 μΩ**
        - All other circuits (except RU): R_circuit < **5 μΩ**,

            where:
            - R_circuit = |U_PNO / I|
            - U_PNO = (U+ - U-)/2
            - U+ = Avg(`U_RES`) at +I_PNO
            - U- = Avg(`U_RES`) at -I_PNO
            - I = (I+ - I-)/2
            - I+ = Avg(`I_MEAS`) at +I_PNO
            - I- = Avg(`I_MEAS`) at -I_PNO
        - RU circuits: R_circuit is within **±10%** of the reference\* value, where R_circuit =  |U+ / I+|.

    Note: If U- is higher than U+, the polarity of the voltage taps is likely reversed

7. **Current lead temperature check**
    - This check ensures that the temperatures of the cold (TT891A) and warm (TT893) parts of the current leads remain within an acceptable range. The number of temperature readings depends on the circuit type (see table below). In the table, the (L) and (H) annotations represent the "Low" and "High" warning thresholds defined in Circuit Synoptic application, at which the system issues a warning but allows continued operation. The (LL) and (HH) annotations represent greater thresholds at which the circuit would have to stop operation for safety reasons. In this test, for the RCBX and RQSX circuits, the HH values for the upper TT893 temperature limit are used instead of the standard H value. Historical data analysis has shown that the H threshold is too restrictive and would cause unnecessary test failures.

| Circuit type/group         | RCO     | RCBX, RQSX | RCS     | All other circuits (RCD, ROD, ROF, RQ6, RQS, RQT12, RQT13, RQTD, RQTF, RQTL7, RQTL8, RQTL9, RQTL10, RQTL11, RSD, RSF, RSS, RU) |
|----------------------------|---------|------------|---------|--------------------------------------------------------------------------------------------------------------------------------|
| **No. of TT891A readings** | 1       | 0          | 3       | 2                                                                                                                              |
| **TT891A (K) min**         | 35 (L)  | -          | 46 (L)  | 46 (L)                                                                                                                         |
| **TT891A (K) max**         | 44 (H)  | -          | 54 (H)  | 54 (H)                                                                                                                         |
| **No. of TT893 readings**  | 1       | 2          | 3       | 2                                                                                                                              |
| **TT893 (K) min**          | 280 (L) | 280 (L)    | 280 (L) | 280 (L)                                                                                                                        |
| **TT893 (K) max**          | 323 (H) | 331 (HH)   | 323 (H) | 323 (H)                                                                                                                        |

**Note**: `I_DCCT`, `U_DIFF`, and `U_RES` first have spikes removed with thresholds of **600.5 A**, **240 mV**, and **240 mV**, repectively. Afterwards they have a median filter applied to them with a window size of **2** elements to the left and **2** elements to the right.

**Note**: For `RU` circuits, only the positive cycle is performed.

(\*) Note: Due to spikes caused by the power converter turning on, the required duration for the `I_MIN_OP` plateau checks is reduced by **5 s**. The required duration for the `I_PNO` plateau check is also reduced by **0.5 s** due to inconsistencies with the query of the data.

(\*\*) The reference signal is the last successful test.

## Source

1. _Test Procedure and Acceptance Criteria for the 600 A Circuits_, EDMS 874716 v.5.4, <https://edms.cern.ch/document/874716/5.4>.
