# PNO.a1 - BIPOLAR CYCLE (±I_PNO)

Circuits: 60A, 80-120A

## Introduction

The aim of this test is to verify the correct behavior of the magnets and current leads at nominal current.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (class `lhc_self`).

2. **PC current profile consistency check (positive cycle)**
    - `I_REF` reaches the required flat top level (`I_MIN_OP`, `I_PNO`, `I_MIN_OP`) and maintains it for at least (`TIME_ZERO`, `TIME_PNO`, `TIME_ZERO` - **Last\_i\_min\_op\_plateau\_duration\_offset**  \*\*).
    - The measured current (`I_MEAS`) during (initial\* `I_MIN_OP`, `I_PNO`, final `I_MIN_OP`) flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`).

3. **Current leads voltage check (positive cycle)**
    - Abs(Avg(`U_LEAD_POS`)) < **U\_lead\_offset** at initial\* `I_MIN_OP`.
    - **U\_lead\_min** < Abs(Avg(`U_LEAD_POS`)) < **U\_lead\_max** at `I_PNO`.
    - Abs(Avg(`U_LEAD_POS`)) < **U\_lead\_offset** at final `I_MIN_OP`.
    - Abs(Avg(`U_LEAD_NEG`)) < **U\_lead\_offset** at initial\* `I_MIN_OP`.
    - **U\_lead\_min** < Abs(Avg(`U_LEAD_NEG`)) < **U\_lead\_max** at `I_PNO`.
    - Abs(Avg(`U_LEAD_NEG`)) < **U\_lead\_offset** at final `I_MIN_OP`.

4. **PC voltage offset check (positive cycle)**
    - Abs(Avg(`V_MEAS`)) < **V\_meas\_offset**  at initial\* `I_MIN_OP`.
    - Abs(Avg(`V_MEAS`)) < **V\_meas\_offset** at final `I_MIN_OP`.

5. **Current leads resistance check (positive cycle)**
    - **R\_lead_min**  < Avg(R\_LEAD\_POS) < **R\_lead_max** at `I_PNO`.
    - **R\_lead_min**  < Avg(R\_LEAD\_NEG) < **R\_lead_max** at `I_PNO`,

    where:
    - R\_LEAD\_POS = (`U_LEAD_POS` - MEAN\_U\_LEAD\_POS\_OFFSET) / `I_MEAS`,
    - R\_LEAD\_NEG = (`U_LEAD_NEG` - MEAN\_U\_LEAD\_NEG\_OFFSET) / `I_MEAS`,
    - MEAN\_U\_LEAD\_POS\_OFFSET = Avg(`U_LEAD_POS`) at initial\* and final`I_MIN_OP`,
    - MEAN\_U\_LEAD\_NEG\_OFFSET = Avg(`U_LEAD_NEG`) at initial\* and final`I_MIN_OP`.

6. **Current leads resistance slope check (positive cycle)**
    - 0 < Avg(dR\_LEAD\_POS\_dt) < **R\_lead\_slope\_max** at `I_PNO`.
    - 0 < Avg(dR\_LEAD\_NEG\_dt) < **R\_lead\_slope\_max** at `I_PNO`.

    **Note**: The derivative is calculated after applying the Savitzky-Golay filter, with a window duration of **20 s** and a polyorder of **2**. To minimize edge effects, the plateau interval is reduced by half of the window duration on both ends. The slope corresponds to an average value of calculated derivative.

7. **Magnet inductance check (positive cycle)**
    - **L\_mag\_min** < Avg(L\_MAG) < **L\_mag\_max** during ramp up and ramp down (for current between `I_MIN_OP` + 10A and `I_PNO` - 10A),

    where:
     - L\_MAG = Abs((`V_MEAS`- MEAN\_V\_MEAS\_OFFSET - (`U_LEAD_POS`- MEAN\_U\_LEAD\_POS\_OFFSET) - (`U_LEAD_NEG`- MEAN\_U\_LEAD\_NEG\_OFFSET) - `I_MEAS` * `R_TOT_MEASURED`) / `DIDT_PNO`),
     - MEAN\_V\_MEAS\_OFFSET = Avg(`V_MEAS`) at initial\* and final `I_MIN_OP`.

8. **Magnet resistance check (positive cycle)**
    - **R\_mag\_min** < Avg(R\_MAG) < **R\_mag\_max** at `I_PNO`.

    where:
    - R\_MAG = R\_MEAS - R\_LEAD\_POS - R\_LEAD\_NEG - `R_TOT_MEASURED`.
    - R\_MEAS = (`V_MEAS` - MEAN\_V\_MEAS\_OFFSET) / `I_MEAS`.

**Note**: Negative resistance values can arise due to voltage offset subtraction. To account for this, the lower bound has been set based on historical data. If the value falls below this threshold, an expert should review the test results.

**Except for the first check, the same checks are performed for a negative cycle (checks 9-15).**

(\*) Note: During the initial `I_MIN_OP`, the analysis is conducted in the time interval of **`TIME_ZERO` - 5s** relative to the endpoint, in order to avoid spikes caused by the power converter turning on.

(\*\*) Due to the low sampling rate of `I_REF`, the duration check of the final `I_MIN_OP` plateau is performed with a slightly shorter duration than `TIME_ZERO`.

## Criteria

| **Parameter**                                 | **60A - RPLA - RCB** | **80A - RPLB - RCB**                                                                                                                 | **120A - RPLB - RCB**                                            | **80A - RPMC - RCB**     | **120A - RPMC - RCB**                                                          | **Triplet - RPLB - low current [1]** | **Triplet - RPLB - high current [1]**           |
|-----------------------------------------------|----------------------|--------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------|--------------------------|--------------------------------------------------------------------------------|--------------------------------------|-------------------------------------------------|
| **Circuit type / name**                       | RCBH, RCBV           | RCBCH5, RCBCH6, RCBCHS5, RCBCV5, RCBCV6, RCBCVS5, RCBYH4, RCBYH5, RCBYH6, RCBYHS4, RCBYHS5, RCBYV4, RCBYV5, RCBYV6, RCBYVS4, RCBYVS5 | RCBCH7, RCBCH8, RCBCH9, RCBCH10, RCBCV7, RCBCV8, RCBCV9, RCBCV10 | RCBCH6.R3B2, RCBCV6.R3B1 | RCBCH7.R3B1, RCBCH8.R3B2, RCBCH9.R3B1, RCBCH10.R3B2, RCBCV9.R3B2, RCBCV10.R3B1 | RCTX3, RCSSX3.L1                     | RCOSX3, RCOX3, RCSX3, RCSSX3 (except RCSSX3.L1) |
| **Last\_i\_min\_op\_plateau\_duration\_offset (s)** | 0                    | 5                                                                                                                                    | 5                                                                | 5                        | 5                                                                              | 5                                    | 5                                               |
| **U\_lead\_offset (V)**                         | 0.0035               | 0.0035                                                                                                                               | 0.0035                                                           | 0.007                    | 0.007                                                                          | 0.0035                               | 0.0035                                          |
| **U\_lead\_min (V)**                            | 0.025                | 0.015                                                                                                                                | 0.025                                                            | 0.015                    | 0.025                                                                          | 0.055                                | 0.090                                           |
| **U\_lead\_max (V)**                            | 0.075                | 0.045                                                                                                                                | 0.065                                                            | 0.045                    | 0.065                                                                          | 0.115                                | 0.150                                           |
| **V\_meas\_offset (V)**                         | 0.05                 | 0.05                                                                                                                                 | 0.05                                                             | 0.15                     | 0.15                                                                           | 0.05                                 | 0.05                                            |
| **R\_lead\_min (Ohm)**                          | 0.00045              | 0.0002                                                                                                                               | 0.0002                                                           | 0.0002                   | 0.0002                                                                         | 0.0006875                            | 0.0009                                          |
| **R\_lead\_max (Ohm)**                          | 0.00135              | 0.0006                                                                                                                               | 0.0006                                                           | 0.0006                   | 0.0006                                                                         | 0.0014375                            | 0.0015                                          |
| **R\_lead\_slope\_max (Ohm/s)**                  | 0.0000018            | 0.0000014                                                                                                                            | 0.0000014                                                        | 0.0000014                | 0.0000014                                                                      | 0.0000014                            | 0.0000014                                       |
| **L\_mag\_min (H)**                             | 5.5                  | 0.9 * L\_TOT                                                                                                                          | 0.9 * L\_TOT                                                      | 0.9 * L\_TOT              | 0.9 * L\_TOT                                                                    | check skipped                        | check skipped                                   |
| **L\_mag\_max (H)**                             | 6.5                  | 1.1 * L\_TOT                                                                                                                          | 1.1 * L\_TOT                                                      | 1.1 * L\_TOT              | 1.1 * L\_TOT                                                                    | check skipped                        | check skipped                                   |
| **R\_mag\_min (Ohm)**                           | -0.0005              | -0.001                                                                                                                               | -0.001                                                           | -0.001                   | -0.001                                                                          | -0.001                               | -0.001                                          |
| **R\_mag\_max (Ohm)**                           | 0.005                | 0.003                                                                                                                                | 0.003                                                            | 0.003                    | 0.005                                                                          | 0.0035                               | 0.0035                                          |

[1] The distinction is based on the I_PNO levels during the Recommissioning Post-LS2 campaign, with settings of 80 A for RCTX3, 15 A for RCSSX3.L1 and 100 A for the remaining triplet circuits.

## Source

1. _Test Procedure and Acceptance Criteria for the 60 A Circuits_, EDMS 874724 v.4.5, <https://edms.cern.ch/document/874724/4.5>.
2. _Test Procedure and Acceptance Criteria for the 80 A and 120 A Dipole Corrector Circuits_, EDMS 874722 v.4.4, <https://edms.cern.ch/document/874722/4.4>.
