from __future__ import annotations

import re
from collections.abc import Callable
from dataclasses import dataclass, fields

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.analyses.commons import Cycle, PoweringAnalysis, TestParameterInput
from lhcsmpowering.checks import analog_checks, timestamp_checks


@dataclass(frozen=True)
class PlateauParameters:
    initial_i_min_op_shortened_time = 5_000_000_000  # 5 s
    final_i_min_op_shortened_time: int  # in nanoseconds
    factor_of_nominal_current = 0.0002  # 0.02 %


@dataclass(frozen=True)
class CurrentLeadsVoltageParameters:
    voltage_bounds_i_min_op: tuple[float, float]  # in volts
    voltage_bounds_i_pno: tuple[float, float]  # in volts


@dataclass(frozen=True)
class VoltageOffsetParameters:
    v_meas_offset: float  # in volts


@dataclass(frozen=True)
class CurrentLeadsResistanceParameters:
    resistance_bounds: tuple[float, float]  # in ohm


@dataclass(frozen=True)
class CurrentLeadsResistanceSlopeParameters:
    current_leads_resistance_derivative_bounds: tuple[float, float]  # in ohm/s
    sav_gol_window_duration = 20_000_000_000  # 20 s
    sav_gol_polyorder = 2


@dataclass(frozen=True)
class InductanceParameters:
    magnet_inductance_absolute_bounds: tuple[float, float]  # in henries
    current_edge_offsets_at_magnet_inductance_check: tuple[float, float]  # in amperes


@dataclass(frozen=True)
class MagnetResistanceParameters:
    resistance_bounds: tuple[float, float]  # in ohm


@dataclass(frozen=True)
class Parameters:
    plateau_parameters: PlateauParameters
    current_leads_voltage_parameters: CurrentLeadsVoltageParameters
    voltage_offset_parameters: VoltageOffsetParameters
    current_leads_resistance_parameters: CurrentLeadsResistanceParameters
    current_leads_resistance_slope_parameters: CurrentLeadsResistanceSlopeParameters
    inductance_parameters: InductanceParameters | None
    magnet_resistance_parameters: MagnetResistanceParameters


def get_parameters_for_60a(i_min_op: float, i_pno: float, _: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=0),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.0035, 0.0035), voltage_bounds_i_pno=(0.025, 0.075)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.050),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.00045, 0.00135)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.8e-6)
        ),
        inductance_parameters=InductanceParameters(
            magnet_inductance_absolute_bounds=(5.5, 6.5),
            current_edge_offsets_at_magnet_inductance_check=(i_min_op + 10, i_pno - 10),
        ),
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.0005, 0.005)),
    )


def get_parameters_for_80a_rplb_rcb(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.0035, 0.0035), voltage_bounds_i_pno=(0.015, 0.045)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.050),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0002, 0.0006)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=InductanceParameters(
            magnet_inductance_absolute_bounds=(0.9 * l_tot, 1.1 * l_tot),
            current_edge_offsets_at_magnet_inductance_check=(i_min_op + 10, i_pno - 10),
        ),
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.003)),
    )


def get_parameters_for_120a_rplb_rcb(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.0035, 0.0035), voltage_bounds_i_pno=(0.025, 0.065)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.050),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0002, 0.0006)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=InductanceParameters(
            magnet_inductance_absolute_bounds=(0.9 * l_tot, 1.1 * l_tot),
            current_edge_offsets_at_magnet_inductance_check=(i_min_op + 10, i_pno - 10),
        ),
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.003)),
    )


def get_parameters_for_80a_rpmc_rcb(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.007, 0.007), voltage_bounds_i_pno=(0.015, 0.045)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.150),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0002, 0.0006)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=InductanceParameters(
            magnet_inductance_absolute_bounds=(0.9 * l_tot, 1.1 * l_tot),
            current_edge_offsets_at_magnet_inductance_check=(i_min_op + 10, i_pno - 10),
        ),
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.003)),
    )


def get_parameters_for_120a_rpmc_rcb(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.007, 0.007), voltage_bounds_i_pno=(0.025, 0.065)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.150),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0002, 0.0006)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=InductanceParameters(
            magnet_inductance_absolute_bounds=(0.9 * l_tot, 1.1 * l_tot),
            current_edge_offsets_at_magnet_inductance_check=(i_min_op + 10, i_pno - 10),
        ),
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.005)),
    )


def get_parameters_for_low_current_rplb_triplets(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.0035, 0.0035), voltage_bounds_i_pno=(0.055, 0.115)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.050),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0006875, 0.0014375)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=None,
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.0035)),
    )


def get_parameters_for_high_current_rplb_triplets(i_min_op: float, i_pno: float, l_tot: float) -> Parameters:
    return Parameters(
        plateau_parameters=PlateauParameters(final_i_min_op_shortened_time=5_000_000_000),
        current_leads_voltage_parameters=CurrentLeadsVoltageParameters(
            voltage_bounds_i_min_op=(-0.0035, 0.0035), voltage_bounds_i_pno=(0.09, 0.15)
        ),
        voltage_offset_parameters=VoltageOffsetParameters(v_meas_offset=0.050),
        current_leads_resistance_parameters=CurrentLeadsResistanceParameters(resistance_bounds=(0.0009, 0.0015)),
        current_leads_resistance_slope_parameters=CurrentLeadsResistanceSlopeParameters(
            current_leads_resistance_derivative_bounds=(0, 1.4e-6)
        ),
        inductance_parameters=None,
        magnet_resistance_parameters=MagnetResistanceParameters(resistance_bounds=(-0.001, 0.0035)),
    )


def get_pno_a1_parameters_function(circuit_name: str, t_start: int) -> Callable[[float, float, float], Parameters]:
    def is_120a(fgc: str) -> bool:
        def first_number_after_substring(whole_str: str, substr: str) -> int:
            substr_idx = whole_str.find(substr)
            if substr_idx < 0:
                raise ValueError(f"Substring '{substr}' not found in '{whole_str}'")
            after_substr = whole_str[substr_idx + len(substr) :]
            digit_matches = re.search(r"\d+", after_substr)
            if digit_matches is None:
                raise ValueError(f"No digit found after '{substr}' in '{whole_str}'")

            return int(digit_matches.group(0))

        return first_number_after_substring(fgc, "RCB") >= 7

    if signal_metadata.is_60a(circuit_name):
        return get_parameters_for_60a

    if circuit_name.startswith(("RCSSX3.L1", "RCT")):
        return get_parameters_for_low_current_rplb_triplets

    if circuit_name.startswith(("RCO", "RCS")):
        return get_parameters_for_high_current_rplb_triplets

    if circuit_name.startswith("RCB"):
        [fgc] = signal_metadata.get_fgc_names(circuit_name, t_start)

        if fgc.startswith("RPLB"):
            if is_120a(fgc):
                return get_parameters_for_120a_rplb_rcb
            return get_parameters_for_80a_rplb_rcb

        if fgc.startswith("RPMC"):
            if is_120a(fgc):
                return get_parameters_for_120a_rpmc_rcb
            return get_parameters_for_80a_rpmc_rcb

    raise NotImplementedError(f"Parameters for {circuit_name} are not implemented.")


class PnoA1Analysis(PoweringAnalysis[TestParameterInput]):
    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    CURRENT_LEADS_VOLTAGE_CHECK_TAG = "Current leads voltage check"
    CURRENT_LEADS_RESISTANCE_CHECK_TAG = "Current leads resistance check"
    CURRENT_LEADS_RESISTANCE_SLOPE_CHECK_TAG = "Current leads resistance slope check"
    MAGNET_INDUCTANCE_CHECK_TAG = "Magnet inductance check"
    MAGNET_RESISTANCE_CHECK_TAG = "Magnet resistance check"
    PC_VOLTAGE_OFFSET_CHECK_TAG = "PC voltage offset check"

    @dataclass(frozen=True)
    class PoweringData:
        u_lead_pos: pd.Series
        u_lead_neg: pd.Series
        r_lead_pos: pd.Series
        r_lead_neg: pd.Series
        dr_lead_dt_pos: pd.Series
        dr_lead_dt_neg: pd.Series

        timestamp_fgc: int | None
        i_meas: pd.Series
        v_meas: pd.Series
        r_magnet: pd.Series
        l_mag: pd.Series
        l_mag_check_windows: tuple[tuple[int, int] | None, tuple[int, int] | None]
        l_mag_average: float | None
        raw_i_ref_plateaus_i_min_op: tuple[tuple[int, int] | None, tuple[int, int] | None]
        i_ref_plateaus_i_min_op: tuple[tuple[int, int] | None, tuple[int, int] | None]
        i_ref_plateau_i_pno: tuple[int, int] | None

    @dataclass(frozen=True)
    class CycleChecks:
        pc_current_profile_consistency: bool
        current_leads_voltage: bool
        current_leads_resistance: bool
        current_leads_resistance_slope: bool
        magnet_inductance: bool
        magnet_resistance: bool
        pc_voltage_offset: bool

        def __bool__(self) -> bool:
            return all(getattr(self, field.name) for field in fields(self))

    def query(self) -> None:
        super().query()

        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)

        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.time_zero = int(self.hwc_powering_test_parameters[self.fgc]["TIME_ZERO"]["value"] * 1e9)
        self.time_pno = int(self.hwc_powering_test_parameters[self.fgc]["TIME_PNO"]["value"] * 1e9)
        self.i_min_op = float(self.hwc_powering_test_parameters[self.fgc]["I_MIN_OP"]["value"])
        self.didt_pno = float(self.hwc_powering_test_parameters[self.fgc]["DIDT_PNO"]["value"])
        self.l_tot = float(self.hwc_powering_test_parameters[self.fgc]["L_TOT"]["value"])
        self.r_tot_measured = float(self.hwc_powering_test_parameters[self.fgc]["R_TOT_MEASURED"]["value"])

        self.parameters = get_pno_a1_parameters_function(self.input.circuit_name, self.input.t_start)(
            self.i_min_op, self.i_pno, self.l_tot
        )

        self.fgc_pm_self = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.fgc_pm_self_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_self, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # NXCALS (not enough data to detect all the plateaus from PM)
        self.i_ref_nxcals = query.query_cmw_by_variables(
            self._spark, self.input.t_start, self.input.t_end - self.input.t_start, f"{self.fgc}:I_REF"
        )

        pm_pos = self._query_event(0)
        pm_neg = self._query_event(1, pm_pos.timestamp_fgc)
        self.pm = {Cycle.POSITIVE: pm_pos, Cycle.NEGATIVE: pm_neg}

    def _query_event(self, pm_index: int, start_time: int | None = None) -> PoweringData:
        timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_self_data_headers, pm_index)

        i_meas, v_meas, u_lead_pos, u_lead_neg = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_self,
            self.fgc,
            ["ILEADS.I_MEAS", "ILEADS.V_MEAS", "ILEADS.U_LEAD_POS", "ILEADS.U_LEAD_NEG"],
            timestamp_fgc,
        )

        i_ref_nxcals = self.i_ref_nxcals.loc[start_time:timestamp_fgc]

        i_ref_plateau_i_pno = processing.get_first_plateau(i_ref_nxcals.abs(), self.i_pno)
        raw_i_ref_plateaus_i_min_op = (
            calculate_i_min_op_plateaus(i_ref_nxcals, i_ref_plateau_i_pno, self.i_min_op)
            if i_ref_plateau_i_pno is not None
            else (None, None)
        )
        i_ref_plateaus_i_min_op = (
            processing.shorten_time_window_aligned_right(
                raw_i_ref_plateaus_i_min_op[0],
                self.time_zero - self.parameters.plateau_parameters.initial_i_min_op_shortened_time,
            ),
            raw_i_ref_plateaus_i_min_op[1],
        )

        mean_u_lead_pos_offset = processing.calculate_signal_average(u_lead_pos, i_ref_plateaus_i_min_op)
        mean_u_lead_neg_offset = processing.calculate_signal_average(u_lead_neg, i_ref_plateaus_i_min_op)
        r_lead_pos = processing.calculate_resistance(u_lead_pos - mean_u_lead_pos_offset, i_meas, "R_LEAD_POS")
        r_lead_neg = processing.calculate_resistance(u_lead_neg - mean_u_lead_neg_offset, i_meas, "R_LEAD_NEG")

        dr_lead_dt_pos = processing.derivative_sav_gol(
            r_lead_pos,
            self.parameters.current_leads_resistance_slope_parameters.sav_gol_window_duration,
            self.parameters.current_leads_resistance_slope_parameters.sav_gol_polyorder,
            1,
        )
        dr_lead_dt_neg = processing.derivative_sav_gol(
            r_lead_neg,
            self.parameters.current_leads_resistance_slope_parameters.sav_gol_window_duration,
            self.parameters.current_leads_resistance_slope_parameters.sav_gol_polyorder,
            1,
        )

        mean_v_meas_offset = processing.calculate_signal_average(v_meas, i_ref_plateaus_i_min_op)
        r_meas = processing.calculate_resistance(v_meas - mean_v_meas_offset, i_meas, "R_MEAS")
        r_magnet = calculate_magnet_resistance(r_meas, r_lead_pos, r_lead_neg, self.r_tot_measured)

        l_mag = calculate_magnet_inductance(
            v_meas - mean_v_meas_offset,
            u_lead_pos - mean_u_lead_pos_offset,
            u_lead_neg - mean_u_lead_neg_offset,
            i_meas,
            self.r_tot_measured,
            self.didt_pno,
        )

        l_mag_check_windows = (
            calculate_l_mag_check_windows(
                i_meas,
                self.parameters.inductance_parameters.current_edge_offsets_at_magnet_inductance_check,
                i_ref_plateau_i_pno,
                i_ref_plateaus_i_min_op,
            )
            if self.parameters.inductance_parameters is not None
            else (None, None)
        )

        l_mag_average = processing.calculate_signal_average(l_mag, l_mag_check_windows)

        return PnoA1Analysis.PoweringData(
            u_lead_pos=u_lead_pos,
            u_lead_neg=u_lead_neg,
            r_lead_pos=r_lead_pos,
            r_lead_neg=r_lead_neg,
            dr_lead_dt_pos=dr_lead_dt_pos,
            dr_lead_dt_neg=dr_lead_dt_neg,
            timestamp_fgc=timestamp_fgc,
            i_meas=i_meas,
            v_meas=v_meas,
            r_magnet=r_magnet,
            l_mag=l_mag,
            l_mag_check_windows=l_mag_check_windows,
            l_mag_average=l_mag_average,
            raw_i_ref_plateaus_i_min_op=raw_i_ref_plateaus_i_min_op,
            i_ref_plateaus_i_min_op=i_ref_plateaus_i_min_op,
            i_ref_plateau_i_pno=i_ref_plateau_i_pno,
        )

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()

        self.checks = {cycle: self._analyze_cycle(cycle) for cycle in (Cycle.POSITIVE, Cycle.NEGATIVE)}

        self.analysis_output = (
            self.pm_buffers_presence_and_timestamps_check
            and bool(self.checks[Cycle.POSITIVE])
            and bool(self.checks[Cycle.NEGATIVE])
        )

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        check_logger = commons.start_logging_new_check(self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG)
        return timestamp_checks.check_fgc_pm_data_headers_length(
            self.fgc, self.fgc_pm_self_data_headers, 2, check_logger
        )

    def _analyze_cycle(self, cycle: Cycle) -> CycleChecks:
        return PnoA1Analysis.CycleChecks(
            pc_current_profile_consistency=self._check_pc_current_profile_consistency(cycle),
            current_leads_voltage=self._check_current_leads_voltage(cycle),
            pc_voltage_offset=self._check_pc_voltage_offset(cycle),
            current_leads_resistance=self._check_current_leads_resistance(cycle),
            current_leads_resistance_slope=self._check_current_leads_resistance_slope(cycle),
            magnet_inductance=self._check_magnet_inductance(cycle),
            magnet_resistance=self._check_magnet_resistance(cycle),
        )

    def _check_pc_current_profile_consistency(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if pm.i_ref_plateaus_i_min_op[0] is None or pm.i_ref_plateaus_i_min_op[1] is None:
            check_logger.critical("Check failed: Prerequisite not met: less than 2 I_MIN_OP plateaus found.")
            return False

        if pm.i_ref_plateau_i_pno is None:
            check_logger.critical("Check failed: Prerequisite not met: less than 1 I_PNO plateaus found.")
            return False

        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.i_ref_nxcals, raw_plateau, duration, check_logger
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        self.fgc,
                        pm.i_meas,
                        plateau,
                        expected_plateau_level * cycle.multiplier(),
                        self.parameters.plateau_parameters.factor_of_nominal_current * self.i_pno,
                        "A",
                        check_logger,
                    )
                )
                for plateau, raw_plateau, duration, expected_plateau_level in (
                    (pm.i_ref_plateaus_i_min_op[0], pm.raw_i_ref_plateaus_i_min_op[0], self.time_zero, self.i_min_op),
                    (pm.i_ref_plateau_i_pno, pm.i_ref_plateau_i_pno, self.time_pno, self.i_pno),
                    (
                        pm.i_ref_plateaus_i_min_op[1],
                        pm.raw_i_ref_plateaus_i_min_op[1],
                        self.time_zero - self.parameters.plateau_parameters.final_i_min_op_shortened_time,
                        self.i_min_op,
                    ),
                )
            ]
        )

    def _check_current_leads_voltage(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.CURRENT_LEADS_VOLTAGE_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if pm.i_ref_plateaus_i_min_op[0] is None or pm.i_ref_plateaus_i_min_op[1] is None:
            check_logger.critical("Check failed: Prerequisite not met: less than 2 I_MIN_OP plateaus found.")
            return False

        if pm.i_ref_plateau_i_pno is None:
            check_logger.critical("Check failed: Prerequisite not met: less than 1 I_PNO plateaus found.")
            return False

        return all(
            [
                analog_checks.check_average_value_is_within_bounds(
                    self.fgc,
                    u_lead,
                    plateau,
                    (
                        min(min_voltage * cycle.multiplier(), max_voltage * cycle.multiplier()),
                        max(min_voltage * cycle.multiplier(), max_voltage * cycle.multiplier()),
                    ),
                    "V",
                    check_logger,
                )
                for u_lead in (pm.u_lead_pos, pm.u_lead_neg)
                for plateau, (min_voltage, max_voltage) in (
                    (
                        pm.i_ref_plateaus_i_min_op[0],
                        self.parameters.current_leads_voltage_parameters.voltage_bounds_i_min_op,
                    ),
                    (pm.i_ref_plateau_i_pno, self.parameters.current_leads_voltage_parameters.voltage_bounds_i_pno),
                    (
                        pm.i_ref_plateaus_i_min_op[1],
                        self.parameters.current_leads_voltage_parameters.voltage_bounds_i_min_op,
                    ),
                )
            ]
        )

    def _check_pc_voltage_offset(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.PC_VOLTAGE_OFFSET_CHECK_TAG + self.cycle_suffix(cycle)
        )

        non_none_plateaus = [plateau for plateau in pm.i_ref_plateaus_i_min_op if plateau is not None]
        if len(non_none_plateaus) != len(pm.i_ref_plateaus_i_min_op):
            check_logger.critical("Check failed: Prerequisite not met: One or both I_MIN_OP plateau(s) not found.")
            return False

        return all(
            [
                analog_checks.check_average_value_is_within_bounds(
                    self.fgc,
                    pm.v_meas,
                    plateau,
                    (
                        -self.parameters.voltage_offset_parameters.v_meas_offset,
                        self.parameters.voltage_offset_parameters.v_meas_offset,
                    ),
                    "V",
                    check_logger,
                )
                for plateau in non_none_plateaus
            ]
        )

    def _check_current_leads_resistance(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.CURRENT_LEADS_RESISTANCE_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if pm.i_ref_plateau_i_pno is None:
            check_logger.critical("Check failed: Prerequisite not met: I_PNO plateau not found.")
            return False

        return all(
            [
                analog_checks.check_average_value_is_within_bounds(
                    self.fgc,
                    r_lead,
                    pm.i_ref_plateau_i_pno,
                    self.parameters.current_leads_resistance_parameters.resistance_bounds,
                    "Ohm",
                    check_logger,
                )
                for r_lead in (pm.r_lead_pos, pm.r_lead_neg)
            ]
        )

    def _check_current_leads_resistance_slope(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.CURRENT_LEADS_RESISTANCE_SLOPE_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if pm.i_ref_plateau_i_pno is None:
            check_logger.critical("Check failed: Prerequisite not met: I_PNO plateau not found.")
            return False

        return all(
            [
                analog_checks.check_average_value_is_within_bounds(
                    self.fgc,
                    dr_lead_dt,
                    pm.i_ref_plateau_i_pno,
                    self.parameters.current_leads_resistance_slope_parameters.current_leads_resistance_derivative_bounds,
                    "Ohm/s",
                    check_logger,
                    skipped_nanoseconds_from_the_start=int(
                        self.parameters.current_leads_resistance_slope_parameters.sav_gol_window_duration / 2
                    ),
                    skipped_nanoseconds_at_the_end=int(
                        self.parameters.current_leads_resistance_slope_parameters.sav_gol_window_duration / 2
                    ),
                )
                for dr_lead_dt in (pm.dr_lead_dt_pos, pm.dr_lead_dt_neg)
            ]
        )

    def _check_magnet_inductance(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.MAGNET_INDUCTANCE_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if self.parameters.inductance_parameters is None:
            check_logger.info("Check skipped.")
            return True

        if pm.l_mag_check_windows[0] is None or pm.l_mag_check_windows[1] is None:
            check_logger.critical("Check failed: Prerequisite not met: Time windows for checking L_MAG not found.")
            return False

        return analog_checks.check_value_is_within_bounds(
            self.fgc,
            pm.l_mag_average,
            "The average of L_MAG during ramp up and ramp down",
            self.parameters.inductance_parameters.magnet_inductance_absolute_bounds,
            "H",
            check_logger,
        )

    def _check_magnet_resistance(self, cycle: Cycle) -> bool:
        pm = self.pm[cycle]

        check_logger = commons.start_logging_new_check(
            self._logger, self.MAGNET_RESISTANCE_CHECK_TAG + self.cycle_suffix(cycle)
        )

        if pm.i_ref_plateau_i_pno is None:
            check_logger.critical("Check failed: Prerequisite not met: I_PNO plateau not found.")
            return False

        return analog_checks.check_average_value_is_within_bounds(
            self.fgc,
            pm.r_magnet,
            pm.i_ref_plateau_i_pno,
            self.parameters.magnet_resistance_parameters.resistance_bounds,
            "Ohm",
            check_logger,
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output

    def cycle_suffix(self, cycle: Cycle) -> str:
        return " (positive cycle)" if cycle == Cycle.POSITIVE else " (negative cycle)"


def calculate_i_min_op_plateaus(
    i_ref: pd.Series, i_ref_plateau_i_pno: tuple[int, int], i_min_op: float
) -> tuple[tuple[int, int] | None, tuple[int, int] | None]:
    all_i_ref_nxcals_plateaus_i_min_op = processing.get_regions_equal_to(i_ref.abs(), i_min_op)

    return (
        processing.find_last_plateau_that_starts_before(i_ref_plateau_i_pno[0], all_i_ref_nxcals_plateaus_i_min_op),
        processing.find_first_plateau_that_starts_after(i_ref_plateau_i_pno[1], all_i_ref_nxcals_plateaus_i_min_op),
    )


def calculate_magnet_resistance(
    r_meas: pd.Series, r_lead_pos: pd.Series, r_lead_neg: pd.Series, r_tot_measured: float
) -> pd.Series:
    return r_meas.sub(r_lead_pos).sub(r_lead_neg).sub(r_tot_measured).rename("R_MAG")


def calculate_magnet_voltage(
    v_meas: pd.Series, u_lead_pos: pd.Series, u_lead_neg: pd.Series, i_meas: pd.Series, r_tot_measured: float
) -> pd.Series:
    return v_meas.sub(u_lead_pos).sub(u_lead_neg).sub(i_meas.mul(r_tot_measured)).rename("V_MAG")


def calculate_magnet_inductance(
    v_meas: pd.Series,
    u_lead_pos: pd.Series,
    u_lead_neg: pd.Series,
    i_meas: pd.Series,
    r_tot_measured: float,
    didt_pno: float,
) -> pd.Series:
    return (
        calculate_magnet_voltage(v_meas, u_lead_pos, u_lead_neg, i_meas, r_tot_measured)
        .div(didt_pno)
        .abs()
        .rename("L_MAG")
    )


def calculate_l_mag_check_windows(
    i_meas: pd.Series,
    bounds: tuple[float, float],
    i_ref_plateau_i_pno: tuple[int, int] | None,
    i_ref_plateaus_i_min_op: tuple[tuple[int, int] | None, tuple[int, int] | None],
) -> tuple[tuple[int, int] | None, tuple[int, int] | None]:
    w1, w2 = tuple(
        (
            processing.get_time_window_where_signal_is_between(i_meas.abs(), (plat1[1], plat2[0]), bounds)
            if plat1 is not None and plat2 is not None
            else None
        )
        for plat1, plat2 in (
            (i_ref_plateaus_i_min_op[0], i_ref_plateau_i_pno),
            (i_ref_plateau_i_pno, i_ref_plateaus_i_min_op[1]),
        )
    )
    return (w1, w2)
