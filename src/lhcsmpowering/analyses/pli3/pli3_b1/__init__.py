from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pli3B1Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):
    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    TIMESTAMP_ALLOWED_WINDOW = 40_000_000  # 40 ms

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0002  # 2%

    PC_CURRENT_DECAY_CHECK_TAG = "PC current decay check"
    SKIPPED_NS_START_OF_DECAY = 40_000_000  # 40 ms
    NOMINAL_CURRENT_FACTOR_FOR_CURRENT_DECAY = 0.1  # 10%
    END_DECAY_CURRENT_VALUE = 0.5  # A

    EE_DISCHARGE_CHECK_TAG = "Energy extraction discharge check"

    def query(self) -> None:
        super().query()
        if signal_metadata.get_circuit_type_for_circuit_name(
            self.input.circuit_name
        ) != "600A" or not signal_metadata.has_ee(self.input.circuit_name):
            raise ValueError("This analysis is only for 600A circuits with EE.")

        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        [self.fgc_ref] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)
        self.fgc_pm_self_ref = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )
        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.hwc_powering_test_parameters_ref = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start_ref, self.input.t_end_ref - self.input.t_start_ref
        )
        self.i_interm_2 = float(self.hwc_powering_test_parameters[self.fgc]["I_INTERM_2"]["value"])
        self.i_interm_2_ref = float(self.hwc_powering_test_parameters_ref[self.fgc_ref]["I_INTERM_2"]["value"])
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.time_crowbar = int(self.hwc_powering_test_parameters[self.fgc]["TIME_CROWBAR"]["value"] * 1e9)

        # FGC
        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.fgc_pm_self_data_headers_ref = query.query_pm_data_headers(
            "FGC",
            self.fgc_pm_self_ref,
            self.fgc_ref,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )
        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)
        self.timestamp_fgc_ref = processing.read_timestamp_from_pm_headers_at_index(
            self.fgc_pm_self_data_headers_ref, 0
        )
        self.i_meas, self.i_ref = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc, ["STATUS.I_MEAS", "STATUS.I_REF"], self.timestamp_fgc
        )
        self.i_meas_ref, self.i_ref_ref = query.query_pm_data_signals(
            "FGC", self.fgc_pm_self_ref, self.fgc_ref, ["STATUS.I_MEAS", "STATUS.I_REF"], self.timestamp_fgc_ref
        )
        if not self.i_meas.empty and self.timestamp_fgc is not None:
            self.point_current_reaches_05A = processing.detect_first_point_below_threshold(
                self.i_meas, self.timestamp_fgc, self.i_meas.index[-1], self.END_DECAY_CURRENT_VALUE
            )
        self.interm_2_plateau = processing.get_first_plateau(self.i_ref.loc[: self.timestamp_fgc], self.i_interm_2)
        self.interm_2_plateau_ref = processing.get_first_plateau(self.i_ref_ref, self.i_interm_2_ref)

        # PIC
        self.st_abort_pic = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            f"{self.input.circuit_name}:ST_ABORT_PIC",
            include_latest_data_point_prior_to_start=True,
        )
        self.timestamp_st_abort_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMGNA", self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_qps_a, self.timestamp_qps_b = (
            processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 0),
            processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 1),
        )

        # EE
        [self.ee] = signal_metadata.get_ee_names(self.input.circuit_name, self.input.t_start)
        self.ee_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMSN600", self.ee, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_ee = processing.read_timestamp_from_pm_headers_at_index(self.ee_pm_data_headers, 0)

        if self.timestamp_ee is not None:
            from lhcsmee.analyses import EE600ASignalsAnalysis

            self.ee_analysis = EE600ASignalsAnalysis(
                self.input.circuit_name, self.ee, self.timestamp_ee, True, self._logger
            )
            self.ee_analysis.set_spark(self._spark)
            self.ee_analysis.query()

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        return all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 2, logger_adapter),
                digital_checks.check_signal_changes_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
                    self.timestamp_qps_a, self.timestamp_qps_b, logger_adapter
                ),
                timestamp_checks.check_ee_pm_data_headers_length(self.ee, self.ee_pm_data_headers, 1, logger_adapter),
                timestamp_checks.check_timestamps_are_within_margin(
                    {
                        "FGC": self.timestamp_fgc,
                        "QPS A": self.timestamp_qps_a,
                        "QPS B": self.timestamp_qps_b,
                        "EE": self.timestamp_ee,
                        "PIC": self.timestamp_st_abort_pic,
                    },
                    self.TIMESTAMP_ALLOWED_WINDOW,
                    logger_adapter,
                ),
            ]
        )

    def _check_pc_current_profile(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)
        if self.interm_2_plateau is None:
            logger_adapter.error("Check failed: No plateau found.")
            return False

        return all(
            [
                analog_checks.check_minimum_plateau_duration(
                    self.fgc, self.i_ref, self.interm_2_plateau, self.time_crowbar, logger_adapter
                ),
                analog_checks.check_plateau_values_absolute_deviation(
                    self.fgc,
                    self.i_meas,
                    self.interm_2_plateau,
                    self.i_interm_2,
                    self.FACTOR_OF_THE_NOMINAL_CURRENT * self.i_pno,
                    "A",
                    logger_adapter,
                    skip_last_point=True,
                ),
            ]
        )

    def _check_pc_current_decay(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG)
        if not self.pm_buffers_presence_and_timestamps_check or not self.pc_current_profile_consistency_check:
            logger_adapter.error("Check skipped: Previous checks failed.")
            return False
        return all(
            [
                analog_checks.check_target_current_is_equal_to_reference(
                    self.i_interm_2, self.i_interm_2_ref, logger_adapter
                ),
                analog_checks.check_minimum_plateau_duration(
                    self.fgc_ref, self.i_ref_ref, self.interm_2_plateau_ref, self.time_crowbar, logger_adapter
                ),
                analog_checks.check_absolute_deviation_wrt_reference(
                    self.fgc,
                    self.i_meas,
                    (self.timestamp_fgc, self.point_current_reaches_05A),
                    self.i_meas_ref,
                    (self.timestamp_fgc_ref, self.i_meas_ref.index[-1]),
                    self.NOMINAL_CURRENT_FACTOR_FOR_CURRENT_DECAY * self.i_pno,
                    "A",
                    logger_adapter,
                    skipped_nanoseconds_from_the_start=self.SKIPPED_NS_START_OF_DECAY,
                ),
            ]
        )

    def _check_ee_analyses(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EE_DISCHARGE_CHECK_TAG)
        if self.timestamp_ee is None:
            logger_adapter.error("Check failed: No EE timestamp found.")
            return False
        self.ee_analysis.analyze()
        self.ee_check = self.ee_analysis.get_analysis_output()

        if not self.ee_check:
            logger_adapter.error(
                "EE discharge checks failed. Please run the 600AEE_PM_Analysis.ipynb notebook for more information."
            )
        else:
            logger_adapter.info(
                "EE discharge checks passed. Please run the 600AEE_PM_Analysis.ipynb notebook for more information."
            )
        return self.ee_check

    def analyze(self) -> None:
        super().analyze()
        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.pc_current_profile_consistency_check = self._check_pc_current_profile()
        self.pc_current_decay_check = self._check_pc_current_decay()
        self.ee_check = self._check_ee_analyses()

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.pc_current_profile_consistency_check,
                self.pc_current_decay_check,
                self.ee_check,
            ]
        )
