from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata
from lhcsmqh import analyses as qh

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pic2CircuitQuenchViaQpsForIPQAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    TIMESTAMP_ALLOWED_WINDOW = 40_000_000  # 40 ms

    FGC_B1_STATUS_CHECK_TAG = "FGC B1 status check"
    FGC_B2_STATUS_CHECK_TAG = "FGC B2 status check"
    FGC_FAST_ABORT_TIMESLOT = (-20_000_000, 40_000_000)
    FGC_PC_DISCH_RQ_TIMESLOT = (-20_000_000, 40_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-30_000_000, 40_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_ABORT_PIC_TIMESLOT = (-2_000_000, 0)
    CMD_ABORT_PIC_TIMESLOT = (-1_000, 1_000)

    QPS_STATUS_CHECK_TAG = "QPS status check"
    QPS_ST_CIRCUIT_OK_TIMESLOT = (-1_000_000, 600_000_000)

    QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG = "QDS linear ramp on LD1 check"
    QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL = 0.0031  # 3.1 mV
    QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION = 0.00015  # 0.15 mV
    QDS_ST_LEAD_OK_FLIP_TIMESLOT = (-500_000_000, 500_000_000)

    QH_DISCHARGE_CHECK_TAG = "QH discharge check"

    def query(self) -> None:
        super().query()

        # FGC
        self.fgc_b1, self.fgc_b2 = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")

        self.fgc_pm_data_headers_b1 = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc_b1, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.fgc_pm_data_headers_b2 = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc_b2, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc_b1 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers_b1, 0)
        self.timestamp_fgc_b2 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers_b2, 0)

        self.st_faults_b1, self.st_unlatched_b1 = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc_b1, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc_b1
        )
        self.st_faults_b2, self.st_unlatched_b2 = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc_b2, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc_b2
        )

        # QPS
        qps_pm_class_name = signal_metadata.get_qps_pm_class_name_for_ipq(self.input.circuit_name)
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", qps_pm_class_name, self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_qds_a = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 0)
        self.timestamp_qds_b = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 1)

        st_circuit_ok_qps = f"stat.{self.input.circuit_name}:ST_CIRCUIT_OK_QPS"
        self.st_circuit_ok_qps_a = query.query_pm_data_signals(
            "QPS", qps_pm_class_name, self.input.circuit_name, st_circuit_ok_qps, self.timestamp_qds_a
        )
        self.st_circuit_ok_qps_b = query.query_pm_data_signals(
            "QPS", qps_pm_class_name, self.input.circuit_name, st_circuit_ok_qps, self.timestamp_qds_b
        )
        [dfb] = signal_metadata.get_dfb_names(self.input.circuit_name, self.input.t_start)
        self.ld1_u_hts, self.ld1_st_lead_ok = query.query_pm_data_signals(
            "QPS",
            qps_pm_class_name,
            self.input.circuit_name,
            [f"stat.{dfb}.LD1:U_HTS", f"stat.{dfb}.LD1:ST_LEAD_OK"],
            self.timestamp_qds_a,
        )

        # PIC
        self.st_abort_pic, self.cmd_abort_pic, self.cmd_pwr_perm_b1_pic, self.cmd_pwr_perm_b2_pic = (
            query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [
                    f"{self.input.circuit_name}:ST_ABORT_PIC",
                    f"{self.input.circuit_name}:CMD_ABORT_PIC",
                    f"{self.input.circuit_name}:CMD_PWR_PERM_B1_PIC",
                    f"{self.input.circuit_name}:CMD_PWR_PERM_B2_PIC",
                ],
                include_latest_data_point_prior_to_start=True,
            )
        )
        self.timestamp_st_abort_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # QH
        self.qh_voltage_analysis = qh.QuenchHeaterVoltageAnalysis(
            "Quench Heaters", self.input.circuit_name, 900, self.input.t_start, self.input.t_end
        )
        self.qh_voltage_analysis.query()

        # PM Event Trigger Absolute
        self.pm_event_trigger = min(
            (
                timestamp
                for timestamp in [
                    self.timestamp_fgc_b1,
                    self.timestamp_fgc_b2,
                    self.timestamp_qds_a,
                    self.timestamp_qds_b,
                ]
                if timestamp is not None
            ),
            default=None,
        )

    def analyze(self) -> None:
        super().analyze()

        self.cmd_pwr_perm_b1_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_b1_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )
        self.cmd_pwr_perm_b2_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_b2_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc_b1, self.fgc_pm_data_headers_b1, 1, logger_adapter
                ),
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc_b2, self.fgc_pm_data_headers_b2, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 2, logger_adapter),
                digital_checks.check_signal_changes_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
                    self.timestamp_qds_a, self.timestamp_qds_b, logger_adapter
                ),
                timestamp_checks.check_timestamps_are_within_margin(
                    {
                        "FGC_B1": self.timestamp_fgc_b1,
                        "FGC_B2": self.timestamp_fgc_b2,
                        "QDS A": self.timestamp_qds_a,
                        "QDS B": self.timestamp_qds_b,
                        "PIC": self.timestamp_st_abort_pic,
                    },
                    self.TIMESTAMP_ALLOWED_WINDOW,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_B1_STATUS_CHECK_TAG)
        self.fgc_b1_status_check = all(
            [
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_faults_b1, "FAST_ABORT"),
                    True,
                    self.pm_event_trigger,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched_b1, "PC_DISCH_RQ"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b1, "PC_PERMIT"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_B2_STATUS_CHECK_TAG)
        self.fgc_b2_status_check = all(
            [
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_faults_b2, "FAST_ABORT"),
                    True,
                    self.pm_event_trigger,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched_b2, "PC_DISCH_RQ"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b2, "PC_PERMIT"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        pic_status_checks = []
        for cmd_pwr_perm_pic_false_timestamp in [
            self.cmd_pwr_perm_b1_pic_false_timestamp,
            self.cmd_pwr_perm_b2_pic_false_timestamp,
        ]:
            pic_status_checks.extend(
                [
                    digital_checks.check_signal_changes_once_to(
                        self.st_abort_pic,
                        False,
                        cmd_pwr_perm_pic_false_timestamp,
                        self.ST_ABORT_PIC_TIMESLOT,
                        logger_adapter,
                    ),
                    digital_checks.check_signal_changes_to(
                        self.cmd_abort_pic,
                        False,
                        cmd_pwr_perm_pic_false_timestamp,
                        self.CMD_ABORT_PIC_TIMESLOT,
                        logger_adapter,
                    ),
                ]
            )
        self.pic_status_check = all(pic_status_checks)

        logger_adapter = commons.start_logging_new_check(self._logger, self.QPS_STATUS_CHECK_TAG)
        qps_status_checks = []
        for cmd_pwr_perm_pic_false_timestamp in [
            self.cmd_pwr_perm_b1_pic_false_timestamp,
            self.cmd_pwr_perm_b2_pic_false_timestamp,
        ]:
            for st_circuit_ok in [self.st_circuit_ok_qps_a, self.st_circuit_ok_qps_b]:
                qps_status_checks.append(
                    digital_checks.check_signal_changes_to(
                        st_circuit_ok,
                        False,
                        cmd_pwr_perm_pic_false_timestamp,
                        self.QPS_ST_CIRCUIT_OK_TIMESLOT,
                        logger_adapter,
                    )
                )
        self.qps_status_check = all(qps_status_checks)

        logger_adapter = commons.start_logging_new_check(self._logger, self.QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG)
        self.timestamp_before_st_lead_ok_change = processing.detect_last_point_before_signal_changes_to(
            self.ld1_st_lead_ok, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
        )
        self.qds_linear_ramp_on_ld1_check = all(
            [
                analog_checks.check_signal_value_is_within_absolute_margin(
                    self.ld1_u_hts,
                    self.timestamp_before_st_lead_ok_change,
                    self.QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL,
                    self.QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION,
                    logger_adapter,
                    "V",
                ),
                digital_checks.check_signal_changes_once_to(
                    self.ld1_st_lead_ok,
                    False,
                    min(
                        [
                            timestamp
                            for timestamp in (self.timestamp_qds_a, self.timestamp_qds_b)
                            if timestamp is not None
                        ],
                        default=None,
                    ),
                    self.QDS_ST_LEAD_OK_FLIP_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.QH_DISCHARGE_CHECK_TAG)

        self.qh_voltage_analysis.analyze()
        self.qh_voltage_check = self.qh_voltage_analysis.get_analysis_output()

        if self.qh_voltage_check:
            logger_adapter.info("Check passed: QH discharge parameters are within expected ranges.")
        else:
            logger_adapter.error("Check failed: QH discharge parameters are outside expected ranges.")

        self.analysis_output = all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.fgc_b1_status_check,
                self.fgc_b2_status_check,
                self.pic_status_check,
                self.qps_status_check,
                self.qds_linear_ramp_on_ld1_check,
                self.qh_voltage_analysis,
            ]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
