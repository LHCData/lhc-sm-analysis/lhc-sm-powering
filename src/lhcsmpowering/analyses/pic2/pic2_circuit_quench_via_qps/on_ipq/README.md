# PIC2 CIRCUIT QUENCH VIA QPS - IPQ

## Introduction

The purpose of this test is to commission the protection functionalities of the Powering Interlock Controllers (PIC) and all its connected systems at low operating current (`I_MIN_OP`) in both apertures. During the test, the quench is simulated by applying a voltage ramp on the first (LD1:`U_HTS`) of the three current leads, each protected by the quench detector (DQQDC). Quench detection is based on measuring an absolute value of the voltage (`U_HTS`) over the High Temperature Superconductor (HTS) part of the current lead.  Once a quench is detected, the trip of the Powering Interlock Loop is triggered and the quench heaters of the circuit (DQHDS) are activated.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM**.
    - **2 x QPS PM**.
    - `ST_ABORT_PIC` changes to `False` during duration of the entire test.
    - QPS PM Board A and B are **1 ms** apart.
    - FGC PMs, QPS PMs and `ST_ABORT_PIC` from NXCALS are within **±40 ms**.

2. **FGC status check (PM event corresponds to the earliest timestamp among FGC PMs and QPS PMs)**
    - `ST_FAULTS` for bit `FAST_ABORT` changes once to `True` starting **20 ms** before and ending **40 ms** after the PM event for each FGC.
    - `ST_UNLATCHED` for bit `PC_DISCH_RQ` is equal to `False` starting **20 ms** before and ending **40 ms** after the PM event for each FGC.
    - `ST_UNLATCHED` for bit `PC_PERMIT` changes once to `False` starting **30 ms** before and ending **40 ms** after the PM event for each FGC.

3. **PIC status check**
    - `ST_ABORT_PIC` changes once to `False` between **2 ms** before and ending at `CMD_PWR_PERM_B1_PIC` change\*.
    - `ST_ABORT_PIC` changes once to `False` between **2 ms** before and ending at `CMD_PWR_PERM_B2_PIC` change\*.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** after `CMD_PWR_PERM_B1_PIC` change\*.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** after `CMD_PWR_PERM_B2_PIC` change\*.

4. **QPS status check**
    - `ST_CIRCUIT_OK` changes to `False` starting **1 ms** before and ending **600 ms** after `CMD_PWR_PERM_B1_PIC` change\* for each QPS board.
    - `ST_CIRCUIT_OK` changes to `False` starting **1 ms** before and ending **600 ms** after `CMD_PWR_PERM_B2_PIC` change\* for each QPS board.

5. **QDS linear ramp on LD1 check**
    - Quench detection level is **3.1 ± 0.15 mV** (defined from experimental data).
    - Quench detection time is within **±500 ms** w.r.t. QPS PM timestamp (discrimination time is longer than two data acquisition periods). If this criterion is not met, check that quench detection time is within **+18 to +22s** w.r.t. QPS PM timestamp (defined from experimental data).

6. **QH discharge check**
    - Initial voltage is from **810 V to 1000 V**.
    - Final voltage is from **0 V to 20 V**.
    - Discharge decay time is within **±5 ms** w.r.t. reference discharge.

(\*) `CMD_PWR_PERM_B1_PIC` / `CMD_PWR_PERM_B2_PIC` change refers to the first change to `False` that should occur starting **40 ms** before and ending **40 ms** after the first PM event.

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.2, <https://edms.cern.ch/document/874884/4.2>.
2. _Some details about QPS_, <https://twiki.cern.ch/twiki/pub/MP3/QPS/Some_Details_of_QPS-JS.pdf>.
