from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata
from lhcsmqh import analyses as qh

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pic2CircuitQuenchViaQpsForIPDAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    TIMESTAMP_ALLOWED_WINDOW = 40_000_000  # 40 ms

    QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG = "QDS linear ramp on LD1 check"
    QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL = 0.0031  # 3.1 mV
    QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION = 0.00015  # 0.15 mV

    QH_DISCHARGE_CHECK_TAG = "QH discharge check"

    def query(self) -> None:
        super().query()

        # FGC
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)

        # PIC
        self.st_abort_pic = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            f"{self.input.circuit_name}:ST_ABORT_PIC",
            include_latest_data_point_prior_to_start=True,
        )
        self.timestamp_st_abort_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # QPS
        qps_pm_class_name = signal_metadata.get_qps_pm_class_name_for_ipd(self.input.circuit_name, self.input.t_start)
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", qps_pm_class_name, self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_qds_a = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 0)
        self.timestamp_qds_b = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 1)

        [dfb] = signal_metadata.get_dfb_names(self.input.circuit_name, self.input.t_start)
        self.ld1_u_hts, self.ld1_st_lead_ok = query.query_pm_data_signals(
            "QPS",
            qps_pm_class_name,
            self.input.circuit_name,
            [f"stat.{dfb}.LD1:U_HTS", f"stat.{dfb}.LD1:ST_LEAD_OK"],
            self.timestamp_qds_a,
        )

        # QH
        self.qh_analysis = qh.QuenchHeaterVoltageAnalysis(
            "Quench Heaters", self.input.circuit_name, 900, self.input.t_start, self.input.t_end
        )
        self.qh_analysis.query()

        self.pm_event_trigger = min(
            (
                timestamp
                for timestamp in [self.timestamp_fgc, self.timestamp_qds_a, self.timestamp_qds_b]
                if timestamp is not None
            ),
            default=None,
        )

    def analyze(self) -> None:
        super().analyze()

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 2, logger_adapter),
                digital_checks.check_signal_changes_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
                    self.timestamp_qds_a, self.timestamp_qds_b, logger_adapter
                ),
                timestamp_checks.check_timestamps_are_within_margin(
                    {
                        "FGC": self.timestamp_fgc,
                        "QDS A": self.timestamp_qds_a,
                        "QDS B": self.timestamp_qds_b,
                        "PIC": self.timestamp_st_abort_pic,
                    },
                    self.TIMESTAMP_ALLOWED_WINDOW,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG)
        self.timestamp_before_st_lead_ok_change = processing.detect_last_point_before_signal_changes_to(
            self.ld1_st_lead_ok, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
        )
        self.qds_linear_ramp_on_ld1_check = analog_checks.check_signal_value_is_within_absolute_margin(
            self.ld1_u_hts,
            self.timestamp_before_st_lead_ok_change,
            self.QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL,
            self.QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION,
            logger_adapter,
            "V",
        )

        # QH check
        logger_adapter = commons.start_logging_new_check(self._logger, self.QH_DISCHARGE_CHECK_TAG)

        self.qh_analysis.analyze()

        self.qh_voltage_check = self.qh_analysis.get_analysis_output()

        if self.qh_voltage_check:
            logger_adapter.info("Check passed: QH discharge parameters are within expected ranges.")
        else:
            logger_adapter.error("Check failed: QH discharge parameters are outside expected ranges.")

        self.analysis_output = all(
            [self.pm_buffers_presence_and_timestamps_check, self.qds_linear_ramp_on_ld1_check, self.qh_voltage_check]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
