# PIC2 CIRCUIT QUENCH VIA QPS - IPD

## Introduction

The purpose of this test is to commission the protection functionalities of the Powering Interlock Controllers (PIC) and all its connected systems at low operating current (`I_MIN_OP`) in both apertures. During the test, the quench is simulated by applying a voltage ramp on the first (LD1:`U_HTS`) of the three current leads, each protected by the quench detector (DQQDC). Quench detection is based on measuring an absolute value of the voltage (`U_HTS`) over the High Temperature Superconductor (HTS) part of the current lead.  Once a quench is detected, the trip of the Powering Interlock Loop is triggered and the quench heaters of the circuit (DQHDS) are activated.

## Acceptance Criteria

1. **PM Buffers presence and timestamps check**
    - **1 x FGC PM**.
    - **2 x QPS PM**.
    - `ST_ABORT_PIC` changes to `False` during duration of the entire test.
    - QPS PM Board A and B are **1 ms** apart.
    - FGC PM, QPS PMs and `ST_ABORT_PIC` from NXCALS are within **±40 ms**.

2. **QDS linear ramp on LD1 check**
    - Quench detection level is **3.1 ± 0.15 mV** (defined from experimental data).

3. **QH discharge check**
    - Initial voltage is from **810 V to 1000 V**.
    - Final voltage is from **0 V to 20 V**.
    - Discharge decay time is within **±5 ms** w.r.t. reference discharge.

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.0, <https://edms.cern.ch/document/874884/4.0> .
2. _Analysis IPQ_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/Analysis_IPQ>.
3. _Some details about QPS_, <https://twiki.cern.ch/twiki/pub/MP3/QPS/Some_Details_of_QPS-JS.pdf>.
