# PIC2 CIRCUIT QUENCH VIA QPS - 600A

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM**.
    - **2 x QPS PM**.
    - **1 x EE PM** (if applicable).
    - `ST_ABORT_PIC` changes to `False` during duration of the entire test.
    - QPS PM Board A and B are **1 ms** apart.
    - FGC PMs, QPS PMs and `ST_ABORT_PIC` from NXCALS are within **±40 ms**.

2. **FGC status check (PM event corresponds to the earliest timestamp among FGC, QPS and EE timestamps)**
    - `ST_FAULTS` for bit `FAST_ABORT` changes once to `True` starting **20 ms** before and ending **30 ms** after the PM event.
    - `ST_UNLATCHED` for bit `PC_DISCH_RQ` is equal to `False` starting **20 ms** before and ending **30 ms** after the PM event.
    - `ST_UNLATCHED` for bit `PWR_FAILURE` is equal to `False` starting **20 ms** before the first PM event and ending **4 ms** after `ST_FAULTS` for bit `FAST_ABORT` changes to `True`.
    - `ST_UNLATCHED` for bit `PC_PERMIT` changes once to `False` starting **20 ms** before and ending **30 ms** after the PM event.

3. **PIC status check**
    - `ST_FAILURE_PIC` is equal to `True` starting **20 ms** before and ending **20 ms** after `CMD_PWR_PERM_PIC` change\*.
    - `ST_ABORT_PIC` changes once to `False` between **2 ms** before and ending at `CMD_PWR_PERM_PIC` change\*.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** at `CMD_PWR_PERM_PIC` change\*.

4. **QPS status check**
    - `ST_CIRCUIT_OK` changes to `False` for circuits protected by DQQDG or remains equal to `True` for circuits protected by nDQQDG, starting **2 ms** before and ending **400 ms** after `CMD_POWER_PERMIT_PIC` changes\* for each QPS board. The sampling interval can be used to validate if the board is protected by DQQDG (200 ms) or nDQQDG (41 ms).

5. **QDS linear ramp on LD1 check**
    - Quench detection level is **3.0 ± 0.2 mV** (defined from experimental data).
    - Quench detection time is within **±500 ms** w.r.t. QPS PM timestamp. However, due to a firmware bug, it can also be from **18 s to 22 s** after the QPS PM timestamp.

6. **EE status check (PM event corresponds to the earliest timestamp among FGC, QPS and EE)**
    - `ST_FPA_REC_0`, `ST_FPA_REC_1`, `ST_SW_A_OPEN` and `ST_SW_B_OPEN` changes to `True` within the **200 ms** period before and after the PM event.
    - `ST_SW_Z_OPEN` remains `False` from 500 ms before the PM event until **2000 ms** after the PM event.

7. **EE voltage check (PM event corresponds to the earliest timestamp among FGC, QPS and EE)**
    - `U_CAP_A` and `U_CAP_B` are equal to 51.0 ± 35 V 30 ms after the PM event.
    - `U_CAP_Z` is equal to 165.0 ± 35 V 30 ms after the PM event.

(\*) `CMD_PWR_PERM_PIC` change refers to the first change to false, that should occur starting 40 ms before and ending 40 ms after the first PM event.

## Source

1. _Test Procedure and Acceptance Criteria for the 600 A Circuits_, EDMS 874716 v.5.4, <https://edms.cern.ch/document/874716/5.4> .
