from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, signal_checks, timestamp_checks


class Pic2CircuitQuenchViaQpsFor600AAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    TIMESTAMP_ALLOWED_WINDOW = 40_000_000  # 40 ms

    FGC_STATUS_CHECK_TAG = "FGC status check"
    FGC_FAST_ABORT_TIMESLOT = (-20_000_000, 30_000_000)
    FGC_PC_DISCH_RQ_TIMESLOT = (-20_000_000, 30_000_000)
    FGC_PWR_FAILURE_TIMESLOT = (-20_000_000, 4_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-20_000_000, 30_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_FAILURE_PIC_TIMESLOT = (-20_000_000, 20_000_000)
    ST_ABORT_PIC_TIMESLOT = (-2_000_000, 0)
    CMD_ABORT_PIC_TIMESLOT = (-1_000, 1_000)

    QPS_STATUS_CHECK_TAG = "QPS status check"
    QPS_ST_CIRCUIT_OK_TIMESLOT = (-2_000_000, 400_000_000)

    QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG = "QDS linear ramp on LD1 check"
    QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL = 0.0030  # 3.0 mV
    QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION = 0.0002  # 0.2 mV
    QDS_ST_LEAD_OK_FLIP_TIMESLOT = (-500_000_000, 500_000_000)
    QDS_ST_LEAD_OK_FLIP_TIMESLOT_BACKUP = (18_000_000_000, 22_000_000_000)

    EE_STATUS_CHECK_TAG = "EE status check"
    ST_FPA_REC_TIMESLOT = (-200_000_000, 200_000_000)
    ST_SW_AB_OPEN_TIMESLOT = (-200_000_000, 200_000_000)
    ST_SW_Z_OPEN_TIMESLOT = (-500_000_000, 2_000_000_000)

    EE_VOLTAGE_CHECK_TAG = "EE voltage check"
    U_CAP_AB_TARGET_VOLTAGE = 51  # 51 V
    U_CAP_ABSOLUTE_MARGIN = 35  # 35 V
    U_CAP_Z_TARGET_VOLTAGE = 165  # 165 V
    U_CAP_OFFSET = 30_000_000  # 30 ms

    def query(self) -> None:
        super().query()

        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)

        # FGC
        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)

        self.st_faults, self.st_unlatched = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc
        )
        self.fast_abort = processing.decode_state(self.st_faults, "FAST_ABORT")
        self.pc_disch_rq = processing.decode_state(self.st_unlatched, "PC_DISCH_RQ")
        self.pwr_failure = processing.decode_state(self.st_unlatched, "PWR_FAILURE")
        self.pc_permit = processing.decode_state(self.st_unlatched, "PC_PERMIT")

        # PIC
        self.cmd_pwr_perm_pic, self.st_failure_pic, self.st_abort_pic, self.cmd_abort_pic = (
            query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [
                    f"{self.input.circuit_name}:CMD_PWR_PERM_PIC",
                    f"{self.input.circuit_name}:ST_FAILURE_PIC",
                    f"{self.input.circuit_name}:ST_ABORT_PIC",
                    f"{self.input.circuit_name}:CMD_ABORT_PIC",
                ],
                include_latest_data_point_prior_to_start=True,
            )
        )
        self.timestamp_st_abort_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMGNA", self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_qds_a = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 0)
        self.timestamp_qds_b = processing.read_timestamp_from_pm_headers_at_index(self.qps_pm_data_headers, 1)

        st_circuit_ok_qps = f"stat.{self.input.circuit_name}:ST_CIRCUIT_OK_QPS"
        self.st_circuit_ok_qps_a = query.query_pm_data_signals(
            "QPS", "DQAMGNA", self.input.circuit_name, st_circuit_ok_qps, self.timestamp_qds_a
        )
        self.st_circuit_ok_qps_b = query.query_pm_data_signals(
            "QPS", "DQAMGNA", self.input.circuit_name, st_circuit_ok_qps, self.timestamp_qds_b
        )
        [dfb] = signal_metadata.get_dfb_names(self.input.circuit_name, self.input.t_start)
        self.ld1_u_hts, self.ld1_st_lead_ok = query.query_pm_data_signals(
            "QPS",
            "DQAMGNA",
            self.input.circuit_name,
            [f"stat.{dfb}.LD1:U_HTS", f"stat.{dfb}.LD1:ST_LEAD_OK"],
            self.timestamp_qds_a,
        )

        # EE
        self.timestamp_ee = None
        self.has_ee = signal_metadata.has_ee(self.input.circuit_name)
        if self.has_ee:
            [self.ee] = signal_metadata.get_ee_names(self.input.circuit_name, self.input.t_start)
            self.ee_pm_data_headers = query.query_pm_data_headers(
                "QPS", "DQAMSN600", self.ee, self.input.t_start, self.input.t_end - self.input.t_start
            )
            self.timestamp_ee = processing.read_timestamp_from_pm_headers_at_index(self.ee_pm_data_headers, 0)

            (
                self.st_fpa_rec_0,
                self.st_fpa_rec_1,
                self.st_sw_a_open,
                self.st_sw_b_open,
                self.st_sw_z_open,
                self.u_cap_a,
                self.u_cap_b,
                self.u_cap_z,
            ) = query.query_pm_data_signals(
                "QPS",
                "DQAMSN600",
                self.ee,
                [
                    "ST_FPA_REC_0",
                    "ST_FPA_REC_1",
                    "ST_SW_A_OPEN",
                    "ST_SW_B_OPEN",
                    "ST_SW_Z_OPEN",
                    "U_CAP_A",
                    "U_CAP_B",
                    "U_CAP_Z",
                ],
                self.timestamp_ee,
            )

        # Compute timestamps
        self.pm_event_trigger = min(
            (
                timestamp
                for timestamp in [self.timestamp_fgc, self.timestamp_qds_a, self.timestamp_qds_b, self.timestamp_ee]
                if timestamp is not None
            ),
            default=None,
        )

        self.fast_abort_true_timestamp = processing.detect_when_signal_first_changes_to(
            self.fast_abort, True, self.pm_event_trigger, self.FGC_FAST_ABORT_TIMESLOT, self._logger
        )

        self.timestamp_cmd_pwr_perm_pic = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.fgc_status_check = self._check_fgc_status()
        self.pic_status_check = self._check_pic_status()
        self.qps_status_check = self._check_qps_status()
        self.qds_linear_ramp_on_ld1_check = self._check_qds_linear_ramp_on_ld1()
        self.ee_status_check = self._check_ee_status()
        self.ee_voltage_check = self._check_ee_voltage()

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        dict_timestamps = {
            "FGC": self.timestamp_fgc,
            "QDS A": self.timestamp_qds_a,
            "QDS B": self.timestamp_qds_b,
            "PIC": self.timestamp_st_abort_pic,
        }
        if self.has_ee:
            dict_timestamps["EE"] = self.timestamp_ee
        return all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 2, logger_adapter),
                (
                    timestamp_checks.check_ee_pm_data_headers_length(
                        self.ee, self.ee_pm_data_headers, 1, logger_adapter
                    )
                    if self.has_ee
                    else True
                ),
                digital_checks.check_signal_changes_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_timestamps_gap(
                    self.timestamp_qds_a, self.timestamp_qds_b, logger_adapter
                ),
                timestamp_checks.check_timestamps_are_within_margin(
                    dict_timestamps, self.TIMESTAMP_ALLOWED_WINDOW, logger_adapter
                ),
            ]
        )

    def _check_fgc_status(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_STATUS_CHECK_TAG)
        return all(
            [
                digital_checks.check_signal_changes_once_to(
                    self.fast_abort, True, self.pm_event_trigger, self.FGC_FAST_ABORT_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.pc_disch_rq, False, self.pm_event_trigger, self.FGC_PC_DISCH_RQ_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_between_two_timestamps_remains_equal_to(
                    self.pwr_failure,
                    False,
                    self.pm_event_trigger,
                    self.fast_abort_true_timestamp,
                    logger_adapter,
                    self.FGC_PWR_FAILURE_TIMESLOT[0],
                    self.FGC_PWR_FAILURE_TIMESLOT[1],
                ),
                digital_checks.check_signal_changes_once_to(
                    self.pc_permit, False, self.pm_event_trigger, self.FGC_PC_PERMIT_TIMESLOT, logger_adapter
                ),
            ]
        )

    def _check_pic_status(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        return all(
            [
                digital_checks.check_signal_remains_equal_to(
                    self.st_failure_pic,
                    True,
                    self.timestamp_cmd_pwr_perm_pic,
                    self.ST_FAILURE_PIC_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    self.st_abort_pic,
                    False,
                    self.timestamp_cmd_pwr_perm_pic,
                    self.ST_ABORT_PIC_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_to(
                    self.cmd_abort_pic,
                    False,
                    self.timestamp_cmd_pwr_perm_pic,
                    self.CMD_ABORT_PIC_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

    def _check_qps_status(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.QPS_STATUS_CHECK_TAG)
        qps_status_checks = []
        for st_circuit_ok in [self.st_circuit_ok_qps_a, self.st_circuit_ok_qps_b]:
            if signal_metadata.get_circuit_location(self.input.circuit_name).startswith(
                "RR"
            ) or signal_metadata.is_undulator(self.input.circuit_name):
                qps_status_checks.extend(
                    [
                        digital_checks.check_signal_remains_equal_to(
                            st_circuit_ok,
                            True,
                            self.timestamp_cmd_pwr_perm_pic,
                            self.QPS_ST_CIRCUIT_OK_TIMESLOT,
                            logger_adapter,
                        ),
                        signal_checks.check_signal_sampling_interval(st_circuit_ok, 41_000_000, logger_adapter),
                    ]
                )
            else:
                qps_status_checks.extend(
                    [
                        digital_checks.check_signal_changes_to(
                            st_circuit_ok,
                            False,
                            self.timestamp_cmd_pwr_perm_pic,
                            self.QPS_ST_CIRCUIT_OK_TIMESLOT,
                            logger_adapter,
                        ),
                        signal_checks.check_signal_sampling_interval(st_circuit_ok, 200_000_000, logger_adapter),
                    ]
                )
        return all(qps_status_checks)

    def _check_qds_linear_ramp_on_ld1(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.QDS_LINEAR_RAMP_ON_LD1_CHECK_TAG)
        self.timestamp_before_st_lead_ok_change = processing.detect_last_point_before_signal_changes_to(
            self.ld1_st_lead_ok, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
        )
        qds_ld1_u_hts_check = analog_checks.check_signal_value_is_within_absolute_margin(
            self.ld1_u_hts,
            self.timestamp_before_st_lead_ok_change,
            self.QDS_EXPECTED_QUENCH_DETECTION_VOLTAGE_LEVEL,
            self.QDS_ALLOWED_QUENCH_DETECTION_VOLTAGE_DEVIATION,
            logger_adapter,
            "V",
        )
        ld1_st_lead_ok_check = digital_checks.check_signal_changes_once_to(
            self.ld1_st_lead_ok,
            False,
            min(
                (timestamp for timestamp in [self.timestamp_qds_a, self.timestamp_qds_b] if timestamp is not None),
                default=None,
            ),
            self.QDS_ST_LEAD_OK_FLIP_TIMESLOT,
            logger_adapter,
        )
        if ld1_st_lead_ok_check is False:
            logger_adapter.info("Trying to check with a different time slot to avoid firmware bug.")
            ld1_st_lead_ok_check = digital_checks.check_signal_changes_once_to(
                self.ld1_st_lead_ok,
                False,
                min(
                    (timestamp for timestamp in [self.timestamp_qds_a, self.timestamp_qds_b] if timestamp is not None),
                    default=None,
                ),
                self.QDS_ST_LEAD_OK_FLIP_TIMESLOT_BACKUP,
                logger_adapter,
            )
        return all([qds_ld1_u_hts_check, ld1_st_lead_ok_check])

    def _check_ee_status(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EE_STATUS_CHECK_TAG)
        if not self.has_ee:
            logger_adapter.info(f"No EE for circuit {self.input.circuit_name}, skipping EE checks")
            return True

        return all(
            [
                digital_checks.check_signal_changes_to(
                    self.st_fpa_rec_0, True, self.pm_event_trigger, self.ST_FPA_REC_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_changes_to(
                    self.st_fpa_rec_1, True, self.pm_event_trigger, self.ST_FPA_REC_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_changes_to(
                    self.st_sw_a_open, True, self.pm_event_trigger, self.ST_SW_AB_OPEN_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_changes_to(
                    self.st_sw_b_open, True, self.pm_event_trigger, self.ST_SW_AB_OPEN_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.st_sw_z_open, False, self.pm_event_trigger, self.ST_SW_Z_OPEN_TIMESLOT, logger_adapter
                ),
            ]
        )

    def _check_ee_voltage(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EE_VOLTAGE_CHECK_TAG)
        if not self.has_ee:
            logger_adapter.info(f"No EE for circuit {self.input.circuit_name}, skipping EE checks")
            return True

        return all(
            [
                analog_checks.check_signal_value_is_within_absolute_margin(
                    self.u_cap_a,
                    self.timestamp_ee,
                    self.U_CAP_AB_TARGET_VOLTAGE,
                    self.U_CAP_ABSOLUTE_MARGIN,
                    logger_adapter,
                    "V",
                    offset=self.U_CAP_OFFSET,
                ),
                analog_checks.check_signal_value_is_within_absolute_margin(
                    self.u_cap_b,
                    self.timestamp_ee,
                    self.U_CAP_AB_TARGET_VOLTAGE,
                    self.U_CAP_ABSOLUTE_MARGIN,
                    logger_adapter,
                    "V",
                    offset=self.U_CAP_OFFSET,
                ),
                analog_checks.check_signal_value_is_within_absolute_margin(
                    self.u_cap_z,
                    self.timestamp_ee,
                    self.U_CAP_Z_TARGET_VOLTAGE,
                    self.U_CAP_ABSOLUTE_MARGIN,
                    logger_adapter,
                    "V",
                    offset=self.U_CAP_OFFSET,
                ),
            ]
        )

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.fgc_status_check,
                self.pic_status_check,
                self.qps_status_check,
                self.qds_linear_ramp_on_ld1_check,
                self.ee_status_check,
                self.ee_voltage_check,
            ]
        )
