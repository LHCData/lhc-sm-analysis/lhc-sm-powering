# PIC2 FAST ABORT REQ VIA PIC - 600A

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM**.
    - **0 x QPS PM**.
    - **1 x EE PM** (if applicable).

1. **FGC status check**
    - `ST_FAULTS` for bit `FAST_ABORT` changes once to `True` starting **20 ms** before and ending **30 ms** after the first PM event\*\*.
    - `ST_UNLATCHED` for bit `PC_DISCH_RQ` is equal to `False` starting **20 ms** before and ending **30 ms** after the first PM event\*\*.
    - `ST_UNLATCHED` for bit `PWR_FAILURE` is equal to `False` starting **20 ms** before the first PM event\*\* and ending **4 ms** after `ST_FAULTS` for bit `FAST_ABORT` changes to `True`.
    - `ST_UNLATCHED` for bit `PC_PERMIT` changes once to `False` starting **20 ms** before and ending **30 ms** after the first PM event\*\*.

1. **PIC status check**
    - `ST_FAILURE_PIC` is equal to `True` starting **20 ms** before and ending **20 ms** after `CMD_POWER_PERMIT_PIC` change\*.
    - `ST_ABORT_PIC` changes to `False` starting at `CMD_POWER_PERMIT_PIC` change\* and ending **2 ms** after.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** at `CMD_POWER_PERMIT_PIC` change\*.

1. **EE status check**
    - `ST_FPA_REC_0`, `ST_FPA_REC_1`, `ST_SW_A_OPEN` and `ST_SW_B_OPEN` changes to `True` within the **200 ms** period before and after the first PM event\*\*.
    - `ST_SW_Z_OPEN` remains `False` from **500 ms** before the first PM event\*\* until **2000 ms** after the first PM event\*\*.

1. **EE voltage check**
    - `U_CAP_A` and `U_CAP_B` are equal to **51.0 ± 35 V** at **30 ms** after the first PM event\*\*.
    - `U_CAP_Z` is equal to **165.0 ± 35 V** at **30ms** after the first PM event\*\*.

(\*) CMD_POWER_PERMIT_PIC change refers to the first change to false, that should occur starting 40 ms before and ending 40 ms after the first PM event\*\*.

(\*\*) The first PM event corresponds to the earliest timestamp among FGC, QPS and EE. (Here, "event" is simply a naming convention for individual PM data dumps).

## Source

1. _Commissioning of the Hardware in the LHC Sectors: Interlock Tests of Powering Subsector Prior & After Connection of the Power Cables to DFB Leads_,  519704 v.1.0, <https://edms.cern.ch/document/519704/1.0>.
