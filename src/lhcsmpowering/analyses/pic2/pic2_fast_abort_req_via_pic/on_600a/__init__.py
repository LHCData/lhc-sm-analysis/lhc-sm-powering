from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pic2FastAbortReqViaPicFor600AAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"

    FGC_STATUS_CHECK_TAG = "FGC status check"
    FGC_FAST_ABORT_TIMESLOT = (-20_000_000, 30_000_000)
    FGC_PC_DISCH_RQ_TIMESLOT = (-20_000_000, 30_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-20_000_000, 30_000_000)
    FGC_PWR_FAILURE_TIMESLOT = (-20_000_000, 4_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_FAILURE_PIC_TIMESLOT = (-20_000_000, 20_000_000)
    ST_ABORT_PIC_TIMESLOT = (0, 2_000_000)
    CMD_ABORT_PIC_TIMESLOT = (-1_000, 1_000)

    EE_STATUS_CHECK_TAG = "EE status check"
    ST_FPA_REC_TIMESLOT = (-200_000_000, 200_000_000)
    ST_SW_AB_OPEN_TIMESLOT = (-200_000_000, 200_000_000)
    ST_SW_Z_OPEN_TIMESLOT = (-500_000_000, 2_000_000_000)

    EE_VOLTAGE_CHECK_TAG = "EE voltage check"
    U_CAP_AB_TARGET_VOLTAGE = 51  # V
    U_CAP_ABSOLUTE_MARGIN = 35  # V
    U_CAP_Z_TARGET_VOLTAGE = 165  # V
    U_CAP_OFFSET = 30_000_000  # ns

    FGC_PM_CLASS_NAME = "DQAMSN600"

    def query(self) -> None:
        super().query()

        # FGC
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")

        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)

        self.st_faults, self.st_unlatched = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc
        )

        self.st_faults_fast_abort = processing.decode_state(self.st_faults, "FAST_ABORT")

        self.timestamp_ee = None
        # EE
        self.has_ee = signal_metadata.has_ee(self.input.circuit_name)
        if self.has_ee:
            [self.ee] = signal_metadata.get_ee_names(self.input.circuit_name, self.input.t_start)
            ee_pm_data_headers = query.query_pm_data_headers(
                "QPS", self.FGC_PM_CLASS_NAME, self.ee, self.input.t_start, self.input.t_end - self.input.t_start
            )
            self.timestamp_ee = processing.read_timestamp_from_pm_headers_at_index(ee_pm_data_headers, 0)

            (
                self.st_fpa_rec_0,
                self.st_fpa_rec_1,
                self.st_sw_a_open,
                self.st_sw_b_open,
                self.st_sw_z_open,
                self.u_cap_a,
                self.u_cap_b,
                self.u_cap_z,
            ) = query.query_pm_data_signals(
                "QPS",
                self.FGC_PM_CLASS_NAME,
                self.ee,
                [
                    "ST_FPA_REC_0",
                    "ST_FPA_REC_1",
                    "ST_SW_A_OPEN",
                    "ST_SW_B_OPEN",
                    "ST_SW_Z_OPEN",
                    "U_CAP_A",
                    "U_CAP_B",
                    "U_CAP_Z",
                ],
                self.timestamp_ee,
            )

        # PIC
        (self.st_failure_pic, self.st_abort_pic, self.cmd_abort_pic, self.cmd_pwr_perm_pic) = (
            query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [
                    f"{self.input.circuit_name}:ST_FAILURE_PIC",
                    f"{self.input.circuit_name}:ST_ABORT_PIC",
                    f"{self.input.circuit_name}:CMD_ABORT_PIC",
                    f"{self.input.circuit_name}:CMD_PWR_PERM_PIC",
                ],
                include_latest_data_point_prior_to_start=True,
            )
        )

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMGNA", self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        self.pm_event_trigger = min(
            [timestamp for timestamp in (self.timestamp_fgc, self.timestamp_ee) if timestamp is not None], default=None
        )
        self.st_faults_fast_abort_true_timestamp = processing.detect_when_signal_first_changes_to(
            self.st_faults_fast_abort, True, self.pm_event_trigger, self.FGC_FAST_ABORT_TIMESLOT, self._logger
        )

    def analyze(self) -> None:
        super().analyze()

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 0, logger_adapter),
            ]
        )

        self.cmd_pwr_perm_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_STATUS_CHECK_TAG)

        self.fgc_signal_check = all(
            [
                digital_checks.check_signal_changes_once_to(
                    self.st_faults_fast_abort, True, self.pm_event_trigger, self.FGC_FAST_ABORT_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched, "PC_DISCH_RQ"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_between_two_timestamps_remains_equal_to(
                    processing.decode_state(self.st_unlatched, "PWR_FAILURE"),
                    False,
                    self.pm_event_trigger,
                    self.st_faults_fast_abort_true_timestamp,
                    logger_adapter,
                    self.FGC_PWR_FAILURE_TIMESLOT[0],
                    self.FGC_PWR_FAILURE_TIMESLOT[1],
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched, "PC_PERMIT"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        self.pic_signal_check = all(
            [
                digital_checks.check_signal_remains_equal_to(
                    self.st_failure_pic,
                    True,
                    self.cmd_pwr_perm_pic_false_timestamp,
                    self.ST_FAILURE_PIC_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_to(
                    self.st_abort_pic,
                    False,
                    self.cmd_pwr_perm_pic_false_timestamp,
                    self.ST_ABORT_PIC_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_to(
                    self.cmd_abort_pic,
                    False,
                    self.cmd_pwr_perm_pic_false_timestamp,
                    self.CMD_ABORT_PIC_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        if self.has_ee:
            logger_adapter = commons.start_logging_new_check(self._logger, self.EE_STATUS_CHECK_TAG)
            self.ee_status_check = all(
                [
                    digital_checks.check_signal_changes_to(
                        self.st_fpa_rec_0, True, self.pm_event_trigger, self.ST_FPA_REC_TIMESLOT, logger_adapter
                    ),
                    digital_checks.check_signal_changes_to(
                        self.st_fpa_rec_1, True, self.pm_event_trigger, self.ST_FPA_REC_TIMESLOT, logger_adapter
                    ),
                    digital_checks.check_signal_changes_to(
                        self.st_sw_a_open, True, self.pm_event_trigger, self.ST_SW_AB_OPEN_TIMESLOT, logger_adapter
                    ),
                    digital_checks.check_signal_changes_to(
                        self.st_sw_b_open, True, self.pm_event_trigger, self.ST_SW_AB_OPEN_TIMESLOT, logger_adapter
                    ),
                    digital_checks.check_signal_remains_equal_to(
                        self.st_sw_z_open, False, self.pm_event_trigger, self.ST_SW_Z_OPEN_TIMESLOT, logger_adapter
                    ),
                ]
            )

            logger_adapter = commons.start_logging_new_check(self._logger, self.EE_VOLTAGE_CHECK_TAG)
            self.ee_voltage_check = all(
                [
                    analog_checks.check_signal_value_is_within_absolute_margin(
                        self.u_cap_a,
                        self.timestamp_ee,
                        self.U_CAP_AB_TARGET_VOLTAGE,
                        self.U_CAP_ABSOLUTE_MARGIN,
                        logger_adapter,
                        "V",
                        offset=self.U_CAP_OFFSET,
                    ),
                    analog_checks.check_signal_value_is_within_absolute_margin(
                        self.u_cap_b,
                        self.timestamp_ee,
                        self.U_CAP_AB_TARGET_VOLTAGE,
                        self.U_CAP_ABSOLUTE_MARGIN,
                        logger_adapter,
                        "V",
                        offset=self.U_CAP_OFFSET,
                    ),
                    analog_checks.check_signal_value_is_within_absolute_margin(
                        self.u_cap_z,
                        self.timestamp_ee,
                        self.U_CAP_Z_TARGET_VOLTAGE,
                        self.U_CAP_ABSOLUTE_MARGIN,
                        logger_adapter,
                        "V",
                        offset=self.U_CAP_OFFSET,
                    ),
                ]
            )

            self.analysis_output = all(
                [self.fgc_signal_check, self.pic_signal_check, self.ee_status_check, self.ee_voltage_check]
            )

        else:
            self.analysis_output = all([self.fgc_signal_check, self.pic_signal_check])

    def get_analysis_output(self) -> bool:
        return self.analysis_output
