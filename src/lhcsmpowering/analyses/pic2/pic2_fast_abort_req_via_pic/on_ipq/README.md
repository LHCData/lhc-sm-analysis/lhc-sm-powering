# PIC2 FAST ABORT REQ VIA PIC - IPQ

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM**.
    - **0 x QPS PM**.

1. **FGC status check (first PM event\*\* corresponds to the earliest timestamp among FGC PMs and QPS PMs)**
    - `ST_FAULTS` for bit `FAST_ABORT` changes once to `True` starting **20 ms** before and ending **40 ms** after the first PM event\*\* for each FGC.
    - `ST_UNLATCHED` for bit `PC_DISCH_RQ` is equal to `False` starting **20 ms** before and ending **40 ms** after the first PM event\*\* for each FGC.
    - `ST_UNLATCHED` for bit `PC_PERMIT` changes once to `False` starting **30 ms** before and ending **40 ms** after the first PM event\*\* for each FGC.

1. **PIC status check**
    - `ST_ABORT_PIC` changes once to `False` between `CMD_PWR_PERM_B1_PIC` change\* and ending **2 ms** after `CMD_PWR_PERM_B1_PIC` change\*.
    - `ST_ABORT_PIC` changes once to `False` between `CMD_PWR_PERM_B2_PIC` change\* and ending **2 ms** after `CMD_PWR_PERM_B2_PIC` change\*.
    - `ST_FAILURE_PIC` is equal to `True` between **20 ms** before `CMD_PWR_PERM_B1_PIC` change\* and **4 ms** after `ST_ABORT_PIC` change to `False`.
    - `ST_FAILURE_PIC` is equal to `True` between **20 ms** before `CMD_PWR_PERM_B2_PIC` change\* and **4 ms** after `ST_ABORT_PIC` change to `False`.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** after `CMD_PWR_PERM_B1_PIC` change\*.
    - `CMD_ABORT_PIC` changes to `False` between **1 µs** before and ending **1 µs** after `CMD_PWR_PERM_B2_PIC` change\*.

(\*) `CMD_PWR_PERM_B1_PIC` / `CMD_PWR_PERM_B2_PIC` change refers to the first change to False that should occur starting **40 ms** before and ending **40 ms** after the first PM event\*\*.

(\*\*) The first PM event corresponds to the earliest timestamp among FGC, QPS and EE ("event" is just a naming convention in this case, we are referring to individual PM data dumps).

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.2, <https://edms.cern.ch/document/874884/4.2>.
