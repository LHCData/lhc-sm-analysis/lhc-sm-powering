# PIC2 POWERING FAILURE - IPQ

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (one per FGC).
    - **0 x QPS PM**.

2. **FGC status check (B1)**
    - `ST_FAULTS` from **B1** for bit `FAST_ABORT` is equal to **False** in the time period starting **20 ms** before and ending **20 ms** after the **first** PM event\*\*.
    - `ST_UNLATCHED` from **B1** for bit `PC_DISCH_RQ` is equal to **False** in the time period starting **20 ms** before and ending **20 ms** after the **first** PM event\*\*.
    - `ST_UNLATCHED` from **B1** for bit `PWR_FAILURE` changes once to **True** in the time period starting **5 ms** before and ending **5 ms** after the **first** PM event\*\*.
    - `ST_UNLATCHED` from **B1** for bit `PC_PERMIT` changes once to **False** in the time period starting **5 ms** before and ending **5 ms** after the **first** PM event\*\*.

3. **FGC status check (B2)**
    - Equivalent checks as the ones for **B1** (above) are performed for **B2** and relative to the **second** PM event**.

4. **PIC status check**
    - `ST_FAILURE_PIC` changes once to **False** in the time period starting **2 ms** before and ending when `CMD_PWR_PERM_B1_PIC` changes to **False**\*.
    - `ST_FAILURE_PIC` changes once to **False** in the time period starting **2 ms** before and ending when `CMD_PWR_PERM_B2_PIC` changes to **False**\*.
    - `CMD_ABORT_PIC` is equal to **True** in the time period starting **10 s** before the **first** PM event and ending **10 s** after the **second** PM event\*\*.
    - `ST_ABORT_PIC` is equal to **True** in the time period starting **10 s** before the **first** PM event and ending **10 s** after the **second** PM event\*\*.

(\*) `CMD_PWR_PERM_B1_PIC` / `CMD_PWR_PERM_B2_PIC` change refers to the first change to **False** that occurs starting **40 ms** before and ending **40 ms** after the **first** PM event for `CMD_PWR_PERM_B1_PIC` and the **second** PM event\*\* for `CMD_PWR_PERM_B2_PIC`.

(\*\*) The first PM event corresponds to the earliest timestamp among FGC ("event" refers to individual PM data dumps as a naming convention). The second PM event corresponds to the second earliest timestamp among FGC.

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.2, <https://edms.cern.ch/document/874884/4.2>.
