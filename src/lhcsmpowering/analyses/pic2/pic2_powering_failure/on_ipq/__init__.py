from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import digital_checks, timestamp_checks


class Pic2PoweringFailureForIPQAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"

    FGC_B1_STATUS_CHECK_TAG = "FGC B1 status check"
    FGC_B2_STATUS_CHECK_TAG = "FGC B2 status check"
    FGC_FAST_ABORT_TIMESLOT = (-20_000_000, 20_000_000)
    FGC_PC_DISCH_RQ_TIMESLOT = (-20_000_000, 20_000_000)
    FGC_PWR_FAILURE_TIMESLOT = (-5_000_000, 5_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-5_000_000, 5_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_FAILURE_PIC_TIMESLOT = (-2_000_000, 0)
    CMD_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)
    ST_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)

    def query(self) -> None:
        super().query()

        # FGC
        self.fgc_b1, self.fgc_b2 = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")

        self.fgc_pm_data_headers_b1 = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc_b1, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.fgc_pm_data_headers_b2 = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc_b2, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc_b1 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers_b1, 0)
        self.timestamp_fgc_b2 = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers_b2, 0)

        self.st_faults_b1, self.st_unlatched_b1 = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc_b1, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc_b1
        )
        self.st_faults_b2, self.st_unlatched_b2 = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc_b2, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc_b2
        )

        # QPS
        qps_pm_class_name = signal_metadata.get_qps_pm_class_name_for_ipq(self.input.circuit_name)
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", qps_pm_class_name, self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # PIC
        (
            self.st_failure_pic,
            self.st_abort_pic,
            self.cmd_abort_pic,
            self.cmd_pwr_perm_b1_pic,
            self.cmd_pwr_perm_b2_pic,
        ) = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            [
                f"{self.input.circuit_name}:ST_FAILURE_PIC",
                f"{self.input.circuit_name}:ST_ABORT_PIC",
                f"{self.input.circuit_name}:CMD_ABORT_PIC",
                f"{self.input.circuit_name}:CMD_PWR_PERM_B1_PIC",
                f"{self.input.circuit_name}:CMD_PWR_PERM_B2_PIC",
            ],
            include_latest_data_point_prior_to_start=True,
        )

        # PM Event Trigger Absolute
        self.pm_event_trigger = min(
            (timestamp for timestamp in [self.timestamp_fgc_b1, self.timestamp_fgc_b2] if timestamp is not None),
            default=None,
        )

        self.cmd_pwr_perm_b1_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_b1_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )
        self.cmd_pwr_perm_b2_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_b2_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

    def analyze(self) -> None:
        super().analyze()

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc_b1, self.fgc_pm_data_headers_b1, 1, logger_adapter
                ),
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc_b2, self.fgc_pm_data_headers_b2, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 0, logger_adapter),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_B1_STATUS_CHECK_TAG)
        self.fgc_b1_signal_check = all(
            [
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_faults_b1, "FAST_ABORT"),
                    False,
                    self.timestamp_fgc_b1,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched_b1, "PC_DISCH_RQ"),
                    False,
                    self.timestamp_fgc_b1,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b1, "PWR_FAILURE"),
                    True,
                    self.timestamp_fgc_b1,
                    self.FGC_PWR_FAILURE_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b1, "PC_PERMIT"),
                    False,
                    self.timestamp_fgc_b1,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_B2_STATUS_CHECK_TAG)
        self.fgc_b2_signal_check = all(
            [
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_faults_b2, "FAST_ABORT"),
                    False,
                    self.timestamp_fgc_b2,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched_b2, "PC_DISCH_RQ"),
                    False,
                    self.timestamp_fgc_b2,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b2, "PWR_FAILURE"),
                    True,
                    self.timestamp_fgc_b2,
                    self.FGC_PWR_FAILURE_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched_b2, "PC_PERMIT"),
                    False,
                    self.timestamp_fgc_b2,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        pic_status_checks = []
        for cmd_pwr_perm_pic in [self.cmd_pwr_perm_b1_pic, self.cmd_pwr_perm_b2_pic]:
            pic_status_checks.append(
                digital_checks.check_signal_changes_once_to(
                    cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, logger_adapter
                )
            )
        for cmd_pwr_perm_pic_false_timestamp in [
            self.cmd_pwr_perm_b1_pic_false_timestamp,
            self.cmd_pwr_perm_b2_pic_false_timestamp,
        ]:
            pic_status_checks.append(
                digital_checks.check_signal_changes_once_to(
                    self.st_failure_pic,
                    False,
                    cmd_pwr_perm_pic_false_timestamp,
                    self.ST_FAILURE_PIC_TIMESLOT,
                    logger_adapter,
                )
            )
        if self.timestamp_fgc_b1 is None or self.timestamp_fgc_b2 is None or self.pm_event_trigger is None:
            logger_adapter.error(
                f"Pre-condition failed: Timestamps for FGCs {self.fgc_b1} and {self.fgc_b2} are not available. "
                f"timestamp_fgc_b1: {self.timestamp_fgc_b1}, timestamp_fgc_b2: {self.timestamp_fgc_b2}"
            )
        else:
            pic_status_checks.extend(
                [
                    digital_checks.check_signal_remains_equal_to(
                        self.cmd_abort_pic,
                        True,
                        self.pm_event_trigger,
                        (
                            min(self.timestamp_fgc_b1, self.timestamp_fgc_b2)
                            + self.CMD_ABORT_PIC_TIMESLOT[0]
                            - self.pm_event_trigger,
                            max(self.timestamp_fgc_b1, self.timestamp_fgc_b2)
                            + self.CMD_ABORT_PIC_TIMESLOT[1]
                            - self.pm_event_trigger,
                        ),
                        logger_adapter,
                    ),
                    digital_checks.check_signal_remains_equal_to(
                        self.st_abort_pic,
                        True,
                        self.pm_event_trigger,
                        (
                            min(self.timestamp_fgc_b1, self.timestamp_fgc_b2)
                            + self.ST_ABORT_PIC_TIMESLOT[0]
                            - self.pm_event_trigger,
                            max(self.timestamp_fgc_b1, self.timestamp_fgc_b2)
                            + self.ST_ABORT_PIC_TIMESLOT[1]
                            - self.pm_event_trigger,
                        ),
                        logger_adapter,
                    ),
                ]
            )
        self.pic_signal_check = all(pic_status_checks)

        self.analysis_output = all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.fgc_b1_signal_check,
                self.fgc_b2_signal_check,
                self.pic_signal_check,
            ]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
