# PIC2 POWERING FAILURE - 600A and IPD circuits

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM**.
    - **0 x QPS PM**.
    - 0 x EE PM (if applicable).

2. **FGC status check**
    - `ST_FAULTS` for bit `FAST_ABORT` is equal to **False** in the time period starting **5 s** before and ending **5 s** after the first PM event\*\*.
    - `ST_UNLATCHED` for bit `PC_DISCH_RQ` is equal to **False** in the time period starting **5 s** before and ending **5 s** after the first PM event\*\*.
    - `ST_UNLATCHED` for bit `PWR_FAILURE` changes once to **True** in the time period starting **25 ms** before and ending **25 ms** after the first PM event\*\*.
    - `ST_UNLATCHED` for bit `PC_PERMIT` changes once to **False** in the time period starting **25 ms** before and ending **45 ms** after the first PM event\*\*.

3. **PIC status check**
    - `ST_FAILURE_PIC` changes once to **False** in the time period starting **2 ms** before and ending **1 ms** before `CMD_POWER_PERMIT_PIC` changes to **False**\*.
    - `CMD_ABORT_PIC` is equal to **True** in the time period starting **10 s** before and ending **10 s** after the first PM event\*\*.
    - `ST_ABORT_PIC` is equal to **True** in the time period starting **10 s** before and ending **10 s** after the first PM event\*\*.

(\*) The `CMD_POWER_PERMIT_PIC` change to **False** is detected in the time window starting **40 ms** before and ending **40 ms** after the first PM event\*\*.

(\*\*) The first PM event corresponds to the FGC timestamp ("event" refers to individual PM data dumps as a naming convention).
