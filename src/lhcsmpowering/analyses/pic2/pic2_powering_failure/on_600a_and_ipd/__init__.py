from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import digital_checks, timestamp_checks


class Pic2PoweringFailureFor600AAndIPDAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"

    FGC_STATUS_CHECK_TAG = "FGC status check"
    FGC_FAST_ABORT_TIMESLOT = (-5_000_000_000, 5_000_000_000)
    FGC_PC_DISCH_RQ_TIMESLOT = (-5_000_000_000, 5_000_000_000)
    FGC_PWR_FAILURE_TIMESLOT = (-25_000_000, 25_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-25_000_000, 45_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_FAILURE_PIC_TIMESLOT = (-2_000_000, -1_000_000)
    CMD_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)
    ST_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)

    def query(self) -> None:
        super().query()

        # FGC
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.circuit_interlock_type = signal_metadata.get_circuit_interlock_type(self.input.circuit_name)

        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)
        self.st_faults, self.st_unlatched = query.query_pm_data_signals(
            "FGC", fgc_pm_class_name, self.fgc, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], self.timestamp_fgc
        )

        # QPS
        qps_pm_class_name = (
            "DQAMGNA"
            if signal_metadata.is_600a(self.input.circuit_name)
            else signal_metadata.get_qps_pm_class_name_for_ipd(self.input.circuit_name, self.input.t_start)
        )
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", qps_pm_class_name, self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # EE
        self.ee = None
        if signal_metadata.is_600a(self.input.circuit_name) and signal_metadata.has_ee(self.input.circuit_name):
            [self.ee] = signal_metadata.get_ee_names(self.input.circuit_name, self.input.t_start)
            self.ee_pm_data_headers = query.query_pm_data_headers(
                "QPS", "DQAMSN600", self.ee, self.input.t_start, self.input.t_end - self.input.t_start
            )

        # PIC
        self.cmd_pwr_perm_pic, self.st_failure_pic, self.cmd_abort_pic, self.st_abort_pic = (
            query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [
                    f"{self.input.circuit_name}:CMD_PWR_PERM_PIC",
                    f"{self.input.circuit_name}:ST_FAILURE_PIC",
                    f"{self.input.circuit_name}:CMD_ABORT_PIC",
                    f"{self.input.circuit_name}:ST_ABORT_PIC",
                ],
                include_latest_data_point_prior_to_start=True,
            )
        )

        # PM Event Trigger Absolute
        self.pm_event_trigger = self.timestamp_fgc

        self.cmd_pwr_perm_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

    def analyze(self) -> None:
        super().analyze()

        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        self.pm_buffers_presence_and_timestamps_check = all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 0, logger_adapter),
                (
                    timestamp_checks.check_ee_pm_data_headers_length(
                        self.ee, self.ee_pm_data_headers, 0, logger_adapter
                    )
                    if self.ee is not None
                    else True
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.FGC_STATUS_CHECK_TAG)
        self.fgc_signal_check = all(
            [
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_faults, "FAST_ABORT"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    processing.decode_state(self.st_unlatched, "PC_DISCH_RQ"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_DISCH_RQ_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched, "PWR_FAILURE"),
                    True,
                    self.pm_event_trigger,
                    self.FGC_PWR_FAILURE_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    processing.decode_state(self.st_unlatched, "PC_PERMIT"),
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    logger_adapter,
                ),
            ]
        )

        logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        self.pic_signal_check = all(
            [
                digital_checks.check_signal_changes_once_to(
                    self.cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_changes_once_to(
                    self.st_failure_pic,
                    False,
                    self.cmd_pwr_perm_pic_false_timestamp,
                    self.ST_FAILURE_PIC_TIMESLOT,
                    logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.cmd_abort_pic, True, self.pm_event_trigger, self.CMD_ABORT_PIC_TIMESLOT, logger_adapter
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.st_abort_pic, True, self.pm_event_trigger, self.ST_ABORT_PIC_TIMESLOT, logger_adapter
                ),
            ]
        )

        self.analysis_output = all(
            [self.pm_buffers_presence_and_timestamps_check, self.fgc_signal_check, self.pic_signal_check]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
