from __future__ import annotations

from dataclasses import dataclass

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import digital_checks, timestamp_checks


class Pic2PoweringFailureForITAnalysis(commons.PoweringAnalysis[commons.TestParameterInput]):

    @dataclass(frozen=True)
    class FgcSignals:
        pm_data_headers: pd.DataFrame
        timestamp: int | None
        fast_abort: pd.Series
        pc_disch_rq: pd.Series
        pwr_failure: pd.Series
        pc_permit: pd.Series

    CMD_PWR_PERM_PIC_TIMESLOT = (-40_000_000, 40_000_000)

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    FGC_PM_DATA_HEADERS_LENGTH = 3

    FGC_MAIN_STATUS_CHECK_TAG = "FGC MAIN status check"
    FGC_TRIM1_STATUS_CHECK_TAG = "FGC TRIM1 status check"
    FGC_TRIM2_STATUS_CHECK_TAG = "FGC TRIM2 status check"
    FGC_FAST_ABORT_TIMESLOT = (-20_000_000, 20_000_000)
    FGC_PC_DISH_RQ_TIMESLOT = (-20_000_000, 20_000_000)
    FGC_PWR_FAILURE_TIMESLOT = (-5_000_000, 5_000_000)
    FGC_PC_PERMIT_TIMESLOT = (-5_000_000, 5_000_000)

    PIC_STATUS_CHECK_TAG = "PIC status check"
    ST_FAILURE_PIC_TIMESLOT = (-2_000_000, 0)
    CMD_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)
    ST_ABORT_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)
    ST_DISCHARGE_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)
    CMD_DISCHARGE_PIC_TIMESLOT = (-10_000_000_000, 10_000_000_000)

    def query(self) -> None:
        super().query()

        # FGC
        self.fgcs = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        self.fgc_main, self.fgc_trim1, self.fgc_trim2 = self.fgcs
        self.fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start, "self"
        )
        self.fgc_signals = {fgc: self._query_fgc_signals(fgc) for fgc in self.fgcs}

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS", "DQAMGNC", self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # PIC
        (
            self.cmd_pwr_perm_pic,
            self.st_failure_pic,
            self.cmd_abort_pic,
            self.st_abort_pic,
            self.st_discharge_pic,
            self.cmd_discharge_pic,
        ) = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            [
                f"{self.input.circuit_name}:CMD_PWR_PERM_PIC",
                f"{self.input.circuit_name}:ST_FAILURE_PIC",
                f"{self.input.circuit_name}:CMD_ABORT_PIC",
                f"{self.input.circuit_name}:ST_ABORT_PIC",
                f"{self.input.circuit_name}:ST_DISCHARGE_PIC",
                f"{self.input.circuit_name}:CMD_DISCHARGE_PIC",
            ],
            include_latest_data_point_prior_to_start=True,
        )

        # PM Event Trigger Absolute
        self.pm_event_trigger = min(
            (fgc_signal.timestamp for fgc_signal in self.fgc_signals.values() if fgc_signal.timestamp is not None),
            default=None,
        )

        self.cmd_pwr_perm_pic_false_timestamp = processing.detect_when_signal_first_changes_to(
            self.cmd_pwr_perm_pic, False, self.pm_event_trigger, self.CMD_PWR_PERM_PIC_TIMESLOT, self._logger
        )

    def _query_fgc_signals(self, fgc: str) -> FgcSignals:
        fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", self.fgc_pm_class_name, fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(fgc_pm_data_headers, 0)
        st_faults, st_unlatched = query.query_pm_data_signals(
            "FGC", self.fgc_pm_class_name, fgc, ["STATUS.ST_FAULTS", "STATUS.ST_UNLATCHED"], timestamp_fgc
        )
        return self.FgcSignals(
            fgc_pm_data_headers,
            timestamp_fgc,
            processing.decode_state(st_faults, "FAST_ABORT"),
            processing.decode_state(st_unlatched, "PC_DISCH_RQ"),
            processing.decode_state(st_unlatched, "PWR_FAILURE"),
            processing.decode_state(st_unlatched, "PC_PERMIT"),
        )

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()

        self.fgc_checks = {
            tag: self._check_fgc_signals(fgc, tag)
            for fgc, tag in (
                (self.fgc_main, self.FGC_MAIN_STATUS_CHECK_TAG),
                (self.fgc_trim1, self.FGC_TRIM1_STATUS_CHECK_TAG),
                (self.fgc_trim2, self.FGC_TRIM2_STATUS_CHECK_TAG),
            )
        }

        self.pic_signal_check = self._check_pic_signals()

        self.analysis_output = all(
            [self.pm_buffers_presence_and_timestamps_check, all(self.fgc_checks.values()), self.pic_signal_check]
        )

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        pm_buffers_presence_and_timestamps_logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        return all(
            [
                all(
                    timestamp_checks.check_fgc_pm_data_headers_length(
                        fgc,
                        self.fgc_signals[fgc].pm_data_headers,
                        self.FGC_PM_DATA_HEADERS_LENGTH,
                        pm_buffers_presence_and_timestamps_logger_adapter,
                    )
                    for fgc in self.fgcs
                ),
                timestamp_checks.check_qps_pm_data_headers_length(
                    self.qps_pm_data_headers, 0, pm_buffers_presence_and_timestamps_logger_adapter
                ),
            ]
        )

    def _check_fgc_signals(self, fgc: str, tag: str) -> bool:
        fgc_status_check_logger_adapter = commons.start_logging_new_check(self._logger, tag)
        return all(
            [
                digital_checks.check_signal_remains_equal_to(
                    self.fgc_signals[fgc].fast_abort,
                    False,
                    self.pm_event_trigger,
                    self.FGC_FAST_ABORT_TIMESLOT,
                    fgc_status_check_logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.fgc_signals[fgc].pc_disch_rq,
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_DISH_RQ_TIMESLOT,
                    fgc_status_check_logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    self.fgc_signals[fgc].pwr_failure,
                    True,
                    self.pm_event_trigger,
                    self.FGC_PWR_FAILURE_TIMESLOT,
                    fgc_status_check_logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    self.fgc_signals[fgc].pc_permit,
                    False,
                    self.pm_event_trigger,
                    self.FGC_PC_PERMIT_TIMESLOT,
                    fgc_status_check_logger_adapter,
                ),
            ]
        )

    def _check_pic_signals(self) -> bool:
        pic_status_logger_adapter = commons.start_logging_new_check(self._logger, self.PIC_STATUS_CHECK_TAG)
        return all(
            [
                digital_checks.check_signal_changes_once_to(
                    self.cmd_pwr_perm_pic,
                    False,
                    self.pm_event_trigger,
                    self.CMD_PWR_PERM_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
                digital_checks.check_signal_changes_once_to(
                    self.st_failure_pic,
                    False,
                    self.cmd_pwr_perm_pic_false_timestamp,
                    self.ST_FAILURE_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.cmd_abort_pic,
                    True,
                    self.pm_event_trigger,
                    self.CMD_ABORT_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.st_abort_pic,
                    True,
                    self.pm_event_trigger,
                    self.ST_ABORT_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.st_discharge_pic,
                    True,
                    self.pm_event_trigger,
                    self.ST_DISCHARGE_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
                digital_checks.check_signal_remains_equal_to(
                    self.cmd_discharge_pic,
                    True,
                    self.pm_event_trigger,
                    self.CMD_DISCHARGE_PIC_TIMESLOT,
                    pic_status_logger_adapter,
                ),
            ]
        )

    def get_analysis_output(self) -> bool:
        return self.analysis_output
