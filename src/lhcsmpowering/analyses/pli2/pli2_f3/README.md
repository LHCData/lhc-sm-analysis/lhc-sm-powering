# PLI2.f3 - UNBALANCED RAMP AND MAGNET QUENCH

Circuit: IPQ

## Introduction

The purpose of this test is to validate the protection mechanism under real powering conditions but with limited amount of stored energy in the circuit. During the test, the current (`I_MEAS`) is both apertures is increased to a certain level (which depends on circuit) and in different number of steps. During the test, the quench is simulated by applying an artificial voltage ramp on the first (LD1:`U_HTS`) of the three current leads. Quench detection is based on measuring an absolute value of the voltage over the High Temperature Superconductor (HTS) part of the current lead (`U_HTS`). Once a quench is detected, the trip of the Powering Interlock Loop is triggered (`ST_ABORT_PIC`) and the quench heaters of the circuit (DQHDS) are activated.  All the relevant signals are sent to the Post Mortem (PM) system, which collects the buffers from different sub-systems, such as Power Converters (PC) and Quench Protection System (QPS).

The first check includes assessing the presence of PM buffers, the PIC signal from NXCALS (`ST_ABORT_PIC`) and checking the synchronization of these timestamps. The consistency check of the current profile includes an assessment of the duration of the plateau at the highest current for B2. In addition, the flattop current level must be within 1% w.r.t reference (*), both for B1 and B2. The current decay time check involves assessment of the decay time constant, which is also compared to the reference value. The earth current check involves an assessment of the maximum earth current expressed in percent (`I_EARTH_PCNT` from PM), which must remain below 20%. For linear ramp check, the Quench Detection System (QDS) must be able to identify a voltage rise above 3 mV within 500 ms w.r.t Function Generator Controller (FGC) PM timestamp. The selected discrimination time is longer than two data acquisition periods. The quench propagation check includes the evaluation of the delay time between the PM QPS timestamp and the timestamp of crossing the differential threshold (typically 100 mV) - criterium for this check is defined from experimental data. In addition, calculation of MIITs is performed. Analysis of the quench heater discharge includes evaluation of the initial and final values of the voltage, as well as the discharge decay time constant, which value must be compared with the reference discharge. The voltage signals are named `U_HDS_x_B1` and `U_HDS_x_B2`, where `x`=1 for a circuit with 2 quench heaters, `x`=<1,2> for a circuit with 4 quench heaters and `x`=<1,4> for a circuit with 8 quench heaters.

## MP3 Analysis

The MP3 Analysis of PLI2.f3 test consists of 7 assessments:

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (one per fgc).
    - **2 x QPS PM**.
    - **1 x PIC**.
    - QPS PMs Board A and B are **1 ms** apart.  
    - PC PMs, QPS PMs and PIC from NXCALS (`ST_ABORT_PIC`) are within **±40 ms**.  

2. **Current profile consistency check**
    - The duration of the third plateau at the highest current level for B2 is more than **300 s**.  
    - Flattop current level is within **1%** w.r.t. reference\* value for B1 and B2.  

   Input parameters in `calculate_plateau_duration_and_level()`: `plateau_end_ts` = 0, `plateau_min_duration_sec` = 1., `delta` = 0.05.

3. **Current decay time check**
    - Decay time is within **10%** w.r.t. reference\* value.

4. **Earth current check**
    - abs(`I_EARTH_PCNT`) < **20%**.  

5. **QDS linear ramp on LD1:U_HTS check**
    - Quench detection level is more than **3 mV**.  
    - Quench detection time is within **+/-500 ms** w.r.t. PM FGC timestamp.  

6. **Quench propagation check**
    - Delay time between PM QPS timestamp and crossing the differential threshold (typically 100mV) is below **0.3 s**.  
    - MIITs for B1 and B2 must be computed (no criteria).  

7. **QH discharge check**
    - Initial voltage is from **810 V to 1000 V**.  
    - Final voltage is from **0 V to 20 V**.  
    - Discharge decay time is within **±5 ms** w.r.t. reference discharge.

(\*) The reference signal is the last successful PLI2.f3 test or STEAM simulation.

**Remark 1**: All events with heater-firing at I>400 A will later be analysed using the FAST POWER ABORT (FPA) notebook and the results will be stored in the quench database.

**Remark 2**:  In the future, it is envisaged that last plateau duration for B2, average current at the last plateau for B1 and B2, and the maximum earth current expressed in percent will be compared with the corresponding parameter values stored in the Accelerator Test Tracking (`TIME_TOP`, `I_INTERM_1`, `I_EARTH_MAX`).  

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.0, <https://edms.cern.ch/ui/#!master/navigator/document?D:100720389:100720389:subDocs>
2. _Analysis IPQ_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/Analysis_IPQ>.
3. _Quench Database_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/QuenchDatabase>
