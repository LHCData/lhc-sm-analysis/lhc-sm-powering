# PLI2.e3 - UNBALANCED SLOW POWER ABORT

Circuit: IPQ

## Introduction

The aim of this test is to verify the response of both power converters in case of a Slow Power Abort (SPA) with unbalanced currents. During the test, the current (`I_MEAS`) is both apertures is increased to a certain level (which depends on circuit) and in different number of steps. Afterwards, the SPA is triggered - power converters (PC) are in state `SLOW_ABORT`.

## Acceptance Criteria

1. **PM buffers absence and timestamps check**
    - FGC state `SLOW_ABORT` changes once to True (for each fgc).
    - FGC state `FLT_OFF` changes once to True (for each fgc).
    - `SLOW_ABORT` state precedes `FLT_OFF` (for each FGC).
    - `SLOW_ABORT` state for FGC B1 and B2 are within **±40ms**.
    - **0 x QPS PM** (during the entire test).
    - **0 x FGC PM** (from test start until the last point of `I_MEAS` above `I_MIN_OP`).

2. **PC current profile consistency check**
    - `I_REF` for B1 reaches the required flat top level (`I_MIN_OP`, `I_INJECTION`, `I_INTERM_2`) and maintains it for at least (`TIME_ZERO`, `TIME_TOP`, 2 \* `TIME_TOP`)  minus **2 s** to compensate for NXCALS low data rate.
    - The measured current (`I_MEAS`) for B1 during the flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`).
    - `I_REF` for B2 reaches the required flat top level (`I_MIN_OP`, `I_INJECTION`, `I_INTERM_2`, `I_INTERM_1`) and maintains it for at least (`TIME_ZERO`, `TIME_TOP`, `TIME_TOP`, `TIME_TOP`) minus **2 s** to compensate for NXCALS low data rate.
    - The measured current (`I_MEAS`) for B2 during the flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`).

3. **DFB regulation check**
    - Temperature `TT891A` at the top of the HTS part of each of the three current leads is from **46 K to 54 K** during the test.
    - Temperature `TT893` at the top of the copper part of each of the three current leads is from **280 K to 320 K** during the test.

**Remark**: All events with heater-firing at I>400 A must be analysed using the FAST POWER ABORT (FPA) notebook and the results must be stored in the quench database.

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v.4.2, <https://edms.cern.ch/document/874884/4.2>.
