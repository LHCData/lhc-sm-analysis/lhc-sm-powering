from __future__ import annotations

from dataclasses import dataclass

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pli2E3Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):
    PM_BUFFERS_ABSENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers absence and timestamps check"
    ALLOWED_SLOW_ABORT_TIMESTAMP_DIFFERENCE = 40_000_000  # ns

    CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "Current profile consistency check"
    FACTOR_OF_THE_NOMINAL_CURRENT = 0.0002  # 0.02 % (percent of I_PNO)
    NXCALS_PLATEAU_DURATION_OFFSET = 2_000_000_000  # 2 s

    DFB_REGULATION_CHECK_TAG = "DFB regulation check"
    TT891A_TEMPERATURE_BOUNDS = (46, 54)  # K
    TT893_TEMPERATURE_BOUNDS = (280, 320)  # K

    @dataclass(frozen=True)
    class FgcData:
        pm_data_headers: pd.DataFrame
        slow_abort: pd.Series
        flt_off: pd.Series
        timestamp_slow_abort: int | None
        timestamp_flt_off: int | None
        i_meas: pd.Series
        i_ref: pd.Series
        plateaus: list[commons.Plateau]
        i_pno: float

    @dataclass
    class DfbData:
        dfb: str
        tt891a_signals: list[pd.Series]
        tt893_signals: list[pd.Series]

    @dataclass(frozen=True)
    class QpsData:
        pm_data_headers: pd.DataFrame

    def query(self) -> None:
        def query_fgc_data(fgc: str) -> Pli2E3Analysis.FgcData:
            i_min_op = float(self.hwc_powering_test_parameters[fgc]["I_MIN_OP"]["value"])
            i_pno = float(self.hwc_powering_test_parameters[fgc]["I_PNO"]["value"])
            state = query.query_cmw_by_variables(
                self._spark, self.input.t_start, self.input.t_end - self.input.t_start, f"{fgc}:STATE"
            )

            slow_abort = processing.decode_state(state, "SLOW_ABORT")
            timestamp_slow_abort = processing.detect_when_signal_first_changes_to(
                slow_abort, True, 0, (self.input.t_start, self.input.t_end), self._logger
            )
            flt_off = processing.decode_state(state, "FLT_OFF")
            timestamp_flt_off = processing.detect_when_signal_first_changes_to(
                flt_off, True, 0, (self.input.t_start, self.input.t_end), self._logger
            )

            i_meas, i_ref = query.query_cmw_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [f"{fgc}:I_MEAS", f"{fgc}:I_REF"],
            )

            def get_fgc_pm_data_headers_before_i_meas_goes_below_i_min_op() -> pd.DataFrame:
                fgc_pm_data_class = signal_metadata.get_fgc_pm_class_name(
                    self.input.circuit_name, self.input.t_start, "self"
                )
                end_search = processing.detect_last_point_above_threshold(
                    i_meas, self.input.t_start, self.input.t_end, i_min_op
                )
                if end_search is None:
                    end_search = self.input.t_end
                return query.query_pm_data_headers(
                    "FGC", fgc_pm_data_class, fgc, self.input.t_start, end_search - self.input.t_start
                )

            def get_plateaus() -> list[commons.Plateau]:
                i_injection = float(self.hwc_powering_test_parameters[fgc]["I_INJECTION"]["value"])
                i_interm_1 = float(self.hwc_powering_test_parameters[fgc]["I_INTERM_1"]["value"])
                i_interm_2 = float(self.hwc_powering_test_parameters[fgc]["I_INTERM_2"]["value"])
                time_zero = int(self.hwc_powering_test_parameters[fgc]["TIME_ZERO"]["value"] * 1e9)
                time_top = int(self.hwc_powering_test_parameters[fgc]["TIME_TOP"]["value"] * 1e9)
                plateaus = [
                    commons.Plateau(
                        processing.get_first_plateau(i_ref, i_min_op), i_min_op, time_zero, commons.DataSource.NXCALS
                    ),
                    commons.Plateau(
                        processing.get_first_plateau(i_ref, i_injection),
                        i_injection,
                        time_top,
                        commons.DataSource.NXCALS,
                    ),
                ]
                if fgc.endswith("B1"):
                    plateaus.append(
                        commons.Plateau(
                            processing.get_first_plateau(i_ref, i_interm_2),
                            i_interm_2,
                            time_top * 2,
                            commons.DataSource.NXCALS,
                        )
                    )
                else:
                    plateaus.extend(
                        [
                            commons.Plateau(
                                processing.get_first_plateau(i_ref, i_interm_2),
                                i_interm_2,
                                time_top,
                                commons.DataSource.NXCALS,
                            ),
                            commons.Plateau(
                                processing.get_first_plateau(i_ref, i_interm_1),
                                i_interm_1,
                                time_top,
                                commons.DataSource.NXCALS,
                            ),
                        ]
                    )
                return plateaus

            return self.FgcData(
                pm_data_headers=get_fgc_pm_data_headers_before_i_meas_goes_below_i_min_op(),
                slow_abort=slow_abort,
                flt_off=flt_off,
                timestamp_slow_abort=timestamp_slow_abort,
                timestamp_flt_off=timestamp_flt_off,
                i_meas=i_meas,
                i_ref=i_ref,
                plateaus=get_plateaus(),
                i_pno=i_pno,
            )

        def query_dfb_data() -> Pli2E3Analysis.DfbData:
            [dfb] = signal_metadata.get_dfb_names(self.input.circuit_name, self.input.t_start)
            dfb_prefixes = signal_metadata.get_dfb_prefixes(self.input.circuit_name, dfb, self.input.t_start)
            tt891a_signals = query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [f"{dfb_prefix}TT891A.TEMPERATURECALC" for dfb_prefix in dfb_prefixes],
            )

            tt893_signals = query.query_winccoa_by_variables(
                self._spark,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
                [f"{dfb_prefix}TT893.TEMPERATURECALC" for dfb_prefix in dfb_prefixes],
            )

            return self.DfbData(dfb=dfb, tt891a_signals=tt891a_signals, tt893_signals=tt893_signals)

        def query_qps_data() -> Pli2E3Analysis.QpsData:
            pm_data_headers = query.query_pm_data_headers(
                "QPS",
                signal_metadata.get_qps_pm_class_name_for_ipq(self.input.circuit_name),
                self.input.circuit_name,
                self.input.t_start,
                self.input.t_end - self.input.t_start,
            )
            return self.QpsData(pm_data_headers=pm_data_headers)

        super().query()
        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.dfb_data = query_dfb_data()
        self.fgc_data = {
            fgc: query_fgc_data(fgc)
            for fgc in signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        }
        self.qps_data = query_qps_data()

    def analyze(self) -> None:
        super().analyze()

        self.timestamps_check = self.__check_pm_buffers_absence_and_timestamps()
        self.current_profile_consistency_check = self.__check_pc_current_profile_consistency()
        self.dfb_regulation_check = self.__check_dfb_regulation()

    def __check_pm_buffers_absence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PM_BUFFERS_ABSENCE_AND_TIMESTAMPS_CHECK_TAG)
        return all(
            [
                all(
                    digital_checks.check_signal_changes_once_to(
                        fgc_data.slow_abort, True, 0, (self.input.t_start, self.input.t_end), logger_adapter
                    )
                    for fgc_data in self.fgc_data.values()
                ),
                all(
                    digital_checks.check_signal_changes_once_to(
                        fgc_data.flt_off, True, 0, (self.input.t_start, self.input.t_end), logger_adapter
                    )
                    for fgc_data in self.fgc_data.values()
                ),
                timestamp_checks.check_timestamps_are_within_margin(
                    {f"{fgc} SLOW ABORT": fgc_data.timestamp_slow_abort for fgc, fgc_data in self.fgc_data.items()},
                    self.ALLOWED_SLOW_ABORT_TIMESTAMP_DIFFERENCE,
                    logger_adapter,
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_data.pm_data_headers, 0, logger_adapter),
                all(
                    [
                        timestamp_checks.check_fgc_pm_data_headers_length(
                            fgc, fgc_data.pm_data_headers, 0, logger_adapter
                        )
                        for fgc, fgc_data in self.fgc_data.items()
                    ]
                ),
                all(
                    [
                        timestamp_checks.check_timestamp_precedence(
                            f"{fgc} SLOW ABORT",
                            fgc_data.timestamp_slow_abort,
                            f"{fgc} FLT OFF",
                            fgc_data.timestamp_flt_off,
                            logger_adapter,
                        )
                        for fgc, fgc_data in self.fgc_data.items()
                    ]
                ),
            ]
        )

    def __check_pc_current_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)
        return all(
            [
                all(
                    [
                        analog_checks.check_minimum_plateau_duration(
                            fgc,
                            fgc_data.i_ref,
                            plateau.edges,
                            plateau.expected_duration - self.NXCALS_PLATEAU_DURATION_OFFSET,
                            logger_adapter,
                        ),
                        analog_checks.check_plateau_values_absolute_deviation(
                            fgc,
                            fgc_data.i_meas,
                            plateau.edges,
                            plateau.level,
                            self.FACTOR_OF_THE_NOMINAL_CURRENT * self.fgc_data[fgc].i_pno,
                            "A",
                            logger_adapter,
                        ),
                    ]
                )
                for fgc, fgc_data in self.fgc_data.items()
                for plateau in fgc_data.plateaus
            ]
        )

    def __check_dfb_regulation(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.DFB_REGULATION_CHECK_TAG)
        return all(
            [
                all(
                    [
                        analog_checks.check_signal_is_within_bounds(
                            self.dfb_data.dfb,
                            tt891a_signal,
                            (self.input.t_start, self.input.t_end),
                            self.TT891A_TEMPERATURE_BOUNDS,
                            "K",
                            logger_adapter,
                        )
                        for tt891a_signal in self.dfb_data.tt891a_signals
                    ]
                ),
                all(
                    [
                        analog_checks.check_signal_is_within_bounds(
                            self.dfb_data.dfb,
                            tt893_signal,
                            (self.input.t_start, self.input.t_end),
                            self.TT893_TEMPERATURE_BOUNDS,
                            "K",
                            logger_adapter,
                        )
                        for tt893_signal in self.dfb_data.tt893_signals
                    ]
                ),
            ]
        )

    def get_analysis_output(self) -> bool:
        return all([self.timestamps_check, self.current_profile_consistency_check, self.dfb_regulation_check])
