"""Contains classes common to all Powering Test classes."""

import dataclasses
import datetime
import logging
from abc import ABC
from collections.abc import MutableMapping
from enum import Enum, auto
from typing import Any, Generic, TypeVar

from lhcsmapi.api import analysis
from lhcsmapi.Time import Time

from lhcsmpowering import analyses


@dataclasses.dataclass(frozen=True)
class TestParameterInput:
    """Holds typical test parameters."""

    hwc_test: str
    circuit_name: str
    campaign: str
    t_start: int
    t_end: int

    @staticmethod
    def create(
        hwc_test: str,
        circuit_name: str,
        campaign: str,
        t_start: int | str | datetime.datetime,
        t_end: int | str | datetime.datetime,
    ) -> "TestParameterInput":
        t_start_ = Time.to_unix_timestamp(t_start)
        t_end_ = Time.to_unix_timestamp(t_end)
        return TestParameterInput(hwc_test, circuit_name, campaign, t_start_, t_end_)

    def __str__(self: "TestParameterInput") -> str:
        return (
            f"hwc_test: {self.hwc_test}\ncircuit name: {self.circuit_name}\ncampaign: {self.campaign}\n"
            f"t_start: {self.t_start} ({Time.to_string_short(self.t_start)})\n"
            f"t_end: {self.t_end} ({Time.to_string_short(self.t_end)})\n"
        )


@dataclasses.dataclass(frozen=True)
class TestParameterInputWithRef:
    """Extends TestParameterInput with the time of the reference test"""

    hwc_test: str
    circuit_name: str
    campaign: str
    t_start: int
    t_end: int
    t_start_ref: int
    t_end_ref: int

    @staticmethod
    def create(
        hwc_test: str,
        circuit_name: str,
        campaign: str,
        t_start: int | str | datetime.datetime,
        t_end: int | str | datetime.datetime,
        t_start_ref: int | str | datetime.datetime,
        t_end_ref: int | str | datetime.datetime,
    ) -> "TestParameterInputWithRef":
        test_parameter_input = TestParameterInput.create(hwc_test, circuit_name, campaign, t_start, t_end)
        t_start_ref_ = Time.to_unix_timestamp(t_start_ref)
        t_end_ref_ = Time.to_unix_timestamp(t_end_ref)
        return TestParameterInputWithRef(
            test_parameter_input.hwc_test,
            test_parameter_input.circuit_name,
            test_parameter_input.campaign,
            test_parameter_input.t_start,
            test_parameter_input.t_end,
            t_start_ref_,
            t_end_ref_,
        )

    def __str__(self) -> str:
        return (
            f"hwc_test: {self.hwc_test}\ncircuit name: {self.circuit_name}\ncampaign: {self.campaign}\n"
            f"t_start: {self.t_start} ({Time.to_string_short(self.t_start)})\n"
            f"t_end: {self.t_end} ({Time.to_string_short(self.t_end)})\n"
            f"t_start_ref: {self.t_start_ref} ({Time.to_string_short(self.t_start_ref)})\n"
            f"t_end_ref: {self.t_end_ref} ({Time.to_string_short(self.t_end_ref)})\n"
        )


class PrefixLoggerAdapter(logging.LoggerAdapter):
    """A logger adapter that adds a prefix to every message"""

    def process(self, msg: str, kwargs: MutableMapping[str, Any]) -> tuple[str, MutableMapping[str, Any]]:
        msg, kwargs = super().process(msg, kwargs)
        extra = self.extra
        if extra is None:
            raise ValueError("The extra attribute must be set.")
        return (str(extra["prefix"]) + msg, kwargs)


T = TypeVar("T", TestParameterInput, TestParameterInputWithRef)


class PoweringAnalysis(Generic[T], analysis.Analysis, ABC):
    """Base class for the PoweringTests."""

    def __init__(self, identifier: str, test_parameters: T):
        super().__init__(identifier)

        expected_analysis_class = analyses.get_analysis_class(test_parameters.hwc_test, test_parameters.circuit_name)
        if not isinstance(self, expected_analysis_class):
            raise ValueError(
                f"Circuit {test_parameters.circuit_name} is not compatible with the analysis {self.__class__.__name__}."
                f" Expected {expected_analysis_class.__name__}."
            )
        self.input: T = test_parameters
        self._logger.debug("Instantiated analysis class %s for %s.", self._identifier, str(test_parameters))


def start_logging_new_check(
    logger: logging.Logger | logging.LoggerAdapter, check: str, indentation: int = 1
) -> logging.LoggerAdapter:
    """Starts a new logging segment for a check. Returns the logger adapter for the segment."""
    logger.info(check)
    return PrefixLoggerAdapter(logger, {"tags": check, "prefix": indentation * "\t"})


class Cycle(Enum):
    POSITIVE = auto()
    NEGATIVE = auto()

    def multiplier(self) -> int:
        return 1 if self == Cycle.POSITIVE else -1


class DataSource(Enum):
    PM = auto()
    NXCALS = auto()


@dataclasses.dataclass(frozen=True)
class Plateau:
    edges: tuple[int, int] | None
    level: float  # A
    expected_duration: int  # nanoseconds
    source: DataSource
