# PLI1.c2 - FAST POWER ABORT IPD

Circuit: IPD

## Introduction

The aim of this test is to verify the free-wheeling diode of the power converter. The PC is turned on and stays at `I_MIN_OP` for `TIME_ZERO`. It then ramps the circuit up to `I_INJECTION` and stays there for `TIME_PLI` until FAST POWER ABORT is received from the PIC. The PC then activates the free-wheeling diode bypass and the current decays according to the natural time constant of the circuit. Note that QPS is not involved in the test such that the quench heaters are not discharged. The current decay is equivalent to SLOW POWER ABORT.

## Acceptance Criteria

1. **PM buffers presence and timestamps check**
    - **1 x FGC PM**.
    - **0 x QPS PM**.
    - PIC from NXCALS (`ST_ABORT_PIC`) is set once to False.
    - PIC from NXCALS (`ST_ABORT_PIC`) is up to **40 ms** before the FGC PM.

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top levels (`I_MIN_OP`, `I_INJECTION`) and maintains it for at least `TIME_ZERO`, `TIME_PLI`.
    - The measured current (`I_MEAS`) during `TIME_ZERO` and `TIME_PLI` flat tops must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`). Note: The last point of the last plateau needs to be skipped, as that value might drop before `I_REF`.

3. **PC voltage profile consistency check**
    - `I_REF` of a reference\* curve reaches the required flat top levels (`I_MIN_OP`, `I_INJECTION`) and maintains it for at least `TIME_ZERO`, `TIME_PLI`.
    - The measured voltage (`V_MEAS`) during `TIME_ZERO` and `TIME_PLI` flat tops must be within **±80mV** of the reference\* curve. Note: The comparison is performed from the beginning of the flat top and lasts until the end of the shorter signal.

4. **PC current decay check**
    - The measured current (`I_MEAS`) decay from the **PM timestamp** and the moment the current reaches **5A** must be within **±1%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`).
    **Note:** If the current does not fully decay down to 5A in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **PC voltage decay check**
    - The measured voltage (`V_MEAS`) decay from **40 ms** after the **PM timestamp** until the current reaches **5A** must be within **±80 mV** of the reference\* decay curve.
    **Note:** If the current does not fully decay down to 5A in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

6. **Earth current check**
    - The maximum measured percentage earth current (`I_EARTH_PCNT`) during the `I_INJECTION` flat top must be between **-10%** and **10%**.

(\*) The reference signal is the last successful test.
