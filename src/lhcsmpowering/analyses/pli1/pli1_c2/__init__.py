from __future__ import annotations

from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pli1C2Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    MAX_ALLOWED_TIMESTAMP_GAP_ABS = 40_000_000  # ns = 40 ms

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    I_PNO_FACTOR_FOR_PROFILE_CHECK = 0.0002  # 0.02%

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = "PC voltage profile consistency check"
    V_MEAS_MAXIMUM_PLATEAU_DEVIATION = 0.08  # 80 mV

    PC_CURRENT_DECAY_CHECK_TAG = "PC current decay check"
    CURRENT_END_DECAY_DETECTION_VALUE = 5  # A
    I_PNO_FACTOR_FOR_DECAY_CHECK = 0.001  # 0.1%

    PC_VOLTAGE_DECAY_CHECK_TAG = "PC voltage decay check"
    V_MEAS_MAXIMUM_DECAY_DEVIATION = 0.08  # 80 mV
    V_MEAS_DECAY_DELAY_AFTER_PM = 40_000_000  # 40 ms

    EARTH_CURRENT_CHECK_TAG = "Earth current check"
    I_EARTH_PCNT_ABSOLUTE_BOUNDS = (-10, 10)  # % (absolute value)

    def query(self) -> None:
        super().query()

        # HWC powering test parameters
        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS",
            signal_metadata.get_qps_pm_class_name_for_ipd(self.input.circuit_name, self.input.t_start),
            self.input.circuit_name,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
        )

        # PIC
        self.st_abort_pic = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            f"{self.input.circuit_name}:ST_ABORT_PIC",
            include_latest_data_point_prior_to_start=True,
        )
        self.timestamp_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # FGC
        [self.fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(self.input.circuit_name, self.input.t_start, "self")
        self.fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC", fgc_pm_class_name, self.fgc, self.input.t_start, self.input.t_end - self.input.t_start
        )
        self.timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers, 0)

        # query FGC data
        self.i_injection = float(self.hwc_powering_test_parameters[self.fgc]["I_INJECTION"]["value"])
        self.i_min_op = float(self.hwc_powering_test_parameters[self.fgc]["I_MIN_OP"]["value"])
        self.i_pno = float(self.hwc_powering_test_parameters[self.fgc]["I_PNO"]["value"])
        self.time_zero = int(float(self.hwc_powering_test_parameters[self.fgc]["TIME_ZERO"]["value"]) * 1e9)
        self.time_pli = int(float(self.hwc_powering_test_parameters[self.fgc]["TIME_PLI"]["value"]) * 1e9)

        self.i_meas, self.v_meas, self.i_ref, self.i_earth_pcnt = query.query_pm_data_signals(
            "FGC",
            fgc_pm_class_name,
            self.fgc,
            ["STATUS.I_MEAS", "STATUS.V_MEAS", "STATUS.I_REF", "I_EARTH_PCNT"],
            self.timestamp_fgc,
        )

        self.i_min_op_plateau = processing.get_first_plateau(self.i_ref.loc[: self.timestamp_fgc], self.i_min_op)
        self.i_injection_plateau = processing.get_first_plateau(self.i_ref.loc[: self.timestamp_fgc], self.i_injection)

        self.end_current_decay = processing.detect_first_point_below_threshold(
            self.i_meas, self.timestamp_fgc, self.input.t_end, self.CURRENT_END_DECAY_DETECTION_VALUE
        )
        if self.end_current_decay is None and not self.i_meas.empty:
            self.end_current_decay = int(self.i_meas.index[-1])

        # Reference FGC
        [self.ref_fgc] = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)
        ref_fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )
        ref_fgc_pm_data_headers = query.query_pm_data_headers(
            "FGC",
            ref_fgc_pm_class_name,
            self.ref_fgc,
            self.input.t_start_ref,
            self.input.t_end_ref - self.input.t_start_ref,
        )
        self.ref_timestamp_fgc = processing.read_timestamp_from_pm_headers_at_index(ref_fgc_pm_data_headers, 0)

        # Query reference FGC data
        self.ref_i_meas, self.ref_v_meas, self.ref_i_ref = query.query_pm_data_signals(
            "FGC",
            ref_fgc_pm_class_name,
            self.ref_fgc,
            ["STATUS.I_MEAS", "STATUS.V_MEAS", "STATUS.I_REF"],
            self.ref_timestamp_fgc,
        )
        self.ref_i_min_op_plateau = processing.get_first_plateau(self.ref_i_ref, self.i_min_op)
        self.ref_i_injection_plateau = processing.get_first_plateau(self.ref_i_ref, self.i_injection)

    def analyze(self) -> None:
        super().analyze()

        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.pc_current_profile_consistency_check = self._check_pc_current_profile_consistency()
        self.pc_voltage_profile_consistency_check = self._check_pc_voltage_profile_consistency()
        self.pc_current_decay_check = self._check_pc_current_decay()
        self.pc_voltage_decay_check = self._check_pc_voltage_decay()
        self.earth_current_check = self._check_earth_current()

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )

        return all(
            [
                timestamp_checks.check_fgc_pm_data_headers_length(
                    self.fgc, self.fgc_pm_data_headers, 1, logger_adapter
                ),
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 0, logger_adapter),
                digital_checks.check_signal_changes_once_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                timestamp_checks.check_timestamp_precedence(
                    "PIC",
                    self.timestamp_pic,
                    f"{self.fgc} FGC",
                    self.timestamp_fgc,
                    logger_adapter,
                    max_allowed_timestamp_gap=self.MAX_ALLOWED_TIMESTAMP_GAP_ABS,
                ),
            ]
        )

    def _check_pc_current_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)

        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.i_ref, self.i_min_op_plateau, self.time_zero, logger_adapter
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        self.fgc,
                        self.i_meas,
                        self.i_min_op_plateau,
                        self.i_min_op,
                        self.I_PNO_FACTOR_FOR_PROFILE_CHECK * self.i_pno,
                        "A",
                        logger_adapter,
                    )
                ),
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.i_ref, self.i_injection_plateau, self.time_pli, logger_adapter
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        self.fgc,
                        self.i_meas,
                        self.i_injection_plateau,
                        self.i_injection,
                        self.I_PNO_FACTOR_FOR_PROFILE_CHECK * self.i_pno,
                        "A",
                        logger_adapter,
                    )
                ),
            ]
        )

    def _check_pc_voltage_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG)
        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.ref_i_ref, self.ref_i_min_op_plateau, self.time_zero, logger_adapter
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        self.ref_fgc,
                        self.ref_i_meas,
                        self.ref_i_min_op_plateau,
                        self.i_min_op,
                        self.I_PNO_FACTOR_FOR_PROFILE_CHECK * self.i_pno,
                        "A",
                        logger_adapter,
                    )
                ),
                (
                    analog_checks.check_minimum_plateau_duration(
                        self.fgc, self.i_ref, self.i_injection_plateau, self.time_pli, logger_adapter
                    )
                    and analog_checks.check_absolute_deviation_wrt_reference(
                        self.fgc,
                        self.v_meas,
                        self.i_injection_plateau,
                        self.ref_v_meas,
                        self.ref_i_injection_plateau,
                        self.V_MEAS_MAXIMUM_PLATEAU_DEVIATION,
                        "V",
                        logger_adapter,
                    )
                ),
            ]
        )

    def _check_pc_current_decay(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG)
        return all(
            [
                analog_checks.check_absolute_deviation_wrt_reference(
                    self.fgc,
                    self.i_meas,
                    (self.timestamp_fgc, self.end_current_decay),
                    self.ref_i_meas,
                    (self.ref_timestamp_fgc, self.ref_i_meas.index[-1]),
                    self.i_pno * self.I_PNO_FACTOR_FOR_DECAY_CHECK,
                    "A",
                    logger_adapter,
                )
            ]
        )

    def _check_pc_voltage_decay(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_TAG)
        return all(
            [
                analog_checks.check_absolute_deviation_wrt_reference(
                    self.fgc,
                    self.v_meas,
                    (self.timestamp_fgc, self.end_current_decay),
                    self.ref_v_meas,
                    (self.ref_timestamp_fgc, self.ref_v_meas.index[-1]),
                    self.V_MEAS_MAXIMUM_DECAY_DEVIATION,
                    "V",
                    logger_adapter,
                    skipped_nanoseconds_from_the_start=self.V_MEAS_DECAY_DELAY_AFTER_PM,
                )
            ]
        )

    def _check_earth_current(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG)
        return analog_checks.check_signal_is_within_bounds(
            self.fgc,
            self.i_earth_pcnt,
            self.i_injection_plateau,
            self.I_EARTH_PCNT_ABSOLUTE_BOUNDS,
            "%",
            logger_adapter,
        )

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.pc_current_profile_consistency_check,
                self.pc_voltage_profile_consistency_check,
                self.pc_current_decay_check,
                self.pc_voltage_decay_check,
                self.earth_current_check,
            ]
        )
