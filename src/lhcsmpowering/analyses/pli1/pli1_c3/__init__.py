from __future__ import annotations

from dataclasses import dataclass

import pandas as pd
from lhcsmapi.api import query
from lhcsmapi.metadata import signal_metadata

from lhcsmpowering import processing
from lhcsmpowering.analyses import commons
from lhcsmpowering.checks import analog_checks, digital_checks, timestamp_checks


class Pli1C3Analysis(commons.PoweringAnalysis[commons.TestParameterInputWithRef]):

    PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG = "PM buffers presence and timestamps check"
    MAX_ALLOWED_TIMESTAMP_GAP_ABS = 40_000_000  # ns = 40 ms

    PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG = "PC current profile consistency check"
    I_PNO_FACTOR_FOR_PROFILE_CHECK = 0.0002  # 0.02%

    PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG = "PC voltage profile consistency check"
    V_MEAS_MAXIMUM_PLATEAU_DEVIATION = 0.06  # 60 mV
    SKIPPED_NANOSECONDS_BEFORE_END_OF_VOLTAGE_PLATEAU = 50_000_000

    PC_CURRENT_DECAY_CHECK_TAG = "PC current decay check"
    CURRENT_END_DECAY_DETECTION_VALUE = 5  # A
    I_PNO_FACTOR_FOR_DECAY_CHECK = 0.001  # 0.1%

    PC_VOLTAGE_DECAY_CHECK_TAG = "PC voltage decay check"
    V_MEAS_MAXIMUM_DECAY_DEVIATION = 0.06  # 60 mV
    V_MEAS_DECAY_DELAY_AFTER_PM = 100_000_000

    EARTH_CURRENT_CHECK_TAG = "Earth current check"
    I_EARTH_PCNT_ABSOLUTE_BOUNDS = (-15, 15)  # % (absolute value)

    @dataclass(frozen=True)
    class FgcData:
        timestamp_fgc: int | None
        i_injection: float
        i_pno: float
        time_top: int
        i_meas: pd.Series
        i_ref: pd.Series
        v_meas: pd.Series
        i_injection_plateau: tuple[int, int] | None
        end_decay: int | None

    @dataclass(frozen=True)
    class ReferenceFgcData:
        timestamp_fgc: int | None
        i_meas: pd.Series
        i_ref: pd.Series
        v_meas: pd.Series
        i_injection_plateau: tuple[int, int] | None

    def query(self) -> None:
        super().query()

        # HWC powering test parameters
        self.hwc_powering_test_parameters = query.query_hwc_powering_test_parameters(
            self.input.circuit_name, self.input.t_start, self.input.t_end - self.input.t_start
        )

        # QPS
        self.qps_pm_data_headers = query.query_pm_data_headers(
            "QPS",
            signal_metadata.get_qps_pm_class_name_for_ipq(self.input.circuit_name),
            self.input.circuit_name,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
        )

        # PIC
        self.st_abort_pic = query.query_winccoa_by_variables(
            self._spark,
            self.input.t_start,
            self.input.t_end - self.input.t_start,
            f"{self.input.circuit_name}:ST_ABORT_PIC",
            include_latest_data_point_prior_to_start=True,
        )
        self.timestamp_pic = processing.detect_when_signal_first_changes_to(
            self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), self._logger
        )

        # FGC
        self.fgcs = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start)
        self.fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start, "self"
        )
        self.fgc_pm_data_headers = {
            fgc: query.query_pm_data_headers(
                "FGC", self.fgc_pm_class_name, fgc, self.input.t_start, self.input.t_end - self.input.t_start
            )
            for fgc in self.fgcs
        }
        self.fgc_data = {
            fgc: self._query_fgc_data(
                fgc, processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers[fgc], 0)
            )
            for fgc in self.fgcs
        }

        self.fgc_earth = signal_metadata.get_fgc_with_earth_measurement(self.input.circuit_name, self.input.t_start)
        self.i_earth_pcnt = query.query_pm_data_signals(
            "FGC",
            self.fgc_pm_class_name,
            self.fgc_earth,
            "I_EARTH_PCNT",
            processing.read_timestamp_from_pm_headers_at_index(self.fgc_pm_data_headers[self.fgc_earth], 0),
        )

        # Reference FGC
        self.reference_fgc_pm_class_name = signal_metadata.get_fgc_pm_class_name(
            self.input.circuit_name, self.input.t_start_ref, "self"
        )
        self.reference_fgcs = signal_metadata.get_fgc_names(self.input.circuit_name, self.input.t_start_ref)
        self.reference_fgc_pm_data_headers = {
            fgc: query.query_pm_data_headers(
                "FGC",
                self.reference_fgc_pm_class_name,
                reference_fgc,
                self.input.t_start_ref,
                self.input.t_end_ref - self.input.t_start_ref,
            )
            for fgc, reference_fgc in zip(self.fgcs, self.reference_fgcs, strict=True)
        }
        self.reference_fgc_data = {
            fgc: self._query_reference_fgc_data(
                fgc,
                reference_fgc,
                processing.read_timestamp_from_pm_headers_at_index(self.reference_fgc_pm_data_headers[fgc], 0),
            )
            for fgc, reference_fgc in zip(self.fgcs, self.reference_fgcs, strict=True)
        }

    def _query_fgc_data(self, fgc: str, timestamp_fgc: int | None) -> Pli1C3Analysis.FgcData:
        i_injection = float(self.hwc_powering_test_parameters[fgc]["I_INJECTION"]["value"])
        i_pno = float(self.hwc_powering_test_parameters[fgc]["I_PNO"]["value"])
        time_top = int(self.hwc_powering_test_parameters[fgc]["TIME_TOP"]["value"] * 1e9)

        i_meas, v_meas, i_ref = query.query_pm_data_signals(
            "FGC", self.fgc_pm_class_name, fgc, ["STATUS.I_MEAS", "STATUS.V_MEAS", "STATUS.I_REF"], timestamp_fgc
        )
        i_injection_plateaus = processing.get_regions_equal_to(i_ref, i_injection)
        i_injection_plateau = i_injection_plateaus[0] if i_injection_plateaus else None

        first_point_below_threshold_fpa_decay = processing.detect_first_point_below_threshold(
            i_meas, timestamp_fgc, self.input.t_end, self.CURRENT_END_DECAY_DETECTION_VALUE
        )
        end_decay = first_point_below_threshold_fpa_decay if first_point_below_threshold_fpa_decay else self.input.t_end
        return self.FgcData(
            timestamp_fgc=timestamp_fgc,
            i_injection=i_injection,
            i_pno=i_pno,
            time_top=time_top,
            i_meas=i_meas,
            v_meas=v_meas,
            i_ref=i_ref,
            i_injection_plateau=i_injection_plateau,
            end_decay=end_decay,
        )

    def _query_reference_fgc_data(
        self, fgc: str, reference_fgc: str, timestamp_fgc: int | None
    ) -> Pli1C3Analysis.ReferenceFgcData:
        i_meas, v_meas, i_ref = query.query_pm_data_signals(
            "FGC",
            self.reference_fgc_pm_class_name,
            reference_fgc,
            ["STATUS.I_MEAS", "STATUS.V_MEAS", "STATUS.I_REF"],
            timestamp_fgc,
        )

        i_injection_plateaus = processing.get_regions_equal_to(i_ref, self.fgc_data[fgc].i_injection)
        i_injection_plateau = i_injection_plateaus[0] if i_injection_plateaus else None

        return self.ReferenceFgcData(
            timestamp_fgc=timestamp_fgc,
            i_meas=i_meas,
            i_ref=i_ref,
            v_meas=v_meas,
            i_injection_plateau=i_injection_plateau,
        )

    def analyze(self) -> None:
        super().analyze()
        self.pm_buffers_presence_and_timestamps_check = self._check_pm_buffers_presence_and_timestamps()
        self.pc_current_profile_consistency_check = self._check_pc_current_profile_consistency()
        self.pc_voltage_profile_consistency_check = self._check_pc_voltage_profile_consistency()
        self.pc_current_decay_check = self._check_pc_current_decay()
        self.pc_voltage_decay_check = self._check_pc_voltage_decay()
        self.earth_current_check = self._check_earth_current()

    def _check_pm_buffers_presence_and_timestamps(self) -> bool:
        logger_adapter = commons.start_logging_new_check(
            self._logger, self.PM_BUFFERS_PRESENCE_AND_TIMESTAMPS_CHECK_TAG
        )
        return all(
            [
                timestamp_checks.check_qps_pm_data_headers_length(self.qps_pm_data_headers, 0, logger_adapter),
                all(
                    [
                        timestamp_checks.check_fgc_pm_data_headers_length(
                            fgc, self.fgc_pm_data_headers[fgc], 1, logger_adapter
                        )
                        for fgc in self.fgcs
                    ]
                ),
                digital_checks.check_signal_changes_once_to(
                    self.st_abort_pic, False, 0, (self.input.t_start, self.input.t_end), logger_adapter
                ),
                all(
                    [
                        timestamp_checks.check_timestamp_precedence(
                            "PIC",
                            self.timestamp_pic,
                            f"{fgc} FGC",
                            self.fgc_data[fgc].timestamp_fgc,
                            logger_adapter,
                            max_allowed_timestamp_gap=self.MAX_ALLOWED_TIMESTAMP_GAP_ABS,
                        )
                        for fgc in self.fgcs
                    ]
                ),
            ]
        )

    def _check_pc_current_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_PROFILE_CONSISTENCY_CHECK_TAG)
        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        fgc,
                        self.fgc_data[fgc].i_ref,
                        self.fgc_data[fgc].i_injection_plateau,
                        self.fgc_data[fgc].time_top,
                        logger_adapter,
                    )
                    and analog_checks.check_plateau_values_absolute_deviation(
                        fgc,
                        self.fgc_data[fgc].i_meas,
                        self.fgc_data[fgc].i_injection_plateau,
                        self.fgc_data[fgc].i_injection,
                        self.I_PNO_FACTOR_FOR_PROFILE_CHECK * self.fgc_data[fgc].i_pno,
                        "A",
                        logger_adapter,
                    )
                )
                for fgc in self.fgcs
            ]
        )

    def _check_pc_voltage_profile_consistency(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_PROFILE_CONSISTENCY_CHECK_TAG)
        return all(
            [
                (
                    analog_checks.check_minimum_plateau_duration(
                        fgc,
                        self.reference_fgc_data[fgc].i_ref,
                        self.reference_fgc_data[fgc].i_injection_plateau,
                        self.fgc_data[fgc].time_top,
                        logger_adapter,
                    )
                    and analog_checks.check_absolute_deviation_wrt_reference(
                        fgc,
                        self.fgc_data[fgc].v_meas,
                        self.fgc_data[fgc].i_injection_plateau,
                        self.reference_fgc_data[fgc].v_meas,
                        self.reference_fgc_data[fgc].i_injection_plateau,
                        self.V_MEAS_MAXIMUM_PLATEAU_DEVIATION,
                        "V",
                        logger_adapter,
                        skipped_nanoseconds_at_the_end=self.SKIPPED_NANOSECONDS_BEFORE_END_OF_VOLTAGE_PLATEAU,
                    )
                )
                for fgc in self.fgcs
            ]
        )

    def _check_pc_current_decay(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_CURRENT_DECAY_CHECK_TAG)
        return all(
            [
                analog_checks.check_absolute_deviation_wrt_reference(
                    fgc,
                    self.fgc_data[fgc].i_meas,
                    (self.fgc_data[fgc].timestamp_fgc, self.fgc_data[fgc].end_decay),
                    self.reference_fgc_data[fgc].i_meas,
                    (self.reference_fgc_data[fgc].timestamp_fgc, self.reference_fgc_data[fgc].i_meas.index[-1]),
                    self.fgc_data[fgc].i_pno * self.I_PNO_FACTOR_FOR_DECAY_CHECK,
                    "A",
                    logger_adapter,
                )
                for fgc in self.fgcs
            ]
        )

    def _check_pc_voltage_decay(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.PC_VOLTAGE_DECAY_CHECK_TAG)
        return all(
            [
                analog_checks.check_absolute_deviation_wrt_reference(
                    fgc,
                    self.fgc_data[fgc].v_meas,
                    (self.fgc_data[fgc].timestamp_fgc, self.fgc_data[fgc].end_decay),
                    self.reference_fgc_data[fgc].v_meas,
                    (self.reference_fgc_data[fgc].timestamp_fgc, self.reference_fgc_data[fgc].v_meas.index[-1]),
                    self.V_MEAS_MAXIMUM_DECAY_DEVIATION,
                    "V",
                    logger_adapter,
                    skipped_nanoseconds_from_the_start=self.V_MEAS_DECAY_DELAY_AFTER_PM,
                )
                for fgc in self.fgcs
            ]
        )

    def _check_earth_current(self) -> bool:
        logger_adapter = commons.start_logging_new_check(self._logger, self.EARTH_CURRENT_CHECK_TAG)
        return analog_checks.check_signal_is_within_bounds(
            self.fgc_earth,
            self.i_earth_pcnt,
            self.fgc_data[self.fgc_earth].i_injection_plateau,
            self.I_EARTH_PCNT_ABSOLUTE_BOUNDS,
            "%",
            logger_adapter,
        )

    def get_analysis_output(self) -> bool:
        return all(
            [
                self.pm_buffers_presence_and_timestamps_check,
                self.pc_current_profile_consistency_check,
                self.pc_voltage_profile_consistency_check,
                self.pc_current_decay_check,
                self.pc_voltage_decay_check,
                self.earth_current_check,
            ]
        )
