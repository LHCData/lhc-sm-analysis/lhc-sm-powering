# PLI1.c3 - FAST POWER ABORT IPQ

Circuit: IPQ

## Introduction

The aim of this test is to verify the free-wheeling diodes of both power converters via a FAST POWER ABORT request after a flat top at `I_INJECTION`. The converters are turned and stay at `I_MIN_OP` for `TIME_ZERO`. They are then ramped up to `I_INJECTION` and stay there for `TIME_TOP` until a Converter FAST POWER ABORT is sent by the PIC. The current then decays according to the time constant of the circuit.

## Acceptance Criteria

The analysis takes into account criteria from MP3 and EPC.

1. **PM buffers presence and timestamps check**
    - **2 x FGC PM** (one per FGC).
    - **0 x QPS PM**.
    - PIC from NXCALS (`ST_ABORT_PIC`) is set once to False.
    - PIC from NXCALS (`ST_ABORT_PIC`) is up to **40 ms** before the FGC PM.

2. **PC current profile consistency check**
    - `I_REF` reaches the required flat top level (`I_INJECTION`) and maintains it for at least `TIME_TOP`.
    - The measured current (`I_MEAS`) during the `I_INJECTION` flat top must be within **±0.02%** of `I_REF`, where % is to be understood as percent of the nominal current (`I_PNO`) for the power converter.

3. **PC voltage profile consistency check**
    - `I_REF` of a reference\* curve reaches the required flat top level (`I_INJECTION`) and maintains it for at least `TIME_TOP`.
    - The measured voltage (`V_MEAS`) during the `I_INJECTION` flat top must be within **±60 mV** of the reference\* curve. The comparison is performed from the beginning of the flat top and lasts until **50 ms** before the end of the shorter signal.

4. **PC current decay check**
    - The measured current (`I_MEAS`) decay starting from PM timestamp until the moment the current reaches **5 A** must be within **±5%** of the reference\* decay curve, where % is to be understood as percent of the nominal current (`I_PNO`). If the current doesn’t fully decay down to 5 A in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

5. **PC voltage decay check**
    - The measured voltage (`V_MEAS`) decay from **100 ms** after the PM timestamp until the current reaches **5 A** must be within **±60 mV** of the reference\* decay curve. If the current doesn’t fully decay down to 5 A in the stored PM data (sometimes not enough data is available), then the check shall be done until the last point of the signal with less data available.

6. **Earth current check on concerned FGC**
   - The measured percentage earth current (`I_EARTH_PCNT`) during the `I_INJECTION` flat top must be between **-15 and 15%**.

(\*) The reference signal is the last successful test.

## Source

1. _Test Procedure for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions_, EDMS 874884 v. 4.1, <https://edms.cern.ch/document/874884/4.1>.
2. _Analysis IPQ_, MP3 Twiki, <https://twiki.cern.ch/twiki/bin/view/MP3/Analysis_IPQ>.
