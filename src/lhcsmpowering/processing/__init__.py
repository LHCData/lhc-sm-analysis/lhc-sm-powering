from __future__ import annotations

import logging
from collections.abc import Callable, Iterable

import pandas as pd
from scipy import signal


def derivative_sav_gol(
    ser: pd.Series, window_duration: int, polyorder: int, deriv: int, dt: float | None = None
) -> pd.Series:
    # Source 1: https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter
    # Source 2: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.savgol_filter.html
    derivative_name = f"{'d' * deriv}{ser.name}"
    if ser.empty:
        return ser.rename(f"d{ser.name}")

    if dt is None:
        dt = (ser.index[1] - ser.index[0]) / 1e9

    window_length = int(window_duration / (dt * 1e9))
    return pd.Series(
        signal.savgol_filter(ser.to_numpy(), window_length=window_length, polyorder=polyorder, deriv=deriv, delta=dt),
        index=ser.index,
        name=derivative_name,
    )


def find_last_plateau_that_starts_before(
    timestamp: int | None, plateaus: list[tuple[int, int]]
) -> tuple[int, int] | None:
    if timestamp is None:
        return None
    result = None
    for plateau in plateaus:
        if plateau[0] < timestamp and (result is None or plateau[0] > result[0]):
            result = plateau
    return result


def find_first_plateau_that_starts_after(
    timestamp: int | None, plateaus: list[tuple[int, int]]
) -> tuple[int, int] | None:
    if timestamp is None:
        return None
    result = None
    for plateau in plateaus:
        if plateau[0] > timestamp and (result is None or plateau[0] < result[0]):
            result = plateau
    return result


def detect_first_point_satisfying_predicate(
    signal: pd.Series, t_start: int | None, t_end: int | None, pred: Callable[[pd.Series], pd.Series[bool]]
) -> int | None:
    if signal.empty or t_start is None or t_end is None:
        return None
    filtered_ser = signal.loc[t_start:t_end]
    satisfies = filtered_ser[pred(filtered_ser)]
    if satisfies.empty:
        return None
    return satisfies.index[0]


def detect_first_point_below_threshold(
    signal: pd.Series, t_start: int | None, t_end: int | None, threshold: float
) -> int | None:
    return detect_first_point_satisfying_predicate(signal, t_start, t_end, lambda x: x < threshold)


def detect_first_point_above_threshold(
    signal: pd.Series, t_start: int | None, t_end: int | None, threshold: float
) -> int | None:
    return detect_first_point_satisfying_predicate(signal, t_start, t_end, lambda x: x > threshold)


def detect_last_point_satisfying_predicate(
    signal: pd.Series, t_start: int | None, t_end: int | None, pred: Callable[[pd.Series], pd.Series[bool]]
) -> int | None:
    if signal.empty or t_start is None or t_end is None:
        return None
    filtered_ser = signal.loc[t_start:t_end]
    satisfies = filtered_ser[pred(filtered_ser)]
    if satisfies.empty:
        return None
    return satisfies.index[-1]


def detect_last_point_below_threshold(
    signal: pd.Series, t_start: int | None, t_end: int | None, threshold: float
) -> int | None:
    return detect_last_point_satisfying_predicate(signal, t_start, t_end, lambda x: x < threshold)


def detect_last_point_above_threshold(
    signal: pd.Series, t_start: int | None, t_end: int | None, threshold: float
) -> int | None:
    return detect_last_point_satisfying_predicate(signal, t_start, t_end, lambda x: x > threshold)


TimeWindow = tuple[int, int]
"""
Represents the time window for a check.

- The first integer indicates the start of the window in nanoseconds after the reference point.
- The second integer indicates the end of the window in nanoseconds after the reference point.
- The window start offset should always be less than the window end offset.

Examples:

1. A time window that starts 15 ns before the reference point and ends 10 ns after:
    >>> window1: TimeWindow = (-15, 10)
    ..<______________0_________>.
    start        reference    end

2. A time window that starts 10 ns after the reference point and ends 25 ns after:
    >>> window2: TimeWindow = (10, 25)
    ....0..........<______________>.
    reference    start           end
"""


def decode_state(signal: pd.Series, target_state: str) -> pd.Series:
    return signal.apply(lambda x: target_state in x).rename(f"{signal.name} {target_state}")


def detect_when_signal_first_changes_to(
    signal: pd.Series,
    change_to: bool,
    timestamp: int | None,
    time_window: TimeWindow,
    log: logging.Logger | logging.LoggerAdapter,
) -> int | None:
    """
    Detects point in time when the signal first changes to the given state.
    Is equal to the following eDSL code
    detectWhere(<signal>).changesOnceTo(<change_to>)
                         .starting(<time_window_start>).after(0)
                         .ending(<time_window_end>).after(0)

    Args:
        signal: the signal to check
        change_to: a value after change
        timestamp: the reference timestamp
        time_window: (time_window_start, time_window_end) in nanoseconds
        log: logging object

    Returns:
        the timestamp in nanoseconds since epoch, or None if the change not detected
    """
    change = +1 if change_to is True else -1

    if timestamp is None:
        log.warning("Reference timestamp is None.")
        return None

    if signal.empty:
        log.warning(f"Change to {change_to} not detected in signal {signal.name}: signal empty.")
        return None

    time_window_start, time_window_end = time_window[0] + timestamp, time_window[1] + timestamp
    diff = signal.astype(int).diff().loc[time_window_start:time_window_end]
    changes = diff[diff == change]
    if changes.empty:
        log.warning(
            f"Change to {change_to} not detected in signal {signal.name} "
            f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
            f"relative to the reference timestamp {timestamp}."
        )
        return None

    if len(changes) > 1:
        log.warning(
            f"Multiple changes to {change_to} detected in signal {signal.name} "
            f"within the {time_window[0]:,} to {time_window[1]:,} nanoseconds time window "
            f"relative to the reference timestamp {timestamp}."
        )

    return int(changes.index[0])


def detect_last_point_before_signal_changes_to(
    signal: pd.Series,
    change_to: bool,
    timestamp: int | None,
    time_window: TimeWindow,
    log: logging.Logger | logging.LoggerAdapter,
) -> int | None:

    timestamp = detect_when_signal_first_changes_to(signal, change_to, timestamp, time_window, log)

    if timestamp is None:
        return None

    return signal.loc[:timestamp].index[-2]


def count_changes(signal: pd.Series, time_window_start: int, time_window_end: int, change_to: bool) -> int:
    change = +1 if change_to else -1
    if signal.empty:
        return 0
    return int(str(signal.astype(int).diff().loc[time_window_start:time_window_end].value_counts().get(change, "0")))


def count_points_with_value(signal: pd.Series, value: bool, time_window_start: int, time_window_end: int) -> int:
    if signal.empty:
        return 0

    series = signal.loc[time_window_start:time_window_end]
    return series[series == value].shape[0]


def get_regions_equal_to(
    signal: pd.Series, value: float, max_allowed_deviation_from_plateau_level: float = 1e-9
) -> list[TimeWindow]:
    regions = []
    start = None
    for i in range(len(signal)):
        if abs(signal.iloc[i] - value) <= max_allowed_deviation_from_plateau_level:
            if start is None:
                start = signal.index[i]
        else:
            if start is not None:
                regions.append((start, signal.index[i - 1]))
                start = None

    if start is not None:
        regions.append((start, signal.index[-1]))

    return regions


def calculate_signal_average(signal: pd.Series, time_frames: Iterable[TimeWindow | None]) -> float:
    """
    Calculate the average of the signal within the given time frames. The average is calculated as the sum of the signal
    within the time frames divided by the number of points in the signal within the time frames.

    Args:
        signal: The signal to calculate the average of.
        time_frames: The time frames to calculate the average over.

    Returns:
        The average of the signal within the given time frames.
        If the signal is empty or all the time frames are empty, the return value is 0.0.
    """
    count = 0
    total = 0.0
    for tf in time_frames:
        if tf is None:
            continue
        signal_slice = signal.loc[tf[0] : tf[1]].dropna()
        if signal_slice.empty:
            continue
        total += signal_slice.sum()
        count += len(signal_slice)
    return total if count == 0 else total / count


def read_timestamp_from_pm_headers_at_index(fgc_pm_headers: pd.DataFrame, index: int) -> int | None:
    return int(fgc_pm_headers["timestamp"].iat[index]) if len(fgc_pm_headers) > index else None


def get_time_window_where_signal_is_between(
    signal: pd.Series, time_window: TimeWindow, value_bounds: tuple[float, float]
) -> TimeWindow | None:
    """Returns the timestamps for the first point of the signal (within the given time frame) within the bounds
    given by value_bounds (inclusive) and the last point within the bounds given by value_bounds (inclusive). None
    if no points are within the bound (and given time frame).
    """
    if signal.empty:
        return None
    constrained_signal = signal.loc[time_window[0] : time_window[1]]
    constrained_signal = constrained_signal[
        (constrained_signal >= min(value_bounds)) & (constrained_signal <= max(value_bounds))
    ]
    return (constrained_signal.index[0], constrained_signal.index[-1]) if not constrained_signal.empty else None


def shorten_time_window_aligned_right(time_window: TimeWindow | None, max_time: int) -> TimeWindow | None:
    """Shortens the time window as little as possible and such that it does not exceed max_time in length. The
    shortening is performed in a right-aligned fashion.

    Args:
        time_window: The time window to shorten.
        max_time: The maximum time that the returned time window can be.

    Returns:
        A new time window which is the longest subwindow of time_window that is at most max_time in length and with the
        latter timestamp equal to the latter timestamp of time_window. None if time_window is None.
    """
    return (max(time_window[0], time_window[1] - max_time), time_window[1]) if time_window is not None else None


def get_first_plateau(
    signal: pd.Series, plateau_level: float, max_allowed_deviation_from_plateau_level: float = 1e-9
) -> tuple[int, int] | None:
    all_plateaus = get_regions_equal_to(signal, plateau_level, max_allowed_deviation_from_plateau_level)
    return all_plateaus[0] if len(all_plateaus) > 0 else None


def interpolate_signal(signal: pd.Series, timestamps: list[int] | pd.Index | pd.MultiIndex) -> pd.Series:
    """
    Interpolates signal values to match the given timestamps using pandas interpolation.

    Args:
        signal: Original signal series with timestamps as index
        timestamps: Target timestamps where we want interpolated values

    Returns:
        New series with interpolated values at the requested timestamps.
        Values outside the original signal's time range will be NaN.

    Example:
        If signal has points (0s, 0A) and (2s, 2A), and we request a value
        at 1s, it will return 1A through linear interpolation.
    """
    if signal.empty or len(timestamps) == 0:
        return pd.Series([], dtype=float, name=f"{signal.name}_INTERPOLATED")

    mask = (timestamps >= signal.index.min()) & (timestamps <= signal.index.max())
    timestamps_within_signal = pd.Index(timestamps)[mask]

    if len(timestamps_within_signal) == 0:
        return pd.Series([], dtype=float, name=f"{signal.name}_INTERPOLATED")

    full_interpolated_signal = (
        signal.reindex(signal.index.union(timestamps_within_signal))
        .interpolate(method="index")
        .rename(f"{signal.name}_INTERPOLATED")
    )
    return full_interpolated_signal.loc[timestamps_within_signal]


def calculate_resistance(u: pd.Series, i: pd.Series, name: str) -> pd.Series:
    """
    Calculate resistance from voltage and current signals.

    Args:
        u: Voltage signal.
        i: Current signal.
        name: Name of the resistance signal.

    Returns:
        Resistance signal.
    """
    return u.div(i).rename(name)


def remove_spikes(signal: pd.Series, threshold: float) -> pd.Series:
    """
    Parameters:
        signal (pd.Series): The input signal where the values represent the measured signal.
        threshold (float): The maximum allowed absolute value in the signal.
            Any data points with absolute value exceeding this threshold will be removed.

    Returns:
        pd.Series: A filtered Pandas Series where absolute values greater than the specified threshold are excluded.
    """

    return signal[signal.abs() <= threshold]
