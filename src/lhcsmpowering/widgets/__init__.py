from __future__ import annotations

import os
import pathlib
import sys
import time
from collections.abc import Callable, Iterable
from datetime import datetime
from typing import Any

import numpy as np
import pandas as pd
from IPython.display import Javascript, display
from lhcsmapi.analysis.expert_input import get_expert_decision
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time

from lhcsmpowering import analyses


def create_timestamp_table(timestamp_dct: dict[str, int | None]) -> pd.DataFrame:
    """Method creating a reference timestamp table for display in a notebook.

    Args:
        timestamp_dct: dictionary of timestamps
    """
    timestamp_df = (
        pd.DataFrame(
            {
                "Source": pd.Series(list(timestamp_dct.keys()), dtype=str),
                "Timestamp": pd.Series(list(v if v is not None else 0 for v in timestamp_dct.values()), dtype=int),
            }
        )
        .sort_values(by=["Timestamp", "Source"])
        .reset_index(drop=True)
    )

    timestamp_df["Date and time"] = timestamp_df["Timestamp"].apply(
        lambda col: Time.to_string_short(col) if col != 0 else np.nan
    )

    # Put zero rows at the end
    zero_rows = timestamp_df.loc[timestamp_df["Timestamp"] == 0]
    non_zero_rows = timestamp_df.loc[timestamp_df["Timestamp"] != 0]
    timestamp_df = pd.concat([non_zero_rows, zero_rows]).reset_index(drop=True)

    # Compute time difference w.r.t. the first event
    timestamp_first_event = timestamp_df.loc[0, "Timestamp"]
    timestamp_df["dt w.r.t. origin [ms]"] = timestamp_df["Timestamp"].apply(
        lambda col: (int((col - timestamp_first_event) * 1e-6) if col != 0 and timestamp_first_event != 0 else np.nan)
    )

    # Compute time difference w.r.t. the FGC and PIC events
    for source in ["FGC", "PIC"]:
        if any(timestamp_df["Source"].str.contains(source)):
            timestamp = timestamp_df.loc[timestamp_df["Source"].str.contains(source), "Timestamp"].values[0]
            timestamp_df[f"dt w.r.t. {source} [ms]"] = timestamp_df["Timestamp"].apply(
                lambda col, timestamp=timestamp: (
                    int((col - timestamp) * 1e-6) if col != 0 and timestamp != 0 else np.nan
                )
            )

    # Format the Timestamp column to avoid scientific notation
    timestamp_df["Timestamp"] = timestamp_df["Timestamp"].apply(lambda x: str(int(x)) if x != 0 else np.nan)

    return timestamp_df


def is_parameter_inductance(parameter: str) -> bool:
    """Check if the parameter is an inductance parameter"""
    return parameter.startswith("L_")


def is_parameter_resistance(parameter: str) -> bool:
    """Check if the parameter is a resistance parameter"""
    return parameter.startswith("R_")


def display_circuit_test_parameters(hwc_powering_test_parameters: dict[str, dict[str, dict[str, Any]]]) -> None:
    print("Circuits test parameters:")
    for pc_name, pc_parameters in hwc_powering_test_parameters.items():
        print(f"\t{pc_name}")
        for parameter_name, parameter_value in pc_parameters.items():
            if is_parameter_inductance(parameter_name) or is_parameter_resistance(parameter_name):
                print(f"\t\t{parameter_name} = {parameter_value['value']} {parameter_value['unit']}")


def display_powering_test_parameters(
    hwc_powering_test_parameters: dict[str, dict[str, dict[str, Any]]], powering_test_parameters: Iterable[str]
) -> None:
    print("Powering test parameters:")
    for pc_name, pc_parameters in hwc_powering_test_parameters.items():
        print(f"\t{pc_name}")
        for parameter in powering_test_parameters:
            print(f"\t\t{parameter} = {pc_parameters[parameter]['value']} {pc_parameters[parameter]['unit']}")


def prompt_user(user_prompt: str, answers: list[str]) -> str:
    return get_expert_decision(user_prompt, answers)


def get_report_destination(
    base_path: pathlib.Path, circuit_name: str, hwc_test: str, t_start: int, expert_decision: str
) -> pathlib.Path:

    sanitized_hwc_test = hwc_test.replace(" ", "_")

    report_destination_mapping: dict[Callable[[str], bool], Callable[[str], pathlib.Path]] = {
        signal_metadata.is_60a: lambda cn: base_path / "60A" / cn.split(".")[0] / cn / sanitized_hwc_test,
        signal_metadata.is_80_120a: lambda cn: base_path / "80-120A" / cn.split(".")[0] / cn / sanitized_hwc_test,
        signal_metadata.is_600a: lambda cn: base_path
        / "600A"
        / signal_metadata.get_family_name_for_600A(cn)
        / cn
        / sanitized_hwc_test,
        signal_metadata.is_ipd: lambda cn: base_path / "IPD" / cn / sanitized_hwc_test,
        signal_metadata.is_ipq: lambda cn: base_path / "IPQ" / cn.split(".")[0] / cn / sanitized_hwc_test,
        signal_metadata.is_inner_triplet: lambda cn: base_path / "IT" / cn / sanitized_hwc_test,
        signal_metadata.is_main_dipole: lambda cn: base_path / "RB" / cn / sanitized_hwc_test,
        signal_metadata.is_main_quadrupole: lambda cn: base_path
        / "RQ"
        / (cn[:2] + cn[3:])  # RQD.A12 / RQF.A12 -> RQ.A12
        / sanitized_hwc_test,
    }

    # Find the appropriate path using the mapping
    for condition, path_func in report_destination_mapping.items():
        if condition(circuit_name):
            report_destination_path = path_func(circuit_name)
            break
    else:
        raise NotImplementedError(f"Unsupported circuit name: {circuit_name}")

    # Construct the report filename
    start_time_str = Time.to_datetime(t_start).strftime("%Y-%m-%d-%Hh%M")
    current_time_str = datetime.now().strftime("%Y-%m-%d-%Hh%M")
    report_filename = f"{circuit_name}_{sanitized_hwc_test}-{start_time_str}-{current_time_str}_{expert_decision}.html"

    return report_destination_path / report_filename


def export_notebook_report(circuit_name: str, hwc_test: str, t_start: int, expert_decision: str) -> None:
    base_path = pathlib.Path("/eos/project/m/mp3")
    if not base_path.exists():
        raise FileNotFoundError(f"The notebook cannot be saved, as the base path {base_path} does not exist.")

    report_destination = get_report_destination(base_path, circuit_name, hwc_test, t_start, expert_decision)

    # Create the parent folder if it does not exist
    report_destination.parent.mkdir(parents=True, exist_ok=True)

    # Save the notebook
    display(Javascript("IPython.notebook.save_notebook();"))
    time.sleep(5)  # This is still a magic number; adjust as necessary

    os.system(
        f"{sys.executable} -m jupyter nbconvert {analyses.get_notebook_path(hwc_test, circuit_name).name} "
        f"--to html --output {report_destination}"
    )

    print("Notebook report saved on EOS, and can be accessed at the following locations:")
    print(f"\t- Windows: //eosproject-smb{report_destination}".replace("/", "\\"))
    print(f"\t- Linux: {report_destination}")
    print(f"\t- CERNBox: https://cernbox.cern.ch/files/spaces{report_destination}")
